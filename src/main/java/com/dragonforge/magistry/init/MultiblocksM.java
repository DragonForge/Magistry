package com.dragonforge.magistry.init;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.multiblock.BlockComponent;
import com.dragonforge.magistry.api.multiblock.BlockInput;
import com.dragonforge.magistry.api.multiblock.MultiblockRecipe;
import com.dragonforge.magistry.api.multiblock.MultiblockTileRecipe;
import com.dragonforge.magistry.api.multiblock.MultiblockRecipe.PosMapBuilder;
import com.dragonforge.magistry.blocks.BlockDarkPillar;
import com.zeitheron.hammercore.lib.zlib.tuple.TwoTuple;
import com.zeitheron.hammercore.utils.OnetimeCaller;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.api.parts.EnumBlockMetalPart;

import akka.util.Collections;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Rotation;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.oredict.OreDictionary;

public class MultiblocksM
{
	public static final OnetimeCaller register = new OnetimeCaller(MultiblocksM::$);
	
	private static void $()
	{
		// x
		// 3^
		// 2| + + +
		// 1| + + +
		// 0| + + +
		// * - - - -> z
		// 0* 1 2 3
		
		int[][] outerCorners = new int[][] { { 0, 0 }, { 0, 2 }, { 2, 0 }, { 2, 2 } };
		int[][] innerCorners = new int[][] { { 0, 1 }, { 1, 0 }, { 2, 1 }, { 1, 2 } };
		
		{
			PosMapBuilder<BlockComponent> builder = new PosMapBuilder<>();
			BlockComponent bricks = new BlockComponent(BlocksM.DARK_BRICKS.getDefaultState(), Blocks.STONEBRICK);
			for(int x = 0; x < 3; ++x)
				for(int y = 0; y < 3; ++y)
					for(int z = 0; z < 3; ++z)
						builder.put(x, y, z, bricks);
			new MultiblockRecipe("dark_bricks", builder.build()).setFX(false, true).setActivator(ItemsM.BURNT_BLUE_QUARTZ).setResearch(KnowledgeM.ORES.getRegistryName().toString()).register();
		}
		
		{
			PosMapBuilder<BlockComponent> builder = new PosMapBuilder<>();
			BlockComponent logWood = new BlockComponent(BlocksM.DARK_WOOD_LOG.getDefaultState(), new BlockInput("logWood").removeStates(wood -> wood.getBlock() == BlocksM.DARK_WOOD_LOG));
			for(int x = 0; x < 3; ++x)
				for(int y = 0; y < 3; ++y)
					for(int z = 0; z < 3; ++z)
						builder.put(x, y, z, logWood);
			new MultiblockRecipe("dark_wood_logs", builder.build()).setFX(true, false).setActivator(ItemsM.STARDUST).setResearch(KnowledgeM.STARDUST.getRegistryName().toString()).register();
		}
		
		{
			PosMapBuilder<BlockComponent> builder = new PosMapBuilder<>();
			BlockComponent oreIron = new BlockComponent(BlocksM.DARK_IRON_ORE.getDefaultState(), "oreIron");
			for(int x = 0; x < 3; ++x)
				for(int y = 0; y < 3; ++y)
					for(int z = 0; z < 3; ++z)
						builder.put(x, y, z, oreIron);
			new MultiblockRecipe("dark_iron", builder.build()).setFX(true, true).setActivator(ItemsM.BURNT_BLUE_QUARTZ).setResearch(KnowledgeM.ORES.getRegistryName().toString()).register();
		}
		
		{
			PosMapBuilder<BlockInput> builder = new PosMapBuilder<>();
			for(int x = 0; x < 3; ++x)
				for(int z = 0; z < 3; ++z)
					builder.put(x, 0, z, new BlockInput(BlocksM.DARK_BRICKS));
			builder.remove(1, 0, 1);
			
			builder.put(1, 1, 1, new BlockInput(BlocksM.BLUE_QUARTZ_BLOCK));
			
			for(int[] i : outerCorners)
			{
				builder.put(i[0], 1, i[1], new BlockInput(BlocksM.DARK_WOOD_PLANKS));
				builder.put(i[0], 2, i[1], new BlockInput(BlocksM.DARK_WOOD_LOG));
			}
			
			for(int[] i : innerCorners)
			{
				builder.put(i[0], 1, i[1], new BlockInput("blockBrass"));
				builder.put(i[0], 2, i[1], new BlockInput("blockIron"));
			}
			
			for(int i = 2; i <= 4; ++i)
				builder.put(1, i, 1, new BlockInput(BlocksM.INDUCTION_COIL));
			
			new MultiblockTileRecipe("generator", builder.build(), new BlockPos(1, 0, 1), new TwoTuple<>(new BlockInput("blockCopper"), BlocksM.GENERATOR.getStateFromMeta(0))).setFX(true, false).setActivator(ItemsM.STARDUST).setResearch(Magistry.MOD_ID + ":generator").register();
		}
		
		{
			PosMapBuilder<BlockInput> builder = new PosMapBuilder<>();
			for(int x = 0; x < 3; ++x)
				for(int z = 0; z < 3; ++z)
				{
					builder.put(x, 0, z, new BlockInput(BlocksM.DARK_WOOD_LOG));
					builder.put(x, 3, z, new BlockInput(BlocksM.DARK_WOOD_LOG));
				}
			for(int[] i : outerCorners)
			{
				builder.put(i[0], 1, i[1], new BlockInput(BlocksM.DARK_WOOD_FENCE));
				builder.put(i[0], 2, i[1], new BlockInput(BlocksM.DARK_WOOD_FENCE));
			}
			
			for(int[] i : innerCorners)
			{
				builder.put(i[0], 1, i[1], new BlockInput(BlocksM.DARK_IRON_SLAB));
				builder.put(i[0], 2, i[1], new BlockInput(BlocksM.DARK_IRON_SLAB));
			}
			
			builder.put(1, 1, 1, new BlockInput(BlocksM.BLUE_QUARTZ_BLOCK));
			builder.put(1, 2, 1, new BlockInput(BlocksM.BLUE_QUARTZ_BLOCK));
			builder.put(1, 3, 1, new BlockInput(BlocksM.DARK_BRICKS));
			builder.remove(1, 0, 1);
			
			new MultiblockTileRecipe("ether_collector", builder.build(), new BlockPos(1, 0, 1), new TwoTuple<>(new BlockInput(BlocksM.DARK_BRICKS), BlocksM.ETHER_COLLECTOR.getDefaultState())).setFX(true, false).setActivator(ItemsM.STARDUST).register();
		}
		
		{
			IBlockState pillar = BlocksM.DARK_PILLAR.getDefaultState().withProperty(BlockDarkPillar.AXIS, Axis.Y);
			
			PosMapBuilder<BlockInput> builder = new PosMapBuilder<BlockInput>();
			
			builder.put(0, 0, 0, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(0, 0, 1, new BlockInput("blockIron"));
			builder.put(0, 0, 2, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(1, 0, 1, new BlockInput(BlocksM.DARK_BRICKS));
			builder.put(1, 0, 0, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(1, 0, 2, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(0, 1, 0, new BlockInput(BlocksM.DARK_PILLAR));
			builder.put(1, 1, 0, new BlockInput("blockBrass"));
			builder.put(0, 1, 1, new BlockInput(BlocksM.INDUCTION_COIL));
			builder.put(1, 1, 1, new BlockInput(BlocksM.INDUCTION_COIL));
			builder.put(0, 1, 2, new BlockInput("casingIron"));
			builder.put(1, 1, 2, new BlockInput("casingIron"));
			
			new MultiblockTileRecipe("crusher_south", builder.build(), new BlockPos(0, 1, 0), new TwoTuple<>(new BlockInput(pillar), BlocksM.CRUSHER.getDefaultState()), EnumFacing.SOUTH).setFX(true, true).setActivator(ItemsM.STARDUST).setResearch(Magistry.MOD_ID + ":crusher").register();
			
			builder = new PosMapBuilder<BlockInput>();
			
			builder.put(0, 0, 0, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(0, 0, 1, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(1, 0, 1, new BlockInput(BlocksM.DARK_BRICKS));
			builder.put(1, 0, 0, new BlockInput("blockIron"));
			builder.put(2, 0, 0, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(2, 0, 1, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(2, 1, 1, new BlockInput("blockBrass"));
			builder.put(1, 1, 0, new BlockInput(BlocksM.INDUCTION_COIL));
			builder.put(1, 1, 1, new BlockInput(BlocksM.INDUCTION_COIL));
			builder.put(0, 1, 0, new BlockInput("casingIron"));
			builder.put(0, 1, 1, new BlockInput("casingIron"));
			
			new MultiblockTileRecipe("crusher_west", builder.build(), new BlockPos(2, 1, 0), new TwoTuple<>(new BlockInput(pillar), BlocksM.CRUSHER.getDefaultState()), EnumFacing.WEST).setFX(true, true).setActivator(ItemsM.STARDUST).setResearch(Magistry.MOD_ID + ":crusher").register();
			
			builder = new PosMapBuilder<BlockInput>();
			
			builder.put(0, 0, 0, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(1, 0, 1, new BlockInput("blockIron"));
			builder.put(0, 0, 2, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(0, 0, 1, new BlockInput(BlocksM.DARK_BRICKS));
			builder.put(1, 0, 0, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(1, 0, 2, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(0, 1, 0, new BlockInput("casingIron"));
			builder.put(1, 1, 0, new BlockInput("casingIron"));
			builder.put(0, 1, 1, new BlockInput(BlocksM.INDUCTION_COIL));
			builder.put(1, 1, 1, new BlockInput(BlocksM.INDUCTION_COIL));
			builder.put(0, 1, 2, new BlockInput("blockBrass"));
			
			new MultiblockTileRecipe("crusher_north", builder.build(), new BlockPos(1, 1, 2), new TwoTuple<>(new BlockInput(pillar), BlocksM.CRUSHER.getDefaultState()), EnumFacing.NORTH).setFX(true, true).setActivator(ItemsM.STARDUST).setResearch(Magistry.MOD_ID + ":crusher").register();
			
			builder = new PosMapBuilder<BlockInput>();
			
			builder.put(0, 0, 0, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(0, 0, 1, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(1, 0, 0, new BlockInput(BlocksM.DARK_BRICKS));
			builder.put(1, 0, 1, new BlockInput("blockIron"));
			builder.put(2, 0, 0, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(2, 0, 1, new BlockInput(BlocksM.DARK_WOOD_LOG));
			builder.put(0, 1, 0, new BlockInput("blockBrass"));
			builder.put(1, 1, 0, new BlockInput(BlocksM.INDUCTION_COIL));
			builder.put(1, 1, 1, new BlockInput(BlocksM.INDUCTION_COIL));
			builder.put(2, 1, 0, new BlockInput("casingIron"));
			builder.put(2, 1, 1, new BlockInput("casingIron"));
			
			new MultiblockTileRecipe("crusher_east", builder.build(), new BlockPos(0, 1, 1), new TwoTuple<>(new BlockInput(pillar), BlocksM.CRUSHER.getDefaultState()), EnumFacing.EAST).setFX(true, true).setActivator(ItemsM.STARDUST).setResearch(Magistry.MOD_ID + ":crusher").register();
		}
		
		{
			PosMapBuilder<BlockInput> builder = new PosMapBuilder<BlockInput>();
			
			builder.put(0, 0, 1, new BlockInput(BlocksM.DARK_WOOD_TABLE));
			builder.put(1, 0, 1, new BlockInput(BlocksM.DARK_WOOD_TABLE));
			builder.put(1, 0, 0, new BlockInput(BlocksM.DARK_WOOD_TABLE));
			
			new MultiblockTileRecipe("research_table", builder.build(), new BlockPos(0, 0, 0), new TwoTuple<>(new BlockInput(BlocksM.DARK_WOOD_TABLE), BlocksM.RESEARCH_TABLE.getDefaultState())).setFX(true, false).setActivator(ItemsM.STARDUST).register();
		}
		
		new MultiblockTileRecipe("inductor", new PosMapBuilder<BlockInput>().put(0, 0, 0, new BlockInput(BlocksM.INDUCTION_COIL)).build(), new BlockPos(0, 1, 0), new TwoTuple<>(new BlockInput(BlocksM.PEDESTAL), BlocksM.INDUCTOR.getStateFromMeta(0))).setResearch(KnowledgeM.INDUCTOR.getRegistryName().toString()).setFX(true, true).register();
		
		new MultiblockRecipe("pedestal", new PosMapBuilder<BlockComponent>().put(0, 0, 0, new BlockComponent(BlocksM.PEDESTAL.getDefaultState(), BlocksM.DARK_BRICKS)).build()).setResearch(KnowledgeM.INDUCTOR.getRegistryName().toString()).setFX(true, false).register();
	}
}