package com.dragonforge.magistry.init;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.crafting.RecipesPress;
import com.dragonforge.magistry.api.crafting.recipes.CrusherRecipe;
import com.dragonforge.magistry.api.crafting.recipes.InductorRecipe;
import com.dragonforge.magistry.api.crafting.recipes.PressRecipe;
import com.dragonforge.magistry.api.crafting.recipes.ShapedResearchRecipe;
import com.dragonforge.magistry.api.crafting.recipes.ShapelessResearchRecipe;
import com.dragonforge.magistry.api.item.CountableIngredient;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import com.zeitheron.hammercore.utils.recipes.helper.RecipeRegistry;
import com.zeitheron.hammercore.utils.recipes.helper.RegisterRecipes;
import com.zeitheron.hammermetals.api.AlloyInfo;
import com.zeitheron.hammermetals.api.MetalData;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.api.parts.EnumItemMetalPart;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.OreIngredient;
import net.minecraftforge.oredict.ShapedOreRecipe;

@RegisterRecipes(modid = Magistry.MOD_ID)
public class RecipesM extends RecipeRegistry
{
	@Override
	public void crafting()
	{
		shapedResearch("magistry:inductor", new ItemStack(BlocksM.INDUCTION_COIL), "sss", "ccc", "sss", 's', BlocksM.DARK_SLAB, 'c', "coilCopper");
		shaped(BlocksM.BLUE_QUARTZ_BLOCK, "qq", "qq", 'q', "gemQuartzBlue");
		shaped(BlocksM.BURNT_BLUE_QUARTZ_BLOCK, "qq", "qq", 'q', "gemQuartzBlueBurnt");
		recipe(new ShapedOreRecipe(new ResourceLocation(""), new ItemStack(BlocksM.DARK_WOOD_STAIRS, 4), CraftingHelper.parseShaped("d  ", "dd ", "ddd", 'd', BlocksM.DARK_WOOD_PLANKS)));
		recipe(new ShapedOreRecipe(new ResourceLocation(""), new ItemStack(BlocksM.DARK_STAIRS, 4), CraftingHelper.parseShaped("d  ", "dd ", "ddd", 'd', BlocksM.DARK_BRICKS)));
		recipe(new ShapedOreRecipe(new ResourceLocation(""), new ItemStack(BlocksM.DARK_IRON_STAIRS, 4), CraftingHelper.parseShaped("d  ", "dd ", "ddd", 'd', BlocksM.DARK_IRON_BLOCK)));
		shapeless(new ItemStack(BlocksM.DARK_WOOD_PLANKS, 4), BlocksM.DARK_WOOD_LOG);
		shaped(new ItemStack(BlocksM.DARK_WOOD_SLAB, 6), "ddd", 'd', BlocksM.DARK_WOOD_PLANKS);
		shaped(new ItemStack(BlocksM.DARK_SLAB, 6), "ddd", 'd', BlocksM.DARK_BRICKS);
		shaped(new ItemStack(BlocksM.DARK_IRON_SLAB, 6), "ddd", 'd', BlocksM.DARK_IRON_BLOCK);
		shapedResearch("magistry:ores", new ItemStack(ItemsM.CRYSTAL_STAFF), " gs", " tg", "q  ", 'g', "nuggetGold", 's', "gemShardCrystal", 't', "stickWood", 'q', "blockQuartzBlue");
		shaped(new ItemStack(BlocksM.DARK_WOOD_FENCE, 3), "psp", "psp", 'p', BlocksM.DARK_WOOD_PLANKS, 's', "stickWood");
		shapedResearch("magistry:ores", new ItemStack(ItemsM.VIAL, 16), " q ", "g g", " g ", 'q', "gemQuartzBlue", 'g', "blockGlassQuartz");
		
		shaped(BlocksM.DARK_IRON_BLOCK, "iii", "iii", "iii", 'i', "ingotIronDark");
		shapeless(new ItemStack(ItemsM.DARK_IRON_NUGGET, 9), "ingotIronDark");
		shapeless(new ItemStack(ItemsM.DARK_IRON_INGOT, 9), "blockIronDark");
		shaped(ItemsM.DARK_IRON_INGOT, "iii", "iii", "iii", 'i', "nuggetIronDark");
		shaped(ItemsM.DARK_IRON_GEAR, " i ", "ici", " i ", 'i', "ingotIronDark", 'c', "ingotCopper");
		shaped(ItemsM.DARK_IRON_GEAR, " i ", "ici", " i ", 'i', "ingotIronDark", 'c', "ingotIron");
		shapedResearch("magistry:glass_cutter", new ItemStack(ItemsM.GLASS_CUTTER), " gq", " dg", "g  ", 'g', "nuggetGold", 'q', "gemQuartzBlue", 'd', new ItemStack(BlocksM.DARK_WOOD_FENCE));
		
		shaped(BlocksM.DARK_BRICKS_CARVED, "d", "d", 'd', BlocksM.DARK_SLAB);
		shaped(new ItemStack(BlocksM.BLUE_QUARTZ_GLASS, 8), "ggg", "gqg", "ggg", 'g', "blockGlassColorless", 'q', "gemQuartzBlue");
		
		shaped(new ItemStack(BlocksM.DARK_PILLAR, 3), "b", "b", "b", 'b', BlocksM.DARK_BRICKS);
		shaped(new ItemStack(BlocksM.DARK_WOOD_TABLE), "sss", "gpg", " l ", 's', BlocksM.DARK_WOOD_SLAB, 'g', "nuggetGold", 'p', BlocksM.DARK_WOOD_PLANKS, 'l', BlocksM.DARK_WOOD_LOG);
		
//		shapeless(new ItemStack(ItemsM.CRYSTAL_TOME), new ItemStack(BlocksM.NATURAL_CRYSTAL), new ItemStack(Items.BOOK), "dyeBlue");
	}
	
	@Override
	public void smelting()
	{
		smelting(new ItemStack(BlocksM.BLUE_QUARTZ_ORE), new ItemStack(ItemsM.BLUE_QUARTZ), 1);
		smelting(new ItemStack(BlocksM.CRYSTAL_ORE), new ItemStack(ItemsM.CRYSTAL_SHARD), 1);
		smelting(new ItemStack(ItemsM.BLUE_QUARTZ), new ItemStack(ItemsM.BURNT_BLUE_QUARTZ), .5F);
		smelting(new ItemStack(BlocksM.BLUE_QUARTZ_BLOCK), new ItemStack(BlocksM.BURNT_BLUE_QUARTZ_BLOCK), 2);
		smelting(new ItemStack(BlocksM.DARK_IRON_ORE), new ItemStack(ItemsM.DARK_IRON_INGOT), .25F);
		smelting(ItemsM.DARK_IRON_GEAR, new ItemStack(ItemsM.DARK_IRON_INGOT, 4), 0);
		smelting(new ItemStack(ItemsM.DARK_IRON_DUST), new ItemStack(ItemsM.DARK_IRON_INGOT), .125F);
		
		inductor();
		crusher();
		press();
	}
	
	public void crusher()
	{
		new CrusherRecipe(new OreIngredient("stone"), 1F, new ItemStack(Blocks.COBBLESTONE)).add();
		new CrusherRecipe(new OreIngredient("cobblestone"), 1F, new ItemStack(Blocks.GRAVEL)).add();
		new CrusherRecipe(new OreIngredient("gravel"), 1F, new ItemStack(Blocks.SAND)).add();
		
		if(OreDictionary.doesOreNameExist("dust"))
		{
			ItemStack dustItem = OreDictionary.getOres("dust").get(0);
			new CrusherRecipe(new OreIngredient("sand"), 1F, dustItem).add();
		}
		
		new CrusherRecipe(new OreIngredient("oreRedstone"), 2.5F, new ItemStack(Items.REDSTONE, 10)).withDropTransformer(i ->
		{
			if(i.getItem() == Items.REDSTONE)
				i.grow((int) Math.round(Math.random() * 5));
			return i;
		}).add();
		
		new CrusherRecipe(new OreIngredient("oreLapis"), 2.5F, new ItemStack(Items.DYE, 10, EnumDyeColor.BLUE.getDyeDamage())).withDropTransformer(i ->
		{
			if(i.getItem() == Items.DYE)
				i.grow((int) Math.round(Math.random() * 5));
			return i;
		}).add();
		
		List<String> blackList = new ArrayList<>();
		
		blackList.add("oreRedstone");
		blackList.add("oreLapis");
		
		for(String name : OreDictionary.getOreNames())
		{
			if(name.startsWith("ore") && !OreDictionary.getOres(name).isEmpty() && !blackList.contains(name))
			{
				String dustMat = "dust" + name.substring(3);
				if(!OreDictionary.getOres(dustMat).isEmpty())
				{
					ItemStack dust = OreDictionary.getOres(dustMat).get(0).copy();
					dust.setCount(2);
					new CrusherRecipe(new OreIngredient(name), 2F, dust).add();
				}
				
				String gemMat = "gem" + name.substring(3);
				if(!OreDictionary.getOres(gemMat).isEmpty())
				{
					ItemStack gem = OreDictionary.getOres(gemMat).get(0).copy();
					gem.setCount(2);
					new CrusherRecipe(new OreIngredient(name), 2.5F, gem).add();
				}
			}
		}
	}
	
	public void inductor()
	{
		final Random rng = new Random();
		InductorRecipe ir;
		
		MetalRegistry.metalSet().stream().map(MetalRegistry::getMetal).filter(md -> md.info.alloy != null).forEach(metal ->
		{
			AlloyInfo alloy = metal.info.alloy;
			
			if(metal.has(EnumItemMetalPart.INGOT))
			{
				ItemStack output = new ItemStack(metal.get(EnumItemMetalPart.INGOT), alloy.output);
				List<Ingredient> inputs = new ArrayList<>();
				for(String sub : alloy.metals)
				{
					MetalData md = MetalRegistry.getMetal(sub);
					if(md == null)
						return;
					List<ItemStack> items = new ArrayList<>();
					if(md.has(EnumItemMetalPart.DUST))
						items.add(new ItemStack(md.get(EnumItemMetalPart.DUST)));
					if(md.has(EnumItemMetalPart.INGOT))
						items.add(new ItemStack(md.get(EnumItemMetalPart.INGOT)));
					if(md.has(EnumItemMetalPart.ROD))
						items.add(new ItemStack(md.get(EnumItemMetalPart.ROD)));
					inputs.add(Ingredient.fromStacks(items.toArray(new ItemStack[items.size()])));
				}
				new InductorRecipe(output, inputs).add();
			}
		});
		
		ir = new InductorRecipe(new ItemStack(BlocksM.NATURAL_CRYSTAL), Ingredient.fromStacks(new ItemStack(ItemsM.CRYSTAL_SHARD)), Ingredient.fromStacks(new ItemStack(ItemsM.CRYSTAL_SHARD)), new OreIngredient("stone"));
		ir.outputTransformer = stack ->
		{
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setFloat("Color", ColorHelper.packRGB(rng.nextFloat(), rng.nextFloat(), rng.nextFloat()));
			nbt.setFloat("Saturation", .05F + rng.nextFloat() * .95F);
			nbt.setFloat("Charge", 1000F);
			nbt.setFloat("MaxCharge", 1000F);
			stack.setTagCompound(nbt);
			return stack;
		};
		ir.add();
		
		new InductorRecipe(new ItemStack(BlocksM.GREATER_CRYSTAL, 2), Ingredient.fromStacks(new ItemStack(BlocksM.DARK_BRICKS)), Ingredient.fromStacks(new ItemStack(BlocksM.DARK_BRICKS)), Ingredient.fromStacks(new ItemStack(BlocksM.DARK_BRICKS)), new OreIngredient("gemDiamond"), new OreIngredient("gemShardCrystal"), new OreIngredient("gemShardCrystal"), new OreIngredient("gemShardCrystal"), new OreIngredient("gemShardCrystal"), new OreIngredient("gemShardCrystal")).add();
		new InductorRecipe(new ItemStack(ItemsM.CIRCUIT_BOARD), new OreIngredient("ingotCopper"), new OreIngredient("ingotCopper"), new OreIngredient("plateIronDark"), Ingredient.fromItem(Items.REDSTONE), Ingredient.fromItem(Items.REPEATER)).add();
	}
	
	public void press()
	{
		for(String name : OreDictionary.getOreNames())
		{
			if(name.startsWith("ingot") && !OreDictionary.getOres(name).isEmpty())
			{
				String base = name.substring(5);
				OreIngredient oi = new OreIngredient(name);
				
				String plateMat = "plate" + base;
				if(!OreDictionary.getOres(plateMat).isEmpty())
					new PressRecipe(OreDictionary.getOres(plateMat).get(0).copy(), RecipesPress.MOLD_PLATE, new CountableIngredient(oi, 1)).add();
				
				String rodMat = "rod" + base;
				if(!OreDictionary.getOres(rodMat).isEmpty())
					new PressRecipe(OreDictionary.getOres(rodMat).get(0).copy(), RecipesPress.MOLD_ROD, new CountableIngredient(oi, 1)).add();
				
				String gearMat = "gear" + base;
				if(!OreDictionary.getOres(gearMat).isEmpty())
					new PressRecipe(OreDictionary.getOres(gearMat).get(0).copy(), RecipesPress.MOLD_GEAR, new CountableIngredient(oi, 4)).add();
			}
			
			if(name.startsWith("nugget") && !OreDictionary.getOres(name).isEmpty())
			{
				String base = name.substring(6);
				OreIngredient oi = new OreIngredient(name);
				
				String ingotMat = "ingot" + base;
				if(!OreDictionary.getOres(ingotMat).isEmpty())
					new PressRecipe(OreDictionary.getOres(ingotMat).get(0).copy(), RecipesPress.MOLD_INGOT, new CountableIngredient(oi, 9)).add();
			}
		}
	}
	
	protected IRecipe shapedResearch(String research, ItemStack out, Object... recipeComponents)
	{
		return recipe(new ShapedResearchRecipe(research, out, recipeComponents));
	}
	
	protected IRecipe shapelessResearch(String research, ItemStack out, Object... recipeComponents)
	{
		return recipe(new ShapelessResearchRecipe(research, out, recipeComponents));
	}
}