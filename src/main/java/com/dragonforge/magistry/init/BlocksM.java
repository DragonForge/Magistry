package com.dragonforge.magistry.init;

import com.dragonforge.magistry.blocks.BlockBlueQuartzGlass;
import com.dragonforge.magistry.blocks.BlockBlueQuartzOre;
import com.dragonforge.magistry.blocks.BlockCrystalOre;
import com.dragonforge.magistry.blocks.BlockDarkPillar;
import com.dragonforge.magistry.blocks.BlockDarkPlanks;
import com.dragonforge.magistry.blocks.BlockDarkWoodTable;
import com.dragonforge.magistry.blocks.BlockGreaterCrystal;
import com.dragonforge.magistry.blocks.BlockInductionCoil;
import com.dragonforge.magistry.blocks.BlockMFence;
import com.dragonforge.magistry.blocks.BlockMLog;
import com.dragonforge.magistry.blocks.BlockMSlab;
import com.dragonforge.magistry.blocks.BlockMStairs;
import com.dragonforge.magistry.blocks.BlockMultiblockPart;
import com.dragonforge.magistry.blocks.BlockNaturalCrystal;
import com.dragonforge.magistry.blocks.BlockOreCrystal;
import com.dragonforge.magistry.blocks.BlockPedestal;
import com.dragonforge.magistry.blocks.BlockPhyllite;
import com.dragonforge.magistry.blocks.BlockStarflower;
import com.dragonforge.magistry.blocks.BlockStupid;
import com.dragonforge.magistry.blocks.machines.BlockCrusher;
import com.dragonforge.magistry.blocks.machines.BlockEtherCollector;
import com.dragonforge.magistry.blocks.machines.BlockGenerator;
import com.dragonforge.magistry.blocks.machines.BlockInductor;
import com.dragonforge.magistry.blocks.machines.BlockResearchTable;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlocksM
{
	public static final Block DARK_BRICKS = new Block(Material.ROCK).setHardness(1.5F).setTranslationKey("dark_bricks");
	public static final Block DARK_BRICKS_CARVED = new Block(Material.ROCK).setHardness(1.5F).setTranslationKey("dark_bricks_carved");
	public static final BlockPhyllite PHYLLITE = new BlockPhyllite();
	public static final BlockStupid DARK_IRON_ORE = new BlockStupid(Material.ROCK).setHardness(1.5F).setHarvestLvl("pickaxe", 1).setTranslationKey("dark_iron_ore").setOredictionary("oreIronDark");
	public static final BlockStupid DARK_IRON_BLOCK = new BlockStupid(Material.IRON).setSoundType(SoundType.METAL).setHardness(1.5F).setHarvestLvl("pickaxe", 1).setTranslationKey("dark_iron_block").setOredictionary("blockIronDark");
	public static final Block BLUE_QUARTZ_BLOCK = new Block(Material.ROCK).setHardness(1.5F).setTranslationKey("blue_quartz_block");
	public static final Block BURNT_BLUE_QUARTZ_BLOCK = new Block(Material.ROCK).setHardness(1.5F).setTranslationKey("burnt_blue_quartz_block");
	
	public static final BlockDarkPillar DARK_PILLAR = new BlockDarkPillar();
	public static final BlockPedestal PEDESTAL = new BlockPedestal();
	public static final BlockStarflower STARFLOWER = new BlockStarflower();
	public static final BlockBlueQuartzOre BLUE_QUARTZ_ORE = new BlockBlueQuartzOre();
	public static final BlockCrystalOre CRYSTAL_ORE = new BlockCrystalOre();
	public static final BlockMultiblockPart MULTIBLOCK_PART = new BlockMultiblockPart();
	public static final BlockInductionCoil INDUCTION_COIL = new BlockInductionCoil();
	public static final BlockGreaterCrystal GREATER_CRYSTAL = new BlockGreaterCrystal();
	public static final BlockNaturalCrystal NATURAL_CRYSTAL = new BlockNaturalCrystal();
	public static final BlockOreCrystal ORE_CRYSTAL = new BlockOreCrystal();
	public static final BlockDarkPlanks DARK_WOOD_PLANKS = new BlockDarkPlanks();
	public static final BlockMLog DARK_WOOD_LOG = new BlockMLog("dark_wood");
	public static final BlockDarkWoodTable DARK_WOOD_TABLE = new BlockDarkWoodTable();
	public static final BlockBlueQuartzGlass BLUE_QUARTZ_GLASS = new BlockBlueQuartzGlass();
	
	public static final BlockInductor INDUCTOR = new BlockInductor();
	public static final BlockGenerator GENERATOR = new BlockGenerator();
	public static final BlockEtherCollector ETHER_COLLECTOR = new BlockEtherCollector();
	public static final BlockCrusher CRUSHER = new BlockCrusher();
	public static final BlockResearchTable RESEARCH_TABLE = new BlockResearchTable();
	
	// FENCE, STAIR AND SLAB HELL BELOW
	
	public static final BlockMFence DARK_WOOD_FENCE = (BlockMFence) new BlockMFence(Material.WOOD).setSoundType(SoundType.WOOD).setTranslationKey("dark_wood_fence");
	
	public static final BlockMStairs DARK_WOOD_STAIRS = new BlockMStairs(DARK_WOOD_PLANKS.getDefaultState(), "dark_wood");
	public static final BlockMStairs DARK_STAIRS = new BlockMStairs(DARK_BRICKS.getDefaultState(), "dark");
	public static final BlockMStairs DARK_IRON_STAIRS = new BlockMStairs(DARK_IRON_BLOCK.getDefaultState(), "dark_iron");
	
	public static final BlockMSlab.Double DARK_WOOD_SLAB_DOUBLE = new BlockMSlab.Double(Material.WOOD, SoundType.WOOD, "dark_wood");
	public static final BlockMSlab.Half DARK_WOOD_SLAB = new BlockMSlab.Half(Material.WOOD, SoundType.WOOD, DARK_WOOD_SLAB_DOUBLE, "dark_wood");
	public static final BlockMSlab.Double DARK_SLAB_DOUBLE = new BlockMSlab.Double(Material.ROCK, SoundType.STONE, "dark");
	public static final BlockMSlab.Half DARK_SLAB = new BlockMSlab.Half(Material.ROCK, SoundType.STONE, DARK_SLAB_DOUBLE, "dark");
	public static final BlockMSlab.Double DARK_IRON_SLAB_DOUBLE = new BlockMSlab.Double(Material.IRON, SoundType.METAL, "dark_iron");
	public static final BlockMSlab.Half DARK_IRON_SLAB = new BlockMSlab.Half(Material.IRON, SoundType.METAL, DARK_IRON_SLAB_DOUBLE, "dark_iron");
	
	static
	{
		DARK_BRICKS.setHarvestLevel("pickaxe", 1);
		BLUE_QUARTZ_BLOCK.setHarvestLevel("pickaxe", 1);
		BURNT_BLUE_QUARTZ_BLOCK.setHarvestLevel("pickaxe", 1);
	}
}