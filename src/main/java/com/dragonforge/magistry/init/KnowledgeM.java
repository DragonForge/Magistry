package com.dragonforge.magistry.init;

import static com.dragonforge.magistry.api.research.ResearchTag.TAG_ELECTRICAL_FIELD;
import static com.dragonforge.magistry.api.research.ResearchTag.TAG_ELECTRICITY;
import static com.dragonforge.magistry.api.research.ResearchTag.TAG_MACHINERY;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.api.research.ResearchItemBuilder;
import com.dragonforge.magistry.api.research.ResearchTag;
import com.dragonforge.magistry.api.research.client.AdditionalPageCraftingRecipe;
import com.dragonforge.magistry.api.research.client.AdditionalPageInductor;
import com.dragonforge.magistry.api.research.client.AdditionalPageMultiblock;
import com.dragonforge.magistry.api.research.client.AdditionalPageSmelting;
import com.dragonforge.magistry.api.research.client.ItemPreview;
import com.dragonforge.magistry.api.research.scan.ScanBlock;
import com.dragonforge.magistry.api.research.scan.ScanEntity;
import com.dragonforge.magistry.api.research.scan.ScanItem;
import com.dragonforge.magistry.api.research.scan.ScanOreDict;
import com.dragonforge.magistry.api.research.scan.ScanRegistry;
import com.dragonforge.magistry.scans.MScanAlchemy;
import com.dragonforge.magistry.scans.MScanFluid;
import com.dragonforge.magistry.scans.MScanHeat;
import com.dragonforge.magistry.scans.MScanLight;
import com.dragonforge.magistry.scans.MScanPlant;
import com.zeitheron.hammercore.utils.OnetimeCaller;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.api.parts.EnumItemMetalPart;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class KnowledgeM
{
	public static final OnetimeCaller register = new OnetimeCaller(KnowledgeM::$);
	
	public static ResearchBase CRYSTALS;
	public static ResearchBase STARDUST;
	public static ResearchBase ORES;
	public static ResearchBase INDUCTOR;
	public static ResearchBase GLASS_CUTTER;
	public static ResearchBase GENERATOR;
	public static ResearchBase GREATER_CRYSTAL;
	public static ResearchBase CRUSHER;
	
	private static void $()
	{
		ItemStack ncryst = new ItemStack(BlocksM.NATURAL_CRYSTAL);
		ncryst.setTagCompound(new NBTTagCompound());
		ncryst.getTagCompound().setFloat("Saturation", 1F);
		ncryst.getTagCompound().setFloat("MaxCharge", 1000F);
		ncryst.getTagCompound().setFloat("Charge", 1000F);
		ncryst.getTagCompound().setInteger("Color", 0x00FFFF);
		
		CRYSTALS = new ResearchBase("crystals").setPreview(ncryst).register();
		STARDUST = new ResearchBase("stardust").setPreview(new ItemStack(ItemsM.STARDUST)).addPages(new AdditionalPageMultiblock(new ResourceLocation(Magistry.MOD_ID, "dark_wood_logs")), new AdditionalPageCraftingRecipe(new ItemStack(BlocksM.DARK_WOOD_PLANKS)), new AdditionalPageCraftingRecipe(new ItemStack(BlocksM.DARK_WOOD_SLAB)), new AdditionalPageCraftingRecipe(new ItemStack(BlocksM.DARK_WOOD_STAIRS)), new AdditionalPageCraftingRecipe(new ItemStack(BlocksM.DARK_WOOD_TABLE)), new AdditionalPageMultiblock(new ResourceLocation(Magistry.MOD_ID, "research_table"))).register();
		ORES = new ResearchBase("ores").setPreview(new ItemPreview(new ItemStack[] { new ItemStack(BlocksM.CRYSTAL_ORE), new ItemStack(BlocksM.BLUE_QUARTZ_ORE), new ItemStack(BlocksM.DARK_IRON_ORE) })).addPages(new AdditionalPageCraftingRecipe(new ItemStack(ItemsM.CRYSTAL_STAFF)), new AdditionalPageCraftingRecipe(new ItemStack(BlocksM.BLUE_QUARTZ_GLASS)), new AdditionalPageCraftingRecipe(new ItemStack(ItemsM.VIAL)), new AdditionalPageSmelting(new ItemStack(ItemsM.BURNT_BLUE_QUARTZ)), new AdditionalPageMultiblock(new ResourceLocation(Magistry.MOD_ID, "dark_iron")), new AdditionalPageMultiblock(new ResourceLocation(Magistry.MOD_ID, "dark_bricks")), new AdditionalPageCraftingRecipe(new ItemStack(BlocksM.DARK_PILLAR)), new AdditionalPageCraftingRecipe(new ItemStack(BlocksM.DARK_SLAB)), new AdditionalPageCraftingRecipe(new ItemStack(BlocksM.DARK_STAIRS)), new AdditionalPageCraftingRecipe(new ItemStack(BlocksM.DARK_BRICKS_CARVED)), new AdditionalPageMultiblock(new ResourceLocation(Magistry.MOD_ID, "pedestal"))).register();
		INDUCTOR = new ResearchBase("inductor").setPreview(new ItemPreview(new ItemStack(BlocksM.INDUCTOR))).addPages(new AdditionalPageCraftingRecipe(new ItemStack(BlocksM.INDUCTION_COIL)), new AdditionalPageMultiblock(new ResourceLocation(Magistry.MOD_ID, "inductor")), new AdditionalPageInductor(new ItemStack(BlocksM.NATURAL_CRYSTAL))).setResearchable(ResearchItemBuilder.start().add(new ItemStack(MetalRegistry.getMetal("copper").get(EnumItemMetalPart.INGOT), 2)).add(new ItemStack(Items.REDSTONE)).add(new ItemStack(BlocksM.DARK_BRICKS, 3)), TAG_ELECTRICITY, TAG_ELECTRICAL_FIELD, TAG_MACHINERY).register();
		GLASS_CUTTER = new ResearchBase("glass_cutter").setPreview(new ItemStack(ItemsM.GLASS_CUTTER)).setResearchable(ResearchItemBuilder.start().add(1, ItemsM.BLUE_QUARTZ).add(new ItemStack(Blocks.GLASS, 2)), ResearchTag.TAG_MACHINERY, ResearchTag.TAG_OPTIC).setParents(ORES.getRegistryName()).addPages(new AdditionalPageCraftingRecipe(new ItemStack(ItemsM.GLASS_CUTTER))).register();
		GENERATOR = new ResearchBase("generator").setPreview(new ItemStack(BlocksM.GENERATOR)).setResearchable(ResearchItemBuilder.start().add(Item.getItemFromBlock(BlocksM.NATURAL_CRYSTAL)).add(Item.getItemFromBlock(BlocksM.INDUCTION_COIL)), ResearchTag.TAG_CRYSTAL_ENERGY, ResearchTag.TAG_ELECTRICAL_FIELD, ResearchTag.TAG_ELECTRICITY, ResearchTag.TAG_MACHINERY).setParents(INDUCTOR.getRegistryName()).addPages(new AdditionalPageMultiblock(new ResourceLocation(Magistry.MOD_ID, "generator"))).register();
		GREATER_CRYSTAL = new ResearchBase("greater_crystal").setPreview(new ItemStack(BlocksM.GREATER_CRYSTAL)).setResearchable(ResearchItemBuilder.start().add(Item.getItemFromBlock(BlocksM.NATURAL_CRYSTAL)).add(2, ItemsM.CRYSTAL_SHARD), ResearchTag.TAG_CRYSTAL_ENERGY, ResearchTag.TAG_OPTIC).setParents(GENERATOR.getRegistryName()).addPages(new AdditionalPageInductor(new ItemStack(BlocksM.GREATER_CRYSTAL))).register();
		CRUSHER = new ResearchBase("crusher").setPreview(new ItemStack(BlocksM.CRUSHER)).setResearchable(ResearchItemBuilder.start().add(Items.IRON_INGOT).add(Items.REDSTONE).add(Item.getItemFromBlock(BlocksM.INDUCTION_COIL)), ResearchTag.TAG_CRYSTAL_ENERGY, ResearchTag.TAG_MACHINERY, ResearchTag.TAG_MINERALS).setParents(GREATER_CRYSTAL.getRegistryName()).addPages(new AdditionalPageMultiblock(new ResourceLocation(Magistry.MOD_ID, "crusher_south"))).register();
		
		registerScans();
	}
	
	private static void registerScans()
	{
		ScanRegistry.register(new ScanEntity(EntityLivingBase.class, "s_undead").setAcceptor(e -> e.getCreatureAttribute() == EnumCreatureAttribute.UNDEAD));
		ScanRegistry.register(new ScanBlock(BlocksM.BLUE_QUARTZ_ORE, "magistry:ores"));
		ScanRegistry.register(new ScanBlock(BlocksM.DARK_IRON_ORE, "magistry:ores"));
		ScanRegistry.register(new ScanBlock(BlocksM.NATURAL_CRYSTAL, "s_crystal"));
		ScanRegistry.register(new ScanBlock(BlocksM.CRYSTAL_ORE, "magistry:ores"));
		ScanRegistry.register(new ScanBlock(BlocksM.STARFLOWER, "magistry:stardust"));
		ScanRegistry.register(new ScanItem(ItemsM.VIAL_ETHER, "s_ether"));
		ScanRegistry.register(new ScanOreDict("ingot*", "s_any_ingot"));
		ScanRegistry.register(new ScanOreDict("ore*", "s_any_ore"));
		ScanRegistry.register(new ScanItem(Items.CLOCK, "s_clock"));
		ScanRegistry.register(new MScanAlchemy());
		ScanRegistry.register(new MScanPlant());
		ScanRegistry.register(new MScanLight());
		ScanRegistry.register(new MScanFluid());
		ScanRegistry.register(new MScanHeat());
	}
}