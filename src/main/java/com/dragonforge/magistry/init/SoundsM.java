package com.dragonforge.magistry.init;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import com.dragonforge.magistry.Magistry;
import com.zeitheron.hammercore.utils.SoundObject;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class SoundsM
{
	public static final SoundEvent FIZZ = new SoundEvent(new ResourceLocation(Magistry.MOD_ID, "fizz"));
	public static final SoundEvent PAGE_TURNS = new SoundEvent(new ResourceLocation(Magistry.MOD_ID, "page_turns"));
	
	public static void register(IForgeRegistry<SoundEvent> r)
	{
		for(Field f : SoundsM.class.getDeclaredFields())
			if(SoundEvent.class.isAssignableFrom(f.getType()) && Modifier.isStatic(f.getModifiers()))
			{
				f.setAccessible(true);
				try
				{
					SoundEvent se = (SoundEvent) f.get(null);
					se.setRegistryName(se.soundName);
					r.register(se);
				} catch(ReflectiveOperationException e)
				{
					e.printStackTrace();
				}
			}
	}
}