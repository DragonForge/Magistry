package com.dragonforge.magistry.init;

import java.util.ArrayList;
import java.util.List;

import com.dragonforge.magistry.items.ItemCrystalStaff;
import com.dragonforge.magistry.items.ItemCrystalTome;
import com.dragonforge.magistry.items.ItemEtherFilter;
import com.dragonforge.magistry.items.ItemGlassCutter;
import com.dragonforge.magistry.items.ItemLostNotes;
import com.dragonforge.magistry.items.ItemMMaterial;
import com.dragonforge.magistry.items.ItemMultiblockActivator;
import com.dragonforge.magistry.items.ItemScanner;
import com.zeitheron.hammercore.internal.blocks.BlockLyingItem;
import com.zeitheron.hammercore.tile.TileLyingItem;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemGlassBottle;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;

public class ItemsM
{
	public static final ItemLostNotes LOST_NOTES = new ItemLostNotes();
	public static final ItemCrystalTome CRYSTAL_TOME = new ItemCrystalTome();
	public static final ItemMMaterial VIAL = new ItemMMaterial("vial", "vialEmpty", "vialAny");
	public static final ItemMMaterial VIAL_ETHER = new ItemMMaterial("vial_ether", "vialFilledEther", "vialAny");
	public static final ItemMMaterial ASH = new ItemMMaterial("ash", "dustAsh");
	public static final ItemMMaterial CIRCUIT_BOARD = new ItemMMaterial("circuit_board", "circuitDarkIron");
	public static final ItemMMaterial CRYSTAL_SHARD = new ItemMMaterial("crystal_shard", "gemShardCrystal");
	public static final ItemEtherFilter ETHER_FILTER = new ItemEtherFilter();
	public static final ItemMultiblockActivator STARDUST = new ItemMultiblockActivator("stardust");
	public static final ItemMultiblockActivator BURNT_BLUE_QUARTZ = new ItemMultiblockActivator("burnt_blue_quartz");
	public static final ItemMMaterial BLUE_QUARTZ = new ItemMMaterial("blue_quartz", "gemQuartzBlue", "gemQuartzAny");
	public static final ItemMMaterial DARK_IRON_DUST = new ItemMMaterial("dark_iron_dust", "dustIronDark");
	public static final ItemMMaterial DARK_IRON_GEAR = new ItemMMaterial("dark_iron_gear", "gearIronDark");
	public static final ItemMMaterial DARK_IRON_INGOT = new ItemMMaterial("dark_iron_ingot", "ingotIronDark");
	public static final ItemMMaterial DARK_IRON_NUGGET = new ItemMMaterial("dark_iron_nugget", "nuggetIronDark");
	public static final ItemMMaterial DARK_IRON_PLATE = new ItemMMaterial("dark_iron_plate", "plateIronDark");
	public static final ItemCrystalStaff CRYSTAL_STAFF = new ItemCrystalStaff();
	public static final ItemGlassCutter GLASS_CUTTER = new ItemGlassCutter();
	public static final ItemScanner SCANNER = new ItemScanner();
	
	public static final ItemMMaterial MOLD_GEAR = new ItemMMaterial("mold_gear", 1, "moldGear");
	public static final ItemMMaterial MOLD_INGOT = new ItemMMaterial("mold_ingot", 1, "moldIngot");
	public static final ItemMMaterial MOLD_PLATE = new ItemMMaterial("mold_plate", 1, "moldPlate");
	public static final ItemMMaterial MOLD_ROD = new ItemMMaterial("mold_rod", 1, "moldRod");
	
	public static boolean spawnNotesNearPlayer(EntityPlayerMP mp, String research)
	{
		ItemStack drop = new ItemStack(LOST_NOTES);
		drop.setTagCompound(new NBTTagCompound());
		drop.getTagCompound().setString("Research", research);
		
		int x = mp.getPosition().getX();
		int z = mp.getPosition().getZ();
		
		List<BlockPos> positions = new ArrayList<>();
		int attempts = 8192;
		int rad = 8;
		while(--attempts > 0 && positions.size() < (rad * 2 + 1) * (rad * 2 + 1))
		{
			BlockPos pos = new BlockPos(x + (mp.getRNG().nextInt(rad) - mp.getRNG().nextInt(rad)), 255, z + (mp.getRNG().nextInt(rad) - mp.getRNG().nextInt(rad)));
			if(!positions.contains(pos))
				positions.add(pos);
			pos = mp.world.getHeight(pos).up();
			TileLyingItem tli = BlockLyingItem.place(mp.world, pos, drop);
			if(tli != null)
			{
				positions.clear();
				return true;
			}
		}
		
		return false;
	}
}