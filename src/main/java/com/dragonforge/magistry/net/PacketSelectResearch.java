package com.dragonforge.magistry.net;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.api.research.ResearchTag;
import com.dragonforge.magistry.inventory.ContainerResearch;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class PacketSelectResearch implements IPacket
{
	ResearchBase res;
	
	public PacketSelectResearch(ResearchBase res)
	{
		this.res = res;
	}
	
	public PacketSelectResearch()
	{
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString("res", res.getRegistryName().toString());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		res = MagistryAPI.getResearch(nbt.getString("res"));
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		if(res == null)
			return null;
		EntityPlayerMP mp = net.getSender();
		if(mp != null && mp.openContainer instanceof ContainerResearch)
		{
			ContainerResearch cr = (ContainerResearch) mp.openContainer;
			IPlayerKnowledge ipk = MagistryAPI.getPlayerKnowledge(mp);
			if(cr.t.discovery == null && res.isResearchable(mp, cr.t.enabledTags) && ipk != null && !ipk.hasResearch(res.getRegistryName().toString()))
			{
				InventoryDummy id = cr.t.inventory;
				ItemStack paper = id.getStackInSlot(0);
				if(!paper.isEmpty() && paper.getItem() == Items.PAPER)
				{
					cr.t.discovery = res;
					id.decrStackSize(0, 1);
				}
			}
			cr.t.sync();
		}
		return null;
	}
}