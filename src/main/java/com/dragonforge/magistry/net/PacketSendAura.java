package com.dragonforge.magistry.net;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.aura.IChunkAura;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.nbt.NBTTagCompound;

public class PacketSendAura implements IPacket
{
	static
	{
		IPacket.handle(PacketSendAura.class, () -> new PacketSendAura());
	}
	
	public NBTTagCompound nbt;
	
	public PacketSendAura(IChunkAura aura)
	{
		nbt = aura.serializeNBT();
	}
	
	public PacketSendAura()
	{
	}
	
	@Override
	public IPacket executeOnClient(PacketContext net)
	{
		Magistry.proxy.readClientAura(nbt);
		return null;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.merge(this.nbt);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.nbt = nbt.copy();
	}
}