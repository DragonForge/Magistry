package com.dragonforge.magistry.net;

import java.util.Random;

import com.dragonforge.magistry.client.fx.FXWisp;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.net.internal.thunder.Thunder.Layer;
import com.zeitheron.hammercore.proxy.ParticleProxy_Client;
import com.zeitheron.hammercore.utils.AABBUtils;
import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketBreakCrystal implements IPacket
{
	static
	{
		IPacket.handle(PacketBreakCrystal.class, () -> new PacketBreakCrystal(null));
	}
	
	public BlockPos crystal;
	
	public PacketBreakCrystal(BlockPos crystal)
	{
		this.crystal = crystal;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setLong("c", crystal.toLong());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		crystal = BlockPos.fromLong(nbt.getLong("c"));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		World world = Minecraft.getMinecraft().world;
		Random rng = world.rand;
		int particle = rng.nextInt(64) + 64;
		for(int i = 0; i < particle; ++i)
		{
			AxisAlignedBB baseBB = new AxisAlignedBB(crystal);
			Vec3d crPos = AABBUtils.randomPosWithin(baseBB, rng);
			Vec3d target = AABBUtils.randomPosWithin(baseBB.grow(rng.nextDouble() * 6), rng);
			int col = ColorHelper.packRGB(rng.nextFloat(), rng.nextFloat(), rng.nextFloat());
			FXWisp fx;
			ParticleProxy_Client.queueParticleSpawn(fx = new FXWisp(world, crPos.x, crPos.y, crPos.z, target.x, target.y, target.z, .3F + rng.nextFloat() * .7F, new Layer(1, col, true)));
			fx.canCollide = false;
		}
		return null;
	}
}