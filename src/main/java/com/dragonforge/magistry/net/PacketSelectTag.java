package com.dragonforge.magistry.net;

import com.dragonforge.magistry.api.research.ResearchTag;
import com.dragonforge.magistry.inventory.ContainerResearch;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

public class PacketSelectTag implements IPacket
{
	ResearchTag tag;
	
	public PacketSelectTag(ResearchTag tag)
	{
		this.tag = tag;
	}
	
	public PacketSelectTag()
	{
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString("tag", tag.toString());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		tag = ResearchTag.TAGS.get(nbt.getString("tag"));
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		if(tag == null)
			return null;
		EntityPlayerMP mp = net.getSender();
		if(mp != null && mp.openContainer instanceof ContainerResearch)
		{
			ContainerResearch cr = (ContainerResearch) mp.openContainer;
			if(tag.isSelectable(mp))
				cr.t.selectTag(tag);
			cr.t.sync();
		}
		return null;
	}
}