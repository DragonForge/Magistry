package com.dragonforge.magistry.net;

import java.util.Objects;
import java.util.Random;

import com.dragonforge.magistry.blocks.machines.tiles.TileGenerator;
import com.dragonforge.magistry.blocks.tiles.TileNaturalCrystal;
import com.dragonforge.magistry.client.fx.FXWisp;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.net.internal.thunder.Thunder.Layer;
import com.zeitheron.hammercore.proxy.ParticleProxy_Client;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketDischargeCrystal implements IPacket
{
	static
	{
		IPacket.handle(PacketDischargeCrystal.class, () -> new PacketDischargeCrystal(null, null));
	}
	
	public BlockPos core;
	public BlockPos crystal;
	
	public PacketDischargeCrystal(BlockPos core, BlockPos crystal)
	{
		this.core = core;
		this.crystal = crystal;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setLong("r", core.toLong());
		nbt.setLong("c", crystal.toLong());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		core = BlockPos.fromLong(nbt.getLong("r"));
		crystal = BlockPos.fromLong(nbt.getLong("c"));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		World world = Minecraft.getMinecraft().world;
		if(Objects.equals(core, BlockPos.ORIGIN) || world.isBlockLoaded(core))
		{
			Random rng = world.rand;
			
			TileGenerator gen = WorldUtil.cast(world.getTileEntity(core), TileGenerator.class);
			TileNaturalCrystal tnc = WorldUtil.cast(world.getTileEntity(crystal), TileNaturalCrystal.class);
			
			if(gen != null)
			{
				int particle = rng.nextInt(32) + 8;
				for(int i = 0; i < particle; ++i)
				{
					Vec3d crPos = new Vec3d(crystal).add(.5, rng.nextDouble() * .4 + .1, .5);
					Vec3d target = gen.rngCoilPos();
					int col = tnc != null ? tnc.getColor() : ColorHelper.packRGB(rng.nextFloat(), rng.nextFloat(), rng.nextFloat());
					FXWisp fx;
					ParticleProxy_Client.queueParticleSpawn(fx = new FXWisp(world, crPos.x, crPos.y, crPos.z, target.x, target.y, target.z, .3F + gen.getRNG().nextFloat() * .7F, new Layer(1, col, true)));
					fx.canCollide = false;
				}
			}
		}
		return null;
	}
}