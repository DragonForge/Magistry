package com.dragonforge.magistry.net;

import com.dragonforge.magistry.api.research.ResearchTag;
import com.dragonforge.magistry.event.ClientEventHandler.RenderableUV;
import com.zeitheron.hammercore.client.utils.UV;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.EnumHand;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketAddTags implements IPacket
{
	String[] tags;
	EnumHand hand;
	
	public PacketAddTags()
	{
	}
	
	public PacketAddTags(EnumHand hand, ResearchTag... tags)
	{
		this.tags = new String[tags.length];
		for(int i = 0; i < tags.length; ++i)
			this.tags[i] = tags[i].toString();
		this.hand = hand;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		NBTTagList list = new NBTTagList();
		for(String s : tags)
			list.appendTag(new NBTTagString(s));
		nbt.setTag("l", list);
		nbt.setByte("h", (byte) hand.ordinal());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		NBTTagList list = nbt.getTagList("l", NBT.TAG_STRING);
		tags = new String[list.tagCount()];
		for(int i = 0; i < list.tagCount(); ++i)
			tags[i] = list.getStringTagAt(i);
		hand = EnumHand.values()[nbt.getInteger("h")];
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		for(String st : tags)
		{
			ResearchTag tag = ResearchTag.TAGS.get(st);
			if(tag != null)
				RenderableUV.create(new UV(tag.getTexture(), 0, 0, 256, 256), hand);
		}
		return null;
	}
}