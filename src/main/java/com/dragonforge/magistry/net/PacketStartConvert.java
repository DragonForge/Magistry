package com.dragonforge.magistry.net;

import com.dragonforge.magistry.items.ItemMultiblockActivator;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketStartConvert implements IPacket
{
	static
	{
		IPacket.handle(PacketStartConvert.class, () -> new PacketStartConvert(null, null, 0, false, false));
	}
	
	public AxisAlignedBB particleBB;
	public ItemMultiblockActivator activator;
	public boolean zaps, orbs;
	public int maxTicks;
	
	public PacketStartConvert(ItemMultiblockActivator activator, AxisAlignedBB aabb, int maxTicks, boolean zaps, boolean orbs)
	{
		this.maxTicks = maxTicks;
		this.activator = activator;
		this.particleBB = aabb;
		this.zaps = zaps;
		this.orbs = orbs;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		new ItemMultiblockActivator.CLProcessParticles(activator, particleBB, maxTicks, zaps, orbs).start();
		return null;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setDouble("mx", particleBB.minX);
		nbt.setDouble("my", particleBB.minY);
		nbt.setDouble("mz", particleBB.minZ);
		nbt.setDouble("xx", particleBB.maxX);
		nbt.setDouble("xy", particleBB.maxY);
		nbt.setDouble("xz", particleBB.maxZ);
		nbt.setInteger("mt", maxTicks);
		nbt.setString("ac", activator.getRegistryName().toString());
		nbt.setBoolean("za", zaps);
		nbt.setBoolean("ob", orbs);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		particleBB = new AxisAlignedBB(nbt.getDouble("mx"), nbt.getDouble("my"), nbt.getDouble("mz"), nbt.getDouble("xx"), nbt.getDouble("xy"), nbt.getDouble("xz"));
		maxTicks = nbt.getInteger("mt");
		activator = (ItemMultiblockActivator) GameRegistry.findRegistry(Item.class).getValue(new ResourceLocation(nbt.getString("ac")));
		zaps = nbt.getBoolean("za");
		orbs = nbt.getBoolean("ob");
	}
}