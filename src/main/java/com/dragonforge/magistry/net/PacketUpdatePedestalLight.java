package com.dragonforge.magistry.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketUpdatePedestalLight implements IPacket
{
	static
	{
		IPacket.handle(PacketUpdatePedestalLight.class, PacketUpdatePedestalLight::new);
	}
	
	public BlockPos pos;
	
	public PacketUpdatePedestalLight setPos(BlockPos pos)
	{
		this.pos = pos;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setLong("l", pos.toLong());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		pos = BlockPos.fromLong(nbt.getLong("l"));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		World world = Minecraft.getMinecraft().world;
		Minecraft.getMinecraft().addScheduledTask(() -> world.checkLight(pos));
//		TileEntity tile = world.getTileEntity(pos);
//		world.setBlockState(pos, world.getBlockState(pos), 3);
//		if(tile != null)
//			tile.validate();
//		world.setTileEntity(pos, tile);
		return null;
	}
}