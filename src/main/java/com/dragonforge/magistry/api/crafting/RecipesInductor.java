package com.dragonforge.magistry.api.crafting;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.dragonforge.magistry.api.crafting.recipes.InductorRecipe;
import com.dragonforge.magistry.blocks.tiles.TilePedestal;
import com.zeitheron.hammercore.utils.InterItemStack;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraftforge.oredict.OreDictionary;

public class RecipesInductor
{
	public static final List<InductorRecipe> RECIPES = RecipesRoot.getListOfRecipes(InductorRecipe.class);
	public static final List<InductorBlacklistStack> BLACKLIST = RecipesRoot.getListOfRecipes(InductorBlacklistStack.class);
	
	public static boolean isStackBlacklisted(ItemStack stack)
	{
		return BLACKLIST.isEmpty() && BLACKLIST.stream().filter(s -> s.matches(stack)).findAny().isPresent();
	}
	
	public static InductorRecipe findRecipe(NonNullList<ItemStack> stacks)
	{
		for(int i = 0; i < RECIPES.size(); ++i)
			if(RECIPES.get(i).canCraft(stacks))
			{
				InductorRecipe rec = RECIPES.get(i);
				if(!isStackBlacklisted(rec.output))
					return rec;
			}
		return null;
	}
	
	public static ItemStack mapper(TilePedestal pedestal)
	{
		return pedestal != null ? pedestal.getItem() : ItemStack.EMPTY;
	}
	
	public static NonNullList<ItemStack> convertToItems(List<TilePedestal> pedestals)
	{
		List<ItemStack> itemsNL = pedestals.stream().map(RecipesInductor::mapper).collect(Collectors.toList());
		return NonNullList.from(ItemStack.EMPTY, itemsNL.toArray(new ItemStack[itemsNL.size()]));
	}
	
	public static boolean canCraft(List<TilePedestal> pedestals)
	{
		return findRecipe(convertToItems(pedestals)) != null;
	}
	
	public static ItemStack consumeItemsAndGenerateOutput(List<TilePedestal> pedestals)
	{
		NonNullList<ItemStack> items = convertToItems(pedestals);
		InductorRecipe recipe = findRecipe(items);
		if(recipe != null)
			return recipe.consumeItemsAndReturnOutput(items);
		return ItemStack.EMPTY;
	}
	
	public static class InductorBlacklistStack
	{
		Item item;
		int meta;
		NBTTagCompound nbt;
		
		public InductorBlacklistStack(ItemStack stack)
		{
			if(InterItemStack.isStackNull(stack))
				throw new IllegalArgumentException("stack may not be empty!");
			item = stack.getItem();
			meta = stack.getItemDamage();
			nbt = stack.hasTagCompound() ? stack.getTagCompound().copy() : null;
		}
		
		public boolean matches(ItemStack stack)
		{
			return !stack.isEmpty() && stack.getItem() == item && (stack.getItemDamage() == meta || meta == OreDictionary.WILDCARD_VALUE) && Objects.equals(nbt, stack.getTagCompound());
		}
		
		public void add()
		{
			if(!RecipesInductor.BLACKLIST.contains(this))
				RecipesInductor.BLACKLIST.add(this);
		}
		
		public void remove()
		{
			if(RecipesInductor.BLACKLIST.contains(this))
				RecipesInductor.BLACKLIST.remove(this);
		}
	}
}