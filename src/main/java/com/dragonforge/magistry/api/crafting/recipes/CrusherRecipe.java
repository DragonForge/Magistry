package com.dragonforge.magistry.api.crafting.recipes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.crafting.RecipesCrusher;
import com.dragonforge.magistry.api.crafting.RecipesInductor;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tileentity.TileEntity;

public class CrusherRecipe implements IMagistryRecipe
{
	public Ingredient input;
	public float cost = 0.1F;
	public List<ItemStack> outputs;
	public Function<Integer, ItemStack> dropTransformer;
	
	public CrusherRecipe(Ingredient input, float cu, ItemStack... outputs)
	{
		this.input = input;
		this.cost = cu;
		this.outputs = new ArrayList<>(Arrays.asList(outputs));
		this.dropTransformer = this.outputs::get;
		this.dropTransformer = this.dropTransformer.andThen(ItemStack::copy);
	}
	
	public CrusherRecipe withDropTransformer(Function<ItemStack, ItemStack> dropTransformer)
	{
		this.dropTransformer = this.dropTransformer.andThen(dropTransformer);
		return this;
	}
	
	public boolean testInput(ItemStack input)
	{
		return this.input.apply(input);
	}
	
	public void createDrop(TileEntity tile, Consumer<ItemStack> drop)
	{
		ItemStack is;
		for(int i = 0; i < outputs.size(); ++i)
		{
			is = dropTransformer.apply(i);
			if(!is.isEmpty())
				drop.accept(is);
		}
	}
	
	@Override
	public IMagistryRecipe add()
	{
		if(!RecipesCrusher.RECIPES.contains(this))
			RecipesCrusher.RECIPES.add(this);
		return this;
	}
	
	@Override
	public void remove()
	{
		if(RecipesCrusher.RECIPES.contains(this))
			RecipesCrusher.RECIPES.remove(this);
	}
}