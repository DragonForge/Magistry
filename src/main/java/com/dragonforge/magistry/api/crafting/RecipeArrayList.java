package com.dragonforge.magistry.api.crafting;

import java.util.ArrayList;
import java.util.stream.Collectors;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class RecipeArrayList extends ArrayList<IRecipe>
{
	final ItemStack resulting;
	
	public RecipeArrayList(ItemStack resulting)
	{
		this.resulting = resulting;
	}
	
	public void refresh()
	{
		clear();
		addAll(ForgeRegistries.RECIPES.getValuesCollection().stream().filter(r -> resulting.isItemEqual(r.getRecipeOutput())).collect(Collectors.toList()));
	}
}