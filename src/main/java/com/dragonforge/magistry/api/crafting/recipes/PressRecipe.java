package com.dragonforge.magistry.api.crafting.recipes;

import java.util.function.Function;

import com.dragonforge.magistry.api.crafting.RecipesPress;
import com.dragonforge.magistry.api.item.CountableIngredient;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;

public class PressRecipe implements IMagistryRecipe
{
	public Ingredient mold;
	public CountableIngredient input;
	public ItemStack output;
	
	public Function<ItemStack, ItemStack> outputTransformer = stack -> stack;
	
	@Override
	public IMagistryRecipe add()
	{
		if(!RecipesPress.RECIPES.contains(this))
			RecipesPress.RECIPES.add(this);
		return this;
	}
	
	@Override
	public void remove()
	{
		if(RecipesPress.RECIPES.contains(this))
			RecipesPress.RECIPES.remove(this);
	}
	
	public PressRecipe(ItemStack output, Ingredient mold, ItemStack input)
	{
		this(output, mold, new CountableIngredient(Ingredient.fromStacks(input), input.getCount()));
	}
	
	public PressRecipe(ItemStack output, Ingredient mold, CountableIngredient input)
	{
		this.input = input;
		this.mold = mold;
		this.output = output;
	}
	
	public boolean canCraft(ItemStack mold, ItemStack input)
	{
		return this.mold.test(mold) && this.input.matches(input);
	}
	
	public ItemStack consumeItemsAndReturnOutput(ItemStack mold, ItemStack input)
	{
		if(canCraft(mold, input))
		{
			input.shrink(this.input.quantity);
			return outputTransformer.apply(output.copy());
		}
		
		return outputTransformer.apply(ItemStack.EMPTY);
	}
}