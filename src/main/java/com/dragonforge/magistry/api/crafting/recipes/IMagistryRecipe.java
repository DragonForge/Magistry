package com.dragonforge.magistry.api.crafting.recipes;

public interface IMagistryRecipe
{
	IMagistryRecipe add();
	
	void remove();
}