package com.dragonforge.magistry.api.crafting;

import java.util.ArrayList;
import java.util.List;

import com.dragonforge.magistry.api.crafting.recipes.CrusherRecipe;

import net.minecraft.item.ItemStack;

public class RecipesCrusher
{
	public static final List<CrusherRecipe> RECIPES = RecipesRoot.getListOfRecipes(CrusherRecipe.class);
	
	public static CrusherRecipe findRecipe(ItemStack stack)
	{
		for(int i = 0; i < RECIPES.size(); ++i)
			if(RECIPES.get(i).testInput(stack))
				return RECIPES.get(i);
		return null;
	}
}