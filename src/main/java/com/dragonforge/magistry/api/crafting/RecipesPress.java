package com.dragonforge.magistry.api.crafting;

import java.util.ArrayList;
import java.util.List;

import com.dragonforge.magistry.api.crafting.recipes.CrusherRecipe;
import com.dragonforge.magistry.api.crafting.recipes.PressRecipe;

import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreIngredient;

public class RecipesPress
{
	public static final List<PressRecipe> RECIPES = RecipesRoot.getListOfRecipes(PressRecipe.class);
	
	public static final OreIngredient MOLD_GEAR = new OreIngredient("moldGear");
	public static final OreIngredient MOLD_INGOT = new OreIngredient("moldIngot");
	public static final OreIngredient MOLD_PLATE = new OreIngredient("moldPlate");
	public static final OreIngredient MOLD_ROD = new OreIngredient("moldRod");
	
	public static PressRecipe findRecipe(ItemStack mold, ItemStack input)
	{
		for(int i = 0; i < RECIPES.size(); ++i)
			if(RECIPES.get(i).canCraft(mold, input))
				return RECIPES.get(i);
		return null;
	}
}