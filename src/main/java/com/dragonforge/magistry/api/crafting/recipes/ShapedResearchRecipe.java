package com.dragonforge.magistry.api.crafting.recipes;

import java.lang.reflect.Field;

import com.dragonforge.magistry.utils.ContainerHelper;

import net.minecraft.inventory.Container;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.oredict.ShapedOreRecipe;

public class ShapedResearchRecipe extends ShapedOreRecipe
{
	final String research;
	
	public ShapedResearchRecipe(String research, ItemStack result, Object... recipe)
	{
		super(new ResourceLocation("magistry", "research_crafting"), result, recipe);
		this.research = research;
	}
	
	@Override
	public boolean matches(InventoryCrafting inv, World world)
	{
		return super.matches(inv, world) && ContainerHelper.doesInventoryCraftingHaveResearch(inv, research);
	}
	
	@Override
	public ItemStack getCraftingResult(InventoryCrafting inv)
	{
		return ContainerHelper.doesInventoryCraftingHaveResearch(inv, research) ? super.getCraftingResult(inv) : ItemStack.EMPTY;
	}
}