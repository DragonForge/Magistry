package com.dragonforge.magistry.api.crafting.recipes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.crafting.RecipesInductor;
import com.zeitheron.hammercore.utils.ConsumableItem;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraftforge.oredict.OreIngredient;

public class InductorRecipe implements IMagistryRecipe
{
	public List<Ingredient> inputs;
	public ItemStack output;
	
	public Function<ItemStack, ItemStack> outputTransformer = stack -> stack;
	
	@Override
	public IMagistryRecipe add()
	{
		if(!RecipesInductor.RECIPES.contains(this))
			RecipesInductor.RECIPES.add(this);
		return this;
	}
	
	@Override
	public void remove()
	{
		if(RecipesInductor.RECIPES.contains(this))
			RecipesInductor.RECIPES.remove(this);
	}
	
	public InductorRecipe(ItemStack output, Ingredient... ingredients)
	{
		this(output, Arrays.asList(ingredients));
	}
	
	public InductorRecipe(ItemStack output, List<Ingredient> ingredients)
	{
		this.inputs = Collections.unmodifiableList(ingredients);
		this.output = output;
	}
	
	public boolean canCraft(NonNullList<ItemStack> items)
	{
		InventoryDummy inv = new InventoryDummy(items.size());
		for(int i = 0; i < inv.getSizeInventory(); ++i)
			inv.setInventorySlotContents(i, items.get(i).copy());
		
		for(Ingredient ing : inputs)
		{
			if(ing.getMatchingStacks().length == 0)
				continue;
			if(ing instanceof OreIngredient)
				ing = Ingredient.fromStacks(ing.getMatchingStacks());
			ConsumableItem ci = new ConsumableItem(1, ing);
			if(!ci.consume(inv))
				return false;
		}
		
		return true;
	}
	
	public ItemStack consumeItemsAndReturnOutput(NonNullList<ItemStack> items)
	{
		if(canCraft(items))
		{
			InventoryDummy inv = new InventoryDummy(items.size());
			for(int i = 0; i < inv.getSizeInventory(); ++i)
				inv.setInventorySlotContents(i, items.get(i));
			for(Ingredient ing : inputs)
			{
				if(ing.getMatchingStacks().length == 0)
					continue;
				if(ing instanceof OreIngredient)
					ing = Ingredient.fromStacks(ing.getMatchingStacks());
				ConsumableItem ci = new ConsumableItem(1, ing);
				if(!ci.consume(inv))
					return ItemStack.EMPTY;
			}
			return outputTransformer.apply(output.copy());
		}
		
		return outputTransformer.apply(ItemStack.EMPTY);
	}
}