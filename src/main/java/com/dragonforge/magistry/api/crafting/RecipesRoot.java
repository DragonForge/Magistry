package com.dragonforge.magistry.api.crafting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecipesRoot
{
	private static final Map<Class, List> ROOTS = new HashMap<>();
	
	public static <T> List<T> getListOfRecipes(Class<T> type)
	{
		List<T> l = ROOTS.get(type);
		if(l == null)
			ROOTS.put(type, l = new ArrayList<>());
		return l;
	}
}