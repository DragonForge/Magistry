package com.dragonforge.magistry.api.aura;

import com.dragonforge.magistry.api.MagistryAPI;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;

public class ChunkAuraProvider implements ICapabilityProvider, INBTSerializable<NBTTagCompound>
{
	private final ChunkAuraImpl aura = new ChunkAuraImpl();
	
	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		aura.deserializeNBT(nbt);
	}
	
	@Override
	public NBTTagCompound serializeNBT()
	{
		return aura.serializeNBT();
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == MagistryAPI.CAPABILITY_AURA;
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == MagistryAPI.CAPABILITY_AURA)
			return (T) aura;
		return null;
	}
}