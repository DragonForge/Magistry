package com.dragonforge.magistry.api.aura;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.dragonforge.magistry.api.MagistryAPI;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class ChunkAuraImpl implements IChunkAura
{
	public int auraUnits, auraPollutionUnits;
	public boolean generated;
	
	public boolean updated;
	
	@Override
	public void update(Chunk c)
	{
		World w = c.getWorld();
		List<IChunkAura> chunks = new ArrayList<>();
		for(int x = -1; x < 2; ++x)
			for(int z = -1; z < 2; ++z)
				chunks.add(MagistryAPI.getAura(w, c.x + x, c.z + z));
		chunks.remove(this);
		balance(chunks);
		setUpdated(true);
	}
	
	@Override
	public void generate(Random rand)
	{
		auraUnits = rand.nextInt(10000);
		auraPollutionUnits = 0;
		generated = true;
	}
	
	@Override
	public boolean hasGenerated()
	{
		return generated;
	}
	
	@Override
	public boolean hasUpdated()
	{
		return updated;
	}
	
	@Override
	public void setUpdated(boolean upd)
	{
		updated = upd;
	}
	
	@Override
	public int getAU()
	{
		return auraUnits;
	}
	
	@Override
	public int getPollution()
	{
		return auraPollutionUnits;
	}
	
	@Override
	public void setAU(int au)
	{
		auraUnits = au;
	}
	
	@Override
	public void setPollution(int apu)
	{
		auraPollutionUnits = apu;
	}
	
	@Override
	public NBTTagCompound serializeNBT()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setInteger("AU", auraUnits);
		nbt.setInteger("APU", auraPollutionUnits);
		nbt.setBoolean("Gen", generated);
		return nbt;
	}
	
	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		auraUnits = nbt.getInteger("AU");
		auraPollutionUnits = nbt.getInteger("APU");
		generated = nbt.getBoolean("Gen");
	}
}