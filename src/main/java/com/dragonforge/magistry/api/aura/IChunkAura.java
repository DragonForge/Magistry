package com.dragonforge.magistry.api.aura;

import java.util.Random;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.util.INBTSerializable;

public interface IChunkAura extends INBTSerializable<NBTTagCompound>
{
	void generate(Random rand);
	
	default void balance(Iterable<IChunkAura> auras)
	{
		int au = getAU();
		int apu = getPollution();
		
		int t = 0;
		for(IChunkAura a : auras)
			if(a != this)
			{
				t++;
				au += a.getAU();
				apu += a.getPollution();
			}
		
		int perAU = au / t;
		int perAPU = apu / t;
		
		setAU(au - perAU * t);
		setPollution(apu - perAPU * t);
		
		for(IChunkAura a : auras)
			if(a != this)
			{
				a.setAU(perAU);
				a.setPollution(perAPU);
			}
	}
	
	boolean hasGenerated();
	
	void update(Chunk c);
	
	void setUpdated(boolean upd);
	
	boolean hasUpdated();
	
	int getAU();
	
	void setAU(int au);
	
	int getPollution();
	
	void setPollution(int apu);
}