package com.dragonforge.magistry.api.item;

import com.zeitheron.hammercore.api.crafting.IngredientStack;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;

public class CountableIngredient
{
	public final Ingredient input;
	public int quantity = 0;
	
	public CountableIngredient(Ingredient input)
	{
		this.input = input;
		quantity = 1;
	}
	
	public CountableIngredient(Ingredient input, int size)
	{
		this.input = input;
		quantity = size;
	}
	
	public boolean isEmpty()
	{
		return input == null || input.getMatchingStacks().length == 0 || quantity < 1;
	}
	
	public boolean matches(ItemStack stack)
	{
		return input.apply(stack) && stack.getCount() > quantity;
	}
}