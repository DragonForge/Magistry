package com.dragonforge.magistry.api.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public interface IEtherFilter
{
	float getEtherRate(ItemStack stack);
	
	boolean canFilter(ItemStack stack);
	
	default ItemStack onFilter(ItemStack stack)
	{
		return stack.copy();
	}
	
	default Item asItem()
	{
		return (Item) this;
	}
}