package com.dragonforge.magistry.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.aura.IChunkAura;
import com.dragonforge.magistry.api.ce.ICrystalStorage;
import com.dragonforge.magistry.api.multiblock.MultiblockRecipe;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.api.research.ResearchTag;
import com.dragonforge.magistry.api.research.scan.ScanRegistry;
import com.dragonforge.magistry.init.ItemsM;
import com.dragonforge.magistry.net.PacketAddTags;
import com.google.common.base.Predicates;
import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.raytracer.RayTracer;
import com.zeitheron.hammercore.utils.SoundUtil;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFire;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistry;

public class MagistryAPI
{
	@CapabilityInject(IPlayerKnowledge.class)
	public static Capability<IPlayerKnowledge> CAPABILITY_KNOWLEDGE;
	
	@CapabilityInject(IChunkAura.class)
	public static Capability<IChunkAura> CAPABILITY_AURA;
	
	@CapabilityInject(ICrystalStorage.class)
	public static Capability<ICrystalStorage> CAPABILITY_CRYSTAL_STORAGE;
	
	public static final DamageSource ELECTRICAL_DAMAGE = new DamageSource(Magistry.MOD_ID + ":electricity").setDifficultyScaled();
	
	public static IForgeRegistry<ResearchBase> RESEARCH_REGISTRY;
	public static IForgeRegistry<MultiblockRecipe> MULTIBLOCK_REGISTRY;
	
	public static INetworkAPI network;
	private static final Map<String, ResearchBase> RES_MAP = new HashMap<>();
	private static final Set<Block> HOT_BLOCKS = new HashSet<Block>(), HOT_BLOCKS_VIEW = Collections.unmodifiableSet(HOT_BLOCKS);
	
	public static ResearchBase getResearch(String id)
	{
		if(!RES_MAP.containsKey(id))
			RES_MAP.put(id, RESEARCH_REGISTRY.getValue(new ResourceLocation(id)));
		return RES_MAP.get(id);
	}
	
	public static IBlockState[] getValidStates(Block block)
	{
		List<IBlockState> states = new ArrayList<>(block.getBlockState().getValidStates());
		return states.toArray(new IBlockState[states.size()]);
	}
	
	public static IBlockState[] getOreDictionaryStates(String oredict)
	{
		List<IBlockState> states = new ArrayList<>();
		OreDictionary.getOres(oredict).stream() //
		        .map(stack -> stack.getItem()).filter(item -> item instanceof ItemBlock) //
		        .map(i -> getValidStates(((ItemBlock) i).getBlock())) //
		        .forEach(sa -> states.addAll(Arrays.asList(sa)));
		return states.toArray(new IBlockState[states.size()]);
	}
	
	public static IPlayerKnowledge getPlayerKnowledge(EntityPlayer player)
	{
		if(player == null || player instanceof FakePlayer)
			return null;
		return player.getCapability(CAPABILITY_KNOWLEDGE, null);
	}
	
	public static IChunkAura getAura(World world, BlockPos pos)
	{
		return getAura(world.getChunk(pos));
	}
	
	public static IChunkAura getAura(World world, ChunkPos pos)
	{
		return getAura(world, pos.x, pos.z);
	}
	
	public static IChunkAura getAura(World world, int cx, int cz)
	{
		return getAura(world.getChunk(cx, cz));
	}
	
	public static IChunkAura getAura(Chunk chunk)
	{
		return chunk.getCapability(CAPABILITY_AURA, null);
	}
	
	public static boolean isClientHoldingCrystalStaff()
	{
		EntityPlayer player = HammerCore.renderProxy.getClientPlayer();
		if(player != null)
			for(EnumHand hand : EnumHand.values())
				if(!player.getHeldItem(hand).isEmpty() && player.getHeldItem(hand).getItem() == ItemsM.CRYSTAL_STAFF)
					return true;
		return false;
	}
	
	public static Predicate<String> wildcardMatcher(String token)
	{
		if(token == null || token.isEmpty())
			return Predicates.alwaysTrue();
		boolean anyStart = token.startsWith("*");
		boolean anyEnd = token.endsWith("*");
		if(anyStart)
			token = token.substring(1);
		if(anyEnd)
			token = token.substring(0, token.length() - 1);
		String[] vals = token.split("[*]");
		return in ->
		{
			if(anyStart)
			{
				int ix = in.indexOf(vals[0]);
				if(ix == -1)
					return false;
				in = in.substring(ix);
			}
			if(anyEnd)
			{
				int ix = in.lastIndexOf(vals[vals.length - 1]) + vals[vals.length - 1].length();
				if(ix == -1)
					return false;
				in = in.substring(0, ix);
			}
			if(!in.startsWith(vals[0]))
				return false;
			if(!in.endsWith(vals[vals.length - 1]))
				return false;
			int ind = 0;
			int i;
			for(int j = anyStart ? 1 : 0; j < (vals.length - (anyEnd ? 1 : 0)); ++j)
			{
				String cur = vals[j];
				if((i = in.indexOf(cur, ind)) >= ind)
					ind = i + cur.length();
				else
					return false;
			}
			return true;
		};
	}
	
	public static void addHotBlock(Block block)
	{
		HOT_BLOCKS.add(block);
	}
	
	public static Set<Block> getHotBlocks()
	{
		return HOT_BLOCKS_VIEW;
	}
	
	public static boolean performScan(EntityPlayerMP mp, EnumHand hand, @Nullable SoundEvent success, @Nullable SoundEvent fail)
	{
		World world = mp.world;
		
		List<ResearchTag> avail = ResearchTag.TAGS.values().stream().filter(t -> t.isSelectable(mp)).collect(Collectors.toList());
		
		boolean didScan = false;
		Entity entity = RayTracer.retraceEntity(mp);
		RayTraceResult rtr = mp.world.rayTraceBlocks(RayTracer.getStartVec(mp), RayTracer.getEndVec(mp), true, false, true);
		if(entity != null && (rtr == null || (entity.getDistanceSq(mp) <= mp.getDistanceSq(rtr.getBlockPos()))))
			didScan |= ScanRegistry.doScan(mp, rtr.entityHit);
		else if(rtr != null)
		{
			IFluidHandler fH = null;
			BlockPos fHP = null;
			
			if(rtr.typeOfHit == Type.BLOCK)
			{
				BlockPos pos = rtr.getBlockPos();
				for(EntityItem ent : world.getEntitiesWithinAABB(EntityItem.class, new AxisAlignedBB(pos.offset(rtr.sideHit))))
					if(ent.getEntityBoundingBox().grow(.25).calculateIntercept(RayTracer.getStartVec(mp), RayTracer.getEndVec(mp)) != null)
						didScan |= ScanRegistry.doScan(mp, ent.getItem());
				IBlockState potentialFire = world.getBlockState(pos.offset(rtr.sideHit));
				if(potentialFire.getBlock() instanceof BlockFire)
					didScan |= ScanRegistry.doScan(mp, potentialFire);
				didScan |= ScanRegistry.doScan(mp, world.getBlockState(pos));
				TileEntity tile = world.getTileEntity(pos);
				if(tile != null)
				{
					didScan |= ScanRegistry.doScan(mp, tile);
					if(tile.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, rtr.sideHit))
					{
						fH = tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, rtr.sideHit);
						fHP = pos;
						if(fH != null && fHP != null)
							for(IFluidTankProperties props : fH.getTankProperties())
							{
								FluidStack fs = props.getContents();
								didScan |= ScanRegistry.doScan((EntityPlayerMP) mp, fs);
							}
					}
				}
				RayTraceResult rtr2 = mp.world.rayTraceBlocks(RayTracer.getStartVec(mp), RayTracer.getEndVec(mp), true);
				if(rtr2 != null && !rtr2.getBlockPos().equals(rtr.getBlockPos()))
				{
					fH = FluidUtil.getFluidHandler(world, fHP = rtr2.getBlockPos(), rtr2.sideHit);
					if(fH != null && fHP != null)
						for(IFluidTankProperties props : fH.getTankProperties())
						{
							FluidStack fs = props.getContents();
							didScan |= ScanRegistry.doScan((EntityPlayerMP) mp, fs);
						}
				}
			}
			
			if(fH != null && fHP != null)
				for(IFluidTankProperties props : fH.getTankProperties())
				{
					FluidStack fs = props.getContents();
					didScan |= ScanRegistry.doScan((EntityPlayerMP) mp, world.getBlockState(fHP));
				}
		}
		if(didScan)
		{
			if(success != null)
				SoundUtil.playSoundEffect(world, success.soundName.toString(), mp.getPosition(), 1F, 1F, SoundCategory.PLAYERS);
			
			List<ResearchTag> ntl = new ArrayList<>(ResearchTag.TAGS.values().stream().filter(t -> t.isSelectable(mp)).collect(Collectors.toList()));
			ntl.removeAll(avail);
			
			if(!ntl.isEmpty())
				HCNet.INSTANCE.sendTo(new PacketAddTags(hand, ntl.toArray(new ResearchTag[ntl.size()])), mp);
		} else if(fail != null)
			SoundUtil.playSoundEffect(world, fail.soundName.toString(), mp.getPosition(), 1F, 1F, SoundCategory.PLAYERS);
		return didScan;
	}
}