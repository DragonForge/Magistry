package com.dragonforge.magistry.api.tile;

import net.minecraft.util.EnumFacing;

public interface IRotateable
{
	void setFacing(EnumFacing face);
	
	EnumFacing getFacing();
}