package com.dragonforge.magistry.api.gui;

import com.dragonforge.magistry.api.research.IPlayerKnowledge;

import net.minecraft.client.gui.GuiScreen;

/**
 * Implement on {@link GuiScreen} to make it accept {@link IPlayerKnowledge}
 * when a sync task happens.
 */
public interface IGuiKnowledgeAcceptor
{
	void accept(IPlayerKnowledge knowledge);
}