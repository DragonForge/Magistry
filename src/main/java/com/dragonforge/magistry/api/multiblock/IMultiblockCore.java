package com.dragonforge.magistry.api.multiblock;

import com.dragonforge.magistry.blocks.tiles.TileMultiblockCore;
import com.zeitheron.hammercore.api.ITileBlock;

import net.minecraft.block.ITileEntityProvider;

public interface IMultiblockCore<T extends TileMultiblockCore> extends ITileBlock<T>, ITileEntityProvider
{
}