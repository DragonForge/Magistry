package com.dragonforge.magistry.api.multiblock;

import java.util.HashMap;
import java.util.Map;

import com.dragonforge.magistry.api.tile.IRotateable;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockCore;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockPart;
import com.dragonforge.magistry.init.BlocksM;
import com.zeitheron.hammercore.lib.zlib.tuple.TwoTuple;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.util.BlockSnapshot;

public class MultiblockTileRecipe extends MultiblockRecipe
{
	public final BlockPos corePos;
	
	public MultiblockTileRecipe(String name, Map<BlockPos, BlockInput> components, BlockPos corePos, TwoTuple<BlockInput, IBlockState> coreInfo)
	{
		super(name, convert(components, corePos, coreInfo, null));
		this.corePos = corePos;
		this.components.values().stream().filter(c -> c instanceof TileComponent).map(c -> (TileComponent) c).forEach(c -> c.recipe = getRegistryName());
	}
	
	public MultiblockTileRecipe(String name, Map<BlockPos, BlockInput> components, BlockPos corePos, TwoTuple<BlockInput, IBlockState> coreInfo, EnumFacing lFace)
	{
		super(name, convert(components, corePos, coreInfo, lFace));
		this.corePos = corePos;
		this.components.values().stream().filter(c -> c instanceof TileComponent).map(c -> (TileComponent) c).forEach(c -> c.recipe = getRegistryName());
	}
	
	private static Map<BlockPos, TileComponent> convert(Map<BlockPos, BlockInput> components, BlockPos corePos, TwoTuple<BlockInput, IBlockState> coreInfo, EnumFacing lFace)
	{
		Map<BlockPos, TileComponent> ncs = new HashMap<>();
		for(BlockPos p : components.keySet())
			ncs.put(p, new TileComponent(components.get(p)));
		
		ncs.put(corePos, new TileComponent(coreInfo.get2(), coreInfo.get1()));
		ncs.get(corePos).lFace = lFace;
		
		ncs.keySet().forEach(pos ->
		{
			TileComponent tc = ncs.get(pos);
			tc.corePos = corePos;
			tc.relPos = pos;
		});
		return ncs;
	}
	
	public static class TileComponent extends BlockComponent
	{
		BlockPos corePos;
		BlockPos relPos;
		ResourceLocation recipe;
		EnumFacing lFace;
		
		public TileComponent(BlockInput input)
		{
			super(BlocksM.MULTIBLOCK_PART.getDefaultState(), input);
		}
		
		public TileComponent(IBlockState output, BlockInput input)
		{
			super(output, input);
		}
		
		@Override
		public void output(World world, BlockPos pos)
		{
			if(matches(world, pos))
			{
				IBlockState state = output.apply(world.getBlockState(pos));
				
				if(state != null && !(state.getBlock() instanceof IMultiblockCore))
				{
					TileMultiblockPart part = new TileMultiblockPart();
					part.before = new BlockSnapshot(world, pos, world.getBlockState(pos));
					part.thisX = relPos.getX();
					part.thisY = relPos.getY();
					part.thisZ = relPos.getZ();
					part.multiblock = recipe;
					part.core = pos.subtract(relPos).add(corePos);
					world.destroyBlock(pos, false);
					world.setBlockState(pos, state);
					world.setTileEntity(pos, part);
				} else if(state != null && state.getBlock() instanceof IMultiblockCore)
				{
					TileMultiblockCore core = (TileMultiblockCore) ((IMultiblockCore) state.getBlock()).createNewTileEntity(world, state.getBlock().getMetaFromState(state));
					core.before = new BlockSnapshot(world, pos, world.getBlockState(pos));
					world.destroyBlock(pos, false);
					world.setBlockState(pos, state);
					world.setTileEntity(pos, core);
					if(core instanceof IRotateable)
						((IRotateable) core).setFacing(lFace);
					core.setMultiblock(recipe);
				} else if(state != null)
				{
					world.destroyBlock(pos, false);
					world.setBlockState(pos, state);
				} else
					world.destroyBlock(pos, false);
			}
		}
		
		public void deform(World world, BlockPos pos)
		{
			if(world.isRemote)
				return;
			if(world.getTileEntity(pos) instanceof TileMultiblockPart)
			{
				TileMultiblockPart part = (TileMultiblockPart) world.getTileEntity(pos);
				part.before.restoreToLocation(world, pos, true, true);
			} else if(world.getTileEntity(pos) instanceof TileMultiblockCore)
			{
				TileMultiblockCore core = (TileMultiblockCore) world.getTileEntity(pos);
				core.createDrop(null, world, pos);
				core.before.restoreToLocation(world, pos, true, true);
			}
		}
	}
}