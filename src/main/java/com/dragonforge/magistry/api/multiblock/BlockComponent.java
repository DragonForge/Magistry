package com.dragonforge.magistry.api.multiblock;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import com.dragonforge.magistry.api.MagistryAPI;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockComponent
{
	public final BlockInput input;
	public final Function<IBlockState, IBlockState> output;
	public final Map<IBlockState, IBlockState> outputMap = new HashMap<>();
	
	boolean constantValue;
	
	public BlockComponent(IBlockState output, String od)
	{
		this(anyState -> output, MagistryAPI.getOreDictionaryStates(od));
		constantValue = true;
	}
	
	public BlockComponent(IBlockState output, BlockInput input)
	{
		this.input = input;
		this.output = anyState -> output;
		constantValue = true;
		initStateMap();
	}
	
	public BlockComponent(IBlockState output, Block block)
	{
		this(anyState -> output, MagistryAPI.getValidStates(block));
		constantValue = true;
	}
	
	public BlockComponent(IBlockState output, IBlockState input)
	{
		this(anyState -> output, input);
		constantValue = true;
	}
	
	public BlockComponent(Map<IBlockState, IBlockState> transform)
	{
		this.input = new BlockInput(transform.keySet().toArray(new IBlockState[transform.size()]));
		output = state -> transform.getOrDefault(state, transform.get(input.states[0]));
		outputMap.putAll(transform);
		constantValue = false;
		initStateMap();
	}
	
	public BlockComponent(Function<IBlockState, IBlockState> output, IBlockState... states)
	{
		this.output = output;
		this.input = new BlockInput(states);
		initStateMap();
		constantValue = false;
	}
	
	private void initStateMap()
	{
		outputMap.clear();
		for(IBlockState state : input.states)
		{
			IBlockState out = output.apply(state);
			if(out != null)
				outputMap.put(state, out);
		}
	}
	
	public boolean matches(World world, BlockPos pos)
	{
		if(outputMap.isEmpty())
			initStateMap();
		return matches(world.getBlockState(pos));
	}
	
	public void output(World world, BlockPos pos)
	{
		if(outputMap.isEmpty())
			initStateMap();
		if(matches(world, pos))
		{
			IBlockState state = output.apply(world.getBlockState(pos));
			world.destroyBlock(pos, false);
			if(state != null)
				world.setBlockState(pos, state);
		}
	}
	
	public boolean matches(IBlockState state)
	{
		if(outputMap.isEmpty())
			initStateMap();
		return input.test(state);
	}
}