package com.dragonforge.magistry.api.multiblock;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.dragonforge.magistry.api.MagistryAPI;
import com.google.common.base.Predicates;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;

public class BlockInput implements Predicate<IBlockState>
{
	public final IBlockState[] states;
	
	private Predicate<IBlockState> tester = Predicates.alwaysTrue();
	
	public BlockInput(String od)
	{
		this(MagistryAPI.getOreDictionaryStates(od));
	}
	
	public BlockInput(Block block)
	{
		this(MagistryAPI.getValidStates(block));
	}
	
	public BlockInput(IBlockState... states)
	{
		this.states = states;
	}
	
	public BlockInput withTester(Predicate<IBlockState> tester)
	{
		this.tester = tester;
		return this;
	}
	
	public BlockInput removeStates(Predicate<IBlockState> lf)
	{
		List<IBlockState> statesNew = Arrays.asList(states).stream().filter(lf.negate()).collect(Collectors.toList());
		return new BlockInput(statesNew.toArray(new IBlockState[statesNew.size()])).withTester(tester);
	}
	
	@Override
	public boolean test(IBlockState t)
	{
		for(IBlockState s : states)
			if(t.equals(s) && tester.test(t))
				return true;
		return false;
	}
}