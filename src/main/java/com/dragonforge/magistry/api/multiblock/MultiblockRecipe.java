package com.dragonforge.magistry.api.multiblock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.init.ItemsM;
import com.dragonforge.magistry.items.ItemMultiblockActivator;

import it.unimi.dsi.fastutil.longs.Long2ObjectArrayMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class MultiblockRecipe extends IForgeRegistryEntry.Impl<MultiblockRecipe>
{
	public final Long2ObjectMap<BlockComponent> components = new Long2ObjectArrayMap<>();
	private final List<MultiblockRecipe> childs = new ArrayList<>();
	
	public ItemMultiblockActivator activator = ItemsM.STARDUST;
	
	public boolean spawnOrbs, spawnZaps;
	private int sx, sy, sz;
	
	public String research;
	
	public static class PosMapBuilder<V>
	{
		final Map<BlockPos, V> components = new HashMap<>();
		
		public PosMapBuilder<V> put(int x, int y, int z, V v)
		{
			return put(new BlockPos(x, y, z), v);
		}
		
		public PosMapBuilder<V> put(BlockPos pos, V v)
		{
			components.put(pos, v);
			return this;
		}
		
		public PosMapBuilder<V> remove(int x, int y, int z)
		{
			components.remove(new BlockPos(x, y, z));
			return this;
		}
		
		public PosMapBuilder<V> transform(Function<BlockPos, BlockPos> mapper)
		{
			PosMapBuilder<V> tranformed = new PosMapBuilder<>();
			for(BlockPos pos : components.keySet())
				tranformed.components.put(mapper.apply(pos), components.get(pos));
			return tranformed;
		}
		
		public PosMapBuilder<V> rotate(Rotation rot)
		{
			if(rot == Rotation.NONE)
				return this;
			return transform(pos -> pos.rotate(rot)).normalize();
		}
		
		public Vec3i getNormalizationOffset()
		{
			int xm = 0, ym = 0, zm = 0;
			for(BlockPos p : components.keySet())
			{
				xm = Math.min(p.getX(), xm);
				ym = Math.min(p.getY(), ym);
				zm = Math.min(p.getZ(), zm);
			}
			return new Vec3i(xm, ym, zm);
		}
		
		public PosMapBuilder<V> normalize()
		{
			Vec3i vec = getNormalizationOffset();
			if(vec.equals(Vec3i.NULL_VECTOR))
				return transform(pos -> pos);
			return transform(pos -> pos.add(vec));
		}
		
		public Map<BlockPos, V> build()
		{
			return Collections.unmodifiableMap(components);
		}
	}
	
	public MultiblockRecipe(String name, Map<BlockPos, ? extends BlockComponent> components)
	{
		setId(name);
		for(BlockPos pos : components.keySet())
		{
			this.components.put(pos.toLong(), components.get(pos));
			sx = Math.max(sx, pos.getX());
			sy = Math.max(sy, pos.getY());
			sz = Math.max(sz, pos.getZ());
		}
	}
	
	public MultiblockRecipe setResearch(String research)
	{
		this.research = research;
		return this;
	}
	
	public MultiblockRecipe setActivator(ItemMultiblockActivator activator)
	{
		this.activator = activator;
		return this;
	}
	
	public MultiblockRecipe addChild(MultiblockRecipe child)
	{
		if(!this.childs.contains(child))
			this.childs.add(child);
		return this;
	}
	
	private MultiblockRecipe setId(String name)
	{
		String modid = Magistry.MOD_ID;
		
		ModContainer mc = Loader.instance().activeModContainer();
		if(mc != null)
			modid = mc.getModId();
		else
			Magistry.LOG.warn("UNABLE TO TRACK REGISTERING MOD ID FOR MULTIBLOCK NAME '" + name + "'! Assigning magistry mod id.");
		
		setRegistryName(modid, name);
		
		return this;
	}
	
	public MultiblockRecipe setFX(boolean orbs, boolean zaps)
	{
		this.spawnOrbs = orbs;
		this.spawnZaps = zaps;
		return this;
	}
	
	public MultiblockRecipe register()
	{
		MagistryAPI.MULTIBLOCK_REGISTRY.register(this);
		return this;
	}
	
	public int getWidth()
	{
		return sx + 1;
	}
	
	public int getHeight()
	{
		return sy + 1;
	}
	
	public int getDepth()
	{
		return sz + 1;
	}
	
	public String getResearch()
	{
		return research;
	}
	
	public BlockComponent getBlock(int x, int y, int z)
	{
		return components.get(new BlockPos(x, y, z).toLong());
	}
	
	public final int getMatcher(World world, BlockPos pos)
	{
		if(strictMatch(world, pos))
			return 0;
		for(int i = 0; i < childs.size(); ++i)
			if(childs.get(i).strictMatch(world, pos))
				return i + 1;
		return -1;
	}
	
	public final BlockPos startOffset(World world, BlockPos pos)
	{
		BlockPos off = null;
		if((off = strictStartOffset(world, pos)) != null)
			return off;
		for(int i = 0; i < childs.size(); ++i)
			if((off = childs.get(i).strictStartOffset(world, pos)) != null)
				return off;
		return null;
	}
	
	private boolean strictMatch(World world, BlockPos pos)
	{
		for(long offl : components.keySet())
		{
			BlockPos off = BlockPos.fromLong(offl);
			BlockPos npos = pos.add(off);
			if(!components.get(offl).matches(world, npos))
				return false;
		}
		return true;
	}
	
	private BlockPos strictStartOffset(World world, BlockPos pos)
	{
		for(long off : components.keySet())
		{
			BlockPos negp = BlockPos.fromLong(off);
			BlockPos npos = pos.add(-negp.getX(), -negp.getY(), -negp.getZ());
			if(strictMatch(world, npos))
				return npos;
		}
		return null;
	}
	
	public boolean isValid(World world, BlockPos pos, EntityPlayer user)
	{
		IPlayerKnowledge ipk = MagistryAPI.getPlayerKnowledge(user);
		return strictStartOffset(world, pos) != null && (getResearch() == null || (ipk != null && ipk.hasResearch(getResearch())));
	}
	
	public boolean convert(World world, BlockPos pos, EntityPlayer user)
	{
		BlockPos begin = startOffset(world, pos);
		if(begin == null)
			return false;
		for(long offl : components.keySet())
		{
			BlockPos off = BlockPos.fromLong(offl);
			BlockPos npos = begin.add(off);
			components.get(offl).output(world, npos);
		}
		return true;
	}
	
	public AxisAlignedBB getAABB(World world, BlockPos pos)
	{
		BlockPos begin = startOffset(world, pos);
		if(begin == null)
			return null;
		return new AxisAlignedBB(begin.getX(), begin.getY(), begin.getZ(), begin.getX() + getWidth(), begin.getY() + getHeight(), begin.getZ() + getDepth());
	}
}