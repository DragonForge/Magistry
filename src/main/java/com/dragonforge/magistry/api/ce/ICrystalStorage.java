package com.dragonforge.magistry.api.ce;

public interface ICrystalStorage
{
	boolean canAcceptCU();
	
	boolean canExtractCU();
	
	float getCUStored();
	
	float getCUCapacity();
	
	default void setCU(float cu)
	{
	}
	
	default float getRequestedCU()
	{
		return 0F;
	}
	
	default float supplyCU(float cu, boolean simulate)
	{
		return 0;
	}
	
	default float extractCU(float cu, boolean simulate)
	{
		return 0;
	}
}