package com.dragonforge.magistry.api.ce;

import net.minecraft.nbt.NBTTagCompound;

public class CrystalStorage implements ICrystalStorage
{
	protected float maxReceive, maxExtract;
	protected float capacity;
	protected float cu;
	
	protected float requestCU;
	
	public CrystalStorage(float capacity)
	{
		this(capacity, capacity);
	}
	
	public CrystalStorage(float capacity, float maxIO)
	{
		this(capacity, maxIO, maxIO);
	}
	
	public CrystalStorage(float capacity, float maxReceive, float maxExtract)
	{
		this(capacity, maxReceive, maxExtract, 0);
	}
	
	public CrystalStorage(float capacity, float maxReceive, float maxExtract, float cu)
	{
		this.capacity = capacity;
		this.maxReceive = maxReceive;
		this.maxExtract = maxExtract;
		this.cu = cu;
	}
	
	@Override
	public boolean canAcceptCU()
	{
		return maxReceive > 0;
	}
	
	@Override
	public float supplyCU(float cu, boolean simulate)
	{
		cu = Math.min(getCUCapacity() - getCUStored(), Math.min(maxReceive, cu));
		if(!simulate)
		{
			this.cu += cu;
			if(requestCU > 0)
				requestCU = Math.max(0, requestCU - cu);
		}
		return cu;
	}
	
	@Override
	public boolean canExtractCU()
	{
		return maxExtract > 0;
	}
	
	@Override
	public float extractCU(float cu, boolean simulate)
	{
		cu = Math.min(this.cu, Math.min(cu, maxExtract));
		if(!simulate)
			this.cu -= cu;
		return cu;
	}
	
	@Override
	public float getCUStored()
	{
		return cu;
	}
	
	@Override
	public float getCUCapacity()
	{
		return capacity;
	}
	
	@Override
	public void setCU(float cu)
	{
		this.cu = Math.max(0, Math.min(cu, capacity));
	}
	
	@Override
	public float getRequestedCU()
	{
		return requestCU;
	}
	
	public void requestCU(float cu)
	{
		requestCU += cu;
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		nbt.setFloat("CrystalEnergy", cu);
		return nbt;
	}
	
	public CrystalStorage readFromNBT(NBTTagCompound nbt)
	{
		cu = nbt.getFloat("CrystalEnergy");
		return this;
	}
}