package com.dragonforge.magistry.api.research;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ResourceLocation;

public class ResearchTag extends ResourceLocation
{
	public static final Map<String, ResearchTag> TAGS = new HashMap<>();
	
	public static final ResearchTag TAG_ALCHEMY = new ResearchTag(Magistry.MOD_ID, "alchemy").addReqKnowledge("s_alchemy");
	public static final ResearchTag TAG_CHAOS = new ResearchTag(Magistry.MOD_ID, "chaos").addReqKnowledge("wip");
	public static final ResearchTag TAG_CRYSTAL_ENERGY = new ResearchTag(Magistry.MOD_ID, "crystal_energy");
	public static final ResearchTag TAG_DEATH = new ResearchTag(Magistry.MOD_ID, "death").addReqKnowledge("s_undead");
	public static final ResearchTag TAG_ELECTRICAL_FIELD = new ResearchTag(Magistry.MOD_ID, "electrical_field");
	public static final ResearchTag TAG_ELECTRICITY = new ResearchTag(Magistry.MOD_ID, "electricity");
	public static final ResearchTag TAG_EQUILIBRIUM = new ResearchTag(Magistry.MOD_ID, "equilibrium").addReqKnowledge("wip");
	public static final ResearchTag TAG_ETHER = new ResearchTag(Magistry.MOD_ID, "ether").addReqKnowledge("s_ether");
	public static final ResearchTag TAG_FLORA = new ResearchTag(Magistry.MOD_ID, "flora").addReqKnowledge("s_flora");
	public static final ResearchTag TAG_FLUID = new ResearchTag(Magistry.MOD_ID, "fluid").addReqKnowledge("s_fluid");
	public static final ResearchTag TAG_HEAT = new ResearchTag(Magistry.MOD_ID, "heat").addReqKnowledge("s_heat");
	public static final ResearchTag TAG_ILLUSION = new ResearchTag(Magistry.MOD_ID, "illusion").addReqKnowledge("wip");
	public static final ResearchTag TAG_INFINITY = new ResearchTag(Magistry.MOD_ID, "infinity").addReqKnowledge("wip");
	public static final ResearchTag TAG_LIGHT = new ResearchTag(Magistry.MOD_ID, "light").addReqKnowledge("s_light");
	public static final ResearchTag TAG_MACHINERY = new ResearchTag(Magistry.MOD_ID, "machinery");
	public static final ResearchTag TAG_MATTER = new ResearchTag(Magistry.MOD_ID, "matter").addReqKnowledge("wip");
	public static final ResearchTag TAG_METAL = new ResearchTag(Magistry.MOD_ID, "metal").addReqKnowledge("s_any_ingot");
	public static final ResearchTag TAG_MINERALS = new ResearchTag(Magistry.MOD_ID, "minerals").addReqKnowledge("s_any_ore");
	public static final ResearchTag TAG_OPTIC = new ResearchTag(Magistry.MOD_ID, "optic").addReqKnowledge("s_crystal");
	public static final ResearchTag TAG_SPACE = new ResearchTag(Magistry.MOD_ID, "space").addReqKnowledge("wip");
	public static final ResearchTag TAG_TIME = new ResearchTag(Magistry.MOD_ID, "time").addReqKnowledge("s_clock");
	
	final ResourceLocation texture;
	final List<String> reqKnowledge = new ArrayList<>();
	
	public ResearchTag(String namespaceIn, String pathIn)
	{
		super(namespaceIn, pathIn);
		this.texture = new ResourceLocation(namespaceIn, "textures/research_tags/" + pathIn + ".png");
		if(TAGS.containsKey(toString()))
			throw new IllegalArgumentException("Duplicate research tag: \"" + toString() + "\"");
		TAGS.put(toString(), this);
	}
	
	public ResourceLocation getTexture()
	{
		return this.texture;
	}
	
	public ResearchTag addReqKnowledge(ResearchBase research)
	{
		return addReqKnowledge(research.getRegistryName());
	}
	
	public ResearchTag addReqKnowledge(ResourceLocation research)
	{
		return addReqKnowledge(research.toString());
	}
	
	public ResearchTag addReqKnowledge(String research)
	{
		if(research != null && !research.isEmpty() && !reqKnowledge.contains(research))
			reqKnowledge.add(research);
		return this;
	}
	
	public boolean isSelectable(EntityPlayerMP player)
	{
		return isSelectable(MagistryAPI.getPlayerKnowledge(player));
	}
	
	public boolean isSelectable(IPlayerKnowledge know)
	{
		if(reqKnowledge.isEmpty())
			return true;
		if(know == null)
			return false;
		for(String v : reqKnowledge)
			if(!know.hasResearch(v))
				return false;
		return true;
	}
	
	@Override
	public final String toString()
	{
		return super.toString();
	}
}