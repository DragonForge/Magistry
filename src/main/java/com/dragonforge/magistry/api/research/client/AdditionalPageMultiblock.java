package com.dragonforge.magistry.api.research.client;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Rectangle;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.multiblock.BlockComponent;
import com.dragonforge.magistry.api.multiblock.MultiblockRecipe;
import com.dragonforge.magistry.client.MagistryFonts;
import com.dragonforge.magistry.init.SoundsM;
import com.dragonforge.magistry.utils.PosHelpers;
import com.zeitheron.hammercore.client.render.world.VirtualWorld;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.lib.zlib.utils.Joiner;
import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AdditionalPageMultiblock implements IAdditionalPage
{
	final ResourceLocation[] multiblocks;
	final MultiblockRecipe[] multiblockRecipe;
	
	VirtualWorld world = new VirtualWorld();
	
	float angleX, angleY;
	float scale = 1F;
	int layer = 0;
	
	boolean forceAssembledView = false;
	
	public AdditionalPageMultiblock(ResourceLocation... multiblock)
	{
		this.multiblocks = multiblock;
		this.multiblockRecipe = new MultiblockRecipe[multiblock.length];
	}
	
	public MultiblockRecipe getMultiblock()
	{
		int index = (int) (System.currentTimeMillis() % (1000L * multiblocks.length) / 1000L);
		ResourceLocation multiblock = multiblocks[index];
		if(multiblockRecipe[index] == null)
			multiblockRecipe[index] = MagistryAPI.MULTIBLOCK_REGISTRY.getValue(multiblock);
		return multiblockRecipe[index];
	}
	
	@Override
	public void onAppear()
	{
		angleX = 0;
		angleY = 0;
		scale = 1F;
	}
	
	@SideOnly(Side.CLIENT)
	public void build()
	{
		world.clear();
		
		MultiblockRecipe r = getMultiblock();
		if(r != null)
		{
			boolean shift = forceAssembledView || GuiScreen.isShiftKeyDown();
			
			Vec3i norm = PosHelpers.getHalfNormalized(r.components.keySet());
			
			int my = 0;
			
			for(long key : r.components.keySet())
			{
				BlockComponent comp = r.components.get(key);
				int variants = comp.input.states.length;
				IBlockState state = comp.input.states[(int) ((System.currentTimeMillis() % (1000L * variants)) / 1000L)];
				BlockPos pos = BlockPos.fromLong(key);
				
				my = Math.max(my, pos.getY());
				
				if(!shift && layer > 0 && pos.getY() > layer - 1)
					continue;
				
				if(shift)
					state = comp.output.apply(state);
				
				pos = pos.add(norm);
				world.setBlockState(pos, state);
			}
			
			layer = Math.min(layer, my + 1);
		}
	}
	
	java.awt.Rectangle question = new java.awt.Rectangle();
	java.awt.Rectangle toggleAssembledView = new java.awt.Rectangle();
	
	java.awt.Rectangle arrowUp = new java.awt.Rectangle();
	java.awt.Rectangle arrowDown = new java.awt.Rectangle();
	
	@Override
	@SideOnly(Side.CLIENT)
	public void renderPage(float partialTicks, int relMouseX, int relMouseY, int width, int height, int page)
	{
		build();
		
		if(Mouse.isButtonDown(0) && relMouseX > 0 && relMouseY > 0 && relMouseX < width && relMouseY < height)
		{
			int dx = Mouse.getDX();
			int dy = Mouse.getDY();
			
			if(dx != 0)
			{
				angleY += dx / 1.5F;
				while(angleY < 0)
					angleY += 360;
				angleY %= 360F;
			}
			
			if(dy != 0)
			{
				angleX += dy / 1.5F;
				angleX = Math.max(-60, Math.min(120, angleX));
			}
		}
		
		String text = I18n.format("multiblock." + getMultiblock().getRegistryName() + ".name");
		int tw = MagistryFonts.PARCHMENT.getWidth(text);
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(width / 2F, 0, 0);
		GlStateManager.scale(1.5F, 1.5F, 1.5F);
		MagistryFonts.PARCHMENT.renderWithShadow(text, -tw / 2, 0, 0xFFFFFFFF);
		GlStateManager.popMatrix();
		
		text = "?";
		tw = MagistryFonts.PARCHMENT.getWidth(text);
		
		question.setBounds((int) (width / 1.25F - tw / 2 * 1.5F), (int) (12 * 1.5F), (int) (tw * 1.5F + 2), (int) (MagistryFonts.PARCHMENT.getHeight() * 1.5F));
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(width / 1.25F, 0, 0);
		GlStateManager.scale(1.5F, 1.5F, 1.5F);
		MagistryFonts.PARCHMENT.renderWithShadow(text, -tw / 2, 12, 0xFFB6B69C);
		GlStateManager.popMatrix();
		
		UtilsFX.bindTexture("textures/gui/gui_manual_overlay.png");
		GlStateManager.pushMatrix();
		GlStateManager.translate(width / 2 - 15.5F, 14.5F, 0);
		GlStateManager.scale(2, 2, 2);
		GlStateManager.color(1, 1, 1, 1);
		RenderUtil.drawTexturedModalRect(0, 0, 20, 3, 16, 16);
		GlStateManager.popMatrix();
		
		renderStack(getMultiblock().activator.getDefaultInstance(), width / 2 - 11, 18, 24, 24);
		
		GlStateManager.enableDepth();
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(width / 14, height / 7, 200);
		renderVirtualWorld(world, Minecraft.getMinecraft(), new Rectangle(0, 0, width, height), -30F + angleX, 45F + angleY, 800);
		GlStateManager.popMatrix();
		
		UtilsFX.bindTexture(Magistry.MOD_ID, "textures/gui/overlays.png");
		
		toggleAssembledView.setBounds(width / 2 - 36, 23, 16, 16);
		
		ColorHelper.glColor1ia(toggleAssembledView.contains(mouse) ? 0xBB000000 : 0x88000000);
		RenderUtil.drawTexturedModalRect(width / 2 - 36, 23, 118, GuiScreen.isShiftKeyDown() || forceAssembledView ? 16 : 0, 16, 16);
		
		arrowUp.setBounds(width / 2 + 20, 14, 16, 16);
		
		ColorHelper.glColor1ia(arrowUp.contains(mouse) ? 0xBB000000 : 0x88000000);
		RenderUtil.drawTexturedModalRect(width / 2 + 20, 14, 102, 16, 16, 16);
		
		arrowDown.setBounds(width / 2 + 20, 32, 16, 16);
		
		ColorHelper.glColor1ia(arrowDown.contains(mouse) ? 0xBB000000 : 0x88000000);
		RenderUtil.drawTexturedModalRect(width / 2 + 20, 32, 102, 0, 16, 16);
		
		ColorHelper.glColor1ia(0xFFFFFFFF);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(List<String> tooltip, int relMouseX, int relMouseY, int page)
	{
		IAdditionalPage.super.addInformation(tooltip, relMouseX, relMouseY, page);
		
		if(question.contains(relMouseX, relMouseY))
		{
			Map<String, Integer> items = new HashMap<>();
			
			MultiblockRecipe r = getMultiblock();
			if(r != null)
			{
				for(BlockComponent comp : r.components.values())
				{
					IBlockState state = comp.input.states[(int) ((System.currentTimeMillis() % (1000L * comp.input.states.length)) / 1000L)];
					Item i = Item.getItemFromBlock(state.getBlock());
					ItemStack stack = state.getBlock().getPickBlock(state, new RayTraceResult(Type.MISS, Vec3d.ZERO, null, null), Minecraft.getMinecraft().world, Minecraft.getMinecraft().player.getPosition(), Minecraft.getMinecraft().player);
					String n = stack.getDisplayName();
					if(items.containsKey(n))
						items.put(n, items.get(n) + 1);
					else
						items.put(n, 1);
				}
			}
			
			if(!items.isEmpty())
			{
				String tip = I18n.format("tooltip." + Magistry.MOD_ID + ".materials") + ":\n- " + Joiner.on("\n- ").join(items.entrySet().stream().map(e -> e.getValue() + "x " + e.getKey()).collect(Collectors.toList()));
				tooltip.addAll(Arrays.asList(Joiner.NEW_LINE.split(tip)));
			}
		}
		
		if(toggleAssembledView.contains(relMouseX, relMouseY))
		{
			tooltip.add(I18n.format("tooltip." + Magistry.MOD_ID + ".togglembds"));
		}
		
		if(arrowUp.contains(relMouseX, relMouseY))
		{
			tooltip.add(I18n.format("tooltip." + Magistry.MOD_ID + ".addmblayer"));
		}
		
		if(arrowDown.contains(relMouseX, relMouseY))
		{
			tooltip.add(I18n.format("tooltip." + Magistry.MOD_ID + ".slicemblayer"));
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void keyTyped(char typedChar, int keyCode)
	{
		if(!forceAssembledView && !GuiScreen.isShiftKeyDown())
			if(keyCode == Keyboard.KEY_UP)
			{
				++layer;
				Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
			} else if(keyCode == Keyboard.KEY_DOWN)
			{
				if(layer > 0)
					--layer;
				Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
			}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void mouseClicked(int relMouseX, int relMouseY, int mouseButton, int page)
	{
		if(toggleAssembledView.contains(mouse) && !GuiScreen.isShiftKeyDown())
		{
			forceAssembledView = !forceAssembledView;
			Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
		}
		
		if(arrowUp.contains(mouse) && !forceAssembledView && !GuiScreen.isShiftKeyDown())
		{
			++layer;
			Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
		}
		
		if(arrowDown.contains(mouse) && !forceAssembledView && !GuiScreen.isShiftKeyDown())
		{
			if(layer > 0)
				--layer;
			Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
		}
	}
	
	public static void renderVirtualWorld(VirtualWorld world, Minecraft mc, Rectangle panel, float rotX, float rotY, float zoom)
	{
		GlStateManager.enableDepth();
		boolean shouldCut = (panel.getHeight() == 0) && (panel.getWidth() == 0);
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(panel.getX() + panel.getWidth() / 2, panel.getY() + panel.getHeight() / 2, 10.0F);
		
		double sc = Math.sqrt(zoom + 99.0D) - 9.0D;
		GlStateManager.scale(-sc, -sc, -sc);
		
		GlStateManager.translate(.5D, .5D, .5D);
		GlStateManager.rotate(rotX, 1.0F, 0.0F, 0.0F);
		GlStateManager.rotate(rotY, 0.0F, 1.0F, 0.0F);
		GlStateManager.translate(-.5D, -.5D, -.5D);
		
		BlockRendererDispatcher brd = mc.getBlockRendererDispatcher();
		Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		BufferBuilder vb = Tessellator.getInstance().getBuffer();
		vb.begin(GL11.GL_QUADS, DefaultVertexFormats.BLOCK);
		
		for(BlockPos pos : world.getAllPlacedStatePositions())
			brd.renderBlock(world.getBlockState(pos), pos, world, vb);
		
		Tessellator.getInstance().draw();
		
		float p = Minecraft.getMinecraft().getRenderPartialTicks();
		for(BlockPos pos : world.tiles.toKeyArray())
			TileEntityRendererDispatcher.instance.render(world.getTileEntity(pos), pos.getX(), pos.getY(), pos.getZ(), p);
		
		GlStateManager.popMatrix();
		GlStateManager.disableDepth();
	}
}