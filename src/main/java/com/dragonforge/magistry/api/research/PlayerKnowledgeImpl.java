package com.dragonforge.magistry.api.research;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.items.ItemLostNotes;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.Constants.NBT;

public class PlayerKnowledgeImpl implements IPlayerKnowledge
{
	private static final ExecutorService IO_EXECUTOR = Executors.newFixedThreadPool(1);
	
	protected final List<String> unlocked = new ArrayList<>();
	protected NBTTagCompound customData = new NBTTagCompound();
	
	public WeakReference<EntityPlayer> owner = new WeakReference<>(null);
	
	@Override
	public NBTTagCompound serializeNBT()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setTag("AdditionalData", getAdditionalData());
		NBTTagList tl = new NBTTagList();
		for(String s : unlocked)
			tl.appendTag(new NBTTagString(s));
		nbt.setTag("Researches", tl);
		return nbt;
	}
	
	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		customData = nbt.getCompoundTag("AdditionalData");
		
		NBTTagList tl = nbt.getTagList("Researches", NBT.TAG_STRING);
		
		unlocked.clear();
		for(int i = 0; i < tl.tagCount(); ++i)
			unlocked.add(tl.getStringTagAt(i));
	}
	
	@Override
	public boolean hasResearch(String research)
	{
		return unlocked.contains(research);
	}
	
	@Override
	public boolean unlockResearch(String research, boolean force)
	{
		if(unlocked.contains(research))
			return false;
		if(force || canBeginResearch(research))
		{
			boolean b = unlocked.add(research);
			if(force)
			{
				ResearchBase res = MagistryAPI.getResearch(research);
				if(res != null && res.getParents() != null)
					for(ResourceLocation parent : res.getParents())
						unlockResearch(parent.toString(), true);
			}
			if(b)
				sync(null);
			save();
			return b;
		}
		return false;
	}
	
	@Override
	public boolean canBeginResearch(String research)
	{
		ResearchBase rb = MagistryAPI.getResearch(research);
		if(rb != null)
		{
			ResourceLocation[] parents = rb.getParents();
			if(parents != null)
			{
				for(ResourceLocation p : parents)
					if(!hasResearch(p.toString()))
						return false;
					else
						continue;
				return true;
			}
			else
				return true;
		}
		return false;
	}
	
	@Override
	public void unlockAndGiveNotes(String research)
	{
		if(unlocked.contains(research))
			return;
		if(unlockResearch(research, true) && getOwner().isServerWorld())
		{
			ItemStack note = ItemLostNotes.createForResearch(new ResourceLocation(research));
			if(!getOwner().addItemStackToInventory(note))
				WorldUtil.spawnItemStack(getOwner().world, getOwner().getPosition(), note);
		}
	}
	
	@Override
	public void resetKnowledge()
	{
		unlocked.clear();
		save();
	}
	
	@Override
	public List<String> getListOfKnownResearches()
	{
		return unlocked;
	}
	
	@Override
	public EntityPlayer getOwner()
	{
		return owner.get();
	}
	
	@Override
	public void sync(EntityPlayerMP player)
	{
		if(player == null && getOwner() instanceof EntityPlayerMP)
			player = (EntityPlayerMP) getOwner();
		if(player == null)
			return;
		if(getOwner() == null || (getOwner().getGameProfile().getId().equals(player.getGameProfile().getId())))
			MagistryAPI.network.sendResearches(this, player);
	}
	
	@Override
	public NBTTagCompound getAdditionalData()
	{
		if(customData == null)
			customData = new NBTTagCompound();
		return customData;
	}
	
	boolean saveDirty = false;
	
	@Override
	public void save()
	{
		saveDirty = true;
		IO_EXECUTOR.submit(() ->
		{
			if(saveDirty)
			{
				saveDirty = false;
				PlayerDataIO.save(WorldUtil.cast(getOwner(), EntityPlayerMP.class), serializeNBT());
			}
		});
	}
}