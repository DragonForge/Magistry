package com.dragonforge.magistry.api.research.scan;

import com.dragonforge.magistry.api.research.IPlayerKnowledge;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;

public class ScanFluid implements IScanObject
{
	final Fluid block;
	final FluidStack state;
	final String research;
	
	public ScanFluid(Fluid block, String research)
	{
		this.block = block;
		this.state = null;
		this.research = research;
	}
	
	public ScanFluid(FluidStack state, String research)
	{
		this.block = null;
		this.state = state;
		this.research = research;
	}
	
	@Override
	public boolean isThisObject(EntityPlayerMP player, IPlayerKnowledge knowledge, Object v)
	{
		if(v instanceof FluidStack)
		{
			if(this.state != null && this.state.equals(v))
				return true;
			if(this.block != null && this.block == ((FluidStack) v).getFluid())
				return true;
		}
		if(v instanceof Fluid)
		{
			if(this.state != null && this.state.getFluid() == v)
				return true;
			if(this.block != null && this.block == v)
				return true;
		}
		return false;
	}
	
	@Override
	public String getScanKey(EntityPlayerMP player, IPlayerKnowledge knowledge)
	{
		return research;
	}
}