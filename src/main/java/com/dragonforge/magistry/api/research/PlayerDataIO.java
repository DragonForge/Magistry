package com.dragonforge.magistry.api.research;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.dragonforge.magistry.Magistry;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;

public class PlayerDataIO
{
	static void save(EntityPlayerMP mp, NBTTagCompound data)
	{
		if(mp == null || mp.getGameProfile() == null)
			return;
		File base = new File(mp.getServer().worlds[0].getSaveHandler().getWorldDirectory(), "magistry-player-data");
		if(!base.isDirectory())
			base.mkdirs();
		try(FileOutputStream out = new FileOutputStream(new File(base, mp.getGameProfile().getId() + ".dat")))
		{
			CompressedStreamTools.writeCompressed(data, out);
		} catch(IOException ioe)
		{
			Magistry.LOG.error("Failed to save Magistry data for player " + mp + "!", ioe);
		}
	}
	
	public static NBTTagCompound read(EntityPlayerMP mp)
	{
		if(mp == null || mp.getGameProfile() == null)
			return null;
		File base = new File(mp.getServer().worlds[0].getSaveHandler().getWorldDirectory(), "magistry-player-data");
		if(!base.isDirectory())
			base.mkdirs();
		base = new File(base, mp.getGameProfile().getId() + ".dat");
		if(base.isFile())
			try(FileInputStream in = new FileInputStream(base))
			{
				return CompressedStreamTools.readCompressed(in);
			} catch(IOException ioe)
			{
				Magistry.LOG.error("Failed to load Magistry data for player " + mp + "!", ioe);
			}
		return new NBTTagCompound();
	}
}