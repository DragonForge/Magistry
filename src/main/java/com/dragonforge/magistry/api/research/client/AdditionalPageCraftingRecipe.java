package com.dragonforge.magistry.api.research.client;

import java.util.Arrays;
import java.util.List;

import org.lwjgl.opengl.GL11;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.crafting.RecipeArrayList;
import com.dragonforge.magistry.client.MagistryFonts;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.client.utils.texture.gui.theme.GuiTheme;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammercore.utils.ItemStackUtil;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.util.NonNullList;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class AdditionalPageCraftingRecipe implements IAdditionalPage
{
	public final List<IRecipe> recipes;
	
	public AdditionalPageCraftingRecipe(IRecipe... recipes)
	{
		this.recipes = Arrays.asList(recipes);
	}
	
	public AdditionalPageCraftingRecipe(ItemStack output)
	{
		this.recipes = new RecipeArrayList(output);
	}
	
	public void refresh()
	{
		if(this.recipes instanceof RecipeArrayList)
			((RecipeArrayList) this.recipes).refresh();
	}
	
	public IRecipe getCurrentRecipe()
	{
		if(recipes.isEmpty())
			refresh();
		return recipes.isEmpty() ? null : recipes.get((int) (System.currentTimeMillis() % (1000L * recipes.size()) / 1000L));
	}
	
	@Override
	public void renderPage(float partialTicks, int relMouseX, int relMouseY, int width, int height, int page)
	{
		IRecipe tr = getCurrentRecipe();
		IRecipe recipe = null;
		if(tr instanceof ShapedRecipes)
			recipe = (ShapedRecipes) tr;
		else if(tr instanceof ShapelessRecipes)
			recipe = (ShapelessRecipes) tr;
		else if(tr instanceof ShapedOreRecipe)
			recipe = (ShapedOreRecipe) tr;
		else if(tr instanceof ShapelessOreRecipe)
			recipe = (ShapelessOreRecipe) tr;
		if(recipe == null)
			return;
		
		int start = width / 2 - 52;
		int y = 0;
		
		String text;
		int offset;
		
		GL11.glPushMatrix();
		GL11.glPushMatrix();
		GL11.glEnable(3042);
		GL11.glTranslatef(start, 0, 0.0f);
		GL11.glScalef(2.0f, 2.0f, 1.0f);
		GlStateManager.enableAlpha();
		GlStateManager.enableBlend();
		UtilsFX.bindTexture("textures/gui/gui_manual_overlay.png");
		GL11.glColor4f(1, 1, 1, 1);
		
		RenderUtil.drawTexturedModalRect(2, 32, 60, 15, 52, 52);
		RenderUtil.drawTexturedModalRect(20, 12, 20, 3, 16, 16);
		
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glTranslated(0.0, 0.0, 100.0);
		GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		RenderHelper.enableGUIStandardItemLighting();
		GL11.glEnable(2884);
		renderStack(ItemStackUtil.cycleItemStack(recipe.getRecipeOutput()), 48 + start, 32 + y);
		RenderHelper.enableGUIStandardItemLighting();
		GL11.glEnable(2896);
		GL11.glPopMatrix();
		
		NonNullList<Ingredient> items = recipe.getIngredients();
		
		if(recipe != null && (recipe instanceof ShapedRecipes || recipe instanceof ShapedOreRecipe))
		{
			int i;
			int j;
			text = I18n.format("recipe." + Magistry.MOD_ID + ".crafting");
			offset = MagistryFonts.PARCHMENT.getWidth(text);
			GlStateManager.pushMatrix();
			GlStateManager.translate(start + 56, y, 0);
			GlStateManager.scale(1.5F, 1.5F, 1.5F);
			RenderHelper.disableStandardItemLighting();
			MagistryFonts.PARCHMENT.renderWithShadow(text, -offset / 2, 0, 0xFFFFFFFF);
			GlStateManager.popMatrix();
			int rw = 0;
			int rh = 0;
			
			if(recipe instanceof ShapedRecipes)
			{
				rw = ((ShapedRecipes) recipe).getWidth();
				rh = ((ShapedRecipes) recipe).getHeight();
				items = recipe.getIngredients();
			} else
			{
				rw = ((ShapedOreRecipe) recipe).getWidth();
				rh = ((ShapedOreRecipe) recipe).getHeight();
				items = ((ShapedOreRecipe) recipe).getIngredients();
			}
			for(i = 0; i < rw && i < 3; ++i)
			{
				for(j = 0; j < rh && j < 3; ++j)
				{
					ItemStack stack = ItemStackUtil.cycleItemStack(items.get(i + j * rw));
					if(InterItemStack.isStackNull(stack))
						continue;
					GL11.glPushMatrix();
					GL11.glColor4f(1, 1, 1, 1);
					RenderHelper.enableGUIStandardItemLighting();
					GL11.glEnable(2884);
					GL11.glTranslated(0, 0, 100);
					renderStack(stack, start + 12 + i * 32, y + 72 + j * 32, 24, 24);
					RenderHelper.enableGUIStandardItemLighting();
					GL11.glEnable(2896);
					GL11.glPopMatrix();
				}
			}
		}
		
		if(recipe != null && (recipe instanceof ShapelessRecipes || recipe instanceof ShapelessOreRecipe))
		{
			int i;
			text = I18n.format("recipe." + Magistry.MOD_ID + ".crafting.shapeless");
			offset = MagistryFonts.PARCHMENT.getWidth(text);
			GlStateManager.pushMatrix();
			GlStateManager.translate(start + 56, y, 0);
			GlStateManager.scale(1.5F, 1.5F, 1.5F);
			RenderHelper.disableStandardItemLighting();
			MagistryFonts.PARCHMENT.renderWithShadow(text, -offset / 2, 0, 0xFFFFFFFF);
			GlStateManager.popMatrix();
			for(i = 0; i < items.size() && i < 9; ++i)
			{
				if(items.get(i) == null)
					continue;
				GL11.glPushMatrix();
				GL11.glColor4f(1, 1, 1, 1);
				RenderHelper.enableGUIStandardItemLighting();
				GL11.glEnable(2884);
				GL11.glTranslated(0, 0, 100);
				renderStack(ItemStackUtil.cycleItemStack(items.get(i)), start + 12 + i % 3 * 32, y + 72 + i / 3 * 32, 24, 24);
				RenderHelper.enableGUIStandardItemLighting();
				GL11.glEnable(2896);
				GL11.glPopMatrix();
			}
		}
		GL11.glPopMatrix();
		
		RenderHelper.disableStandardItemLighting();
	}
}