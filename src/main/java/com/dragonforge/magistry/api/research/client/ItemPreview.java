package com.dragonforge.magistry.api.research.client;

import com.zeitheron.hammercore.bookAPI.fancy.GuiManualRecipe;
import com.zeitheron.hammercore.utils.InventoryUtils;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;

public class ItemPreview implements IPreviewIcon
{
	public final Object obj;
	
	public ItemPreview(Object obj)
	{
		this.obj = obj;
	}
	
	@Override
	public void renderPreview()
	{
		ItemStack convert = InventoryUtils.cycleItemStack(obj);
		Minecraft.getMinecraft().getRenderItem().renderItemAndEffectIntoGUI(convert, 0, 0);
	}
}