package com.dragonforge.magistry.api.research.scan;

import java.lang.ref.WeakReference;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.PlayerDataIO;
import com.dragonforge.magistry.api.research.PlayerKnowledgeImpl;
import com.dragonforge.magistry.api.research.PlayerKnowledgeProvider;

import net.minecraft.entity.player.EntityPlayerMP;

public interface IScanObject
{
	boolean isThisObject(EntityPlayerMP player, IPlayerKnowledge knowledge, Object v);
	
	String getScanKey(EntityPlayerMP player, IPlayerKnowledge knowledge);
	
	default boolean doScan(EntityPlayerMP player, IPlayerKnowledge knowledge)
	{
		String key = getScanKey(player, knowledge);
		if(key != null && knowledge != null)
			return knowledge.unlockResearch(key, true);
		return false;
	}
}