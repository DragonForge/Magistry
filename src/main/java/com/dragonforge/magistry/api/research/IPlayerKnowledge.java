package com.dragonforge.magistry.api.research;

import java.util.List;
import java.util.stream.Collectors;

import com.dragonforge.magistry.api.MagistryAPI;
import com.google.common.base.Predicates;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.INBTSerializable;

public interface IPlayerKnowledge extends INBTSerializable<NBTTagCompound>
{
	EntityPlayer getOwner();
	
	NBTTagCompound getAdditionalData();
	
	boolean hasResearch(String research);
	
	boolean unlockResearch(String research, boolean force);
	
	void unlockAndGiveNotes(String research);
	
	boolean canBeginResearch(String research);
	
	void resetKnowledge();
	
	void sync(EntityPlayerMP player);
	
	void save();
	
	default void unlockResearches(List<String> researches, boolean force)
	{
		for(String res : researches)
			unlockResearch(res, force);
	}
	
	List<String> getListOfKnownResearches();
	
	default List<ResearchBase> getListOfKnownResearchValues()
	{
		return getListOfKnownResearches().stream().map(ResourceLocation::new).map(MagistryAPI.RESEARCH_REGISTRY::getValue).filter(Predicates.notNull()).collect(Collectors.toList());
	}
}