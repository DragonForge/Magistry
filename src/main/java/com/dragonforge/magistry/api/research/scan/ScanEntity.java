package com.dragonforge.magistry.api.research.scan;

import java.util.function.Predicate;

import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.google.common.base.Predicates;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;

public class ScanEntity<T extends EntityLivingBase> implements IScanObject
{
	final Class<T> ent;
	final String research;
	EntityPredicate<T> acceptor;
	
	public ScanEntity(Class<T> ent, String research)
	{
		this.ent = ent;
		this.research = research;
	}
	
	public ScanEntity<T> setAcceptor(EntityPredicate<T> acceptor)
	{
		this.acceptor = acceptor;
		return this;
	}
	
	@Override
	public boolean isThisObject(EntityPlayerMP player, IPlayerKnowledge knowledge, Object v)
	{
		return v != null && ent.isAssignableFrom(v.getClass()) ? this.acceptor.test((T) v) : false;
	}
	
	@Override
	public String getScanKey(EntityPlayerMP player, IPlayerKnowledge knowledge)
	{
		return research;
	}
	
	public interface EntityPredicate<E extends EntityLivingBase>
	{
		boolean test(E ent);
	}
}