package com.dragonforge.magistry.api.research.client;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface IPreviewIcon
{
	@SideOnly(Side.CLIENT)
	void renderPreview();
}