package com.dragonforge.magistry.api.research.scan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;

import net.minecraft.entity.player.EntityPlayerMP;

public class ScanRegistry
{
	private static final List<IScanObject> SCAN_OBJECTS = new ArrayList<>();
	
	public static void register(IScanObject scan)
	{
		if(!SCAN_OBJECTS.contains(scan))
			SCAN_OBJECTS.add(scan);
	}
	
	@Nullable
	public static List<IScanObject> getScansByThing(EntityPlayerMP forPlayer, Object obj)
	{
		if(forPlayer == null)
			return Collections.emptyList();
		IPlayerKnowledge know = MagistryAPI.getPlayerKnowledge(forPlayer);
		if(know != null)
			return SCAN_OBJECTS.stream().filter(scan -> scan.isThisObject(forPlayer, know, obj)).collect(Collectors.toList());
		return Collections.emptyList();
	}
	
	public static boolean doScan(EntityPlayerMP player, Object obj)
	{
		return getScansByThing(player, obj).stream().filter(scan -> scan.doScan(player, MagistryAPI.getPlayerKnowledge(player))).findAny().isPresent();
	}
}