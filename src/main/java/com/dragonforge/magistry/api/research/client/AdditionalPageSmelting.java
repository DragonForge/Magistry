package com.dragonforge.magistry.api.research.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.lwjgl.opengl.GL11;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.client.MagistryFonts;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.utils.ItemStackUtil;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.Ingredient;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AdditionalPageSmelting implements IAdditionalPage
{
	public final ItemStack output;
	private Ingredient input;
	
	public AdditionalPageSmelting(ItemStack output)
	{
		this.output = output;
	}
	
	public ItemStack getInput()
	{
		if(input == null)
		{
			List<ItemStack> possible = new ArrayList<>();
			for(Entry<ItemStack, ItemStack> entry : FurnaceRecipes.instance().getSmeltingList().entrySet())
				if(entry.getValue().isItemEqual(output))
					possible.add(entry.getKey().copy());
			input = Ingredient.fromStacks(possible.toArray(new ItemStack[possible.size()]));
		}
		return ItemStackUtil.cycleItemStack(input);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void renderPage(float partialTicks, int relMouseX, int relMouseY, int width, int height, int page)
	{
		UtilsFX.bindTexture("textures/gui/gui_manual_overlay.png");
		
		GL11.glPushMatrix();
		{
			GL11.glPushMatrix();
			GL11.glColor4f(1, 1, 1, 1);
			GL11.glEnable(3042);
			GL11.glTranslatef(0, -16, 0);
			GL11.glTranslatef(width / 2 - 56 * 3 / 2, height / 2 - 64 * 3 / 2, 0);
			GL11.glScalef(3, 3, 1);
			RenderUtil.drawTexturedModalRect(0, 0, 0, 192, 56, 64);
			GL11.glPopMatrix();
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(width / 2 - 24F, height / 2 + 44F, 0);
			GlStateManager.scale(3, 3, 3);
			RenderUtil.drawTexturedModalRect(0, 0, 20, 3, 16, 16);
			GlStateManager.popMatrix();
			
			renderStack(getInput(), width / 2 - 16, height / 2 - 20, 32, 32);
			renderStack(output, width / 2 - 16, height / 2 + 52, 32, 32);
		}
		GL11.glPopMatrix();
		
		String txt = I18n.format("recipe." + Magistry.MOD_ID + ".smelting");
		GlStateManager.pushMatrix();
		GlStateManager.translate((width - MagistryFonts.PARCHMENT.getWidth(txt) * 2) / 2, 4, 0);
		GlStateManager.scale(2, 2, 2);
		MagistryFonts.PARCHMENT.renderWithShadow(txt, 0, 0, 0xFFFFFFFF);
		GlStateManager.popMatrix();
	}
}