package com.dragonforge.magistry.api.research.scan;

import com.dragonforge.magistry.api.research.IPlayerKnowledge;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;

public class ScanBlock implements IScanObject
{
	final Block block;
	final IBlockState state;
	final String research;
	
	public ScanBlock(Block block, String research)
	{
		this.block = block;
		this.state = null;
		this.research = research;
	}
	
	public ScanBlock(IBlockState state, String research)
	{
		this.block = null;
		this.state = state;
		this.research = research;
	}
	
	@Override
	public boolean isThisObject(EntityPlayerMP player, IPlayerKnowledge knowledge, Object v)
	{
		if(v instanceof IBlockState)
		{
			if(this.state != null && this.state.equals(v))
				return true;
			if(this.block != null && this.block == ((IBlockState) v).getBlock())
				return true;
		}
		if(v instanceof Block)
		{
			if(this.state != null && this.state.getBlock() == v)
				return true;
			if(this.block != null && this.block == v)
				return true;
		}
		return false;
	}
	
	@Override
	public String getScanKey(EntityPlayerMP player, IPlayerKnowledge knowledge)
	{
		return research;
	}
}