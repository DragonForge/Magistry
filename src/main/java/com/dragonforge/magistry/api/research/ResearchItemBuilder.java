package com.dragonforge.magistry.api.research;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class ResearchItemBuilder
{
	NonNullList<ItemStack> ingredients = NonNullList.create();
	
	ResearchItemBuilder()
	{
	}
	
	public static ResearchItemBuilder start()
	{
		return new ResearchItemBuilder();
	}
	
	public ResearchItemBuilder add(Item input)
	{
		ingredients.add(new ItemStack(input));
		return this;
	}
	
	public ResearchItemBuilder add(int count, Item input)
	{
		ingredients.add(new ItemStack(input, count));
		return this;
	}
	
	public ResearchItemBuilder add(ItemStack input)
	{
		ingredients.add(input.copy());
		return this;
	}
}