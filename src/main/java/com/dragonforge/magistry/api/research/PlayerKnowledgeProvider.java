package com.dragonforge.magistry.api.research;

import java.lang.ref.WeakReference;

import com.dragonforge.magistry.api.MagistryAPI;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;

public class PlayerKnowledgeProvider implements ICapabilityProvider, INBTSerializable<NBTTagCompound>
{
	private final PlayerKnowledgeImpl know = new PlayerKnowledgeImpl();
	
	public PlayerKnowledgeProvider(EntityPlayer player)
	{
		know.owner = new WeakReference<>(player);
		NBTTagCompound nbt = PlayerDataIO.read(WorldUtil.cast(player, EntityPlayerMP.class));
		if(nbt != null)
			know.deserializeNBT(nbt);
	}
	
	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		nbt = PlayerDataIO.read(WorldUtil.cast(know.getOwner(), EntityPlayerMP.class));
		if(nbt != null)
			know.deserializeNBT(nbt);
	}
	
	@Override
	public NBTTagCompound serializeNBT()
	{
		PlayerDataIO.save(WorldUtil.cast(know.getOwner(), EntityPlayerMP.class), know.serializeNBT());
		return new NBTTagCompound();
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == MagistryAPI.CAPABILITY_KNOWLEDGE;
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == MagistryAPI.CAPABILITY_KNOWLEDGE)
			return (T) know;
		return null;
	}
}