package com.dragonforge.magistry.api.research;

import java.util.List;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.ArrayUtils;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.client.IAdditionalPage;
import com.dragonforge.magistry.api.research.client.IPreviewIcon;
import com.dragonforge.magistry.api.research.client.ItemPreview;

import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class ResearchBase extends IForgeRegistryEntry.Impl<ResearchBase>
{
	private ResourceLocation[] parents;
	
	private IPreviewIcon preview;
	private IAdditionalPage[] additionalPages = new IAdditionalPage[0];
	
	private ResearchTag[] researchTags;
	private NonNullList<ItemStack> researchIngredients;
	
	public ResearchBase(String name)
	{
		setId(name);
	}
	
	public ResearchBase setPreview(IPreviewIcon preview)
	{
		this.preview = preview;
		return this;
	}
	
	public ResearchBase setResearchable(ResearchItemBuilder items, ResearchTag... researchTags)
	{
		this.researchIngredients = items.ingredients;
		this.researchTags = researchTags;
		return this;
	}
	
	public ResearchBase addPages(IAdditionalPage... page)
	{
		additionalPages = ArrayUtils.addAll(additionalPages, page);
		return this;
	}
	
	public ResearchBase setPreview(ItemStack stack)
	{
		return setPreview(new ItemPreview(stack));
	}
	
	private ResearchBase setId(String name)
	{
		String modid = Magistry.MOD_ID;
		
		ModContainer mc = Loader.instance().activeModContainer();
		if(mc != null)
			modid = mc.getModId();
		else
			Magistry.LOG.warn("UNABLE TO TRACK REGISTERING MOD ID FOR RESEARCH KEY '" + name + "'! Assigning magistry mod id.");
		
		setRegistryName(modid, name);
		
		return this;
	}
	
	public ResearchBase setParents(ResourceLocation... parents)
	{
		this.parents = parents;
		return this;
	}
	
	public ResourceLocation[] getParents()
	{
		return parents;
	}
	
	public static final IPreviewIcon noPreview = new ItemPreview(new ItemStack(Blocks.BARRIER));
	
	@Nonnull
	public final IPreviewIcon getPreview()
	{
		return preview == null ? noPreview : preview;
	}
	
	public IAdditionalPage[] getAdditionalPages()
	{
		return additionalPages;
	}
	
	public ResearchTag[] getResearchTags()
	{
		return researchTags;
	}
	
	public boolean isResearchable(EntityPlayerMP player, List<ResearchTag> tags)
	{
		if(researchTags == null || researchTags.length == 0)
			return false;
		if(tags.size() != researchTags.length)
			return false;
		else
			for(ResearchTag our : researchTags)
				if(!tags.contains(our))
					return false;
		return true;
	}
	
	public String[] getDescription()
	{
		return I18n.format("research." + getRegistryName().toString() + ".desc").split(" ");
	}
	
	public ResearchBase register()
	{
		MagistryAPI.RESEARCH_REGISTRY.register(this);
		return this;
	}
	
	public NonNullList<ItemStack> getResIngredients()
	{
		return researchIngredients;
	}
}