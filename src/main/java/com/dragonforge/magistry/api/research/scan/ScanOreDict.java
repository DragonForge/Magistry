package com.dragonforge.magistry.api.research.scan;

import java.util.function.Predicate;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class ScanOreDict implements IScanObject
{
	final Predicate<String> orePred;
	final String oreToken, research;
	
	public ScanOreDict(String ore, String research)
	{
		this.oreToken = ore;
		this.research = research;
		this.orePred = MagistryAPI.wildcardMatcher(ore);
	}
	
	@Override
	public boolean isThisObject(EntityPlayerMP player, IPlayerKnowledge knowledge, Object v)
	{
		if(v instanceof ItemStack && !((ItemStack) v).isEmpty())
			for(int i : OreDictionary.getOreIDs((ItemStack) v))
			{
				String on = OreDictionary.getOreName(i);
				if(on != null && orePred.test(on))
					return true;
			}
		if(v instanceof IBlockState)
		{
			IBlockState state = (IBlockState) v;
			Item item = state.getBlock().getItemDropped(state, player.world.rand, 0);
			int damage = state.getBlock().damageDropped(state);
			int count = state.getBlock().quantityDropped(state, 0, player.world.rand);
			
			boolean valid = false;
			if(item != null && count > 0)
				valid = isThisObject(player, knowledge, new ItemStack(item, count, damage));
			if(!valid)
			{
				item = Item.getItemFromBlock(state.getBlock());
				valid = isThisObject(player, knowledge, new ItemStack(item, 1, damage));
			}
			return valid;
		}
		return false;
	}
	
	@Override
	public String getScanKey(EntityPlayerMP player, IPlayerKnowledge knowledge)
	{
		return research;
	}
}