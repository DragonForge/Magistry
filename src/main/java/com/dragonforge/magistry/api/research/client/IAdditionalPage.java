package com.dragonforge.magistry.api.research.client;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;

import javax.tools.Tool;

import com.dragonforge.magistry.client.MagistryFonts;
import com.zeitheron.hammercore.client.utils.RenderUtil;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.util.ITooltipFlag.TooltipFlags;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface IAdditionalPage
{
	default void onAppear()
	{
	}
	
	default void onDisappear()
	{
	}
	
	@SideOnly(Side.CLIENT)
	void renderPage(float partialTicks, int relMouseX, int relMouseY, int width, int height, int page);
	
	@SideOnly(Side.CLIENT)
	default void mouseClicked(int relMouseX, int relMouseY, int mouseButton, int page)
	{
	}
	
	@SideOnly(Side.CLIENT)
	default void mouseDragged(int relMouseX, int relMouseY, int mouseButton, int page)
	{
	}
	
	default void keyTyped(char typedChar, int keyCode)
	{
		
	}
	
	@SideOnly(Side.CLIENT)
	default void addInformation(List<String> tooltip, int relMouseX, int relMouseY, int page)
	{
		if(!mouseOverStack.get().isEmpty())
			tooltip.addAll(mouseOverStack.get().getTooltip(Minecraft.getMinecraft().player, Minecraft.getMinecraft().gameSettings.advancedItemTooltips ? TooltipFlags.ADVANCED : TooltipFlags.NORMAL));
	}
	
	ThreadLocal<ItemStack> mouseOverStack = ThreadLocal.withInitial(() -> ItemStack.EMPTY);
	Point mouse = new Point();
	Rectangle lastItemRect = new Rectangle();
	
	@SideOnly(Side.CLIENT)
	default void renderStack(ItemStack stack, int x, int y)
	{
		renderStack(stack, x, y, 16, 16);
	}
	
	@SideOnly(Side.CLIENT)
	default void renderStack(ItemStack stack, int x, int y, int width, int height)
	{
		lastItemRect.setBounds(x, y, width, height);
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, 0);
		GlStateManager.scale(width / 16F, height / 16F, 1);
		Minecraft.getMinecraft().getRenderItem().renderItemAndEffectIntoGUI(stack, 0, 0);
		if(stack.getCount() > 1)
		{
			GlStateManager.translate(0, 0, 200);
			MagistryFonts.PARCHMENT.render(stack.getCount() + "", 8.5F, 8.5F, 0xFF000000);
			MagistryFonts.PARCHMENT.render(stack.getCount() + "", 8, 8, 0xFFFFFFFF);
			GlStateManager.translate(0, 0, -200);
		}
		if(lastItemRect.contains(mouse))
		{
			mouseOverStack.set(stack);
			GlStateManager.translate(0, 0, 200);
			GlStateManager.disableTexture2D();
			GlStateManager.color(1, 1, 1, 1);
			RenderUtil.drawColoredModalRect(0, 0, 16, 16, 0x88FFFFFF);
			GlStateManager.enableTexture2D();
		}
		GlStateManager.popMatrix();
	}
}