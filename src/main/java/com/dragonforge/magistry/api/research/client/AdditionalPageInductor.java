package com.dragonforge.magistry.api.research.client;

import java.util.List;
import java.util.stream.Collectors;

import org.lwjgl.opengl.GL11;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.crafting.RecipesInductor;
import com.dragonforge.magistry.api.crafting.recipes.InductorRecipe;
import com.dragonforge.magistry.client.MagistryFonts;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.utils.ItemStackUtil;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;

public class AdditionalPageInductor implements IAdditionalPage
{
	final ItemStack result;
	
	public AdditionalPageInductor(ItemStack result)
	{
		this.result = result;
	}
	
	List<InductorRecipe> recipes;
	
	public InductorRecipe getRecipe()
	{
		if(recipes == null)
			recipes = RecipesInductor.RECIPES.stream().filter(r -> result.isItemEqual(r.output)).collect(Collectors.toList());
		return recipes.isEmpty() ? null : recipes.get((int) (System.currentTimeMillis() % (1000L * recipes.size()) / 1000L));
	}
	
	@Override
	public void renderPage(float partialTicks, int relMouseX, int relMouseY, int width, int height, int page)
	{
		InductorRecipe r = getRecipe();
		
		GL11.glPushMatrix();
		GL11.glPushMatrix();
		GL11.glEnable(3042);
		GL11.glTranslatef(width / 2 - 16, height / 2 - 16, 0.0f);
		GL11.glScalef(2.0f, 2.0f, 1.0f);
		GlStateManager.enableAlpha();
		GlStateManager.enableBlend();
		UtilsFX.bindTexture("textures/gui/gui_manual_overlay.png");
		GL11.glColor4f(1, 1, 1, 1);
		RenderUtil.drawTexturedModalRect(0, 0, 20, 3, 16, 16);
		GL11.glPopMatrix();
		
		GL11.glPushMatrix();
		GL11.glTranslated(0.0, 0.0, 100.0);
		GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		RenderHelper.enableGUIStandardItemLighting();
		GL11.glEnable(2884);
		renderStack(r.output, width / 2 - 12, height / 2 - 12, 24, 24);
		RenderHelper.enableGUIStandardItemLighting();
		GL11.glEnable(2896);
		GL11.glPopMatrix();
		
		String text = I18n.format("recipe." + Magistry.MOD_ID + ".induction");
		int offset = MagistryFonts.PARCHMENT.getWidth(text);
		GlStateManager.pushMatrix();
		GlStateManager.translate(width / 2, 0, 0);
		GlStateManager.scale(1.5F, 1.5F, 1.5F);
		RenderHelper.disableStandardItemLighting();
		MagistryFonts.PARCHMENT.renderWithShadow(text, -offset / 2, 0, 0xFFFFFFFF);
		GlStateManager.popMatrix();
		
		int n = r.inputs.size();
		if(n > 0)
		{
			GL11.glPushMatrix();
			float deg = (float) Math.toRadians(360F / n);
			float cd = 0;
			
			float m = Math.min(width, height) / 2.4F;
			
			for(int i = 0; i < n; ++i)
			{
				float x = width / 2 - 8 + (float) Math.sin(cd) * m;
				float y = height / 2 - 8 + (float) Math.cos(cd) * m;
				
				renderStack(ItemStackUtil.cycleItemStack(r.inputs.get(i)), (int) x, (int) y);
				
				cd += deg;
			}
			
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();
	}
}