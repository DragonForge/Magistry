package com.dragonforge.magistry.api.research.scan;

import java.util.Objects;

import com.dragonforge.magistry.api.research.IPlayerKnowledge;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ScanItem implements IScanObject
{
	final ItemStack item;
	final Item it;
	final String research;
	
	public ScanItem(ItemStack item, String research)
	{
		this.item = item;
		this.it = null;
		this.research = research;
	}
	
	public ScanItem(Item item, String research)
	{
		this.item = ItemStack.EMPTY;
		this.it = item;
		this.research = research;
	}
	
	@Override
	public boolean isThisObject(EntityPlayerMP player, IPlayerKnowledge knowledge, Object v)
	{
		if(v instanceof ItemStack)
		{
			ItemStack stack = (ItemStack) v;
			if(it != null)
				return stack.getItem() == it;
			return stack.isItemEqual(item) && Objects.equals(stack.getTagCompound(), item.getTagCompound());
		}
		if(v instanceof Item)
		{
			Item item = (Item) v;
			if(it != null)
				return it == item;
			return this.item.getItem() == item;
		}
		return false;
	}

	@Override
	public String getScanKey(EntityPlayerMP player, IPlayerKnowledge knowledge)
	{
		return research;
	}
}