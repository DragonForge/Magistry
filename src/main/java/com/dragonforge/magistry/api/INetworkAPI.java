package com.dragonforge.magistry.api;

import com.dragonforge.magistry.api.research.IPlayerKnowledge;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.chunk.Chunk;

public interface INetworkAPI
{
	void sendResearches(IPlayerKnowledge knowledge, EntityPlayerMP mp);
	
	void sendAura(Chunk chunk, EntityPlayerMP mp);
}