package com.dragonforge.magistry;

import java.io.ByteArrayOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dragonforge.magistry.api.INetworkAPI;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.aura.ChunkAuraImpl;
import com.dragonforge.magistry.api.aura.IChunkAura;
import com.dragonforge.magistry.api.ce.CrystalStorage;
import com.dragonforge.magistry.api.ce.ICrystalStorage;
import com.dragonforge.magistry.api.multiblock.MultiblockRecipe;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.PlayerKnowledgeImpl;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.cfg.ConfigsM;
import com.dragonforge.magistry.commands.CommandMagistry;
import com.dragonforge.magistry.compat.crafttweaker.CraftTweakerCompat;
import com.dragonforge.magistry.compat.crafttweaker.core.ICTCompat;
import com.dragonforge.magistry.init.BlocksM;
import com.dragonforge.magistry.init.ItemsM;
import com.dragonforge.magistry.init.KnowledgeM;
import com.dragonforge.magistry.init.MultiblocksM;
import com.dragonforge.magistry.init.SoundsM;
import com.dragonforge.magistry.net.PacketSendAura;
import com.dragonforge.magistry.proxy.CommonProxy;
import com.dragonforge.magistry.transport.Transport_Knowledge;
import com.dragonforge.magistry.worldgen.WorldGenAura;
import com.dragonforge.magistry.worldgen.village.VillagePartStarflowerField;
import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.event.LyingItemPickedUpEvent;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.transport.TransportSessionBuilder;
import com.zeitheron.hammercore.utils.HammerCoreUtils;
import com.zeitheron.hammercore.world.gen.WorldRetroGen;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLConstructionEvent;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLLoadCompleteEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.VillagerRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.RegistryBuilder;

@Mod(modid = Magistry.MOD_ID, name = Magistry.MOD_NAME, version = Magistry.MOD_VERSION, dependencies = "required-after:hammercore;required-after:hammermetals", certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97")
public class Magistry implements INetworkAPI
{
	public static final String MOD_ID = "magistry", MOD_NAME = "Magistry", MOD_VERSION = "@VERSION@";
	
	public static final Logger LOG = LogManager.getLogger(MOD_ID);
	
	@SidedProxy(serverSide = "com.dragonforge.magistry.proxy.CommonProxy", clientSide = "com.dragonforge.magistry.proxy.ClientProxy")
	public static CommonProxy proxy;
	
	public static CreativeTabs tab, lostNotesTab;
	
	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with Magistry jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://minecraft.curseforge.com/projects/296160 !");
		LOG.warn("*****************************");
		HammerCore.invalidCertificates.put(MOD_ID, "https://minecraft.curseforge.com/projects/296160");
	}
	
	@EventHandler
	public void construct(FMLConstructionEvent e)
	{
		MagistryAPI.RESEARCH_REGISTRY = new RegistryBuilder<ResearchBase>().setName(new ResourceLocation(MOD_ID, "research")).setType(ResearchBase.class).create();
		MagistryAPI.MULTIBLOCK_REGISTRY = new RegistryBuilder<MultiblockRecipe>().setName(new ResourceLocation(MOD_ID, "multiblock")).setType(MultiblockRecipe.class).create();
		MagistryAPI.network = this;
		
		proxy.construct();
		ConfigsM.reload();
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		tab = HammerCoreUtils.createStaticIconCreativeTab(MOD_ID, new ItemStack(ItemsM.BURNT_BLUE_QUARTZ));
		lostNotesTab = HammerCoreUtils.createStaticIconCreativeTab(MOD_ID + "_notes", new ItemStack(ItemsM.LOST_NOTES));
		
		SimpleRegistration.registerFieldBlocksFrom(BlocksM.class, MOD_ID, tab);
		SimpleRegistration.registerFieldItemsFrom(ItemsM.class, MOD_ID, tab);
		WorldRetroGen.addWorldGenerator(new WorldGenAura());
		
		OreDictionary.registerOre("gemQuartzBlueBurnt", ItemsM.BURNT_BLUE_QUARTZ);
		OreDictionary.registerOre("blockQuartzBlue", BlocksM.BLUE_QUARTZ_BLOCK);
		OreDictionary.registerOre("blockQuartzBlueBurnt", BlocksM.BURNT_BLUE_QUARTZ_BLOCK);
		
		registerCapabilities();
		
		MinecraftForge.EVENT_BUS.register(this);
		proxy.preInit(e.getAsmData());
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
		
		VillagerRegistry.instance().registerVillageCreationHandler(new VillagePartStarflowerField());
		
		MagistryAPI.addHotBlock(Blocks.MAGMA);
		MagistryAPI.addHotBlock(Blocks.LIT_FURNACE);
		MagistryAPI.addHotBlock(Blocks.TORCH);
		MagistryAPI.addHotBlock(Blocks.FIRE);
		
		KnowledgeM.register.call();
		MultiblocksM.register.call();
		ConfigsM.reloadRecipes();
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent e)
	{
		proxy.postInit();
	}
	
	@EventHandler
	public void loadComplete(FMLLoadCompleteEvent e)
	{
		ICTCompat ictc = CraftTweakerCompat.compat();
		if(ictc != null)
			ictc.onLoadComplete();
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent e)
	{
		e.registerServerCommand(new CommandMagistry());
	}
	
	@SubscribeEvent
	public void pickupLostNotes(LyingItemPickedUpEvent e)
	{
		if(!e.drop.isEmpty() && e.drop.getItem() == ItemsM.LOST_NOTES && e.getEntityPlayer() != null)
		{
			if(e.drop.hasTagCompound())
			{
				String research = e.drop.getTagCompound().getString("Research");
				MagistryAPI.getPlayerKnowledge(e.getEntityPlayer()).unlockResearch(research, true);
			}
			e.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public void registerSounds(RegistryEvent.Register<SoundEvent> e)
	{
		SoundsM.register(e.getRegistry());
	}
	
	private static void registerCapabilities()
	{
		CapabilityManager.INSTANCE.register(IPlayerKnowledge.class, new IStorage<IPlayerKnowledge>()
		{
			@Override
			public NBTBase writeNBT(Capability<IPlayerKnowledge> capability, IPlayerKnowledge instance, EnumFacing side)
			{
				return instance.serializeNBT();
			}
			
			@Override
			public void readNBT(Capability<IPlayerKnowledge> capability, IPlayerKnowledge instance, EnumFacing side, NBTBase nbt)
			{
				instance.deserializeNBT((NBTTagCompound) nbt);
			}
		}, () -> new PlayerKnowledgeImpl());
		
		CapabilityManager.INSTANCE.register(IChunkAura.class, new IStorage<IChunkAura>()
		{
			@Override
			public NBTBase writeNBT(Capability<IChunkAura> capability, IChunkAura instance, EnumFacing side)
			{
				return instance.serializeNBT();
			}
			
			@Override
			public void readNBT(Capability<IChunkAura> capability, IChunkAura instance, EnumFacing side, NBTBase nbt)
			{
				instance.deserializeNBT((NBTTagCompound) nbt);
			}
		}, () -> new ChunkAuraImpl());
		
		CapabilityManager.INSTANCE.register(ICrystalStorage.class, new IStorage<ICrystalStorage>()
		{
			@Override
			public NBTBase writeNBT(Capability<ICrystalStorage> capability, ICrystalStorage instance, EnumFacing side)
			{
				return new NBTTagFloat(instance.getCUStored());
			}
			
			@Override
			public void readNBT(Capability<ICrystalStorage> capability, ICrystalStorage instance, EnumFacing side, NBTBase nbt)
			{
				instance.setCU(((NBTTagFloat) nbt).getFloat());
			}
		}, () -> new CrystalStorage(10));
	}
	
	@Override
	public void sendResearches(IPlayerKnowledge knowledge, EntityPlayerMP mp)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try
		{
			CompressedStreamTools.writeCompressed(knowledge.serializeNBT(), baos);
		} catch(Throwable err)
		{
		}
		new TransportSessionBuilder().setAcceptor(Transport_Knowledge.class).addData(baos.toByteArray()).build().sendTo(mp);
	}
	
	@Override
	public void sendAura(Chunk chunk, EntityPlayerMP mp)
	{
		IChunkAura aura = MagistryAPI.getAura(chunk);
		if(aura != null)
			HCNet.INSTANCE.sendTo(new PacketSendAura(aura), mp);
	}
}