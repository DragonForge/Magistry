package com.dragonforge.magistry.event;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.utils.UV;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumHandSide;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@EventBusSubscriber(Side.CLIENT)
@SideOnly(Side.CLIENT)
public class ClientEventHandler
{
	static Polygon leftHand = new Polygon(), rightHand = new Polygon();
	
	public static void loadPolygons()
	{
		rightHand.reset();
		leftHand.reset();
		
		rightHand.addPoint(6730, 4100);
		rightHand.addPoint(6900, 4100);
		rightHand.addPoint(6940, 4050);
		rightHand.addPoint(6910, 3330);
		rightHand.addPoint(7080, 3330);
		rightHand.addPoint(7110, 3290);
		rightHand.addPoint(7100, 2980);
		rightHand.addPoint(7450, 2980);
		rightHand.addPoint(7500, 2930);
		rightHand.addPoint(7485, 2600);
		rightHand.addPoint(8240, 2600);
		rightHand.addPoint(8270, 2970);
		rightHand.addPoint(8670, 2970);
		rightHand.addPoint(8710, 3330);
		rightHand.addPoint(8915, 3330);
		rightHand.addPoint(9190, 6100);
		rightHand.addPoint(8965, 6100);
		rightHand.addPoint(9050, 6970);
		rightHand.addPoint(8600, 6970);
		rightHand.addPoint(8650, 7420);
		rightHand.addPoint(7790, 7420);
		rightHand.addPoint(7770, 6970);
		rightHand.addPoint(7310, 6970);
		rightHand.addPoint(7290, 6520);
		rightHand.addPoint(7060, 6520);
		rightHand.addPoint(7020, 5660);
		rightHand.addPoint(6800, 5660);
		rightHand.addPoint(6730, 4100);
		
		for(int i = 0; i < rightHand.npoints; ++i)
			leftHand.addPoint(10090 - rightHand.xpoints[i], rightHand.ypoints[i] - 340);
	}
	
	static
	{
		loadPolygons();
	}
	
	@SubscribeEvent
	public static void renderHUD(RenderGameOverlayEvent.Post e)
	{
		if(e.getType() == ElementType.ALL)
		{
			ScaledResolution res = e.getResolution();
			int w = res.getScaledWidth();
			int h = res.getScaledHeight();
			float f = e.getPartialTicks();
			for(int i = 0; i < RenderableUV.uvs.size(); ++i)
			{
				RenderableUV uv = RenderableUV.uvs.get(i);
				uv.render(w, h, f);
			}
		}
	}
	
	@SubscribeEvent
	public static void clientTick(ClientTickEvent e)
	{
		if(e.phase == Phase.START)
		{
			for(int i = 0; i < RenderableUV.uvs.size(); ++i)
			{
				RenderableUV uv = RenderableUV.uvs.get(i);
				uv.tick();
				if(uv.ded)
				{
					uv.uvs.remove(i);
					--i;
					continue;
				}
			}
		}
	}
	
	public static void renderPolygon(Polygon pg, int w, int h)
	{
		Rectangle re = pg.getBounds();
		
		GlStateManager.disableTexture2D();
		GL11.glBegin(GL11.GL_LINE_STRIP);
		GL11.glColor4f(1, 0, 0, 1F);
		for(int i = 0; i < pg.npoints; ++i)
		{
			float xp = pg.xpoints[i % pg.npoints] / 10000F * w;
			float yp = pg.ypoints[i % pg.npoints] / 10000F * h;
			
			GL11.glVertex3f(xp, yp, 0);
		}
		GL11.glEnd();
		GlStateManager.enableTexture2D();
	}
	
	public static class RenderableUV
	{
		public static final List<RenderableUV> uvs = new ArrayList<>();
		
		public int x, y, te;
		public float scale, prevScale;
		public UV uv;
		
		public boolean ded;
		
		public static void create(UV uv, EnumHand hand)
		{
			boolean handsInverted = Minecraft.getMinecraft().gameSettings.mainHand == EnumHandSide.LEFT;
			boolean left = hand == EnumHand.OFF_HAND && !handsInverted;
			Polygon pg = left ? leftHand : rightHand;
			Rectangle rc = pg.getBounds();
			while(true)
			{
				int xs = ThreadLocalRandom.current().nextInt(rc.x, rc.x + rc.width) + 12;
				int ys = ThreadLocalRandom.current().nextInt(rc.y, rc.y + rc.height) + 12;
				if(pg.contains(xs, ys))
				{
					new RenderableUV(uv, xs, ys);
					return;
				}
			}
		}
		
		public RenderableUV(UV uv, int x, int y)
		{
			this.x = x;
			this.y = y;
			this.uv = uv;
			uvs.add(this);
		}
		
		public void tick()
		{
			++te;
			prevScale = scale;
			scale = (float) Math.sin(Math.toRadians(te / 40F * 180F));
			if(te >= 40)
				ded = true;
		}
		
		public void render(int w, int h, float f)
		{
			float s = (prevScale + (scale - prevScale) * f) * 12;
			uv.render(this.x / 10000F * w - s / 2, this.y / 10000F * h - s / 2, s, s);
		}
	}
}