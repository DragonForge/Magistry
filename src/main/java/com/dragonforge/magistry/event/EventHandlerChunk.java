package com.dragonforge.magistry.event;

import java.util.Collection;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.aura.ChunkAuraProvider;
import com.zeitheron.hammercore.annotations.MCFBus;

import net.minecraft.util.ResourceLocation;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;

@MCFBus
public class EventHandlerChunk
{
	@SubscribeEvent
	public void attachCapabilitiesChunk(AttachCapabilitiesEvent<Chunk> event)
	{
		event.addCapability(new ResourceLocation(Magistry.MOD_ID, "aura"), new ChunkAuraProvider());
	}
	
	@SubscribeEvent
	public void worldTick(WorldTickEvent e)
	{
		if(e.phase == Phase.END && e.world instanceof WorldServer)
		{
			WorldServer ws = (WorldServer) e.world;
			Collection<Chunk> chunks = ws.getChunkProvider().getLoadedChunks();
			
//			int mx = Math.min(100, chunks.size() / 100);
//			int left = mx;
//			for(Chunk c : chunks)
//			{
//				if(left <= 0)
//					break;
//				IChunkAura ica = MagistryAPI.getAura(c);
//				if(ica != null && !ica.hasUpdated())
//				{
//					ica.update(c);
//					ica.setUpdated(true);
//					left--;
//				}
//			}
//			
//			if(left == mx)
//				for(Chunk c : chunks)
//				{
//					IChunkAura ica = MagistryAPI.getAura(c);
//					if(ica != null)
//						ica.setUpdated(false);
//				}
		}
	}
}