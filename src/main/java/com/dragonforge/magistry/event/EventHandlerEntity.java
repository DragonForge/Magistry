package com.dragonforge.magistry.event;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.PlayerDataIO;
import com.dragonforge.magistry.api.research.PlayerKnowledgeProvider;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.init.BlocksM;
import com.dragonforge.magistry.init.ItemsM;
import com.dragonforge.magistry.init.KnowledgeM;
import com.zeitheron.hammercore.annotations.MCFBus;
import com.zeitheron.hammercore.event.PlayerLoadReadyEvent;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;

@MCFBus
public class EventHandlerEntity
{
	@SubscribeEvent
	public void attachCapabilitiesPlayer(AttachCapabilitiesEvent<Entity> event)
	{
		if(event.getObject() instanceof EntityPlayer)
			event.addCapability(new ResourceLocation(Magistry.MOD_ID, "knowledge"), new PlayerKnowledgeProvider((EntityPlayer) event.getObject()));
	}
	
	@SubscribeEvent
	public void playerReady(PlayerLoadReadyEvent e)
	{
		EntityPlayerMP mp = WorldUtil.cast(e.getEntityPlayer(), EntityPlayerMP.class);
		if(mp != null)
		{
			Magistry.LOG.info("Sending knowledge data to player " + mp.getGameProfile().getName());
			IPlayerKnowledge pk = MagistryAPI.getPlayerKnowledge(mp);
			pk.sync(mp);
		}
	}
	
	@SubscribeEvent
	public void respawn(PlayerRespawnEvent e)
	{
		if(e.player instanceof EntityPlayerMP)
		{
			EntityPlayerMP mp = (EntityPlayerMP) e.player;
			IPlayerKnowledge pk = MagistryAPI.getPlayerKnowledge(mp);
			pk.deserializeNBT(PlayerDataIO.read(mp));
			pk.sync(mp);
		}
	}
	
	@SubscribeEvent
	public void livingDrops(LivingDropsEvent e)
	{
		EntityLivingBase dead = e.getEntityLiving();
		if(e.getSource() == MagistryAPI.ELECTRICAL_DAMAGE && dead.world.rand.nextBoolean())
		{
			EntityItem entityitem = new EntityItem(dead.world, dead.posX, dead.posY, dead.posZ, new ItemStack(ItemsM.ASH));
			entityitem.setDefaultPickupDelay();
			e.getDrops().add(entityitem);
		}
	}
}