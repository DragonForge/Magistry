package com.dragonforge.magistry.cfg;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.crafting.recipes.IMagistryRecipe;
import com.dragonforge.magistry.api.crafting.recipes.InductorRecipe;
import com.zeitheron.hammercore.cfg.file1132.Configuration;
import com.zeitheron.hammercore.cfg.file1132.io.ConfigEntryCategory;
import com.zeitheron.hammercore.lib.zlib.error.JSONException;
import com.zeitheron.hammercore.lib.zlib.json.JSONArray;
import com.zeitheron.hammercore.lib.zlib.json.JSONObject;
import com.zeitheron.hammercore.lib.zlib.json.JSONTokener;
import com.zeitheron.hammercore.utils.JSONObjectToNBT;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.oredict.OreIngredient;

public class ConfigsM
{
	public static final Configuration MAIN = new Configuration(getSuggestedMagistryFile("main"));
	
	public static boolean gen_starflowers;
	public static boolean gen_blueQuartzOre;
	public static boolean gen_crystalOre;
	public static boolean gen_naturalCrystals;
	public static boolean gen_oreCrystals;
	public static boolean gen_phyllite;
	
	private static String[] inductorRecipes;
	
	public static void reload()
	{
		ConfigEntryCategory recipes = MAIN.getCategory("Custom Recipes").setDescription("Add custom recipes to various crafting systems");
		ConfigEntryCategory worldgen = MAIN.getCategory("World Gen").setDescription("Manage world features that should generate in the world");
		ConfigEntryCategory worldgen_surface = worldgen.getCategory("Surface").setDescription("These features generate on the surface.\nTypically trees, flowers, grass etc.");
		ConfigEntryCategory worldgen_underground = worldgen.getCategory("Underground").setDescription("These features generate underground.\nTypically ores, crystals, minerals etc.");
		
		gen_starflowers = worldgen_surface.getBooleanEntry("Starflowers", true).setDescription("Should Starflowers generate?").getValue();
		gen_blueQuartzOre = worldgen_underground.getBooleanEntry("Blue Quartz Ore", true).setDescription("Should Blue Quartz Ore generate?").getValue();
		gen_crystalOre = worldgen_underground.getBooleanEntry("Crystal Shard Ore", true).setDescription("Should Crystal (Shard) Ore generate?").getValue();
		gen_naturalCrystals = worldgen_underground.getBooleanEntry("Crystals", true).setDescription("Should Crystals generate?").getValue();
		gen_oreCrystals = worldgen_underground.getBooleanEntry("Ore Crystals", true).setDescription("Should Ore Crystals generate?").getValue();
		gen_phyllite = worldgen_underground.getBooleanEntry("Phyllite", true).setDescription("Should phyllite generate?").getValue();
		
		inductorRecipes = recipes.getStringArrayEntry("Inductor", "[\"cobblestone\"]={id:\"minecraft:stone\"}").setDescription("Custom inductor recipes.").getValue();
		
		if(MAIN.hasChanged())
			MAIN.save();
	}
	
	static final List<Runnable> removes = new ArrayList<>();
	
	public static void reloadRecipes()
	{
		removes.forEach(Runnable::run);
		removes.clear();
		
		if(inductorRecipes != null)
			for(String ln : inductorRecipes)
				addRecipe(parseInductor(ln));
	}
	
	static InductorRecipe parseInductor(String line)
	{
		String[] pars = line.split("=", 2);
		int i = 0;
		try
		{
			JSONArray inputs = (JSONArray) new JSONTokener(pars[0]).nextValue();
			++i;
			
			Object result = new JSONTokener(pars[1]).nextValue();
			++i;
			
			List<Ingredient> ingredients = new ArrayList<>();
			for(int j = 0; j < inputs.length(); ++j)
			{
				Ingredient ing = parseIngredient(inputs.get(j));
				if(ing != null)
					ingredients.add(ing);
				else
					Magistry.LOG.error("Unparsed ingredient: " + inputs.get(j));
			}
			
			ItemStack output = parseItem(result);
			
			if(output.isEmpty())
			{
				Magistry.LOG.error("Unparsed recipe result: " + result);
				return null;
			}
			
			if(ingredients.isEmpty())
			{
				Magistry.LOG.error("Empty ingredient result: " + result);
				return null;
			}
			
			Magistry.LOG.info("Parsed new custom INDUCTOR recipe: {" + line + "}");
			return new InductorRecipe(output, ingredients);
		} catch(JSONException e)
		{
			if(i == 0)
				Magistry.LOG.error("Failed to parse input JSON for INDUCTOR recipe: {" + line + "}", e);
			if(i == 1)
				Magistry.LOG.error("Failed to parse output JSON for INDUCTOR recipe: {" + line + "}", e);
			if(i == 2)
				Magistry.LOG.fatal("Invalid JSON for INDUCTOR recipe: {" + line + "}", e);
		}
		Magistry.LOG.warn("Detected invalid INDUCTOR recipe: {" + line + "}");
		return null;
	}
	
	private static ItemStack parseItem(Object object) throws JSONException
	{
		if(object instanceof String)
			return new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation(object.toString())));
		if(object instanceof JSONObject)
		{
			JSONObject obj = (JSONObject) object;
			ItemStack stack = new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation(obj.getString("id"))), obj.optInt("count", 1), obj.optInt("damage", 0));
			JSONObject tag = obj.optJSONObject("tag");
			if(tag != null && !tag.keySet().isEmpty())
				stack.setTagCompound(JSONObjectToNBT.convert(tag));
			return stack;
		}
		return ItemStack.EMPTY;
	}
	
	private static Ingredient parseIngredient(Object object) throws JSONException
	{
		if(object instanceof String)
			return new OreIngredient(object.toString());
		if(object instanceof JSONObject)
		{
			ItemStack stack = parseItem(object);
			if(!stack.isEmpty())
				return Ingredient.fromStacks(stack);
		}
		if(object instanceof JSONArray)
		{
			JSONArray arr = (JSONArray) object;
			NonNullList<ItemStack> items = NonNullList.create();
			for(int i = 0; i < arr.length(); ++i)
			{
				ItemStack stack = parseItem(arr.opt(i));
				if(!stack.isEmpty())
					items.add(stack);
			}
			if(!items.isEmpty())
				return Ingredient.fromStacks(items.toArray(new ItemStack[items.size()]));
		}
		return null;
	}
	
	static void addRecipe(IMagistryRecipe recipe)
	{
		if(recipe != null)
			removes.add(recipe.add()::remove);
	}
	
	public static File getSuggestedMagistryFile(String sub)
	{
		if(!sub.isEmpty())
		{
			File f = new File(Loader.instance().getConfigDir(), Magistry.MOD_ID);
			if(!f.isDirectory())
				f.mkdirs();
			return new File(f, sub + ".cfg");
		}
		return new File(Loader.instance().getConfigDir(), Magistry.MOD_ID + ".cfg");
	}
}