package com.dragonforge.magistry.inventory.slot;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.items.ItemLostNotes;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotResearchNote extends Slot
{
	public SlotResearchNote(IInventory inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
	}
	
	public void putResearchNotes(ResearchBase research)
	{
		putStack(ItemLostNotes.createForResearch(research.getRegistryName()));
	}
	
	@Override
	public boolean canTakeStack(EntityPlayer playerIn)
	{
		IPlayerKnowledge k = MagistryAPI.getPlayerKnowledge(playerIn);
		ItemStack stack = getStack();
		if(!stack.isEmpty() && stack.hasTagCompound() && stack.getTagCompound().hasKey("Research"))
			k.unlockResearch(stack.getTagCompound().getString("Research"), false);
		return true;
	}
	
	@Override
	public boolean isItemValid(ItemStack stack)
	{
		return false;
	}
}