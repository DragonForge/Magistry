package com.dragonforge.magistry.inventory.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;

public class SlotIngredient extends Slot
{
	public final Ingredient input;
	
	public SlotIngredient(IInventory inventoryIn, int index, int xPosition, int yPosition, Ingredient input)
	{
		super(inventoryIn, index, xPosition, yPosition);
		this.input = input;
	}
	
	@Override
	public boolean isItemValid(ItemStack stack)
	{
		return input.test(stack);
	}
}