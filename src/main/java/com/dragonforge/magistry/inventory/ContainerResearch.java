package com.dragonforge.magistry.inventory;

import com.dragonforge.magistry.blocks.machines.tiles.TileResearchTable;
import com.dragonforge.magistry.inventory.slot.SlotIngredient;
import com.dragonforge.magistry.inventory.slot.SlotResearchNote;
import com.google.common.base.Predicates;
import com.zeitheron.hammercore.client.gui.impl.container.ItemTransferHelper.TransferableContainer;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.item.crafting.Ingredient;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerResearch extends TransferableContainer<TileResearchTable>
{
	public ContainerResearch(EntityPlayer player, TileResearchTable tile)
	{
		super(player, tile, 8, 99);
		if(player instanceof EntityPlayerMP)
			tile.user = (EntityPlayerMP) player;
	}
	
	@Override
	public void onContainerClosed(EntityPlayer playerIn)
	{
		t.user = null;
	}
	
	@Override
	protected void addCustomSlots()
	{
		addSlotToContainer(new SlotIngredient(t.inventory, 0, 8, 11, Ingredient.fromItem(Items.PAPER)));
		addSlotToContainer(new SlotResearchNote(t.inventory, 1, 8, 65));
		
		for(int x = 0; x < 2; ++x)
			for(int y = 0; y < 4; ++y)
				addSlotToContainer(new Slot(t.inventory, 2 + x + y * 2, 135 + x * 18, 11 + y * 18));
	}
	
	@Override
	protected void addTransfer()
	{
		transfer.addInTransferRule(0, stack -> !stack.isEmpty() && stack.getItem() == Items.PAPER);
	}
	
	int[] vals = new int[2];
	
	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();
		
		if(vals[0] != t.discoveryProgress)
			sendWindowProp(0, vals[0] = t.discoveryProgress);
		
		if(vals[1] != t.totalItemCount)
			sendWindowProp(0, vals[1] = t.totalItemCount);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int id, int data)
	{
		if(id == 0)
			t.discoveryProgress = data;
		if(id == 1)
			t.totalItemCount = data;
	}
	
	private void sendWindowProp(int id, int val)
	{
		for(int j = 0; j < this.listeners.size(); ++j)
			((IContainerListener) this.listeners.get(j)).sendWindowProperty(this, id, val);
	}
	
	@Override
	public boolean enchantItem(EntityPlayer playerIn, int id)
	{
		return true;
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return t.inventory.isUsableByPlayer(playerIn, t.getPos());
	}
}