package com.dragonforge.magistry.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiPredicate;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.zeitheron.hammercore.HammerCore;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class ContainerHelper
{
	public static final List<BiPredicate<InventoryCrafting, String>> RESEARCH_TESTERS = new ArrayList<>();
	
	static final Field eventHandler, playerList;
	
	static
	{
		Field eh = null;
		for(Field f : InventoryCrafting.class.getDeclaredFields())
			if(Container.class.isAssignableFrom(f.getType()))
			{
				eh = f;
				eh.setAccessible(true);
				break;
			}
		eventHandler = eh;
		playerList = Container.class.getDeclaredFields()[8];
		playerList.setAccessible(true);
		RESEARCH_TESTERS.add((ic, res) ->
		{
			for(EntityPlayer ep : getCrafters(getContainer(ic)))
			{
				IPlayerKnowledge kn = MagistryAPI.getPlayerKnowledge(ep);
				if(kn != null && kn.hasResearch(res))
					return true;
			}
			return false;
		});
	}
	
	public static Container getContainer(InventoryCrafting ic)
	{
		try
		{
			return eventHandler != null ? Container.class.cast(eventHandler.get(ic)) : null;
		} catch(ReflectiveOperationException e)
		{
			return null;
		}
	}
	
	public static Set<EntityPlayer> getCrafters(Container c)
	{
		if(c == null)
			return Collections.emptySet();
		try
		{
			Set<EntityPlayer> hset = new HashSet<EntityPlayer>((Set<EntityPlayer>) playerList.get(c));
			MinecraftServer mcs = FMLCommonHandler.instance().getMinecraftServerInstance();
			if(mcs != null)
				for(EntityPlayerMP mp : mcs.getPlayerList().getPlayers())
					if(mp.openContainer == c)
						hset.add(mp);
			return hset;
		} catch(ReflectiveOperationException e)
		{
			return Collections.emptySet();
		}
	}
	
	public static boolean doesInventoryCraftingHaveResearch(InventoryCrafting ic, String res)
	{
		for(BiPredicate<InventoryCrafting, String> p : RESEARCH_TESTERS)
			if(p.test(ic, res))
				return true;
		return false;
	}
}