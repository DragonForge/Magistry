package com.dragonforge.magistry.utils;

import com.zeitheron.hammercore.api.IProcess;

public class ProcessDelayedTask implements IProcess
{
	int ticks;
	Runnable task;
	
	public ProcessDelayedTask(int ticks, Runnable task)
	{
		this.ticks = ticks;
		this.task = task;
	}
	
	@Override
	public void update()
	{
		ticks--;
		if(ticks == 0)
			task.run();
	}
	
	@Override
	public boolean isAlive()
	{
		return ticks > 0;
	}
}