package com.dragonforge.magistry.utils;

import java.util.concurrent.ThreadLocalRandom;

import javax.annotation.Nullable;

import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.util.NonNullList;
import net.minecraftforge.oredict.OreDictionary;

public class OreHelper
{
	private IBlockState state;
	private ItemStack oreStack;
	
	OreHelper(IBlockState state, ItemStack drop)
	{
		this.state = state;
		this.oreStack = getSilkTouchDrop(state);
	}
	
	protected static ItemStack getSilkTouchDrop(IBlockState state)
	{
		Item item = Item.getItemFromBlock(state.getBlock());
		int i = 0;
		if(item.getHasSubtypes())
			i = state.getBlock().getMetaFromState(state);
		return new ItemStack(item, 1, i);
	}
	
	public ItemStack getDroppedItem()
	{
		Item drop = state.getBlock().getItemDropped(state, ThreadLocalRandom.current(), 0);
		int dmg = state.getBlock().damageDropped(state);
		int q = Math.max(1, state.getBlock().quantityDropped(state, 0, ThreadLocalRandom.current()));
		return new ItemStack(drop, q, dmg);
	}
	
	public ItemStack getSmeltedResult()
	{
		ItemStack drop = getDroppedItem();
		ItemStack smelt = FurnaceRecipes.instance().getSmeltingResult(drop).copy();
		if(!smelt.isEmpty())
		{
			smelt.setCount(smelt.getCount() * drop.getCount());
			return smelt;
		}
		return drop;
	}
	
	public ItemStack getDustedResult()
	{
		ItemStack drop = getDroppedItem();
		boolean ore = false;
		boolean gem = false;
		String result = null;
		String on = null;
		for(int i : OreDictionary.getOreIDs(drop))
			if(((ore = (on = OreDictionary.getOreName(i)).startsWith("ore")) || (gem = on.startsWith("gem"))))
			{
				if(!OreDictionary.getOres("dust" + on.substring(3)).isEmpty())
					result = "dust" + on.substring(3);
				else if(on.startsWith("ore") && !OreDictionary.getOres("gem" + on.substring(3)).isEmpty())
					result = "gem" + on.substring(3);
				else
				{
					ore = gem = false;
					continue;
				}
				break;
			}
		int outBoost = 0;
		if(ore)
			outBoost = 2;
		else if(gem)
			outBoost = 1;
		if(outBoost > 0 && result != null)
		{
			NonNullList<ItemStack> ods = OreDictionary.getOres(result);
			if(!ods.isEmpty())
			{
				ItemStack stack = ods.get(0).copy();
				stack.setCount(stack.getCount() * outBoost * drop.getCount());
				return stack;
			}
		}
		return drop;
	}
	
	@Nullable
	public static OreHelper forOre(IBlockState state)
	{
		Item drop = state.getBlock().getItemDropped(state, ThreadLocalRandom.current(), 0);
		int dmg = state.getBlock().damageDropped(state);
		int q = Math.max(1, state.getBlock().quantityDropped(state, 0, ThreadLocalRandom.current()));
		ItemStack dropStack = new ItemStack(drop, q, dmg);
		if(dropStack.isEmpty())
			return null;
		boolean ore = false;
		String on;
		for(int i : OreDictionary.getOreIDs(dropStack))
			if(ore = ((on = OreDictionary.getOreName(i)).startsWith("ore") || on.startsWith("dust") || on.startsWith("gem")))
				break;
		if(!ore)
			return null;
		return new OreHelper(state, dropStack);
	}
}