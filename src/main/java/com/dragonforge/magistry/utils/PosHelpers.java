package com.dragonforge.magistry.utils;

import it.unimi.dsi.fastutil.longs.LongSet;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;

public class PosHelpers
{
	public static Vec3i getHalfNormalized(LongSet keys)
	{
		int maxX = 0, maxY = 0, maxZ = 0;
		for(long k : keys)
		{
			BlockPos pos = BlockPos.fromLong(k);
			maxX = Math.max(maxX, pos.getX());
			maxY = Math.max(maxY, pos.getY());
			maxZ = Math.max(maxZ, pos.getZ());
		}
		if(maxX % 2 == 1)
			++maxX;
		if(maxY % 2 == 1)
			++maxY;
		if(maxZ % 2 == 1)
			++maxZ;
		return new Vec3i(-maxX / 2, -maxY / 2, -maxZ / 2);
	}
}