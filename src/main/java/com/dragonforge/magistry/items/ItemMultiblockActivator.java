package com.dragonforge.magistry.items;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.multiblock.MultiblockRecipe;
import com.dragonforge.magistry.client.fx.FXWisp;
import com.dragonforge.magistry.init.SoundsM;
import com.dragonforge.magistry.net.PacketStartConvert;
import com.zeitheron.hammercore.api.IProcess;
import com.zeitheron.hammercore.client.particle.def.thunder.ThunderHelper;
import com.zeitheron.hammercore.client.utils.TexturePixelGetter;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.thunder.Thunder;
import com.zeitheron.hammercore.net.internal.thunder.Thunder.Fractal;
import com.zeitheron.hammercore.net.internal.thunder.Thunder.Layer;
import com.zeitheron.hammercore.proxy.ParticleProxy_Client;
import com.zeitheron.hammercore.utils.SoundUtil;
import com.zeitheron.hammercore.utils.WorldLocation;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemMultiblockActivator extends ItemMMaterial
{
	public ItemMultiblockActivator(String name)
	{
		super(name);
	}
	
	public static final List<AxisAlignedBB> BUSY = new ArrayList<>();
	
	@Override
	public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(!worldIn.isRemote)
			for(MultiblockRecipe recipe : MagistryAPI.MULTIBLOCK_REGISTRY.getValuesCollection())
				if((recipe.activator == null || recipe.activator == this) && recipe.isValid(worldIn, pos, player) && !BUSY.stream().filter(b -> b.contains(new Vec3d(pos).add(.5, .5, .5))).findAny().isPresent())
				{
					player.getHeldItem(hand).shrink(1);
					AxisAlignedBB aabb = recipe.getAABB(worldIn, pos);
					if(aabb != null)
					{
						WorldLocation loc;
						ProcessTransform pt = new ProcessTransform(loc = new WorldLocation(worldIn, pos), player, recipe, aabb);
						pt.start();
						HCNet.INSTANCE.sendToAllAround(new PacketStartConvert(this, aabb, pt.maxTicks, recipe.spawnZaps, recipe.spawnOrbs), loc.getPointWithRad(100));
					}
					break;
				}
		return EnumActionResult.SUCCESS;
	}
	
	private static class ProcessTransform implements IProcess
	{
		public WorldLocation location;
		public WeakReference<EntityPlayer> player;
		public final MultiblockRecipe recipe;
		public final AxisAlignedBB particleBB;
		public final int maxTicks;
		public int ticks;
		
		public ProcessTransform(WorldLocation location, EntityPlayer player, MultiblockRecipe recipe, AxisAlignedBB aabb)
		{
			this.particleBB = aabb;
			this.player = new WeakReference<>(player);
			this.location = location;
			this.recipe = recipe;
			int i = recipe.components.size();
			this.maxTicks = i * 20;
		}
		
		@Override
		public void update()
		{
			++ticks;
			
			if(ticks == maxTicks)
				if(!recipe.convert(location.getWorld(), location.getPos(), player.get()))
					SoundUtil.playSoundEffect(location, SoundsM.FIZZ.getRegistryName().toString(), 1F, 1F, SoundCategory.BLOCKS);
		}
		
		@Override
		public boolean isAlive()
		{
			return ticks < maxTicks;
		}
		
		@Override
		public void start()
		{
			BUSY.add(particleBB);
			IProcess.super.start();
		}
		
		@Override
		public void onKill()
		{
			BUSY.remove(particleBB);
			IProcess.super.onKill();
		}
	}
	
	@SideOnly(Side.CLIENT)
	public static class CLProcessParticles implements IProcess
	{
		public final AxisAlignedBB particleBB;
		public final ItemMultiblockActivator activator;
		public boolean zaps, orbs;
		public final int maxTicks;
		public int ticks;
		
		public CLProcessParticles(ItemMultiblockActivator activator, AxisAlignedBB aabb, int maxTicks, boolean zaps, boolean orbs)
		{
			this.activator = activator;
			this.maxTicks = maxTicks;
			this.particleBB = aabb;
			this.zaps = zaps;
			this.orbs = orbs;
		}
		
		@Override
		public void update()
		{
			int p = 8;
			
			if(++ticks < maxTicks - 2)
				p = 1;
			
			World world = Minecraft.getMinecraft().world;
			AxisAlignedBB particleBB = this.particleBB.grow(world.rand.nextFloat() * .15 + .05);
			int[] colors = TexturePixelGetter.getAllColors(activator.getDefaultInstance());
			
			for(int times = 0; times < p; ++times)
			{
				int r = 1 + world.rand.nextInt(8);
				if(orbs)
					for(int i = 0; i < r; ++i)
					{
						double x = particleBB.minX + (particleBB.maxX - particleBB.minX) * world.rand.nextDouble();
						double y = particleBB.minY + (particleBB.maxY - particleBB.minY) * world.rand.nextDouble();
						double z = particleBB.minZ + (particleBB.maxZ - particleBB.minZ) * world.rand.nextDouble();
						ParticleProxy_Client.queueParticleSpawn(new FXWisp(world, x, y, z, .5F, new Layer(1, colors[world.rand.nextInt(colors.length)], true)));
					}
				
				particleBB = this.particleBB.grow(world.rand.nextFloat() * .35 + .15);
				
				r = 1 + world.rand.nextInt(2);
				if(zaps)
					for(int i = 0; i < r; ++i)
					{
						double x2 = this.particleBB.minX + (this.particleBB.maxX - this.particleBB.minX) * world.rand.nextDouble();
						double y2 = this.particleBB.minY + (this.particleBB.maxY - this.particleBB.minY) * world.rand.nextDouble();
						double z2 = this.particleBB.minZ + (this.particleBB.maxZ - this.particleBB.minZ) * world.rand.nextDouble();
						
						double x = particleBB.minX + (particleBB.maxX - particleBB.minX) * world.rand.nextDouble();
						double y = particleBB.minY + (particleBB.maxY - particleBB.minY) * world.rand.nextDouble();
						double z = particleBB.minZ + (particleBB.maxZ - particleBB.minZ) * world.rand.nextDouble();
						
						int rgb = colors[world.rand.nextInt(colors.length)];
						ThunderHelper.thunder(world, new Vec3d(x, y, z), new Vec3d(x2, y2, z2), new Thunder(world.rand.nextLong(), 1, 1F), new Layer(771, rgb, true), new Layer(1, rgb, true), Fractal.DEFAULT_FRACTAL);
					}
			}
		}
		
		@Override
		public boolean isAlive()
		{
			return ticks <= maxTicks;
		}
	}
}