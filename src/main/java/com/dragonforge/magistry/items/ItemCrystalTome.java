package com.dragonforge.magistry.items;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.init.SoundsM;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class ItemCrystalTome extends Item
{
	public ItemCrystalTome()
	{
		setTranslationKey("crystal_tome");
		setMaxStackSize(1);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		if(worldIn.isRemote)
			try
			{
				IPlayerKnowledge k = MagistryAPI.getPlayerKnowledge(playerIn);
				if(k != null && !k.getListOfKnownResearchValues().isEmpty())
					Class.forName("com.dragonforge.magistry.client.gui.GuiCrystalTome").getDeclaredConstructor().newInstance();
				else
					playerIn.sendStatusMessage(new TextComponentTranslation("tooltip." + Magistry.MOD_ID + ".nobook"), true);
			} catch(ReflectiveOperationException roe)
			{
			}
		worldIn.playSound(null, playerIn.getPosition(), SoundsM.PAGE_TURNS, SoundCategory.PLAYERS, .5F, 1F);
		
		return new ActionResult<ItemStack>(EnumActionResult.FAIL, playerIn.getHeldItem(handIn));
	}
}