package com.dragonforge.magistry.items;

import java.util.List;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.init.ItemsM;
import com.dragonforge.magistry.init.SoundsM;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemLostNotes extends ItemMMaterial
{
	public ItemLostNotes()
	{
		super("lost_notes");
		setMaxStackSize(1);
	}
	
	public static ItemStack createForResearch(ResourceLocation id)
	{
		ItemStack ln = new ItemStack(ItemsM.LOST_NOTES);
		ln.setTagCompound(new NBTTagCompound());
		ln.getTagCompound().setString("Research", id.toString());
		return ln;
	}
	
	@Override
	protected boolean isInCreativeTab(CreativeTabs targetTab)
	{
		return targetTab == Magistry.lostNotesTab;
	}
	
	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items)
	{
		if(isInCreativeTab(tab))
			MagistryAPI.RESEARCH_REGISTRY.getKeys().stream().map(ItemLostNotes::createForResearch).forEach(items::add);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn)
	{
		NBTTagCompound tag = stack.getTagCompound();
		if(tag != null && tag.hasKey("Research"))
		{
			ResearchBase res = MagistryAPI.getResearch(tag.getString("Research"));
			if(res != null)
				tooltip.add(I18n.format("research." + res.getRegistryName().toString() + ".name"));
			else
				tooltip.add(TextFormatting.DARK_RED + "INVALID RESEARCH");
		}
		tooltip.add(TextFormatting.DARK_GRAY + "Hold [" + (GuiScreen.isShiftKeyDown() ? TextFormatting.DARK_GREEN : TextFormatting.DARK_RED) + "SHIFT" + TextFormatting.DARK_GRAY + "] to see preview.");
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		NBTTagCompound tag = playerIn.getHeldItem(handIn).getTagCompound();
		if(tag != null && tag.hasKey("Research"))
		{
			ResearchBase res = MagistryAPI.getResearch(tag.getString("Research"));
			if(res != null)
			{
				if(worldIn.isRemote)
					try
					{
						Class.forName("com.dragonforge.magistry.client.gui.GuiLostNotes").getDeclaredConstructor(ResearchBase.class).newInstance(res);
					} catch(ReflectiveOperationException roe)
					{
					}
				worldIn.playSound(null, playerIn.getPosition(), SoundsM.PAGE_TURNS, SoundCategory.PLAYERS, .5F, 1F);
			}
		}
		
		return new ActionResult<ItemStack>(EnumActionResult.FAIL, playerIn.getHeldItem(handIn));
	}
}