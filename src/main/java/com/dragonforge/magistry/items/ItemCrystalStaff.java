package com.dragonforge.magistry.items;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.blocks.tiles.TileGreaterCrystal;
import com.dragonforge.magistry.client.fx.FXWisp;
import com.zeitheron.hammercore.client.particle.def.thunder.ThunderHelper;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.thunder.Thunder;
import com.zeitheron.hammercore.net.internal.thunder.Thunder.Fractal;
import com.zeitheron.hammercore.net.internal.thunder.Thunder.Layer;
import com.zeitheron.hammercore.proxy.ParticleProxy_Client;
import com.zeitheron.hammercore.raytracer.RayTracer;
import com.zeitheron.hammercore.utils.AABBUtils;
import com.zeitheron.hammercore.utils.NBTUtils;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemCrystalStaff extends Item
{
	public ItemCrystalStaff()
	{
		setTranslationKey("crystal_staff");
		setMaxStackSize(1);
	}
	
	@Override
	public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		ItemStack stack = player.getHeldItem(hand);
		
		if(player.isSneaking())
		{
			TileGreaterCrystal tgc = WorldUtil.cast(worldIn.getTileEntity(pos), TileGreaterCrystal.class);
			if(tgc != null)
			{
				NBTTagCompound nbt = stack.hasTagCompound() ? stack.getTagCompound() : new NBTTagCompound();
				nbt.setLong("Bound", pos.toLong());
				stack.setTagCompound(nbt);
				HCNet.swingArm(player, hand);
			} else
				NBTUtils.removeTagFromItemStack(stack, "Bound");
		} else
		{
			if(stack.hasTagCompound())
			{
				BlockPos target = BlockPos.fromLong(stack.getTagCompound().getLong("Bound"));
				TileGreaterCrystal tgc = WorldUtil.cast(worldIn.getTileEntity(target), TileGreaterCrystal.class);
				
				if(tgc != null)
				{
					if(tgc.link(pos))
					{
						HCNet.swingArm(player, hand);
						NBTUtils.removeTagFromItemStack(stack, "Bound");
					}
				}
			}
		}
		return EnumActionResult.PASS;
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		RayTraceResult res = RayTracer.retrace(playerIn);
		if(res != null && res.typeOfHit != Type.MISS || !playerIn.isSneaking())
			return super.onItemRightClick(worldIn, playerIn, handIn);
		ItemStack stack = playerIn.getHeldItem(handIn);
		NBTUtils.removeTagFromItemStack(stack, "Bound");
		return new ActionResult<ItemStack>(EnumActionResult.PASS, stack);
	}
	
	@Override
	public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
	{
		if(isSelected && worldIn.isRemote && entityIn instanceof EntityPlayer)
			invokeParticles(stack, worldIn, entityIn, itemSlot, isSelected);
	}
	
	@SideOnly(Side.CLIENT)
	private void invokeParticles(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
	{
		if(isSelected && worldIn.isRemote && entityIn instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) entityIn;
			Vec3d look = Vec3d.fromPitchYaw(0, player.rotationYawHead + 20);
			BlockPos target = null;
			if(stack.hasTagCompound() && stack.getTagCompound().hasKey("Bound"))
				target = BlockPos.fromLong(stack.getTagCompound().getLong("Bound"));
			if(target == null)
				return;
			
			double x = entityIn.posX + look.x;
			double y = entityIn.posY + entityIn.getEyeHeight() * .6;
			double z = entityIn.posZ + look.z;
			
			Vec3d tg = AABBUtils.randomPosWithin(new AxisAlignedBB(target), worldIn.rand);
			FXWisp wisp;
			if(worldIn.getTotalWorldTime() % 20L == 0L)
				Magistry.proxy.spawnSimpleLine(worldIn, new Vec3d(x, y, z), new Vec3d(target).add(.5, .5, .5), worldIn.rand.nextLong(), 5, .1F, new Layer(771, 0xFF33FF, true), new Layer(771, 0xFF33FF, true));
			ParticleProxy_Client.queueParticleSpawn(wisp = new FXWisp(worldIn, x, y, z, tg.x, tg.y, tg.z, .5F, new Layer(771, 0xFF33FF, true)));
			wisp.canCollide = false;
		}
	}
	
	public static BlockPos getCrystalPos(ItemStack stack)
	{
		if(stack.hasTagCompound())
			return BlockPos.fromLong(stack.getTagCompound().getLong("Bound"));
		return null;
	}
}