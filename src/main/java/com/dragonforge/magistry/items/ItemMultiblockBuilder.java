package com.dragonforge.magistry.items;

import java.util.function.Function;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.multiblock.BlockComponent;
import com.dragonforge.magistry.api.multiblock.MultiblockRecipe;
import com.dragonforge.magistry.utils.PosHelpers;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

public class ItemMultiblockBuilder extends ItemBlock
{
	final Function<EntityPlayer, ResourceLocation> multiblock;
	
	public ItemMultiblockBuilder(Block block, Function<EntityPlayer, ResourceLocation> multiblock)
	{
		super(block);
		this.multiblock = multiblock;
	}
	
	@Override
	public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, IBlockState newState)
	{
		MultiblockRecipe r = MagistryAPI.MULTIBLOCK_REGISTRY.getValue(multiblock.apply(player));
		
		if(r != null)
		{
			Vec3i norm = PosHelpers.getHalfNormalized(r.components.keySet());
			
			for(long key : r.components.keySet())
			{
				BlockPos cpos = pos.add(BlockPos.fromLong(key));
				
				cpos = cpos.add(norm.getX(), 0, norm.getZ());
				
				BlockComponent bc = r.components.get(key);
				IBlockState place = bc.input.states[0];
				world.setBlockState(cpos, place);
			}
			
			return true;
		}
		return false;
	}
}