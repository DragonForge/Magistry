package com.dragonforge.magistry.items;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.dragonforge.magistry.init.ItemsM;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Enchantments;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.oredict.OreDictionary;

@EventBusSubscriber
public class ItemGlassCutter extends ItemTool
{
	public static final ToolMaterial GLASS_CUTTER_MATERIAL = EnumHelper.addToolMaterial("MAGISTRY_GLASS_CUTTER", 2, 512, 1F, 4, 0);
	private static final Set<Block> GLASS_BLOCKS = new HashSet<Block>();
	
	public static Set<Block> getGlassBlocks()
	{
		if(GLASS_BLOCKS.isEmpty())
		{
			Block e;
			for(String str : OreDictionary.getOreNames())
				if(str.startsWith("blockGlass"))
					for(ItemStack item : OreDictionary.getOres(str))
						if(!item.isEmpty() && (e = Block.getBlockFromItem(item.getItem())) != null)
							GLASS_BLOCKS.add(e);
		}
		return GLASS_BLOCKS;
	}
	
	public ItemGlassCutter()
	{
		super(GLASS_CUTTER_MATERIAL, GLASS_BLOCKS);
		setTranslationKey("glass_cutter");
	}
	
	@Override
	public float getDestroySpeed(ItemStack stack, IBlockState state)
	{
		getGlassBlocks();
		this.efficiency = 8F;
		return super.getDestroySpeed(stack, state);
	}
	
	@Override
	public boolean canApplyAtEnchantingTable(ItemStack stack, Enchantment enchantment)
	{
		return super.canApplyAtEnchantingTable(stack, enchantment) || enchantment == Enchantments.EFFICIENCY;
	}
	
	@Override
	public boolean isRepairable()
	{
		return true;
	}
	
	@Override
	public boolean getIsRepairable(ItemStack toRepair, ItemStack repair)
	{
		if(!toRepair.isEmpty() && toRepair.getItem() == this && repair.getItem() == ItemsM.BLUE_QUARTZ)
			return true;
		return false;
	}
	
	final Set<String> tools = com.google.common.collect.ImmutableSet.of("glass_cutter");
	
	@Override
	public Set<String> getToolClasses(ItemStack stack)
	{
		return tools;
	}
	
	@Override
	public boolean onBlockDestroyed(ItemStack stack, World worldIn, IBlockState state, BlockPos pos, EntityLivingBase entityLiving)
	{
		return super.onBlockDestroyed(stack, worldIn, state, pos, entityLiving);
	}
	
	@Override
	public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
	{
		if(EnchantmentHelper.getEnchantmentLevel(Enchantments.SILK_TOUCH, stack) > 0)
		{
			Map<Enchantment, Integer> enchMap = EnchantmentHelper.getEnchantments(stack);
			enchMap.remove(Enchantments.SILK_TOUCH);
			EnchantmentHelper.setEnchantments(enchMap, stack);
		}
	}
	
	@SubscribeEvent
	public static void blockBreakPre(BlockEvent.BreakEvent e)
	{
		ItemStack mainhand = e.getPlayer().getHeldItemMainhand();
		if(!mainhand.isEmpty() && mainhand.getItem() instanceof ItemGlassCutter && GLASS_BLOCKS.contains(e.getState().getBlock()))
		{
			Map<Enchantment, Integer> enchMap = EnchantmentHelper.getEnchantments(mainhand);
			enchMap.put(Enchantments.SILK_TOUCH, 1);
			EnchantmentHelper.setEnchantments(enchMap, mainhand);
			e.setExpToDrop(0);
		}
	}
	
	@SubscribeEvent
	public static void blockDrops(BlockEvent.HarvestDropsEvent e)
	{
		ItemStack mainhand = e.getHarvester() != null ? e.getHarvester().getHeldItemMainhand() : ItemStack.EMPTY;
		if(!mainhand.isEmpty() && mainhand.getItem() instanceof ItemGlassCutter)
		{
			Map<Enchantment, Integer> enchMap = EnchantmentHelper.getEnchantments(mainhand);
			enchMap.remove(Enchantments.SILK_TOUCH);
			EnchantmentHelper.setEnchantments(enchMap, mainhand);
		}
	}
}