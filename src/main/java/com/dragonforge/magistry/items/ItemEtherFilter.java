package com.dragonforge.magistry.items;

import com.dragonforge.magistry.api.item.IEtherFilter;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemEtherFilter extends Item implements IEtherFilter
{
	public ItemEtherFilter()
	{
		setTranslationKey("ether_filter");
		setMaxDamage(48);
	}
	
	@Override
	public boolean canFilter(ItemStack stack)
	{
		return true;
	}
	
	@Override
	public float getEtherRate(ItemStack stack)
	{
		return 1F + (stack.getItemDamage() / (float) stack.getItemDamage()) * 3F;
	}
	
	@Override
	public ItemStack onFilter(ItemStack stack)
	{
		stack = IEtherFilter.super.onFilter(stack);
		stack.setItemDamage(stack.getItemDamage() + 1);
		if(stack.getItemDamage() >= stack.getMaxDamage())
			return ItemStack.EMPTY;
		return stack;
	}
}