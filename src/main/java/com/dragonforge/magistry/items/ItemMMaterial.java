package com.dragonforge.magistry.items;

import com.zeitheron.hammercore.utils.IRegisterListener;

import net.minecraft.item.Item;
import net.minecraftforge.oredict.OreDictionary;

public class ItemMMaterial extends Item implements IRegisterListener
{
	String[] ods;
	
	public ItemMMaterial(String name, String... oredictNames)
	{
		setTranslationKey(name);
		this.ods = oredictNames;
	}
	
	public ItemMMaterial(String name, int maxSS, String... oredictNames)
	{
		setTranslationKey(name);
		setMaxStackSize(maxSS);
		this.ods = oredictNames;
	}
	
	@Override
	public void onRegistered()
	{
		if(ods != null)
			for(String s : ods)
				OreDictionary.registerOre(s, this);
	}
}