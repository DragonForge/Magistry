package com.dragonforge.magistry.scans;

import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.scan.IScanObject;

import net.minecraft.block.Block;
import net.minecraft.block.BlockBrewingStand;
import net.minecraft.block.BlockCauldron;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;

public class MScanAlchemy implements IScanObject
{
	@Override
	public String getScanKey(EntityPlayerMP player, IPlayerKnowledge knowledge)
	{
		return "s_alchemy";
	}
	
	@Override
	public boolean isThisObject(EntityPlayerMP player, IPlayerKnowledge knowledge, Object v)
	{
		if(v instanceof ItemStack)
		{
			ItemStack stack = (ItemStack) v;
			Block bl = Block.getBlockFromItem(stack.getItem());
			if(stack.getItem() instanceof ItemPotion || bl instanceof BlockBrewingStand || bl instanceof BlockCauldron)
				return true;
		}
		return false;
	}
}