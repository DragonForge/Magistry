package com.dragonforge.magistry.scans;

import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.scan.IScanObject;

import net.minecraft.block.Block;
import net.minecraft.block.IGrowable;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;

public class MScanPlant implements IScanObject
{
	@Override
	public String getScanKey(EntityPlayerMP player, IPlayerKnowledge knowledge)
	{
		return "s_flora";
	}
	
	@Override
	public boolean isThisObject(EntityPlayerMP player, IPlayerKnowledge knowledge, Object v)
	{
		if(v instanceof IBlockState)
			v = ((IBlockState) v).getBlock();
		Material mat = v instanceof Block ? ((Block) v).material : null;
		return v instanceof IGrowable || mat == Material.PLANTS || mat == Material.LEAVES || mat == Material.CACTUS || mat == Material.VINE;
	}
}