package com.dragonforge.magistry.scans;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.scan.IScanObject;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class MScanHeat implements IScanObject
{
	@Override
	public String getScanKey(EntityPlayerMP player, IPlayerKnowledge knowledge)
	{
		return "s_heat";
	}
	
	@Override
	public boolean isThisObject(EntityPlayerMP player, IPlayerKnowledge knowledge, Object v)
	{
		if(v instanceof FluidStack && ((FluidStack) v).getFluid().getTemperature((FluidStack) v) >= 1274)
			return true;
		if(v instanceof Fluid && ((Fluid) v).getTemperature() >= 1274)
			return true;
		if(v instanceof IBlockState && MagistryAPI.getHotBlocks().contains(((IBlockState) v).getBlock()))
			return true;
		if(v instanceof Block && MagistryAPI.getHotBlocks().contains(v))
			return true;
		if(v instanceof ItemStack)
		{
			ItemStack stack = (ItemStack) v;
			if(stack.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY, null))
			{
				IFluidHandlerItem i = stack.getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY, null);
				if(i != null)
					for(IFluidTankProperties p : i.getTankProperties())
					{
						FluidStack fs = p.getContents();
						if(fs != null && fs.getFluid().getTemperature(fs) >= 1274)
							return true;
					}
			}
		}
		return false;
	}
}