package com.dragonforge.magistry.scans;

import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.scan.IScanObject;
import com.dragonforge.magistry.blocks.BlockPedestal;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;

public class MScanLight implements IScanObject
{
	@Override
	public boolean isThisObject(EntityPlayerMP player, IPlayerKnowledge knowledge, Object v)
	{
		if(v instanceof ItemStack)
		{
			if(BlockPedestal.getLightForItem((ItemStack) v) > 0)
				return true;
		}
		if(v instanceof IBlockState)
		{
			IBlockState state = (IBlockState) v;
			if(state.getBlock().getLightValue(state) > 0)
				return true;
		}
		return false;
	}

	@Override
	public String getScanKey(EntityPlayerMP player, IPlayerKnowledge knowledge)
	{
		return "s_light";
	}
}