package com.dragonforge.magistry.compat.jei;

import com.dragonforge.magistry.api.crafting.RecipesCrusher;
import com.dragonforge.magistry.api.crafting.RecipesInductor;
import com.dragonforge.magistry.api.crafting.recipes.CrusherRecipe;
import com.dragonforge.magistry.api.crafting.recipes.InductorRecipe;
import com.dragonforge.magistry.compat.jei.core.IJEICompat;
import com.dragonforge.magistry.compat.jei.crusher.CrusherCategory;
import com.dragonforge.magistry.compat.jei.crusher.CrusherWrapper;
import com.dragonforge.magistry.compat.jei.inductor.InductorCategory;
import com.dragonforge.magistry.compat.jei.inductor.InductorWrapper;
import com.dragonforge.magistry.init.BlocksM;
import com.dragonforge.magistry.init.ItemsM;

import mezz.jei.api.IJeiRuntime;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.JEIPlugin;
import mezz.jei.api.recipe.IFocus;
import mezz.jei.api.recipe.IRecipeCategoryRegistration;
import net.minecraft.item.ItemStack;

@JEIPlugin
public class JEICompatImpl implements IJEICompat, IModPlugin
{
	{
		JEICompat.ct = this;
	}
	
	IJeiRuntime runtime;
	
	@Override
	public void onRuntimeAvailable(IJeiRuntime jeiRuntime)
	{
		runtime = jeiRuntime;
	}
	
	@Override
	public void register(IModRegistry registry)
	{
		registry.handleRecipes(InductorRecipe.class, InductorWrapper::new, JEICompat.INDUCTOR_ID);
		registry.addRecipeCatalyst(new ItemStack(BlocksM.INDUCTOR), JEICompat.INDUCTOR_ID);
		registry.addRecipes(RecipesInductor.RECIPES, JEICompat.INDUCTOR_ID);
		
		registry.handleRecipes(CrusherRecipe.class, CrusherWrapper::new, JEICompat.CRUSHER_ID);
		registry.addRecipeCatalyst(new ItemStack(BlocksM.CRUSHER), JEICompat.CRUSHER_ID);
		registry.addRecipes(RecipesCrusher.RECIPES, JEICompat.CRUSHER_ID);
		
		registry.addIngredientInfo(new ItemStack(BlocksM.NATURAL_CRYSTAL), ItemStack.class, "description.magistry.natural_crystal");
		registry.addIngredientInfo(new ItemStack(ItemsM.ASH), ItemStack.class, "description.magistry.ash");
	}
	
	@Override
	public void registerCategories(IRecipeCategoryRegistration registry)
	{
		registry.addRecipeCategories(new InductorCategory(), new CrusherCategory());
	}
}