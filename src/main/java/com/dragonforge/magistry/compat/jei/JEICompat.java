package com.dragonforge.magistry.compat.jei;

import com.dragonforge.magistry.compat.crafttweaker.CTCompatImpl;
import com.dragonforge.magistry.compat.crafttweaker.CraftTweakerCompat;
import com.dragonforge.magistry.compat.crafttweaker.core.ICTCompat;
import com.dragonforge.magistry.compat.jei.core.IJEICompat;

import net.minecraftforge.fml.common.Loader;

public class JEICompat implements IJEICompat
{
	static IJEICompat ct = null;
	
	public static final String INDUCTOR_ID = "magistry.inductor";
	public static final String CRUSHER_ID = "magistry.crusher";
	
	public static IJEICompat compat()
	{
		if(ct == null)
			ct = new JEICompat();
		return ct;
	}
}