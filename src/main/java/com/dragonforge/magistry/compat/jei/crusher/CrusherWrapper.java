package com.dragonforge.magistry.compat.jei.crusher;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.dragonforge.magistry.api.crafting.recipes.CrusherRecipe;
import com.dragonforge.magistry.api.crafting.recipes.InductorRecipe;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.utils.FileSizeMetric;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;

public class CrusherWrapper implements IRecipeWrapper
{
	public final CrusherRecipe recipe;
	
	public CrusherWrapper(CrusherRecipe recipe)
	{
		this.recipe = recipe;
	}
	
	int sy;
	
	@Override
	public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY)
	{
		List<ItemStack> ins = recipe.outputs;
		
		GuiTexBakery gtb = GuiTexBakery.start();
		for(int i = 0; i < ins.size(); ++i)
			gtb.slot(44 + (i % 3) * 18, 18 * 3 - (int) (Math.ceil(ins.size() / 3F) * 9F) + i / 3 * 18);
		gtb.slot(0, sy = 18 * 3 - 9);
		gtb.bake().render(0, 0);
		GuiWidgets.drawFurnaceArrow(20, sy + 1, System.currentTimeMillis() % 10000L / 10000F);
	}
	
	@Override
	public List<String> getTooltipStrings(int mouseX, int mouseY)
	{
		if(mouseX >= 20 && mouseY >= sy + 1 && mouseX < 44 && mouseY < sy + 17)
			return Arrays.asList(I18n.format("tooltip.magistry.cucost", FileSizeMetric.format.format(recipe.cost)));
		return Collections.emptyList();
	}
	
	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setInputs(ItemStack.class, Arrays.asList(recipe.input.getMatchingStacks()));
		ingredients.setOutputs(ItemStack.class, recipe.outputs);
	}
}