package com.dragonforge.magistry.compat.jei.inductor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.dragonforge.magistry.api.crafting.recipes.InductorRecipe;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;

public class InductorWrapper implements IRecipeWrapper
{
	public final InductorRecipe recipe;
	
	public InductorWrapper(InductorRecipe recipe)
	{
		this.recipe = recipe;
	}
	
	@Override
	public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY)
	{
		List<Ingredient> ins = recipe.inputs;
		
		GuiTexBakery gtb = GuiTexBakery.start();
		for(int i = 0; i < ins.size(); ++i)
			gtb.slot((i % 3) * 18, 18 * 3 - (int) (Math.ceil(ins.size() / 3F) * 9F) + i / 3 * 18);
		int sy;
		gtb.slot(82, sy = 18 * 3 - 9);
		gtb.bake().render(0, 0);
		GuiWidgets.drawFurnaceArrow(57, sy + 1, System.currentTimeMillis() % 10000L / 10000F);
	}
	
	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setOutput(ItemStack.class, recipe.output.copy());
		ingredients.setInputLists(ItemStack.class, recipe.inputs.stream().map(i -> Arrays.asList(i.getMatchingStacks())).collect(Collectors.toList()));
	}
}