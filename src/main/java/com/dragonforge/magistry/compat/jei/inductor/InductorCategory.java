package com.dragonforge.magistry.compat.jei.inductor;

import java.util.Arrays;
import java.util.List;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.compat.jei.JEICompat;
import com.dragonforge.magistry.init.BlocksM;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;

import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.crafting.Ingredient;

public class InductorCategory implements IRecipeCategory<InductorWrapper>
{
	@Override
	public String getUid()
	{
		return JEICompat.INDUCTOR_ID;
	}
	
	@Override
	public String getTitle()
	{
		return I18n.format(BlocksM.INDUCTOR.getTranslationKey() + ".name");
	}
	
	@Override
	public String getModName()
	{
		return Magistry.MOD_NAME;
	}
	
	IDrawable bg = new IDrawable()
	{
		DynGuiTex tex;
		
		public void init()
		{
			GuiTexBakery g = GuiTexBakery.start();
			tex = g.bake();
		}
		
		@Override
		public int getWidth()
		{
			return 100;
		}
		
		@Override
		public int getHeight()
		{
			return 18 * 6;
		}
		
		@Override
		public void draw(Minecraft minecraft, int xOffset, int yOffset)
		{
			if(tex == null)
				init();
			tex.render(xOffset, yOffset);
		}
	};
	
	@Override
	public IDrawable getBackground()
	{
		return bg;
	}
	
	@Override
	public void setRecipe(IRecipeLayout recipeLayout, InductorWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiItemStackGroup items = recipeLayout.getItemStacks();
		List<Ingredient> ins = recipeWrapper.recipe.inputs;
		for(int i = 0; i < ins.size(); ++i)
		{
			recipeLayout.setShapeless();
			items.init(1 + i, true, (i % 3) * 18, 18 * 3 - (int) (Math.ceil(ins.size() / 3F) * 9F) + i / 3 * 18);
			items.set(1 + i, Arrays.asList(ins.get(i).getMatchingStacks()));
		}
		
		items.init(0, false, 82, 18 * 3 - 9);
		items.set(0, recipeWrapper.recipe.output.copy());
	}
}