package com.dragonforge.magistry.compat.crafttweaker;

import java.util.LinkedList;

import com.dragonforge.magistry.compat.crafttweaker.core.ICTCompat;

import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;

public class CTCompatImpl implements ICTCompat
{
	private static final LinkedList<IAction> lateActions = new LinkedList<>();
	
	@Override
	public void onLoadComplete()
	{
		lateActions.forEach(CraftTweakerAPI::apply);
	}
	
	@Override
	public void addLateAction(Object action)
	{
		lateActions.addLast((IAction) action);
	}
}