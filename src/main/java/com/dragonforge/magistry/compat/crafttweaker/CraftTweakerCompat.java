package com.dragonforge.magistry.compat.crafttweaker;

import com.dragonforge.magistry.compat.crafttweaker.core.ICTCompat;

import net.minecraftforge.fml.common.Loader;

public class CraftTweakerCompat implements ICTCompat
{
	private static ICTCompat ct = null;
	
	public static ICTCompat compat()
	{
		if(ct == null)
			if(Loader.isModLoaded("crafttweaker"))
				ct = new CTCompatImpl();
			else
				ct = new CraftTweakerCompat();
		return ct;
	}
	
	@Override
	public void onLoadComplete()
	{
	}

	@Override
	public void addLateAction(Object action)
	{
	}
}