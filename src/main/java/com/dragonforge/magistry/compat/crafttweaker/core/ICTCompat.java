package com.dragonforge.magistry.compat.crafttweaker.core;

public interface ICTCompat
{
	void onLoadComplete();
	
	void addLateAction(Object action);
}