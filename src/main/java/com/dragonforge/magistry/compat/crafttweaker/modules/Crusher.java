package com.dragonforge.magistry.compat.crafttweaker.modules;

import com.dragonforge.magistry.api.crafting.RecipesCrusher;
import com.dragonforge.magistry.api.crafting.RecipesInductor;
import com.dragonforge.magistry.api.crafting.RecipesInductor.InductorBlacklistStack;
import com.dragonforge.magistry.api.crafting.recipes.CrusherRecipe;
import com.dragonforge.magistry.api.crafting.recipes.InductorRecipe;
import com.dragonforge.magistry.compat.crafttweaker.CraftTweakerCompat;
import com.dragonforge.magistry.compat.crafttweaker.core.BaseAction;
import com.dragonforge.magistry.compat.crafttweaker.core.InputHelper;

import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenRegister
@ZenClass("mods.magistry.Crusher")
public class Crusher
{
	@ZenMethod
	public static void add(IIngredient input, float cu, IItemStack... outputs)
	{
		Ingredient in = InputHelper.toIngredient(input);
		ItemStack[] out = new ItemStack[outputs.length];
		for(int i = 0; i < out.length; ++i)
			out[i] = InputHelper.toStack(outputs[i]);
		CraftTweakerCompat.compat().addLateAction(new Add(new CrusherRecipe(in, cu, out)));
	}
	
	@ZenMethod
	public static void remove(IItemStack input)
	{
		final ItemStack nms = InputHelper.toStack(input);
		RecipesCrusher.RECIPES.stream().filter(ir -> ir.input.test(nms)).forEach(ir -> CraftTweakerCompat.compat().addLateAction(new Rem(ir)));
	}
	
	private static final class Add extends BaseAction
	{
		private Add(CrusherRecipe rec)
		{
			super("Crusher", rec::add);
		}
	}
	
	private static final class Rem extends BaseAction
	{
		private Rem(CrusherRecipe rec)
		{
			super("Crusher", rec::remove);
		}
	}
	
	private static final class BlacklistOutput extends BaseAction
	{
		private BlacklistOutput(InductorBlacklistStack rec)
		{
			super("Inductor", rec::add);
		}
	}
}