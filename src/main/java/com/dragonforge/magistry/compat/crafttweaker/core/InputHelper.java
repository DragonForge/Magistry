package com.dragonforge.magistry.compat.crafttweaker.core;

import java.util.stream.Collectors;

import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.oredict.IngredientOreDict;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraftforge.oredict.OreIngredient;

public class InputHelper
{
	public static Ingredient toIngredient(IIngredient iIng)
	{
		if(iIng == null)
			return Ingredient.EMPTY;
		
		Object internal = iIng.getInternal();
		
		if(iIng instanceof IngredientOreDict)
		{
			IngredientOreDict o = (IngredientOreDict) iIng;
			internal = o.getItems().stream().map(InputHelper::toStack).collect(Collectors.toList()).toArray(new ItemStack[0]);
		}
		
		if(internal instanceof ItemStack)
			return Ingredient.fromStacks((ItemStack) internal);
		
		if(internal instanceof ItemStack[])
			return Ingredient.fromStacks((ItemStack[]) internal);
		
		if(internal instanceof String)
			return Ingredient.fromStacks(new OreIngredient((String) internal).getMatchingStacks());
		
		if(!(internal instanceof Ingredient))
			return Ingredient.EMPTY;
		
		return (Ingredient) internal;
	}
	
	public static ItemStack toStack(IItemStack iStack)
	{
		if(iStack == null)
			return ItemStack.EMPTY;
		
		Object internal = iStack.getInternal();
		
		if(!(internal instanceof ItemStack))
			return ItemStack.EMPTY;
		
		return (ItemStack) internal;
	}
}