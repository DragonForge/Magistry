package com.dragonforge.magistry.compat.crafttweaker.modules;

import com.dragonforge.magistry.api.crafting.RecipesPress;
import com.dragonforge.magistry.api.crafting.recipes.PressRecipe;
import com.dragonforge.magistry.api.item.CountableIngredient;
import com.dragonforge.magistry.compat.crafttweaker.CraftTweakerCompat;
import com.dragonforge.magistry.compat.crafttweaker.core.BaseAction;
import com.dragonforge.magistry.compat.crafttweaker.core.InputHelper;

import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenRegister
@ZenClass("mods.magistry.Press")
public class Press
{
	@ZenMethod
	public static void add(IIngredient mold, IIngredient input, IItemStack output)
	{
		CraftTweakerCompat.compat().addLateAction(new Add(new PressRecipe(InputHelper.toStack(output), InputHelper.toIngredient(mold), new CountableIngredient(InputHelper.toIngredient(input)))));
	}
	
	@ZenMethod
	public static void add(IIngredient mold, IIngredient input, int inputCount, IItemStack output)
	{
		CraftTweakerCompat.compat().addLateAction(new Add(new PressRecipe(InputHelper.toStack(output), InputHelper.toIngredient(mold), new CountableIngredient(InputHelper.toIngredient(input), inputCount))));
	}
	
	@ZenMethod
	public static void remove(IItemStack result)
	{
		final ItemStack nms = InputHelper.toStack(result);
		RecipesPress.RECIPES.stream().filter(ir -> ir.output.isItemEqual(nms)).forEach(ir -> CraftTweakerCompat.compat().addLateAction(new Rem(ir)));
	}
	
	private static final class Add extends BaseAction
	{
		private Add(PressRecipe rec)
		{
			super("Press", rec::add);
		}
	}
	
	private static final class Rem extends BaseAction
	{
		private Rem(PressRecipe rec)
		{
			super("Press", rec::remove);
		}
	}
}