package com.dragonforge.magistry.compat.crafttweaker.modules;

import com.dragonforge.magistry.api.crafting.RecipesInductor;
import com.dragonforge.magistry.api.crafting.RecipesInductor.InductorBlacklistStack;
import com.dragonforge.magistry.api.crafting.recipes.InductorRecipe;
import com.dragonforge.magistry.compat.crafttweaker.CraftTweakerCompat;
import com.dragonforge.magistry.compat.crafttweaker.core.BaseAction;
import com.dragonforge.magistry.compat.crafttweaker.core.InputHelper;

import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenRegister
@ZenClass("mods.magistry.Inductor")
public class Inductor
{
	@ZenMethod
	public static void add(IItemStack output, IIngredient... ingredients)
	{
		Ingredient[] nmc = new Ingredient[ingredients.length];
		for(int i = 0; i < nmc.length; ++i)
			nmc[i] = InputHelper.toIngredient(ingredients[i]);
		CraftTweakerCompat.compat().addLateAction(new Add(new InductorRecipe(InputHelper.toStack(output), nmc)));
	}
	
	@ZenMethod
	public static void remove(IItemStack output)
	{
		final ItemStack nms = InputHelper.toStack(output);
		RecipesInductor.RECIPES.stream().filter(ir -> ir.output.isItemEqual(nms)).forEach(ir -> CraftTweakerCompat.compat().addLateAction(new Rem(ir)));
	}
	
	@ZenMethod
	public static void addBlacklistOutput(IItemStack output)
	{
		final ItemStack nms = InputHelper.toStack(output);
		if(!nms.isEmpty())
			CraftTweakerCompat.compat().addLateAction(new BlacklistOutput(new InductorBlacklistStack(nms)));
	}
	
	private static final class Add extends BaseAction
	{
		private Add(InductorRecipe rec)
		{
			super("Inductor", rec::add);
		}
	}
	
	private static final class Rem extends BaseAction
	{
		private Rem(InductorRecipe rec)
		{
			super("Inductor", rec::remove);
		}
	}
	
	private static final class BlacklistOutput extends BaseAction
	{
		private BlacklistOutput(InductorBlacklistStack rec)
		{
			super("Inductor", rec::add);
		}
	}
}