package com.dragonforge.magistry.commands;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.google.common.base.Predicates;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.CommandGive;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraftforge.server.command.CommandTreeBase;

public class CommandMagistry extends CommandTreeBase
{
	@Override
	public int getRequiredPermissionLevel()
	{
		return 2;
	}
	
	@Override
	public String getName()
	{
		return "magistry";
	}
	
	@Override
	public String getUsage(ICommandSender sender)
	{
		return "commands.usage.magistry";
	}
	
	{
		addSubcommand(new CommandBase()
		{
			@Override
			public String getUsage(ICommandSender sender)
			{
				return "commands.usage.magistry.research";
			}
			
			@Override
			public String getName()
			{
				return "research";
			}
			
			@Override
			public int getRequiredPermissionLevel()
			{
				return 2;
			}
			
			final List<String> operation = Arrays.asList("give", "revoke");
			final List<String> match = Arrays.asList("everything", "only");
			
			@Override
			public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
			{
				if(args.length == 0)
					throw new CommandException("Missing required sub-argument: <give/revoke>");
				if(!operation.contains(args[0].toLowerCase()))
					throw new CommandException("Invalid required sub-argument: <give/revoke>");
				if(args.length == 1)
					throw new CommandException("Missing required sub-argument: <player>");
				List<EntityPlayerMP> players = getPlayers(server, sender, args[1]);
				if(players.isEmpty())
					throw new PlayerNotFoundException("commands.generic.player.notFound", args[1]);
				if(args.length == 2)
					throw new CommandException("Missing required sub-argument: <everything/only>");
				if(!match.contains(args[2].toLowerCase()))
					throw new CommandException("Invalid required sub-argument: <everything/only>");
				boolean only = args[2].equalsIgnoreCase("only");
				boolean revoke = args[0].equalsIgnoreCase("revoke");
				if(only)
				{
					if(args.length == 3)
						throw new CommandException("Missing required sub-argument: <research>");
					String res = args[3];
					if(res == null)
						throw new CommandException("Invalid required sub-argument: <research>");
					players.stream().map(MagistryAPI::getPlayerKnowledge).filter(Predicates.notNull()).forEach(k ->
					{
						boolean did = false;
						
						if(revoke)
							did = k.getListOfKnownResearches().remove(res);
						else
							did = k.unlockResearch(res, true);
						
						k.sync(null);
						
						TextComponentTranslation inner = new TextComponentTranslation("research." + res + ".name");
						
						inner.getStyle().setColor(TextFormatting.GREEN);
						// inner.getStyle().setHoverEvent(new
						// HoverEvent(HoverEvent.Action.SHOW_TEXT, new
						// TextComponentTranslation("")));
						
						TextComponentTranslation send = new TextComponentTranslation("tooltip." + Magistry.MOD_ID + (revoke ? ".revokedknowledge" : ".unlockedknowledge"), sender.getDisplayName(), inner);
						
						if(did)
							k.getOwner().sendMessage(send);
					});
				} else
					players.stream().map(MagistryAPI::getPlayerKnowledge).filter(Predicates.notNull()).forEach(k ->
					{
						MagistryAPI.RESEARCH_REGISTRY.getKeys().stream().map(ResourceLocation::toString).forEach(r ->
						{
							if(revoke)
								k.getListOfKnownResearches().remove(r);
							else
								k.unlockResearch(r, true);
						});
						
						k.sync(null);
						
						k.getOwner().sendMessage(new TextComponentTranslation("tooltip." + Magistry.MOD_ID + (revoke ? ".revokedallknowledge" : ".unlockedallknowledge"), sender.getDisplayName()));
					});
			}
			
			@Override
			public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos targetPos)
			{
				List<EntityPlayerMP> players;
				try
				{
					return args.length == 1 ? getListOfStringsMatchingLastWord(args, operation) : args.length == 2 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : args.length == 3 ? getListOfStringsMatchingLastWord(args, match) : args.length == 4 && args[2].equalsIgnoreCase("only") && !(players = getPlayers(server, sender, args[1])).isEmpty() ? getListOfStringsMatchingLastWord(args, Stream.concat(players.stream().map(MagistryAPI::getPlayerKnowledge).map(IPlayerKnowledge::getListOfKnownResearches).flatMap(List::stream), MagistryAPI.RESEARCH_REGISTRY.getKeys().stream().map(Object::toString)).collect(Collectors.toSet())) : Collections.emptyList();
				} catch(CommandException e)
				{
					return Collections.emptyList();
				}
			}
		});
	}
}