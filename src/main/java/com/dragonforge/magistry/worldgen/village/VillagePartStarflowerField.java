package com.dragonforge.magistry.worldgen.village;

import java.util.List;
import java.util.Random;

import com.dragonforge.magistry.blocks.BlockStarflower;
import com.dragonforge.magistry.init.BlocksM;

import net.minecraft.block.BlockFarmland;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.gen.structure.MapGenStructureIO;
import net.minecraft.world.gen.structure.StructureBoundingBox;
import net.minecraft.world.gen.structure.StructureComponent;
import net.minecraft.world.gen.structure.StructureVillagePieces;
import net.minecraft.world.gen.structure.StructureVillagePieces.PieceWeight;
import net.minecraft.world.gen.structure.StructureVillagePieces.Start;
import net.minecraft.world.gen.structure.StructureVillagePieces.Village;
import net.minecraftforge.fml.common.registry.VillagerRegistry.IVillageCreationHandler;

public class VillagePartStarflowerField implements IVillageCreationHandler
{
	static
	{
		MapGenStructureIO.registerStructureComponent(StarflowerField.class, "MagistryViSFF");
	}
	
	@Override
	public PieceWeight getVillagePieceWeight(Random random, int i)
	{
		return new PieceWeight(StarflowerField.class, 3, MathHelper.getInt(random, 0, 2));
	}
	
	@Override
	public Class<?> getComponentClass()
	{
		return StarflowerField.class;
	}
	
	@Override
	public Village buildComponent(PieceWeight villagePiece, Start startPiece, List<StructureComponent> pieces, Random random, int p1, int p2, int p3, EnumFacing facing, int p5)
	{
		return StarflowerField.createPiece(startPiece, pieces, random, p1, p2, p3, facing, p5);
	}
	
	public static class StarflowerField extends StructureVillagePieces.Village
	{
		public StarflowerField()
		{
		}
		
		public StarflowerField(StructureVillagePieces.Start start, int type, Random rand, StructureBoundingBox p_i45570_4_)
		{
			super(start, type);
			this.setCoordBaseMode(EnumFacing.SOUTH);
			this.boundingBox = p_i45570_4_;
		}
		
		public static StarflowerField createPiece(StructureVillagePieces.Start start, List<StructureComponent> p_175851_1_, Random rand, int p_175851_3_, int p_175851_4_, int p_175851_5_, EnumFacing facing, int p_175851_7_)
		{
			StructureBoundingBox structureboundingbox = StructureBoundingBox.getComponentToAddBoundingBox(p_175851_3_, p_175851_4_, p_175851_5_, 0, 0, 0, 13, 4, 9, facing);
			return canVillageGoDeeper(structureboundingbox) && StructureComponent.findIntersecting(p_175851_1_, structureboundingbox) == null ? new StarflowerField(start, p_175851_7_, rand, structureboundingbox) : null;
		}
		
		@Override
		public boolean addComponentParts(World worldIn, Random randomIn, StructureBoundingBox structureBoundingBoxIn)
		{
			if(this.averageGroundLvl < 0)
			{
				this.averageGroundLvl = this.getAverageGroundLevel(worldIn, structureBoundingBoxIn);
				
				if(this.averageGroundLvl < 0)
				{
					return true;
				}
				
				this.boundingBox.offset(0, this.averageGroundLvl - this.boundingBox.maxY + 4 - 1, 0);
			}
			
			int size = 9;
			
			fillWithAir(worldIn, structureBoundingBoxIn, 0, 0, 0, size - 1, 11, size - 1);
			fillWithBlocks(worldIn, structureBoundingBoxIn, 0, 0, 0, size - 1, 0, size - 1, BlocksM.DARK_WOOD_LOG.getDefaultState(), BlocksM.DARK_WOOD_LOG.getDefaultState(), false);
			fillWithBlocks(worldIn, structureBoundingBoxIn, 1, 0, 1, size - 2, 0, size - 2, Blocks.FARMLAND.getDefaultState().withProperty(BlockFarmland.MOISTURE, 7), Blocks.FARMLAND.getDefaultState(), false);
			setBlockState(worldIn, Blocks.WATER.getDefaultState(), size / 2, 0, size / 2, structureBoundingBoxIn);
				
			for(int j2 = 1; j2 < size - 1; ++j2)
				for(int k2 = 1; k2 < size - 1; ++k2)
					if(j2 != size / 2 || k2 != size / 2)
						setBlockState(worldIn, BlocksM.STARFLOWER.getDefaultState().withProperty(BlockStarflower.NATURAL, true).withProperty(BlockStarflower.TYPE, randomIn.nextInt(2) * 2), j2, 1, k2, structureBoundingBoxIn);
			
			for(int j2 = 0; j2 <= size; ++j2)
				for(int k2 = 0; k2 <= size; ++k2)
				{
					this.clearCurrentPositionBlocksUpwards(worldIn, k2, 4, j2, structureBoundingBoxIn);
					this.replaceAirAndLiquidDownwards(worldIn, Blocks.DIRT.getDefaultState(), k2, -1, j2, structureBoundingBoxIn);
				}
			
			return true;
		}
	}
}