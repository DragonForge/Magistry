package com.dragonforge.magistry.worldgen;

import java.util.Random;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.aura.IChunkAura;

import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGenAura implements IWorldGenerator
{
	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider)
	{
		Chunk chunk = world.getChunk(chunkX, chunkZ);
		IChunkAura ica = MagistryAPI.getAura(chunk);
		if(!ica.hasGenerated())
			ica.generate(random);
	}
}