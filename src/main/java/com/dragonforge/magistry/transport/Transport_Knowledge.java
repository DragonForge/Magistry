package com.dragonforge.magistry.transport;

import java.io.IOException;
import java.io.InputStream;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.gui.IGuiKnowledgeAcceptor;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.zeitheron.hammercore.net.transport.ITransportAcceptor;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Transport_Knowledge implements ITransportAcceptor
{
	@Override
	public void read(InputStream readable, int length)
	{
		try
		{
			loadKnowledge(CompressedStreamTools.readCompressed(readable));
		} catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	@SideOnly(Side.CLIENT)
	private void loadKnowledge(NBTTagCompound nbt)
	{
		IPlayerKnowledge know = MagistryAPI.getPlayerKnowledge(Minecraft.getMinecraft().player);
		know.deserializeNBT(nbt);
		IGuiKnowledgeAcceptor a = WorldUtil.cast(Minecraft.getMinecraft().currentScreen, IGuiKnowledgeAcceptor.class);
		if(a != null)
			a.accept(know);
	}
}