package com.dragonforge.magistry.proxy;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.aura.ChunkAuraImpl;
import com.dragonforge.magistry.client.MagistryFonts;
import com.dragonforge.magistry.client.teisr.TEISRCrystalStaff;
import com.dragonforge.magistry.client.teisr.TEISRLostNotes;
import com.dragonforge.magistry.client.tesr.TESRCrusher;
import com.dragonforge.magistry.client.tesr.TESREtherCollector;
import com.dragonforge.magistry.client.tesr.TESRGenerator;
import com.dragonforge.magistry.client.tesr.TESRGreaterCrystal;
import com.dragonforge.magistry.client.tesr.TESRInductor;
import com.dragonforge.magistry.client.tesr.TESRNaturalCrystal;
import com.dragonforge.magistry.client.tesr.TESROreCrystal;
import com.dragonforge.magistry.client.tesr.TESRResearchTable;
import com.dragonforge.magistry.client.tex.TextureRandomSparkling;
import com.dragonforge.magistry.init.BlocksM;
import com.dragonforge.magistry.init.ItemsM;
import com.dragonforge.magistry.net.PacketCreateLine;
import com.zeitheron.hammercore.client.particle.def.thunder.ThunderHelper;
import com.zeitheron.hammercore.client.render.item.ItemRenderingHandler;
import com.zeitheron.hammercore.client.utils.texture.TextureFXManager;
import com.zeitheron.hammercore.net.internal.thunder.Thunder;
import com.zeitheron.hammercore.net.internal.thunder.Thunder.Layer;

import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class ClientProxy extends CommonProxy
{
	public static ChunkAuraImpl clientAuraChunk = new ChunkAuraImpl();
	
	@Override
	public void readClientAura(NBTTagCompound nbt)
	{
		clientAuraChunk.deserializeNBT(nbt);
	}
	
	@Override
	public void preInit()
	{
		TextureFXManager.INSTANCE.registerIcon(Magistry.MOD_ID + ":builtin/sparking_stardust", new TextureRandomSparkling("stardust", new ResourceLocation(Magistry.MOD_ID, "textures/items/stardust.png")).withColor(230, 164, 242).texture);
		TextureFXManager.INSTANCE.registerIcon(Magistry.MOD_ID + ":builtin/sparking_starseeds", new TextureRandomSparkling("starseeds", new ResourceLocation(Magistry.MOD_ID, "textures/items/starflower_seeds.png")).withColor(230, 164, 242).texture);
		TextureFXManager.INSTANCE.registerIcon(Magistry.MOD_ID + ":builtin/sparking_blue_quartz", new TextureRandomSparkling("blue_quartz", new ResourceLocation(Magistry.MOD_ID, "textures/items/blue_quartz.png")).withColor(238, 242, 249).texture);
		TextureFXManager.INSTANCE.registerIcon(Magistry.MOD_ID + ":builtin/sparking_burnt_blue_quartz", new TextureRandomSparkling("burnt_blue_quartz", new ResourceLocation(Magistry.MOD_ID, "textures/items/burnt_blue_quartz.png")).withColor(121, 123, 131).texture);
		TextureFXManager.INSTANCE.registerIcon(Magistry.MOD_ID + ":builtin/sparking_crystal_shard", new TextureRandomSparkling("crystal_shard", new ResourceLocation(Magistry.MOD_ID, "textures/items/crystal_shard.png")).withColor(201, 167, 229).texture);
	}
	
	@Override
	public void init()
	{
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksM.NATURAL_CRYSTAL), TESRNaturalCrystal.INSTANCE);
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksM.ORE_CRYSTAL), TESROreCrystal.INSTANCE);
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksM.GREATER_CRYSTAL), TESRGreaterCrystal.INSTANCE);
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksM.GENERATOR), TESRGenerator.INSTANCE);
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksM.INDUCTOR), TESRInductor.INSTANCE);
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksM.ETHER_COLLECTOR), TESREtherCollector.INSTANCE);
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksM.CRUSHER), TESRCrusher.INSTANCE);
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksM.RESEARCH_TABLE), TESRResearchTable.INSTANCE);
		
		ItemRenderingHandler.INSTANCE.bindItemRender(ItemsM.CRYSTAL_STAFF, new TEISRCrystalStaff());
		ItemRenderingHandler.INSTANCE.appendItemRender(ItemsM.LOST_NOTES, new TEISRLostNotes());
		
		MagistryFonts.init();
	}
	
	@Override
	public void spawnSimpleLine(World world, Vec3d start, Vec3d end, long seed, int age, float fractMod, Layer core, Layer aura)
	{
		if(world.isRemote)
			ThunderHelper.thunder(world, start, end, new Thunder(seed, age, fractMod), core, aura, PacketCreateLine.NO_FRACTAL);
		else
			super.spawnSimpleLine(world, start, end, seed, age, fractMod, core, aura);
	}
}