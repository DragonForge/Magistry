package com.dragonforge.magistry.proxy;

import com.dragonforge.magistry.net.PacketCreateLine;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.thunder.Thunder;
import com.zeitheron.hammercore.utils.ILoadable;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint;

public class CommonProxy implements ILoadable
{
	public static final String BUILD = "DEV_BUILD";
	
	public void construct()
	{
	}
	
	public void readClientAura(NBTTagCompound nbt)
	{
		
	}
	
	/**
	 * Creates a new thunder FX using no fractal. Displays as a straight line.
	 * 
	 * @param world
	 *            The world
	 * @param start
	 *            The start
	 * @param end
	 *            The end
	 * @param seed
	 *            The seed
	 * @param age
	 *            The age
	 * @param fractMod
	 *            The fractal modifier
	 * @param core
	 *            The core layer
	 * @param aura
	 *            The aura layer
	 */
	public void spawnSimpleLine(World world, Vec3d start, Vec3d end, long seed, int age, float fractMod, Thunder.Layer core, Thunder.Layer aura)
	{
		PacketCreateLine p = new PacketCreateLine();
		p.base = new Thunder(seed, age, fractMod);
		p.core = core;
		p.aura = aura;
		p.start = start;
		p.end = end;
		
		/** Get center between 2 points */
		double x = start.x + (end.x - start.x) / 2;
		double y = start.y + (end.y - start.y) / 2;
		double z = start.z + (end.z - start.z) / 2;
		
		HCNet.INSTANCE.sendToAllAround(p, new TargetPoint(world.provider.getDimension(), x, y, z, 128));
	}
}