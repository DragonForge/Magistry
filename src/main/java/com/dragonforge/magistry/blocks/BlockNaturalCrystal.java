package com.dragonforge.magistry.blocks;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.blocks.tiles.TileNaturalCrystal;
import com.dragonforge.magistry.cfg.ConfigsM;
import com.dragonforge.magistry.init.ItemsM;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockOrientable;
import com.zeitheron.hammercore.net.internal.thunder.Thunder;
import com.zeitheron.hammercore.utils.IRegisterListener;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import com.zeitheron.hammercore.world.gen.IWorldGenFeature;
import com.zeitheron.hammercore.world.gen.WorldRetroGen;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class BlockNaturalCrystal extends BlockDeviceHC<TileNaturalCrystal> implements IBlockOrientable, IRegisterListener
{
	public static final List<AxisAlignedBB> CRYSTAL_AABBs = Arrays.asList(new AxisAlignedBB[] { new AxisAlignedBB(4 / 16F, 0, 4 / 16F, 12 / 16F, 14 / 16F, 12 / 16F), new AxisAlignedBB(4 / 16F, 2 / 16F, 4 / 16F, 12 / 16F, 1, 12 / 16F), new AxisAlignedBB(4 / 16F, 4 / 16F, 0, 12 / 16F, 12 / 16F, 14 / 16F), new AxisAlignedBB(4 / 16F, 4 / 16F, 2 / 16F, 12 / 16F, 12 / 16F, 1), new AxisAlignedBB(0, 4 / 16F, 4 / 16F, 14 / 16F, 12 / 16F, 12 / 16F), new AxisAlignedBB(2 / 16F, 4 / 16F, 4 / 16F, 1, 12 / 16F, 12 / 16F) });
	
	public BlockNaturalCrystal()
	{
		super(Material.GLASS, TileNaturalCrystal.class, "natural_crystal");
		setSoundType(SoundType.GLASS);
		setHarvestLevel("pickaxe", 1);
		setHardness(2F);
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return CRYSTAL_AABBs.get(state.getValue(IBlockOrientable.FACING).ordinal());
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean canPlaceBlockOnSide(World worldIn, BlockPos pos, EnumFacing side)
	{
		return worldIn.isSideSolid(pos.offset(side.getOpposite()), side);
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.ENTITYBLOCK_ANIMATED;
	}
	
	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos frompos)
	{
		EnumFacing face = state.getValue(IBlockOrientable.FACING);
		if(!canPlaceBlockOnSide(worldIn, pos, face.getOpposite()))
			worldIn.destroyBlock(pos, true);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, World player, List<String> tooltip, ITooltipFlag advanced)
	{
		NBTTagCompound tag = stack.getTagCompound();
		if(tag != null)
		{
			float satur = tag.getFloat("Saturation");
			float charge = tag.getFloat("Charge");
			float maxCharge = tag.getFloat("MaxCharge");
			int color = tag.getInteger("Color");
			
			int rgb = ColorHelper.interpolate(ColorHelper.getBrightnessRGB(color), color, charge / maxCharge);
			
			tooltip.add(I18n.format("info.magistry:crystal.saturation") + ": " + ((int) (satur * 100)) + "%");
			tooltip.add(I18n.format("info.magistry:crystal.charge") + ": " + ((int) (charge / maxCharge * 100)) + "%");
			tooltip.add(I18n.format("info.magistry:crystal.color") + ": (a) " + ColorHelper.getColorName(color) + " (v) " + ColorHelper.getColorName(rgb));
		}
	}
	
	@Override
	public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		return getDefaultState().withProperty(IBlockOrientable.FACING, facing.getOpposite());
	}
	
	@Override
	public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
		TileNaturalCrystal tnc = WorldUtil.cast(world.getTileEntity(pos), TileNaturalCrystal.class);
		if(tnc != null)
			drops.add(tnc.generateStack());
	}
	
	@Override
	public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state)
	{
		TileNaturalCrystal crystal = WorldUtil.cast(worldIn.getTileEntity(pos), TileNaturalCrystal.class);
		if(crystal != null)
			return crystal.generateStack();
		return super.getItem(worldIn, pos, state);
	}
	
	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		TileNaturalCrystal crystal = WorldUtil.cast(worldIn.getTileEntity(pos), TileNaturalCrystal.class);
		if(crystal != null)
			crystal.loadFromStack(stack);
	}
	
	@Override
	public void onRegistered()
	{
		WorldRetroGen.addWorldFeature(new WorldGen().setRegistryName(getRegistryName()));
	}
	
	@Override
	public void onEntityCollision(World worldIn, BlockPos pos, IBlockState state, Entity entityIn)
	{
		if(entityIn instanceof EntityItem)
		{
			EntityItem ei = (EntityItem) entityIn;
			ItemStack item = ei.getItem();
			if(!item.isEmpty() && item.getItem() == Items.BOOK && item.getCount() == 1)
			{
				TileNaturalCrystal tnc = WorldUtil.cast(worldIn.getTileEntity(pos), TileNaturalCrystal.class);
				if(tnc != null)
				{
					NBTTagCompound tag = item.getOrCreateSubCompound("Magistry");
					if(!tag.getBoolean("Initiated"))
					{
						if(tnc.charge / tnc.maxCharge > .3F)
							tag.setBoolean("Initiated", true);
					} else
					{
						float a = tag.getFloat("Absorbed");
						
						if(a < .3F)
						{
							float charge = tnc.charge / tnc.maxCharge;
							if(charge > 0)
							{
								float take = Math.min(0.0015F, charge);
								tag.setFloat("Absorbed", a = a + take);
								charge -= take;
								Magistry.proxy.spawnSimpleLine(worldIn, new Vec3d(pos.getX() + .5, pos.getY() + .5, pos.getZ() + .5), ei.getEntityBoundingBox().getCenter(), worldIn.rand.nextLong(), 2, 0F, new Thunder.Layer(771, tnc.getRenderColor(), true), new Thunder.Layer(771, tnc.getRenderColor(), true));
							}
							tnc.charge = charge * tnc.maxCharge;
							tnc.sync();
							tag.setFloat("Absorbed", a);
						}
						
						if(a >= .3F)
							ei.setItem(new ItemStack(ItemsM.CRYSTAL_TOME));
					}
				}
			}
		}
	}
	
	private class WorldGen extends IForgeRegistryEntry.Impl<WorldGen> implements IWorldGenFeature<WorldGen>
	{
		@Override
		public int getMaxChances(World world, ChunkPos chunk, Random rand)
		{
			return 16;
		}
		
		@Override
		public int getMinY(World world, BlockPos pos, Random rand)
		{
			return 8;
		}
		
		@Override
		public int getMaxY(World world, BlockPos pos, Random rand)
		{
			return 48;
		}
		
		@Override
		public void generate(World world, BlockPos pos, Random rand)
		{
			if(ConfigsM.gen_naturalCrystals)
			{
				EnumFacing attach = getAttachSide(world, pos);
				if(attach != null)
					world.setBlockState(pos, getDefaultState().withProperty(IBlockOrientable.FACING, attach.getOpposite()));
			}
		}
		
		@Nullable
		public EnumFacing getAttachSide(World world, BlockPos pos)
		{
			if(!world.isAirBlock(pos))
				return null;
			ChunkPos cp = new ChunkPos(pos);
			for(EnumFacing face : EnumFacing.VALUES)
				if(sameChunk(cp, pos.offset(face)) && world.isSideSolid(pos.offset(face), face.getOpposite()))
					return face.getOpposite();
			return null;
		}
		
		private boolean sameChunk(ChunkPos cp, BlockPos pos)
		{
			return pos.getX() > cp.getXStart() && pos.getZ() > cp.getZStart() && pos.getX() < cp.getXEnd() && pos.getZ() < cp.getZEnd();
		}
	}
}