package com.dragonforge.magistry.blocks;

import java.util.List;
import java.util.Random;

import com.dragonforge.magistry.cfg.ConfigsM;
import com.dragonforge.magistry.init.ItemsM;
import com.zeitheron.hammercore.utils.EnumMoonPhase;
import com.zeitheron.hammercore.utils.IRegisterListener;
import com.zeitheron.hammercore.world.WorldGenHelper;
import com.zeitheron.hammercore.world.gen.IWorldGenFeature;
import com.zeitheron.hammercore.world.gen.WorldRetroGen;

import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.IShearable;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class BlockStarflower extends BlockTickingBush implements IShearable, IRegisterListener
{
	public static final PropertyInteger TYPE = PropertyInteger.create("type", 0, 2);
	public static final PropertyBool NATURAL = PropertyBool.create("natural");
	
	public BlockStarflower()
	{
		setTranslationKey("starflower");
		setHardness(.4F);
		setHarvestLevel("shears", 1);
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, TYPE, NATURAL);
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return getDefaultState().withProperty(TYPE, meta % 3).withProperty(NATURAL, meta > 2);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		int i = state.getValue(TYPE) + (state.getValue(NATURAL) ? 3 : 0);
		return i;
	}
	
	@Override
	public void onTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
	{
		boolean natural = state.getValue(NATURAL);
		int type = state.getValue(TYPE);
		
		boolean night = !worldIn.isDaytime();
		int phasei = (int) (worldIn.getWorldTime() / 24000L % 8L + 8L) % 8;
		EnumMoonPhase phase = EnumMoonPhase.values()[phasei];
		
		if(natural)
		{
			if(type == 2 && phase == EnumMoonPhase.NEW_MOON && night)
				worldIn.setBlockState(pos, state.withProperty(TYPE, 0));
			if(type != 2)
				worldIn.setBlockState(pos, state.withProperty(TYPE, phase == EnumMoonPhase.FULL && night ? 1 : 0));
		} else
		{
			if(type == 2 && !night)
				worldIn.setBlockState(pos, state.withProperty(TYPE, 0));
			if(type != 2)
				worldIn.setBlockState(pos, state.withProperty(TYPE, night ? 1 : 0));
		}
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(state.getValue(TYPE) == 1)
		{
			worldIn.setBlockState(pos, state.withProperty(TYPE, 2));
			ItemStack stack = new ItemStack(ItemsM.STARDUST);
			if(!worldIn.isRemote && !playerIn.addItemStackToInventory(stack))
			{
				EntityItem ei = playerIn.dropItem(stack, true);
				if(ei != null)
					ei.setNoPickupDelay();
			}
			return true;
		}
		
		return false;
	}
	
	@Override
	public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
	}
	
	@Override
	public boolean isShearable(ItemStack item, IBlockAccess world, BlockPos pos)
	{
		IBlockState state = world.getBlockState(pos);
		return state.getValue(TYPE) != 2 || !state.getValue(NATURAL);
	}
	
	@Override
	public List<ItemStack> onSheared(ItemStack item, IBlockAccess world, BlockPos pos, int fortune)
	{
		NonNullList<ItemStack> drops = NonNullList.create();
		
		IBlockState state = world.getBlockState(pos);
		if(state.getValue(TYPE) != 2)
			if(!state.getValue(NATURAL))
				drops.add(new ItemStack(this));
			else
			{
				Random rand = world instanceof World ? ((World) world).rand : new Random();
				int maxn = 6;
				if(fortune > 0)
					maxn /= fortune;
				int roll = rand.nextInt(maxn);
				if(roll == 0)
					drops.add(new ItemStack(this));
			}
		
		return drops;
	}
	
	@Override
	public void onRegistered()
	{
		WorldRetroGen.addWorldFeature(new WorldGen().setRegistryName(getRegistryName()));
	}
	
	private class WorldGen extends IForgeRegistryEntry.Impl<WorldGen> implements IWorldGenFeature<WorldGen>
	{
		@Override
		public int getMaxChances(World world, ChunkPos chunk, Random rand)
		{
			return 2;
		}
		
		@Override
		public int getMinY(World world, BlockPos pos, Random rand)
		{
			return 0;
		}
		
		@Override
		public int getMaxY(World world, BlockPos pos, Random rand)
		{
			return 1;
		}
		
		@Override
		public void generate(World world, BlockPos pos, Random rand)
		{
			if(ConfigsM.gen_starflowers && rand.nextInt(16) == 0)
			{
				ChunkPos cp = new ChunkPos(pos);
				BlockPos cent = world.getHeight(new BlockPos(cp.getXStart() + 7, 255, cp.getZStart() + 7));
				if(cent.getY() > world.getSeaLevel())
					WorldGenHelper.generateFlower(getDefaultState().withProperty(TYPE, 0).withProperty(NATURAL, true), rand, world, cent, 6, 2, 6, true, WorldGenHelper.GRASS_OR_DIRT_CHECKER);
			}
		}
	}
}