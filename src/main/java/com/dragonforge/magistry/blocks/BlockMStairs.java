package com.dragonforge.magistry.blocks;

import net.minecraft.block.BlockStairs;
import net.minecraft.block.state.IBlockState;

public class BlockMStairs extends BlockStairs
{
	public BlockMStairs(IBlockState modelState, String name)
	{
		super(modelState);
		setTranslationKey(name + "_stairs");
		setHardness(2.0F);
		setResistance(5.0F);
		fullBlock = false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
}