package com.dragonforge.magistry.blocks;

import java.util.Random;

import com.zeitheron.hammercore.api.INoItemBlock;
import com.zeitheron.hammercore.internal.blocks.IItemBlock;

import net.minecraft.block.BlockSlab;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemSlab;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class BlockMSlab extends BlockSlab
{
	public static final PropertyEnum<Variant> VARIANT = PropertyEnum.<Variant> create("variant", Variant.class);
	
	public final BlockMSlab.Double doble;
	
	public BlockMSlab(Material mat, SoundType sound, BlockMSlab.Double doble)
	{
		super(mat);
		setSoundType(sound);
		IBlockState iblockstate = this.blockState.getBaseState();
		
		this.doble = isDouble() ? (BlockMSlab.Double) this : doble;
		
		if(!this.isDouble())
			iblockstate = iblockstate.withProperty(HALF, BlockSlab.EnumBlockHalf.BOTTOM);
		this.setDefaultState(iblockstate.withProperty(VARIANT, Variant.DEFAULT));
	}
	
	@Override
    @SideOnly(Side.CLIENT)
	public float getAmbientOcclusionLightValue(IBlockState state)
	{
		return super.getAmbientOcclusionLightValue(state);
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return isDouble();
	}
	
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return Item.getItemFromBlock(doble.getHalf());
	}
	
	@Override
	public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state)
	{
		return new ItemStack(doble.getHalf());
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		IBlockState iblockstate = this.getDefaultState().withProperty(VARIANT, Variant.DEFAULT);
		
		if(!this.isDouble())
		{
			iblockstate = iblockstate.withProperty(HALF, (meta & 8) == 0 ? BlockSlab.EnumBlockHalf.BOTTOM : BlockSlab.EnumBlockHalf.TOP);
		}
		
		return iblockstate;
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		int i = 0;
		
		if(!this.isDouble() && state.getValue(HALF) == BlockSlab.EnumBlockHalf.TOP)
		{
			i |= 8;
		}
		
		return i;
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return this.isDouble() ? new BlockStateContainer(this, new IProperty[] { VARIANT }) : new BlockStateContainer(this, new IProperty[] { HALF, VARIANT });
	}
	
	@Override
	public String getTranslationKey(int meta)
	{
		return super.getTranslationKey();
	}
	
	@Override
	public IProperty<?> getVariantProperty()
	{
		return VARIANT;
	}
	
	@Override
	public Comparable<?> getTypeForItem(ItemStack stack)
	{
		return Variant.DEFAULT;
	}
	
	public static class Double extends BlockMSlab implements INoItemBlock
	{
		public Double(Material mat, SoundType sound, String name)
		{
			super(mat, sound, null);
			setTranslationKey(name + "_double_slab");
			setHardness(1.5F);
		}
		
		@Override
		public boolean isDouble()
		{
			return true;
		}
		
		@Override
		public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items)
		{
			super.getSubBlocks(itemIn, items);
		}
		
		private BlockMSlab.Half half;
		
		public BlockMSlab.Half getHalf()
		{
			return half;
		}
		
		public void setHalf(BlockMSlab.Half half)
		{
			this.half = half;
		}
	}
	
	public static class Half extends BlockMSlab implements IItemBlock
	{
		public final BlockMSlab.Double doubleSlab;
		
		public final ItemSlab slabItem;
		
		public Half(Material mat, SoundType sound, BlockMSlab.Double doubleSlab, String name)
		{
			super(mat, sound, doubleSlab);
			this.doubleSlab = doubleSlab;
			this.slabItem = new ItemSlab(this, this, doubleSlab);
			doubleSlab.setHalf(this);
			setTranslationKey(name + "_slab");
			setHardness(1.5F);
		}
		
		@Override
		public boolean isDouble()
		{
			return false;
		}
		
		@Override
		public ItemBlock getItemBlock()
		{
			return slabItem;
		}
	}
	
	public static enum Variant implements IStringSerializable
	{
		DEFAULT;
		
		public String getName()
		{
			return "default";
		}
	}
}