package com.dragonforge.magistry.blocks;

import java.util.Random;

import javax.annotation.Nullable;

import com.dragonforge.magistry.cfg.ConfigsM;
import com.dragonforge.magistry.utils.OreHelper;
import com.zeitheron.hammercore.utils.IRegisterListener;
import com.zeitheron.hammercore.world.gen.IWorldGenFeature;
import com.zeitheron.hammercore.world.gen.WorldGenMinableHC;
import com.zeitheron.hammercore.world.gen.WorldRetroGen;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeModContainer;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class BlockPhyllite extends Block implements IRegisterListener
{
	public BlockPhyllite()
	{
		super(Material.ROCK);
		setTranslationKey("phyllite");
		setHarvestLevel("pickaxe", 2);
		setHardness(2F);
	}
	
	@Override
	public void onRegistered()
	{
		WorldRetroGen.addWorldFeature(new WorldGen().setRegistryName(getRegistryName()));
		OreDictionary.registerOre("phyllite", this);
	}
	
	private class WorldGen extends IForgeRegistryEntry.Impl<WorldGen> implements IWorldGenFeature<WorldGen>
	{
		@Override
		public int getMaxChances(World world, ChunkPos chunk, Random rand)
		{
			return 16;
		}
		
		@Override
		public int getMinY(World world, BlockPos pos, Random rand)
		{
			return 22;
		}
		
		@Override
		public int getMaxY(World world, BlockPos pos, Random rand)
		{
			return 32;
		}
		
		@Override
		public void generate(World world, BlockPos pos, Random rand)
		{
			if(ConfigsM.gen_phyllite)
			{
				ChunkPos cp = new ChunkPos(pos);
				
				int h = 1 + rand.nextInt(3);
				for(int i = 0; i < h; ++i)
				{
					int add = i - h / 2;
					
					int xs = 3 + rand.nextInt(7);
					int zs = 3 + rand.nextInt(7);
					
					for(int j = 0; j < xs; ++j)
					{
						for(int k = 0; k < zs; ++k)
						{
							BlockPos ap = pos.add(j, i, k);
							
							if(sameChunk(cp, ap) && world.getBlockState(ap).getBlock() == Blocks.STONE)
								world.setBlockState(ap, getDefaultState());
						}
					}
				}
			}
		}
		
		@Nullable
		public EnumFacing getGold(World world, BlockPos pos)
		{
			ChunkPos cp = new ChunkPos(pos);
			for(EnumFacing face : EnumFacing.VALUES)
				if(sameChunk(cp, pos.offset(face)) && world.getBlockState(pos.offset(face)).getBlock() == Blocks.GOLD_ORE)
					return face;
			return EnumFacing.SOUTH;
		}
		
		private boolean sameChunk(ChunkPos cp, BlockPos pos)
		{
			return pos.getX() > cp.getXStart() && pos.getZ() > cp.getZStart() && pos.getX() < cp.getXEnd() && pos.getZ() < cp.getZEnd();
		}
	}
}