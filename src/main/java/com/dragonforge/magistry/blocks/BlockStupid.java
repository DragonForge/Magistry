package com.dragonforge.magistry.blocks;

import com.zeitheron.hammercore.utils.IRegisterListener;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.oredict.OreDictionary;

public class BlockStupid extends Block implements IRegisterListener
{
	public BlockStupid(Material materialIn)
	{
		super(materialIn);
	}
	
	public BlockStupid setHarvestLvl(String toolClass, int level)
	{
		super.setHarvestLevel(toolClass, level);
		return this;
	}
	
	@Override
	public BlockStupid setSoundType(SoundType sound)
	{
		super.setSoundType(sound);
		return this;
	}
	
	@Override
	public BlockStupid setTranslationKey(String name)
	{
		super.setTranslationKey(name);
		return this;
	}
	
	@Override
	public BlockStupid setHardness(float hardness)
	{
		super.setHardness(hardness);
		return this;
	}
	
	private String[] ods;
	
	public BlockStupid setOredictionary(String... names)
	{
		ods = names;
		return this;
	}
	
	@Override
	public void onRegistered()
	{
		for(String o : ods)
			OreDictionary.registerOre(o, this);
	}
}