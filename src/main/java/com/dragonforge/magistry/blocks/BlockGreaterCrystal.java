package com.dragonforge.magistry.blocks;

import com.dragonforge.magistry.blocks.tiles.TileGreaterCrystal;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockCore;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockPart;
import com.dragonforge.magistry.init.BlocksM;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockOrientable;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockGreaterCrystal extends BlockDeviceHC<TileGreaterCrystal> implements IBlockOrientable
{
	public BlockGreaterCrystal()
	{
		super(Material.GLASS, TileGreaterCrystal.class, "greater_crystal");
		setSoundType(SoundType.GLASS);
		setHarvestLevel("pickaxe", 1);
		setHardness(2F);
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean canPlaceBlockOnSide(World worldIn, BlockPos pos, EnumFacing side)
	{
		IBlockState state = worldIn.getBlockState(pos.offset(side.getOpposite()));
		
		if(worldIn.getTileEntity(pos.offset(side.getOpposite())) instanceof TileMultiblockPart)
		{
			TileMultiblockPart part = (TileMultiblockPart) worldIn.getTileEntity(pos.offset(side.getOpposite()));
			if(part.before != null)
				state = part.before.getReplacedBlock();
		} else if(worldIn.getTileEntity(pos.offset(side.getOpposite())) instanceof TileMultiblockCore)
		{
			TileMultiblockCore part = (TileMultiblockCore) worldIn.getTileEntity(pos.offset(side.getOpposite()));
			if(part.before != null)
				state = part.before.getReplacedBlock();
		}
		
		return worldIn.isSideSolid(pos.offset(side.getOpposite()), side) || (state.getBlock() == BlocksM.DARK_PILLAR && state.getValue(BlockDarkPillar.AXIS) == side.getAxis());
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.ENTITYBLOCK_ANIMATED;
	}
	
	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos frompos)
	{
		EnumFacing face = state.getValue(IBlockOrientable.FACING);
		if(!canPlaceBlockOnSide(worldIn, pos, face.getOpposite()))
		{
			TileGreaterCrystal tmc = WorldUtil.cast(worldIn.getTileEntity(pos), TileGreaterCrystal.class);
			if(tmc != null)
				tmc.dispose();
			worldIn.destroyBlock(pos, true);
		}
	}
	
	@Override
	public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		return getDefaultState().withProperty(IBlockOrientable.FACING, facing.getOpposite());
	}
}