package com.dragonforge.magistry.blocks;

import net.minecraft.block.BlockFence;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockMFence extends BlockFence
{
	public BlockMFence(Material material)
	{
		super(material, material.getMaterialMapColor());
	}
	
	@Override
	public BlockMFence setSoundType(SoundType sound)
	{
		super.setSoundType(sound);
		return this;
	}
}