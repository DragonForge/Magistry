package com.dragonforge.magistry.blocks;

import com.zeitheron.hammercore.utils.IRegisterListener;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.oredict.OreDictionary;

public class BlockDarkPlanks extends Block implements IRegisterListener
{
	public BlockDarkPlanks()
	{
		super(Material.WOOD);
		setSoundType(SoundType.WOOD);
		setHardness(2);
		setResistance(5);
		setTranslationKey("dark_wood_planks");
	}
	
	@Override
	public void onRegistered()
	{
		OreDictionary.registerOre("plankWood", this);
	}
}