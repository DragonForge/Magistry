package com.dragonforge.magistry.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public class BlockInductionCoil extends Block
{
	public static final PropertyBool DOWN = PropertyBool.create("down");
	public static final PropertyBool UP = PropertyBool.create("up");
	
	public BlockInductionCoil()
	{
		super(Material.IRON);
		setSoundType(SoundType.METAL);
		setTranslationKey("induction_coil");
		setHarvestLevel("pickaxe", 2);
		setHardness(2F);
		this.setDefaultState(getDefaultState().withProperty(UP, false).withProperty(DOWN, false));
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, DOWN, UP);
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return getDefaultState().withProperty(DOWN, false).withProperty(UP, false);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return 0;
	}
	
	@Override
	public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos)
	{
		if(state.getValue(UP) || state.getValue(DOWN))
			return state;
		boolean down = worldIn.getBlockState(pos.down()).getBlock() == this;
		boolean up = worldIn.getBlockState(pos.up()).getBlock() == this;
		return getDefaultState().withProperty(DOWN, down).withProperty(UP, up);
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	public AxisAlignedBB partialBB = new AxisAlignedBB(2 / 16D, 0, 2 / 16D, 14 / 16D, 1, 14 / 16D);
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess worldIn, BlockPos pos)
	{
		boolean down = worldIn.getBlockState(pos.down()).getBlock() == this;
		boolean up = worldIn.getBlockState(pos.up()).getBlock() == this;
		return down && up ? partialBB : super.getBoundingBox(state, worldIn, pos);
	}
}