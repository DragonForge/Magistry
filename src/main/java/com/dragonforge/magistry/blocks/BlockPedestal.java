package com.dragonforge.magistry.blocks;

import com.dragonforge.magistry.blocks.tiles.TilePedestal;
import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.utils.SoundUtil;
import com.zeitheron.hammercore.utils.WorldUtil;

import it.unimi.dsi.fastutil.objects.Object2IntArrayMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class BlockPedestal extends Block implements ITileBlock<TilePedestal>, ITileEntityProvider
{
	public AxisAlignedBB aabb = new AxisAlignedBB(1 / 16D, 0, 1 / 16D, 15 / 16D, 12 / 16D, 15 / 16D);
	
	public BlockPedestal()
	{
		super(Material.ROCK);
		setTranslationKey("pedestal");
		setHarvestLevel("pickaxe", 1);
		setHardness(1.5F);
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return aabb;
	}
	
	@Override
	public BlockFaceShape getBlockFaceShape(IBlockAccess p_193383_1_, IBlockState p_193383_2_, BlockPos p_193383_3_, EnumFacing face)
	{
		if(face == EnumFacing.DOWN)
			return BlockFaceShape.MIDDLE_POLE_THICK;
		if(face == EnumFacing.UP)
			return BlockFaceShape.MIDDLE_POLE;
		return BlockFaceShape.UNDEFINED;
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TilePedestal te = WorldUtil.cast(worldIn.getTileEntity(pos), TilePedestal.class);
		if(te != null && !worldIn.isRemote)
		{
			ItemStack stack = te.getItem();
			ItemStack held = playerIn.getHeldItem(hand);
			
			if(stack.isEmpty() && !held.isEmpty())
			{
				te.setItem(held.copy());
				playerIn.setHeldItem(hand, ItemStack.EMPTY);
				SoundUtil.playSoundEffect(worldIn, SoundEvents.ENTITY_ITEM_PICKUP.getRegistryName().toString(), pos, .5F, 2F, SoundCategory.PLAYERS);
			} else if(!stack.isEmpty())
			{
				te.setItem(ItemStack.EMPTY);
				SoundUtil.playSoundEffect(worldIn, SoundEvents.ENTITY_ITEM_PICKUP.getRegistryName().toString(), pos, .5F, 2F, SoundCategory.PLAYERS);
				if(!playerIn.addItemStackToInventory(stack))
				{
					EntityItem ei = playerIn.dropItem(stack, true);
					if(ei != null)
						ei.setNoPickupDelay();
				}
			}
		}
		return true;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TilePedestal();
	}
	
	@Override
	public Class<TilePedestal> getTileClass()
	{
		return TilePedestal.class;
	}
	
	public static final Object2IntMap<Item> LIGHT_VALUES = new Object2IntArrayMap<>();
	
	static
	{
		LIGHT_VALUES.put(Items.GLOWSTONE_DUST, 8);
		LIGHT_VALUES.put(Items.PRISMARINE_CRYSTALS, 10);
		LIGHT_VALUES.put(Items.BLAZE_ROD, 12);
		LIGHT_VALUES.put(Items.BLAZE_POWDER, 6);
		LIGHT_VALUES.put(Items.MAGMA_CREAM, 8);
		LIGHT_VALUES.put(Items.FIRE_CHARGE, 12);
		LIGHT_VALUES.put(Items.NETHER_STAR, 15);
	}
	
	public static int getLightForItem(ItemStack stack)
	{
		if(stack.isEmpty())
			return 0;
		
		IFluidHandlerItem fh = FluidUtil.getFluidHandler(stack);
		Block b = Block.getBlockFromItem(stack.getItem());
		
		if(LIGHT_VALUES.containsKey(stack.getItem()))
			return LIGHT_VALUES.getInt(stack.getItem());
		else if(b != null && b != Blocks.AIR)
			return b.getLightValue(b.getDefaultState());
		else if(fh != null)
		{
			int ma = 0;
			for(IFluidTankProperties prop : fh.getTankProperties())
			{
				FluidStack fluid = prop.getContents();
				ma = Math.max(ma, Math.round(fluid.getFluid().getLuminosity(fluid) * (fluid.amount / (float) prop.getCapacity())));
			}
			return ma;
		}
		
		return 0;
	}
	
	@Override
	public int getLightValue(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		TilePedestal ped = WorldUtil.cast(world.getTileEntity(pos), TilePedestal.class);
		if(ped != null && !ped.getItem().isEmpty())
			return getLightForItem(ped.getItem());
		return super.getLightValue(state, world, pos);
	}
}