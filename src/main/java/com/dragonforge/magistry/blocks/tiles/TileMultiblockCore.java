package com.dragonforge.magistry.blocks.tiles;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.multiblock.MultiblockRecipe;
import com.dragonforge.magistry.api.multiblock.MultiblockTileRecipe;
import com.dragonforge.magistry.api.multiblock.MultiblockTileRecipe.TileComponent;
import com.zeitheron.hammercore.tile.ITileDroppable;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.tile.tooltip.ITooltipTile;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.BlockSnapshot;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class TileMultiblockCore extends TileSyncableTickable implements ITileDroppable, ITooltipTile, ITooltipProviderHC
{
	public BlockSnapshot before;
	public ResourceLocation multiblock;
	public int thisX, thisY, thisZ;
	
	public final Map<BlockPos, AxisAlignedBB> bbSubMap = new HashMap<>();
	public Function<BlockPos, AxisAlignedBB> aabbMapper = pos -> Block.FULL_BLOCK_AABB;
	
	public void setMultiblock(ResourceLocation multiblock)
	{
		MultiblockTileRecipe mtr = WorldUtil.cast(MagistryAPI.MULTIBLOCK_REGISTRY.getValue(multiblock), MultiblockTileRecipe.class);
		if(mtr != null)
		{
			this.thisX = mtr.corePos.getX();
			this.thisY = mtr.corePos.getY();
			this.thisZ = mtr.corePos.getZ();
			this.multiblock = multiblock;
		} else
			System.out.println("Attempted to set an invalid multiblock id: \"" + multiblock + "\" for " + this);
	}
	
	public void remap()
	{
		bbSubMap.clear();
		MultiblockRecipe recipe = MagistryAPI.MULTIBLOCK_REGISTRY.getValue(multiblock);
		if(recipe != null)
			for(long l : recipe.components.keySet())
			{
				BlockPos op = BlockPos.fromLong(l);
				bbSubMap.put(op, aabbMapper.apply(op));
			}
	}
	
	@Override
	public void tick()
	{
		if(world.isRemote)
			return;
		if(before == null)
			world.setBlockToAir(pos);
		if(atTickRate(20) && !checkMultiblockState())
			deform();
		if(multiblock == null)
			deform();
	}
	
	public AxisAlignedBB getSubAABB(BlockPos rel)
	{
		if(bbSubMap.isEmpty())
			remap();
		return bbSubMap.getOrDefault(rel, Block.FULL_BLOCK_AABB);
	}
	
	public boolean checkMultiblockState()
	{
		boolean valid = false;
		MultiblockRecipe recipe = MagistryAPI.MULTIBLOCK_REGISTRY.getValue(multiblock);
		if(recipe != null)
		{
			valid = true;
			BlockPos start = pos.add(-thisX, -thisY, -thisZ);
			for(long offl : recipe.components.keySet())
			{
				BlockPos off = BlockPos.fromLong(offl);
				BlockPos npos = start.add(off);
				if(!recipe.components.get(offl).outputMap.containsValue(world.getBlockState(npos)))
				{
					valid = false;
					break;
				}
			}
		}
		return valid;
	}
	
	public void deform()
	{
		MultiblockRecipe recipe = MagistryAPI.MULTIBLOCK_REGISTRY.getValue(multiblock);
		if(recipe != null)
		{
			BlockPos start = pos.add(-thisX, -thisY, -thisZ);
			for(long offl : recipe.components.keySet())
			{
				BlockPos off = BlockPos.fromLong(offl);
				BlockPos npos = start.add(off);
				((TileComponent) recipe.components.get(offl)).deform(world, npos);
			}
		}
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		if(multiblock != null)
			nbt.setString("Multiblock", multiblock.toString());
		if(before != null)
		{
			NBTTagCompound tag;
			before.writeToNBT(tag = new NBTTagCompound());
			nbt.setTag("FormedFrom", tag);
		}
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		setMultiblock(new ResourceLocation(nbt.getString("Multiblock")));
		before = BlockSnapshot.readFromNBT(nbt.getCompoundTag("FormedFrom"));
	}
	
	public boolean hasSubCapability(BlockPos part, Capability<?> capability, EnumFacing facing)
	{
		return hasCapability(capability, facing);
	}
	
	public <T> T getSubCapability(BlockPos part, Capability<T> capability, EnumFacing facing)
	{
		return getCapability(capability, facing);
	}
	
	boolean deforming = false;
	
	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		if(!deforming)
		{
			deforming = true;
			if(before != null && player != null)
			{
				before.restoreToLocation(world, pos, true, true);
				world.destroyBlock(pos, true);
			}
			deform();
		}
	}
	
	@Override
	public String getF3Registry()
	{
		if(before == null)
			return super.getF3Registry();
		return before.getRegistryName().toString();
	}
	
	@Override
	public ItemStack getItemIconOverride()
	{
		if(before == null)
			return ItemStack.EMPTY;
		Block b = ForgeRegistries.BLOCKS.getValue(before.getRegistryName());
		Item it = Item.getItemFromBlock(b);
		if(it != null)
			return new ItemStack(it);
		return ItemStack.EMPTY;
	}
	
	public void getSubTooltip(BlockPos relPos, List<String> list, EntityPlayer player)
	{
		getTextTooltip(list, player);
	}
	
	@Override
	public void getTextTooltip(List<String> list, EntityPlayer player)
	{
	}
	
	@Override
	public void addInformation(ITooltip tip)
	{
	}
	
	public boolean isSubTooltipDirty(BlockPos relPos)
	{
		return isTooltipDirty();
	}
	
	public void setSubTooltipDirty(BlockPos relPos, boolean dirty)
	{
		setTooltipDirty(dirty);
	}
	
	public void addSubInformation(BlockPos relPos, ITooltip tip)
	{
		addInformation(tip);
	}
	
	public boolean onSubBlockActivated(World worldIn, BlockPos relPos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		return false;
	}
	
	protected boolean tooltipDirty = false;
	
	@Override
	public boolean isTooltipDirty()
	{
		return tooltipDirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		tooltipDirty = dirty;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getRenderBoundingBox()
	{
		MultiblockRecipe recipe = MagistryAPI.MULTIBLOCK_REGISTRY.getValue(multiblock);
		if(recipe != null)
		{
			BlockPos start = pos.add(-thisX, -thisY, -thisZ);
			return new AxisAlignedBB(start, start.add(recipe.getWidth(), recipe.getHeight(), recipe.getDepth()));
		}
		return super.getRenderBoundingBox();
	}
}