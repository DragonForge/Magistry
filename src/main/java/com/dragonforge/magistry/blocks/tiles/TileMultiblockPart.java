package com.dragonforge.magistry.blocks.tiles;

import java.util.List;

import javax.annotation.Nullable;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.multiblock.MultiblockRecipe;
import com.dragonforge.magistry.api.multiblock.MultiblockTileRecipe.TileComponent;
import com.zeitheron.hammercore.tile.ITileDroppable;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.tile.tooltip.ITooltipTile;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.BlockSnapshot;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class TileMultiblockPart extends TileSyncable implements ITileDroppable, ITooltipTile, ITooltipProviderHC
{
	public BlockSnapshot before;
	public BlockPos core;
	
	public ResourceLocation multiblock;
	public int thisX, thisY, thisZ;
	
	public BlockPos relPos;
	
	public TileMultiblockPart(ResourceLocation multiblock, int thisX, int thisY, int thisZ)
	{
		this.multiblock = multiblock;
		this.thisX = thisX;
		this.thisY = thisY;
		this.thisZ = thisZ;
	}
	
	public TileMultiblockPart()
	{
	}
	
	public ItemStack getItem(RayTraceResult res, EntityPlayer player)
	{
		if(before == null)
			return ItemStack.EMPTY;
		Block b = ForgeRegistries.BLOCKS.getValue(before.getRegistryName());
		return b.getPickBlock(b.getStateFromMeta(before.getMeta()), res, world, res.getBlockPos(), player);
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		if(before != null)
			before.writeToNBT(nbt);
		nbt.setString("Multiblock", multiblock.toString());
		nbt.setInteger("tx", thisX);
		nbt.setInteger("ty", thisY);
		nbt.setInteger("tz", thisZ);
		nbt.setLong("Owner", core.toLong());
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		if(nbt.hasKey("blockName"))
			before = BlockSnapshot.readFromNBT(nbt);
		else
		{
			before = null;
			if(readNBT_world != null && pos != null)
				readNBT_world.setBlockToAir(pos);
		}
		this.multiblock = new ResourceLocation(nbt.getString("Multiblock"));
		this.thisX = nbt.getInteger("tx");
		this.thisY = nbt.getInteger("ty");
		this.thisZ = nbt.getInteger("tz");
		core = BlockPos.fromLong(nbt.getLong("Owner"));
	}
	
	@Nullable
	public TileMultiblockCore getCore()
	{
		if(core == null)
			return null;
		return WorldUtil.cast(world.getTileEntity(core), TileMultiblockCore.class);
	}
	
	public AxisAlignedBB getAABB()
	{
		TileMultiblockCore core = getCore();
		if(core != null)
			return core.getSubAABB(getRelativePos());
		return Block.FULL_BLOCK_AABB;
	}
	
	public BlockPos getRelativePos()
	{
		if(relPos != null)
			return relPos;
		return relPos = new BlockPos(thisX, thisY, thisZ);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		TileMultiblockCore core = getCore();
		if(core != null && core.hasSubCapability(getRelativePos(), capability, facing))
			return true;
		return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		TileMultiblockCore core = getCore();
		if(core != null)
		{
			T t = core.getSubCapability(getRelativePos(), capability, facing);
			if(t != null)
				return t;
		}
		return super.getCapability(capability, facing);
	}
	
	boolean deforming = false;
	
	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		if(!deforming)
		{
			deforming = true;
			if(before != null)
			{
				before.restoreToLocation(world, pos, true, true);
				world.destroyBlock(pos, true);
			}
			deform();
		}
	}
	
	public void deform()
	{
		TileMultiblockCore core = getCore();
		if(core != null)
			core.deform();
		else
		{
			MultiblockRecipe recipe = MagistryAPI.MULTIBLOCK_REGISTRY.getValue(multiblock);
			if(recipe != null)
			{
				BlockPos start = pos.add(-thisX, -thisY, -thisZ);
				for(long offl : recipe.components.keySet())
				{
					BlockPos off = BlockPos.fromLong(offl);
					BlockPos npos = start.add(off);
					((TileComponent) recipe.components.get(offl)).deform(world, npos);
				}
			}
		}
	}
	
	@Override
	public String getF3Registry()
	{
		if(before == null)
			return super.getF3Registry();
		return before.getRegistryName().toString();
	}
	
	@Override
	public ItemStack getItemIconOverride()
	{
		if(before == null)
			return ItemStack.EMPTY;
		Block b = ForgeRegistries.BLOCKS.getValue(before.getRegistryName());
		Item it = Item.getItemFromBlock(b);
		if(it != null)
			return new ItemStack(it);
		return ItemStack.EMPTY;
	}
	
	@Override
	public void getTextTooltip(List<String> list, EntityPlayer player)
	{
		TileMultiblockCore core = getCore();
		if(core != null)
			core.getSubTooltip(getRelativePos(), list, player);
	}
	
	@Override
	public boolean isTooltipDirty()
	{
		TileMultiblockCore core = getCore();
		if(core != null)
			return core.isSubTooltipDirty(getRelativePos());
		return false;
	}

	@Override
	public void setTooltipDirty(boolean dirty)
	{
		TileMultiblockCore core = getCore();
		if(core != null)
			core.setSubTooltipDirty(getRelativePos(), dirty);
	}

	@Override
	public void addInformation(ITooltip tip)
	{
		TileMultiblockCore core = getCore();
		if(core != null)
			core.addSubInformation(getRelativePos(), tip);
	}
}