package com.dragonforge.magistry.blocks.tiles;

import java.lang.ref.WeakReference;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.init.BlocksM;
import com.dragonforge.magistry.net.PacketBreakCrystal;
import com.dragonforge.magistry.utils.OreHelper;
import com.dragonforge.magistry.utils.ProcessDelayedTask;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.ITileDroppable;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileOreCrystal extends TileSyncable implements ITileDroppable, ITooltipProviderHC
{
	public IBlockState growing = Blocks.AIR.getDefaultState();
	
	public ItemStack generateStack()
	{
		ItemStack is = new ItemStack(BlocksM.ORE_CRYSTAL);
		NBTTagCompound nbt = new NBTTagCompound();
		writeNBT(nbt);
		is.setTagCompound(nbt);
		return is;
	}
	
	public void loadFromStack(ItemStack stack)
	{
		if(stack.hasTagCompound())
		{
			readNBT(stack.getTagCompound());
			sendChangesToNearby();
		}
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		if(growing == null)
			return;
		nbt.setString("Block", growing.getBlock().getRegistryName().toString());
		int m = growing.getBlock().getMetaFromState(growing);
		if(m != 0)
			nbt.setInteger("Meta", m);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		growing = null;
		
		Block block = ForgeRegistries.BLOCKS.getValue(new ResourceLocation(nbt.getString("Block")));
		if(block != null)
		{
			int meta = nbt.getInteger("Meta");
			growing = block.getStateFromMeta(meta);
		}
		
		tooltipDirty = true;
	}
	
	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		IPlayerKnowledge ipk = MagistryAPI.getPlayerKnowledge(player);
		
		if(ipk != null)
			ipk.unlockResearch(Magistry.MOD_ID + ":ore_crystals", true);
		
		if(!player.capabilities.isCreativeMode && player.canHarvestBlock(world.getBlockState(pos)))
			WorldUtil.spawnItemStack(world, pos, generateStack());
	}
	
	protected boolean tooltipDirty;
	protected boolean lastHeld;
	
	@Override
	public boolean isTooltipDirty()
	{
		boolean held = MagistryAPI.isClientHoldingCrystalStaff();
		if(held != lastHeld)
		{
			lastHeld = held;
			tooltipDirty = true;
		}
		
		return tooltipDirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		this.tooltipDirty = dirty;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		if(MagistryAPI.isClientHoldingCrystalStaff())
		{
			tip.append(new ItemStackTooltipInfo(generateStack(), 16, 16));
			tip.append(new TranslationTooltipInfo(BlocksM.ORE_CRYSTAL.getTranslationKey() + ".name"));
			tip.append(new StringTooltipInfo(":"));
			tip.newLine();
			
			ItemStack drop = new ItemStack(growing.getBlock());
			
			tip.append(new StringTooltipInfo("- "));
			tip.append(new ItemStackTooltipInfo(drop, 16, 16));
			tip.append(new StringTooltipInfo(drop.getDisplayName()));
			tip.newLine();
			
			drop = OreHelper.forOre(growing).getDustedResult();
			
			tip.append(new StringTooltipInfo("-> "));
			tip.append(new ItemStackTooltipInfo(drop, 16, 16));
			tip.append(new StringTooltipInfo(drop.getDisplayName()));
		}
	}
}