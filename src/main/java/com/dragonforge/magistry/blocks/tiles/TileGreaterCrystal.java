package com.dragonforge.magistry.blocks.tiles;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.ce.CrystalStorage;
import com.dragonforge.magistry.api.ce.ICrystalStorage;
import com.zeitheron.hammercore.net.internal.thunder.Thunder.Layer;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.WorldUtil;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagLongArray;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.Constants.NBT;

public class TileGreaterCrystal extends TileSyncableTickable
{
	public List<BlockPos> linked = new ArrayList<>();
	public Map<BlockPos, Float> balanceDelta = new HashMap<>();
	
	public CrystalStorage cu = new CrystalStorage(getCapacity());
	
	public void dispose()
	{
		List<TileGreaterCrystal> tiles = new ArrayList<>();
		for(int i = 0; i < linked.size(); ++i)
		{
			TileGreaterCrystal tmc = WorldUtil.cast(world.getTileEntity(linked.get(i)), TileGreaterCrystal.class);
			if(tmc != null && tmc != this)
				tiles.add(tmc);
			else
			{
				linked.remove(i);
				--i;
			}
		}
		float totalCU = cu.getCUStored();
		for(TileGreaterCrystal tmc : tiles)
			totalCU += tmc.cu.getCUStored();
		float perCU = totalCU / tiles.size();
		for(TileGreaterCrystal tmc : tiles)
		{
			float prev = tmc.cu.getCUStored();
			tmc.cu.setCU(perCU);
			tmc.linked.remove(getPos());
		}
		linked.clear();
	}
	
	public float getCapacity()
	{
		return 16;
	}
	
	@Override
	public void tick()
	{
		if(world.isRemote)
			return;
		
		if(atTickRate(20))
			balance();
		if(atTickRate(10))
			for(int i = 0; i < linked.size(); ++i)
			{
				BlockPos pos = linked.get(i);
				
				TileEntity tile = world.getTileEntity(pos);
				if(tile instanceof TileGreaterCrystal)
					continue;
				ICrystalStorage rem = tile != null && tile.hasCapability(MagistryAPI.CAPABILITY_CRYSTAL_STORAGE, null) ? tile.getCapability(MagistryAPI.CAPABILITY_CRYSTAL_STORAGE, null) : null;
				
				if(rem == null)
				{
					linked.remove(i);
					--i;
					continue;
				}
				
				float prev = rem.getCUStored();
				
				if(rem.canExtractCU())
					cu.supplyCU(rem.extractCU(cu.supplyCU(rem.getCUStored(), true), false), false);
				if(rem.canAcceptCU())
					cu.extractCU(rem.supplyCU(cu.extractCU(rem.getCUCapacity() - rem.getCUStored(), true), false), false);
				
				float d = rem.getCUStored() - prev;
				
				if(Math.abs(d) > 1.0E-4)
				{
					Vec3d thiz = new Vec3d(this.pos).add(.5, .5, .5);
					Vec3d that = new Vec3d(pos).add(.5, .5, .5);
					
					Magistry.proxy.spawnSimpleLine(world, d > 0 ? thiz : that, d > 0 ? that : thiz, getRNG().nextLong(), 5, 0F, new Layer(771, 0x55FFFF, true), new Layer(771, 0x55FFFF, true));
				}
			}
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == MagistryAPI.CAPABILITY_CRYSTAL_STORAGE || super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == MagistryAPI.CAPABILITY_CRYSTAL_STORAGE)
			return (T) cu;
		return super.getCapability(capability, facing);
	}
	
	public boolean accessibleFrom(EnumFacing face)
	{
		return true;
	}
	
	public boolean link(BlockPos pos)
	{
		TileEntity tile = world.getTileEntity(pos);
		TileGreaterCrystal tgc = WorldUtil.cast(tile, TileGreaterCrystal.class);
		ICrystalStorage rem = tile != null && tile.hasCapability(MagistryAPI.CAPABILITY_CRYSTAL_STORAGE, null) ? tile.getCapability(MagistryAPI.CAPABILITY_CRYSTAL_STORAGE, null) : null;
		
		if(rem == null)
		{
			if(linked.contains(pos))
				linked.remove(pos);
			return false;
		}
		
		if(linked.contains(pos))
			return true;
		
		if(linked.size() >= 8)
			return false;
		
		double dist = Math.sqrt(getPos().distanceSq(pos));
		if(dist < 16)
		{
			linked.add(pos);
			if(tgc != null && !tgc.linked.contains(getPos()))
				tgc.linked.add(getPos());
			return true;
		}
		
		return false;
	}
	
	public void balance()
	{
		List<TileGreaterCrystal> tiles = new ArrayList<>();
		for(int i = 0; i < linked.size(); ++i)
		{
			TileGreaterCrystal tmc = WorldUtil.cast(world.getTileEntity(linked.get(i)), TileGreaterCrystal.class);
			if(tmc != null && tmc != this)
				tiles.add(tmc);
		}
		
		float totalCU = cu.getCUStored();
		for(TileGreaterCrystal tmc : tiles)
			totalCU += tmc.cu.getCUStored();
		float perCU = totalCU / (tiles.size() + 1);
		cu.setCU(perCU);
		for(TileGreaterCrystal tmc : tiles)
		{
			float prev = tmc.cu.getCUStored();
			tmc.cu.setCU(perCU);
			
			float d;
			balanceDelta.put(pos, d = tmc.cu.getCUStored() - prev);
			
			if(Math.abs(d) > .00001)
			{
				Vec3d thiz = new Vec3d(pos).add(.5, .5, .5);
				Vec3d that = new Vec3d(tmc.pos).add(.5, .5, .5);
				
				Magistry.proxy.spawnSimpleLine(world, d > 0 ? thiz : that, d > 0 ? that : thiz, getRNG().nextLong(), 5, 0F, new Layer(771, 0x55FFFF, true), new Layer(771, 0x55FFFF, true));
			}
		}
	}
	
	public static long[] getLongArray(NBTTagCompound nbt, String key)
	{
		NBTBase base = nbt.getTag(key);
		if(base instanceof NBTTagLongArray)
		{
			Field data = NBTTagLongArray.class.getDeclaredFields()[0];
			data.setAccessible(true);
			try
			{
				return (long[]) data.get(base);
			} catch(Throwable err)
			{
				err.printStackTrace();
			}
		}
		return new long[0];
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		cu.readFromNBT(nbt);
		NBTBase base = nbt.getTag("Linked");
		linked = new LongArrayList(getLongArray(nbt, "Linked")).stream().map(BlockPos::fromLong).collect(Collectors.toList());
		
		NBTTagList deltas = nbt.getTagList("Deltas", NBT.TAG_COMPOUND);
		for(int i = 0; i < deltas.tagCount(); ++i)
		{
			NBTTagCompound n = deltas.getCompoundTagAt(i);
			balanceDelta.put(BlockPos.fromLong(n.getLong("p")), n.getFloat("d"));
		}
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		cu.writeToNBT(nbt);
		nbt.setTag("Linked", new NBTTagLongArray(linked.stream().map(p -> p.toLong()).collect(Collectors.toList())));
		
		NBTTagList deltas = new NBTTagList();
		for(BlockPos pos : balanceDelta.keySet())
		{
			NBTTagCompound comp = new NBTTagCompound();
			comp.setLong("p", pos.toLong());
			comp.setFloat("d", balanceDelta.get(pos));
			deltas.appendTag(comp);
		}
		nbt.setTag("Deltas", deltas);
		
		balanceDelta.clear();
	}
}