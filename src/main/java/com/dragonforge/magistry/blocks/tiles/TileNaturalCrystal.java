package com.dragonforge.magistry.blocks.tiles;

import java.lang.ref.WeakReference;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.init.BlocksM;
import com.dragonforge.magistry.net.PacketBreakCrystal;
import com.dragonforge.magistry.utils.ProcessDelayedTask;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.ITileDroppable;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileNaturalCrystal extends TileSyncable implements ITileDroppable, ITooltipProviderHC
{
	private Integer color;
	public float saturation;
	public float charge;
	public float maxCharge = 1000F;
	
	{
		for(int i = 0; i < 8; ++i)
			getRNG().nextLong();
		getRNG().setSeed(getRNG().nextLong());
		generateCrystal();
	}
	
	public void generateCrystal()
	{
		color = genColor();
		saturation = .05F + getRNG().nextFloat() * .95F;
		charge = 100F + getRNG().nextFloat() * 900F;
	}
	
	public float discharge(float amount)
	{
		amount = Math.min(charge, amount);
		charge -= amount;
		if(charge <= 0F)
		{
			final WeakReference<World> wref = new WeakReference<World>(world);
			final BlockPos pos = this.pos;
			
			new ProcessDelayedTask(12, () ->
			{
				wref.get().destroyBlock(pos, false);
				HCNet.INSTANCE.sendToAllAround(new PacketBreakCrystal(pos), new WorldLocation(wref.get(), pos).getPointWithRad(100));
			}).start();
		} else
			sendChangesToNearby();
		return amount;
	}
	
	public int getRenderColor()
	{
		return ColorHelper.interpolate(ColorHelper.getBrightnessRGB(getColor()), getColor(), charge / maxCharge);
	}
	
	public ItemStack generateStack()
	{
		ItemStack is = new ItemStack(BlocksM.NATURAL_CRYSTAL);
		NBTTagCompound nbt = new NBTTagCompound();
		writeNBT(nbt);
		is.setTagCompound(nbt);
		return is;
	}
	
	public void loadFromStack(ItemStack stack)
	{
		if(stack.hasTagCompound())
		{
			readNBT(stack.getTagCompound());
			sendChangesToNearby();
		}
	}
	
	protected int genColor()
	{
		return ColorHelper.packRGB(getRNG().nextFloat(), getRNG().nextFloat(), getRNG().nextFloat());
	}
	
	public int getColor()
	{
		if(color == null)
			color = genColor();
		return color.intValue();
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("Color", getColor());
		nbt.setFloat("Saturation", saturation);
		nbt.setFloat("Charge", charge);
		nbt.setFloat("MaxCharge", maxCharge);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		generateCrystal();
		
		if(nbt.hasKey("Color"))
			color = nbt.getInteger("Color");
		if(nbt.hasKey("Saturation"))
			saturation = MathHelper.clamp(nbt.getFloat("Saturation"), 0, 1);
		if(nbt.hasKey("MaxCharge"))
			maxCharge = nbt.getFloat("MaxCharge");
		if(nbt.hasKey("Charge"))
			charge = MathHelper.clamp(nbt.getFloat("Charge"), 0, maxCharge);
		
		tooltipDirty = true;
	}
	
	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		IPlayerKnowledge ipk = MagistryAPI.getPlayerKnowledge(player);
		if(ipk != null)
			ipk.unlockAndGiveNotes(Magistry.MOD_ID + ":crystals");
		
		if(!player.capabilities.isCreativeMode && player.canHarvestBlock(world.getBlockState(pos)))
			WorldUtil.spawnItemStack(world, pos, generateStack());
	}
	
	protected boolean tooltipDirty;
	protected boolean lastHeld;
	
	@Override
	public boolean isTooltipDirty()
	{
		boolean held = MagistryAPI.isClientHoldingCrystalStaff();
		if(held != lastHeld)
		{
			lastHeld = held;
			tooltipDirty = true;
		}
		
		return tooltipDirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		this.tooltipDirty = dirty;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		if(MagistryAPI.isClientHoldingCrystalStaff())
		{
			tip.append(new ItemStackTooltipInfo(generateStack(), 16, 16));
			tip.append(new TranslationTooltipInfo(BlocksM.NATURAL_CRYSTAL.getTranslationKey() + ".name"));
			tip.append(new StringTooltipInfo(":"));
			tip.newLine();
			
			tip.append(new TranslationTooltipInfo("info.magistry:crystal.saturation"));
			tip.append(new StringTooltipInfo(": "));
			{
				StringTooltipInfo sti = new StringTooltipInfo(((int) (saturation * 100)) + "%");
				sti.color = MathHelper.hsvToRGB(Math.max(0, saturation) / 3, 1, 1);
				tip.append(sti);
			}
			tip.newLine();
			
			tip.append(new TranslationTooltipInfo("info.magistry:crystal.charge"));
			tip.append(new StringTooltipInfo(": "));
			{
				float f = charge / maxCharge;
				StringTooltipInfo sti = new StringTooltipInfo(((int) (f * 100)) + "%");
				sti.color = ColorHelper.packRGB(f, f, f);
				tip.append(sti);
			}
			tip.newLine();
			
			tip.append(new TranslationTooltipInfo("info.magistry:crystal.color"));
			tip.append(new StringTooltipInfo(": "));
			tip.newLine();
			{
				int rc = getRenderColor();
				int ac = getColor();
				
				tip.append(new StringTooltipInfo("(a) " + ColorHelper.getColorName(ac) + " ("));
				StringTooltipInfo sti = new StringTooltipInfo("#" + Integer.toHexString(ac));
				sti.color = ac;
				tip.append(sti);
				tip.append(new StringTooltipInfo(")"));
				
				tip.newLine();
				
				tip.append(new StringTooltipInfo("(v) " + ColorHelper.getColorName(rc) + " ("));
				sti = new StringTooltipInfo("#" + Integer.toHexString(rc));
				sti.color = rc;
				tip.append(sti);
				tip.append(new StringTooltipInfo(")"));
			}
		}
	}
}