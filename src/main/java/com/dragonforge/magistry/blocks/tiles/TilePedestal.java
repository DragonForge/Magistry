package com.dragonforge.magistry.blocks.tiles;

import java.util.Map;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.net.PacketUpdatePedestalLight;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.ITileDroppable;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;
import com.zeitheron.hammercore.utils.inventory.IInventoryListener;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

public class TilePedestal extends TileSyncable implements ITooltipProviderHC, ITileDroppable
{
	public final InventoryDummy item = new InventoryDummy(1);
	private ItemStack prevDrawn = ItemStack.EMPTY;
	
	public ItemStack controlItem = ItemStack.EMPTY;
	
	{
		item.listener = new IInventoryListener()
		{
			@Override
			public void slotChange(int slot, ItemStack stack)
			{
				sendChangesToNearby();
				refreshLightLevel();
			}
			
			@Override
			public IInventory getInventory()
			{
				return item;
			}
		};
	}
	
	public void refreshLightLevel()
	{
		world.checkLight(pos);
		sendChangesToNearby();
		HCNet.INSTANCE.sendToAllAround(new PacketUpdatePedestalLight().setPos(pos), getLocation().getPointWithRad(500));
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		tooltipDirty = true;
		item.readFromNBT(nbt);
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		item.writeToNBT(nbt);
	}
	
	@Override
	public void addProperties(Map<String, Object> properties, RayTraceResult trace)
	{
		properties.put("item", getItem().isEmpty() ? "empty" : (getItem().getCount() + "x\"" + getItem().getItem().getRegistryName() + "\":" + getItem().getItemDamage()));
	}
	
	private boolean tooltipDirty = false;
	
	@Override
	public boolean isTooltipDirty()
	{
		if(!prevDrawn.isItemEqual(getItem()) || prevDrawn.getCount() != getItem().getCount())
			return true;
		return tooltipDirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		tooltipDirty = dirty;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		prevDrawn = item.getStackInSlot(0);
		
		String info = "info." + Magistry.MOD_ID + ":";
		if(!item.getStackInSlot(0).isEmpty())
		{
			tip.append(new StringTooltipInfo(item.getStackInSlot(0).getCount() + "x"));
			tip.append(new ItemStackTooltipInfo(item.getStackInSlot(0), 12, 12));
			tip.append(new StringTooltipInfo(" " + item.getStackInSlot(0).getDisplayName()));
		} else
			tip.append(new TranslationTooltipInfo(info + "empty"));
	}
	
	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		item.drop(world, pos);
	}
	
	public ItemStack getItem()
	{
		return item.getStackInSlot(0);
	}
	
	public void setItem(ItemStack stack)
	{
		item.setInventorySlotContents(0, stack);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
	}
	
	InvWrapper iw;
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
			return (T) (iw != null ? iw : (iw = new InvWrapper(item)));
		return super.getCapability(capability, facing);
	}
	
	public boolean controlChanged()
	{
		return !controlItem.isItemEqual(getItem()) || getItem().getCount() != controlItem.getCount();
	}
}