package com.dragonforge.magistry.blocks;

import com.zeitheron.hammercore.utils.IRegisterListener;

import net.minecraft.block.Block;
import net.minecraft.block.BlockGlass;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

public class BlockBlueQuartzGlass extends Block implements IRegisterListener
{
	public BlockBlueQuartzGlass()
	{
		super(Material.GLASS);
		setTranslationKey("blue_quartz_glass");
		setSoundType(SoundType.GLASS);
		setHardness(.5F);
		setResistance(300F);
	}
	
	@Override
	public void onRegistered()
	{
		OreDictionary.registerOre("blockGlass", this);
		OreDictionary.registerOre("blockGlassQuartz", this);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public BlockRenderLayer getRenderLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@SideOnly(Side.CLIENT)
	public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
	{
		IBlockState iblockstate = blockAccess.getBlockState(pos.offset(side));
		Block block = iblockstate.getBlock();
		if(blockState != iblockstate)
			return true;
		if(block == this)
			return false;
		return super.shouldSideBeRendered(blockState, blockAccess, pos, side);
	}
}