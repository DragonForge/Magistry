package com.dragonforge.magistry.blocks.machines.tiles;

import java.util.ArrayList;
import java.util.List;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.api.research.ResearchTag;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockCore;
import com.dragonforge.magistry.client.gui.GuiResearch;
import com.dragonforge.magistry.inventory.ContainerResearch;
import com.dragonforge.magistry.items.ItemLostNotes;
import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants.NBT;

public class TileResearchTable extends TileMultiblockCore
{
	public final InventoryDummy inventory = new InventoryDummy(10);
	
	public final List<ResearchTag> enabledTags = new ArrayList<>();
	public final List<ResearchBase> matchingIncomplete = new ArrayList<>();
	
	public ResearchBase discovery;
	public int discoveryProgress;
	public NonNullList<ItemStack> leftovers = NonNullList.create();
	public int totalItemCount;
	
	public EntityPlayerMP user;
	
	public void selectTag(ResearchTag tag)
	{
		if(!enabledTags.contains(tag))
			enabledTags.add(tag);
		else
			enabledTags.remove(tag);
		matchingIncomplete.clear();
		if(!world.isRemote)
			sync();
	}
	
	@Override
	public void tick()
	{
		super.tick();
		if(!world.isRemote)
		{
			if(!matchingIncomplete.isEmpty())
			{
				if(user == null)
					matchingIncomplete.clear();
			} else if(user != null)
			{
				matchingIncomplete.clear();
				IPlayerKnowledge ipk = MagistryAPI.getPlayerKnowledge(user);
				if(ipk != null)
				{
					MagistryAPI.RESEARCH_REGISTRY.getValuesCollection().stream().filter(r -> !ipk.hasResearch(r.getRegistryName().toString()) && r.isResearchable(user, enabledTags) && ipk.canBeginResearch(r.getRegistryName().toString())).forEach(matchingIncomplete::add);
					sync();
				}
			}
			
			if(discovery != null)
			{
				if(discoveryProgress < 200)
				{
					++discoveryProgress;
					if(discoveryProgress == 200)
					{
						leftovers.clear();
						for(ItemStack ci : discovery.getResIngredients())
							leftovers.add(ci.copy());
						sync();
					}
				} else if(!leftovers.isEmpty())
				{
					if(atTickRate(20))
					{
						ItemStack ingr = leftovers.get(0);
						for(int i = 2; i < inventory.getSizeInventory(); ++i)
							if(ingr.isItemEqual(inventory.getStackInSlot(i)))
							{
								inventory.decrStackSize(i, 1);
								ingr.shrink(1);
								if(ingr.isEmpty())
									leftovers.remove(0);
								sync();
								break;
							}
					}
				} else if(inventory.getStackInSlot(1).isEmpty())
				{
					inventory.setInventorySlotContents(1, ItemLostNotes.createForResearch(discovery.getRegistryName()));
					discoveryProgress = totalItemCount = 0;
					discovery = null;
					matchingIncomplete.clear();
					enabledTags.clear();
					sync();
				}
			} else
			{
				if(discoveryProgress > 0)
					--discoveryProgress;
				totalItemCount = 0;
				leftovers.clear();
			}
		} else
		{
			if(totalItemCount == 0 && discovery != null)
			{
				for(ItemStack is : discovery.getResIngredients())
					totalItemCount += is.getCount();
			} else if(discovery == null)
			{
				if(discoveryProgress > 0)
					--discoveryProgress;
				totalItemCount = 0;
				leftovers.clear();
			}
		}
	}
	
	public boolean isTagEnabled(ResearchTag tag)
	{
		return enabledTags.contains(tag);
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiResearch(player, this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return user != null ? null : new ContainerResearch(player, this);
	}
	
	@Override
	public boolean onSubBlockActivated(World worldIn, BlockPos relPos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(user == null)
			GuiManager.openGui(playerIn, this);
		return true;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("Items", inventory.writeToNBT(new NBTTagCompound()));
		nbt.setTag("Leftovers", ItemStackHelper.saveAllItems(new NBTTagCompound(), leftovers));
		
		NBTTagList tags = new NBTTagList();
		for(int i = 0; i < enabledTags.size(); ++i)
			tags.appendTag(new NBTTagString(enabledTags.get(i).toString()));
		nbt.setTag("Tags", tags);
		
		NBTTagList research = new NBTTagList();
		for(int i = 0; i < matchingIncomplete.size(); ++i)
			research.appendTag(new NBTTagString(matchingIncomplete.get(i).getRegistryName().toString()));
		nbt.setTag("Research", research);
		
		if(discovery != null)
			nbt.setString("Discovery", discovery.getRegistryName().toString());
		
		nbt.setInteger("DiscoveryProgress", discoveryProgress);
		
		super.writeNBT(nbt);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		inventory.readFromNBT(nbt.getCompoundTag("Items"));
		
		NBTTagCompound lov = nbt.getCompoundTag("Leftovers");
		leftovers.clear();
		int c = lov.getTagList("Items", NBT.TAG_COMPOUND).tagCount();
		for(int i = 0; i < c; ++i)
			leftovers.add(ItemStack.EMPTY);
		ItemStackHelper.loadAllItems(lov, leftovers);
		
		enabledTags.clear();
		NBTTagList tags = nbt.getTagList("Tags", NBT.TAG_STRING);
		for(int i = 0; i < tags.tagCount(); ++i)
		{
			ResearchTag tag = ResearchTag.TAGS.get(tags.getStringTagAt(i));
			if(tag != null)
				enabledTags.add(tag);
		}
		
		matchingIncomplete.clear();
		NBTTagList research = nbt.getTagList("Research", NBT.TAG_STRING);
		for(int i = 0; i < research.tagCount(); ++i)
		{
			ResearchBase rb = MagistryAPI.getResearch(research.getStringTagAt(i));
			if(rb != null)
				matchingIncomplete.add(rb);
		}
		
		if(nbt.hasKey("Discovery", NBT.TAG_STRING))
			discovery = MagistryAPI.getResearch(nbt.getString("Discovery"));
		
		discoveryProgress = nbt.getInteger("DiscoveryProgress");
		
		super.readNBT(nbt);
	}
	
	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		inventory.drop(world, pos);
		super.createDrop(player, world, pos);
	}
}