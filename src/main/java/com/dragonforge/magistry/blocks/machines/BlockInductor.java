package com.dragonforge.magistry.blocks.machines;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.blocks.machines.tiles.TileInductor;
import com.dragonforge.magistry.init.BlocksM;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.SoundUtil;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockInductor extends BlockMultiblockCore<TileInductor>
{
	public BlockInductor()
	{
		super(Material.IRON, TileInductor.class, "inductor");
		this.fxSprites = new ResourceLocation(Magistry.MOD_ID, "textures/models/inductor.png");
		setHarvestLevel("pickaxe", 1);
		setHardness(2);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileInductor inductor = WorldUtil.cast(worldIn.getTileEntity(pos), TileInductor.class);
		if(inductor != null)
		{
			ItemStack stack = inductor.output.getStackInSlot(0);
			if(!stack.isEmpty())
			{
				if(!worldIn.isRemote)
				{
					HCNet.swingArm(playerIn, hand);
					SoundUtil.playSoundEffect(worldIn, SoundEvents.ENTITY_ITEM_PICKUP.getRegistryName().toString(), pos, .5F, 2F, SoundCategory.PLAYERS);
					if(!playerIn.addItemStackToInventory(stack))
					{
						EntityItem ei = playerIn.dropItem(stack, true);
						if(ei != null)
							ei.setNoPickupDelay();
					}
					inductor.output.setInventorySlotContents(0, ItemStack.EMPTY);
				}
				return true;
			}
		}
		return false;
	}
	
	@Override
	public ResourceLocation getMultiblockForPlacement(EntityPlayer player)
	{
		return new ResourceLocation(Magistry.MOD_ID, "inductor");
	}
}