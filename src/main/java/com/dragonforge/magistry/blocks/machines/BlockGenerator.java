package com.dragonforge.magistry.blocks.machines;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.blocks.machines.tiles.TileGenerator;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.api.parts.EnumBlockMetalPart;

import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class BlockGenerator extends BlockMultiblockCore<TileGenerator>
{
	public BlockGenerator()
	{
		super(Material.IRON, TileGenerator.class, "generator");
		this.fxSprites = new ResourceLocation(Magistry.MOD_ID, "textures/models/generator.png");
		setHarvestLevel("pickaxe", 1);
		setHardness(2);
	}
	
	@Override
	public ResourceLocation getMultiblockForPlacement(EntityPlayer player)
	{
		return new ResourceLocation(Magistry.MOD_ID, "generator");
	}
}