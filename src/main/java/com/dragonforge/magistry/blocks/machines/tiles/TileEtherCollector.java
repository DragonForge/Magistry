package com.dragonforge.magistry.blocks.machines.tiles;

import com.dragonforge.magistry.blocks.tiles.TileMultiblockCore;
import com.zeitheron.hammercore.utils.FrictionRotator;

import net.minecraft.block.Block;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;

public class TileEtherCollector extends TileMultiblockCore
{
	public final FrictionRotator rotator = new FrictionRotator();
	
	public TileEtherCollector()
	{
		aabbMapper = this::$aabb;
	}
	
	@Override
	public void tick()
	{
		super.tick();
		rotator.friction = .05F;
		rotator.update();
		
		if(ticksExisted % 200 >= 100)
			rotator.speedup(.1F);
	}
	
	private AxisAlignedBB $aabb(BlockPos rel)
	{
		int x = rel.getX();
		int y = rel.getY();
		int z = rel.getZ();
		
		if(x == 0 && z == 0)
		{
			if(y == 0 || y == 3)
				return new AxisAlignedBB(.5, 0, .5, 1, 1, 1);
			return new AxisAlignedBB(9 / 16D, 0, 9 / 16D, 11 / 16D, 1, 11 / 16D);
		}
		
		if(x == 2 && z == 0)
		{
			if(y == 0 || y == 3)
				return new AxisAlignedBB(0, 0, .5, .5, 1, 1);
			return new AxisAlignedBB(5 / 16D, 0, 9 / 16D, 7 / 16D, 1, 11 / 16D);
		}
		
		if(x == 0 && z == 2)
		{
			if(y == 0 || y == 3)
				return new AxisAlignedBB(.5, 0, 0, 1, 1, .5);
			return new AxisAlignedBB(9 / 16D, 0, 5 / 16D, 11 / 16D, 1, 7 / 16D);
		}
		
		if(x == 2 && z == 2)
		{
			if(y == 0 || y == 3)
				return new AxisAlignedBB(0, 0, 0, .5, 1, .5);
			return new AxisAlignedBB(5 / 16D, 0, 5 / 16D, 7 / 16D, 1, 7 / 16D);
		}
		
		if(x == 1 && z == 1)
			if(y == 0)
				return new AxisAlignedBB(0, 0, 0, 1, 1.5, 1);
			else if(y == 1 || y == 2)
				return Block.FULL_BLOCK_AABB.offset(0, y == 1 ? .5 : -.5, 0);
			else if(y == 3)
				return new AxisAlignedBB(0, -.5, 0, 1, 1, 1);
			
		if(x == 1 && z == 0)
			if(y == 0)
				return new AxisAlignedBB(0, 0, .5, 1, .5, 1);
			else if(y == 3)
				return new AxisAlignedBB(0, .5, .5, 1, 1, 1);
			
		if(x == 0 && z == 1)
			if(y == 0)
				return new AxisAlignedBB(.5, 0, 0, 1, .5, 1);
			else if(y == 3)
				return new AxisAlignedBB(.5, .5, 0, 1, 1, 1);
			
		if(x == 1 && z == 2)
			if(y == 0)
				return new AxisAlignedBB(0, 0, 0, 1, .5, .5);
			else if(y == 3)
				return new AxisAlignedBB(0, .5, 0, 1, 1, .5);
			
		if(x == 2 && z == 1)
			if(y == 0)
				return new AxisAlignedBB(0, 0, 0, .5, .5, 1);
			else if(y == 3)
				return new AxisAlignedBB(0, .5, 0, .5, 1, 1);
			
		return null;
	}
}