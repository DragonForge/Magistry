package com.dragonforge.magistry.blocks.machines.tiles;

import java.util.List;
import java.util.function.Consumer;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.ce.ICrystalStorage;
import com.dragonforge.magistry.api.crafting.RecipesCrusher;
import com.dragonforge.magistry.api.crafting.recipes.CrusherRecipe;
import com.dragonforge.magistry.api.multiblock.BlockComponent;
import com.dragonforge.magistry.api.multiblock.MultiblockTileRecipe;
import com.dragonforge.magistry.api.tile.IRotateable;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockCore;
import com.dragonforge.magistry.init.BlocksM;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.api.parts.EnumBlockMetalPart;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class TileCrusher extends TileMultiblockCore implements IRotateable
{
	EnumFacing orientation = EnumFacing.SOUTH;
	
	ItemStack processing = ItemStack.EMPTY;
	CrusherRecipe recipe;
	float consumedCU;
	
	public TileCrusher()
	{
		this.aabbMapper = this::$aabb;
	}
	
	@Override
	public EnumFacing getFacing()
	{
		return orientation;
	}
	
	@Override
	public void setFacing(EnumFacing face)
	{
		this.orientation = face;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		super.writeNBT(nbt);
		nbt.setString("Facing", orientation.getName());
		nbt.setFloat("CCU", consumedCU);
		nbt.setTag("Proc", processing.serializeNBT());
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		super.readNBT(nbt);
		orientation = EnumFacing.byName(nbt.getString("Facing"));
		consumedCU = nbt.getFloat("CCU");
		processing = new ItemStack(nbt.getCompoundTag("Proc"));
	}
	
	@Override
	public void tick()
	{
		if(world.isRemote)
			return;
		
		if(!processing.isEmpty() && recipe == null)
		{
			recipe = RecipesCrusher.findRecipe(processing);
			if(recipe == null)
			{
				drop(processing);
				processing = ItemStack.EMPTY;
			}
		}
		
		if(recipe != null)
		{
			TileEntity up = world.getTileEntity(pos.up());
			if(up != null && up.hasCapability(MagistryAPI.CAPABILITY_CRYSTAL_STORAGE, EnumFacing.DOWN))
			{
				ICrystalStorage cs = up.getCapability(MagistryAPI.CAPABILITY_CRYSTAL_STORAGE, EnumFacing.DOWN);
				if(cs != null && cs.canExtractCU())
				{
					float extract = Math.min(recipe.cost / 150, cs.getCUStored());
					consumedCU += cs.extractCU(Math.min(extract, recipe.cost - consumedCU), false);
					if(consumedCU >= recipe.cost)
					{
						float extra = consumedCU - recipe.cost;
						if(extra > 0)
							cs.supplyCU(extra, false);
					}
				}
			}
			
			if(consumedCU >= recipe.cost)
			{
				consumedCU = 0;
				recipe.createDrop(this, dropper);
				recipe = null;
				processing.shrink(1);
			}
		}
		
		if(processing.isEmpty())
			processing = consume();
		else if(atTickRate(5))
		{
			BlockPos sp = pos;
			
			Vec3d smoke = new Vec3d(sp.getX() + orientation.getXOffset() * 2.25 + .5 + orientation.rotateYCCW().getXOffset() * getRNG().nextFloat(), sp.getY() + .5F, sp.getZ() + orientation.getZOffset() * 2.25 + .5 + orientation.rotateYCCW().getZOffset() * getRNG().nextFloat());
			HCNet.spawnParticle(world, getRNG().nextBoolean() ? EnumParticleTypes.SMOKE_NORMAL : EnumParticleTypes.SMOKE_LARGE, smoke.x, smoke.y, smoke.z, orientation.getXOffset() * .1F, 0, orientation.getZOffset() * .1F);
			
			if(getRNG().nextBoolean())
			{
				smoke = new Vec3d(sp.getX() + orientation.getXOffset() * 2.25 + .5 + orientation.rotateYCCW().getXOffset() * getRNG().nextFloat(), sp.getY() + .5F, sp.getZ() + orientation.getZOffset() * 2.25 + .5 + orientation.rotateYCCW().getZOffset() * getRNG().nextFloat());
				HCNet.spawnParticle(world, EnumParticleTypes.ITEM_CRACK, smoke.x, smoke.y, smoke.z, orientation.getXOffset() * .1F, .1F, orientation.getZOffset() * .1F, Item.getIdFromItem(processing.getItem()));
			}
		}
		
		super.tick();
	}
	
	public ItemStack consume()
	{
		BlockPos inPos = pos.offset(getFacing().rotateYCCW()).up();
		List<EntityItem> inputs = world.getEntitiesWithinAABB(EntityItem.class, new AxisAlignedBB(inPos.getX() + .3, inPos.getY(), inPos.getZ() + .3, inPos.getX() + .7, inPos.getY() + .001, inPos.getZ() + .7));
		if(!inputs.isEmpty())
			inputs.get(0).setDead();
		return inputs.isEmpty() ? ItemStack.EMPTY : inputs.get(0).getItem();
	}
	
	final Consumer<ItemStack> dropper = this::drop;
	
	private void drop(ItemStack stack)
	{
		BlockPos outPos = pos.offset(getFacing()).down();
		
		TileEntity tile = world.getTileEntity(outPos.offset(orientation.rotateY()));
		
		if(tile != null && tile.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, orientation.rotateYCCW()))
		{
			IItemHandler ih = tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, orientation.rotateYCCW());
			if(ih != null)
				for(int i = 0; i < ih.getSlots() && !stack.isEmpty(); ++i)
					stack = ih.insertItem(i, stack, false);
		}
		
		if(stack.isEmpty())
			return;
		
		EnumFacing spitFace = getFacing().rotateY();
		Vec3d spitVec = new Vec3d(spitFace.getXOffset(), spitFace.getYOffset(), spitFace.getZOffset());
		Vec3d sp = new Vec3d(outPos).add(.5, .5, .5).add(spitVec.scale(.6));
		Vec3d v = spitVec.scale(.1);
		EntityItem ei = WorldUtil.spawnItemStack(world, sp.x, sp.y, sp.z, stack);
		ei.motionX = v.x;
		ei.motionY = v.y;
		ei.motionZ = v.z;
	}
	
	@Override
	public boolean hasSubCapability(BlockPos part, Capability<?> capability, EnumFacing facing)
	{
		if(facing == EnumFacing.UP && capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			MultiblockTileRecipe recipe = WorldUtil.cast(MagistryAPI.MULTIBLOCK_REGISTRY.getValue(multiblock), MultiblockTileRecipe.class);
			if(recipe != null)
			{
				BlockComponent bi = recipe.getBlock(part.getX(), part.getY(), part.getZ());
				return bi.input.test(MetalRegistry.getMetal("brass").get(EnumBlockMetalPart.BLOCK).getDefaultState());
			}
		}
		
		return false;
	}
	
	@Override
	public <T> T getSubCapability(BlockPos part, Capability<T> capability, EnumFacing facing)
	{
		if(facing == EnumFacing.UP && capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			MultiblockTileRecipe recipe = WorldUtil.cast(MagistryAPI.MULTIBLOCK_REGISTRY.getValue(multiblock), MultiblockTileRecipe.class);
			if(recipe != null)
			{
				BlockComponent bi = recipe.getBlock(part.getX(), part.getY(), part.getZ());
				if(bi.input.test(MetalRegistry.getMetal("brass").get(EnumBlockMetalPart.BLOCK).getDefaultState()))
				{
					IItemHandler thisHandler = new IItemHandler()
					{
						@Override
						public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
						{
							if(processing.isEmpty())
							{
								processing = stack.copy();
								processing.setCount(1);
								
								stack.shrink(1);
								
								return ItemStack.EMPTY;
							}
							
							return stack;
						}
						
						@Override
						public ItemStack getStackInSlot(int slot)
						{
							return processing;
						}
						
						@Override
						public int getSlots()
						{
							return 1;
						}
						
						@Override
						public int getSlotLimit(int slot)
						{
							return 1;
						}
						
						@Override
						public ItemStack extractItem(int slot, int amount, boolean simulate)
						{
							return ItemStack.EMPTY;
						}
					};
					
					return (T) thisHandler;
				}
			}
		}
		
		return null;
	}
	
	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		super.createDrop(player, world, pos);
		if(!processing.isEmpty())
			WorldUtil.spawnItemStack(world, pos, processing);
	}
	
	private AxisAlignedBB $aabb(BlockPos rel)
	{
		MultiblockTileRecipe recipe = WorldUtil.cast(MagistryAPI.MULTIBLOCK_REGISTRY.getValue(multiblock), MultiblockTileRecipe.class);
		if(recipe != null)
		{
			BlockComponent bi = recipe.getBlock(rel.getX(), rel.getY(), rel.getZ());
			
			if(bi.input.test(BlocksM.INDUCTION_COIL.getDefaultState()))
				return new AxisAlignedBB(0, 0, 0, 1, 9.2 / 16F, 1);
			
			if(bi.input.test(MetalRegistry.getMetal("iron").get(EnumBlockMetalPart.CASING).getDefaultState()))
				return new AxisAlignedBB(0, 0, 0, 1, 15 / 16F, 1);
			
			if(bi.input.test(MetalRegistry.getMetal("brass").get(EnumBlockMetalPart.BLOCK).getDefaultState()))
				return new AxisAlignedBB(0, 0, 0, 1, 15 / 16F, 1);
		}
		return Block.FULL_BLOCK_AABB;
	}
}