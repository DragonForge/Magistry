package com.dragonforge.magistry.blocks.machines;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.blocks.machines.tiles.TileResearchTable;

import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

public class BlockResearchTable extends BlockMultiblockCore<TileResearchTable>
{
	public BlockResearchTable()
	{
		super(Material.WOOD, TileResearchTable.class, "research_table");
		this.fxSprites = new ResourceLocation(Magistry.MOD_ID, "textures/models/research_table.png");
		setHardness(2);
	}
	
	@Override
	public ResourceLocation getMultiblockForPlacement(EntityPlayer player)
	{
		return new ResourceLocation(Magistry.MOD_ID, "research_table");
	}
}