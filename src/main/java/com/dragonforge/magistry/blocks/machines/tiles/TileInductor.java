package com.dragonforge.magistry.blocks.machines.tiles;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.crafting.RecipesInductor;
import com.dragonforge.magistry.api.crafting.recipes.InductorRecipe;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockCore;
import com.dragonforge.magistry.blocks.tiles.TilePedestal;
import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.thunder.Thunder.Layer;
import com.zeitheron.hammercore.utils.AABBUtils;
import com.zeitheron.hammercore.utils.ListUtils;
import com.zeitheron.hammercore.utils.SoundUtil;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.inventory.IInventoryListener;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

public class TileInductor extends TileMultiblockCore implements IEnergyStorage
{
	public final InventoryDummy output = new InventoryDummy(1);
	public FEEnergyStorage storage = new FEEnergyStorage(8000);
	public int craftTime = 0;
	public int maxCraftTime;
	public int prevCT;
	
	public TileInductor()
	{
		aabbMapper = this::$aabb;
		output.validSlots = (slot, stack) -> false;
		
		output.listener = new IInventoryListener()
		{
			@Override
			public void slotChange(int slot, ItemStack stack)
			{
				sendChangesToNearby();
			}
			
			@Override
			public IInventory getInventory()
			{
				return output;
			}
		};
	}
	
	@Override
	public void tick()
	{
		if(!world.isRemote && storage.getEnergyStored() >= 100)
			if(canCraft())
			{
				maxCraftTime = getCraftTime();
				
				storage.extractEnergy(100, false);
				++craftTime;
				spawnZaps();
				
				if(craftTime >= maxCraftTime && craft())
				{
					SoundUtil.playSoundEffect(world, "entity.generic.explode", pos, .25F, .85F, SoundCategory.BLOCKS);
					SoundUtil.playSoundEffect(world, "entity.firework.large_blast", pos, .5F, .7F, SoundCategory.BLOCKS);
					SoundUtil.playSoundEffect(world, "entity.firework.twinkle", pos, .5F, .7F, SoundCategory.BLOCKS);
					craftTime = 0;
				}
				
				if(craftTime % 4 == 0)
				{
					AxisAlignedBB aabb = new AxisAlignedBB(pos.getX(), pos.getY() - 1, pos.getZ(), pos.getX() + 1, pos.getY() + 1, pos.getZ() + 1).grow(2.5);
					List<EntityLivingBase> entitiesToMurder = world.getEntitiesWithinAABB(EntityLivingBase.class, aabb);
					if(!entitiesToMurder.isEmpty() && getRNG().nextBoolean())
					{
						EntityLivingBase murder = ListUtils.random(entitiesToMurder, getRNG());
						craftTime -= Math.min(craftTime, 4);
						murder.setFire(1);
						murder.attackEntityFrom(MagistryAPI.ELECTRICAL_DAMAGE, 4);
						HammerCore.particleProxy.spawnSimpleThunder(world, coilRNGPos(), AABBUtils.randomPosWithin(murder.getEntityBoundingBox(), getRNG()), getRNG().nextLong(), 5, 3F, new Layer(771, 0x00AAFF, true), new Layer(1, 0x00AAFF, true));
						SoundUtil.playSoundEffect(world, "item.flintandsteel.use", pos, .5F, 1.6F + getRNG().nextFloat() * .4F, SoundCategory.BLOCKS);
					}
				}
				sendChangesToNearby();
			} else if(craftTime > 0)
				craftTime--;
			else
				;
		else if(!world.isRemote)
		{
			if(craftTime > 0)
			{
				if(atTickRate(20))
					--craftTime;
				if(getRNG().nextInt(4) == 0)
					HammerCore.particleProxy.spawnSimpleThunder(world, coilRNGPos(), coilRNGPos(), getRNG().nextLong(), 20, 8F, new Layer(771, 0xFF2222, true), new Layer(1, 0xFF2222, true));
			}
		}
		super.tick();
	}
	
	public Vec3d coilRNGPos()
	{
		return new Vec3d(pos).add(.2 + getRNG().nextDouble() * .6, -1 + 0.125 + getRNG().nextDouble() * 1.75, .2 + getRNG().nextDouble() * .6);
	}
	
	private void spawnZaps()
	{
		List<TilePedestal> pedestals = new ArrayList<>();
		for(int x = -2; x < 3; ++x)
			for(int z = -2; z < 3; ++z)
			{
				BlockPos np = pos.add(x, -1, z);
				TilePedestal t = WorldUtil.cast(world.getTileEntity(np), TilePedestal.class);
				if(t != null && !t.getItem().isEmpty())
					pedestals.add(t);
			}
		
		int rgb = Color.HSBtoRGB(Math.max(0, craftTime / Math.max(maxCraftTime, 1F)) / 3, 1, 1);
		
		boolean crack = false;
		for(TilePedestal t : pedestals)
			if(t.getRNG().nextBoolean() && t.getRNG().nextInt(pedestals.size() * 3) == 0 && (crack = true))
				HammerCore.particleProxy.spawnSimpleThunder(world, coilRNGPos(), new Vec3d(t.getPos()).add(.5, .9, .5), getRNG().nextLong(), 5, 5F, new Layer(771, rgb, true), new Layer(1, rgb, true));
		if(crack)
			SoundUtil.playSoundEffect(world, "item.flintandsteel.use", pos, .5F, 1.6F + getRNG().nextFloat() * .4F, SoundCategory.BLOCKS);
	}
	
	public int getCraftTime()
	{
		List<TilePedestal> pedestals = new ArrayList<>();
		for(int x = -2; x < 3; ++x)
			for(int z = -2; z < 3; ++z)
			{
				BlockPos np = pos.add(x, -1, z);
				TilePedestal t = WorldUtil.cast(world.getTileEntity(np), TilePedestal.class);
				if(t != null && !t.getItem().isEmpty())
					pedestals.add(t);
			}
		
		float ingredients = 0;
		
		InductorRecipe ir = RecipesInductor.findRecipe(RecipesInductor.convertToItems(pedestals));
		if(ir != null)
			ingredients += (ir.inputs.size() * 10F / ir.outputTransformer.apply(ir.output).getCount());
		
		return 40 + (int) (ingredients * 20);
	}
	
	public boolean canCraft()
	{
		List<TilePedestal> pedestals = new ArrayList<>();
		for(int x = -2; x < 3; ++x)
			for(int z = -2; z < 3; ++z)
			{
				BlockPos np = pos.add(x, -1, z);
				TilePedestal t = WorldUtil.cast(world.getTileEntity(np), TilePedestal.class);
				if(t != null && !t.getItem().isEmpty())
					pedestals.add(t);
			}
		
		return output.getStackInSlot(0).isEmpty() && RecipesInductor.canCraft(pedestals);
	}
	
	public boolean craft()
	{
		List<TilePedestal> pedestals = new ArrayList<>();
		for(int x = -2; x < 3; ++x)
			for(int z = -2; z < 3; ++z)
			{
				BlockPos np = pos.add(x, -1, z);
				TilePedestal t = WorldUtil.cast(world.getTileEntity(np), TilePedestal.class);
				if(t != null && !t.getItem().isEmpty())
				{
					t.controlItem = t.getItem().copy();
					pedestals.add(t);
				}
			}
		
		if(RecipesInductor.canCraft(pedestals))
		{
			ItemStack output = RecipesInductor.consumeItemsAndGenerateOutput(pedestals);
			List<TilePedestal> changed = new ArrayList<>();
			pedestals.stream().filter(p -> p.controlChanged()).forEach(changed::add);
			finishCraft(output, changed);
			return true;
		}
		
		return false;
	}
	
	public void finishCraft(ItemStack out, List<TilePedestal> changed)
	{
		for(TilePedestal ped : changed)
		{
			ped.sendChangesToNearby();
			HammerCore.particleProxy.spawnSimpleThunder(world, new Vec3d(ped.getPos()).add(.5, .9, .5), new Vec3d(pos).add(.5, 1.3, .5), getRNG().nextLong(), 40, 4F, new Layer(771, 0x00AAFF, true), new Layer(1, 0x00FF55, true));
		}
		
		for(int i = 0; i < 64; ++i)
			HCNet.spawnParticle(world, EnumParticleTypes.END_ROD, pos.getX() + .5, pos.getY() + 1.3, pos.getZ() + .5, (getRNG().nextDouble() - getRNG().nextDouble()) * .3D, (getRNG().nextDouble() - getRNG().nextDouble()) * .3D, (getRNG().nextDouble() - getRNG().nextDouble()) * .3D);
		
		output.setInventorySlotContents(0, out);
		sendChangesToNearby();
	}
	
	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		output.drop(world, pos);
		super.createDrop(player, world, pos);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		super.readNBT(nbt);
		prevCT = craftTime;
		storage.readFromNBT(nbt);
		craftTime = nbt.getInteger("CraftTime");
		output.readFromNBT(nbt.getCompoundTag("Items"));
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		super.writeNBT(nbt);
		storage.writeToNBT(nbt);
		nbt.setInteger("CraftTime", craftTime);
		nbt.setTag("Items", output.writeToNBT(new NBTTagCompound()));
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return hasSubCapability(pos, capability, facing) || super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(hasSubCapability(pos, capability, facing))
			return getSubCapability(pos, capability, facing);
		return super.getCapability(capability, facing);
	}
	
	@Override
	public boolean hasSubCapability(BlockPos part, Capability<?> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY || capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;
	}
	
	InvWrapper iw;
	
	@Override
	public <T> T getSubCapability(BlockPos part, Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
			return (T) (iw != null ? iw : (iw = new InvWrapper(output)));
		return capability == CapabilityEnergy.ENERGY ? (T) this : null;
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return storage.receiveEnergy(maxReceive, simulate);
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int getEnergyStored()
	{
		return storage.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return storage.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return false;
	}
	
	@Override
	public boolean canReceive()
	{
		return true;
	}
	
	private AxisAlignedBB $aabb(BlockPos rel)
	{
		int x = rel.getX();
		int y = rel.getY();
		int z = rel.getZ();
		
		if(y == 0)
			return new AxisAlignedBB(.1, 0, .1, .9, 1, .9);
		
		if(y == 1)
			return new AxisAlignedBB(.1, 0, .1, .9, 1.25, .9);
		
		return Block.FULL_BLOCK_AABB;
	}
}