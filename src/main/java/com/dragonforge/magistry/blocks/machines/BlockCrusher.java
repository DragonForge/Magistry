package com.dragonforge.magistry.blocks.machines;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.blocks.machines.tiles.TileCrusher;
import com.dragonforge.magistry.blocks.machines.tiles.TileEtherCollector;
import com.dragonforge.magistry.init.BlocksM;

import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class BlockCrusher extends BlockMultiblockCore<TileCrusher>
{
	public BlockCrusher()
	{
		super(Material.IRON, TileCrusher.class, "crusher");
		this.fxSprites = new ResourceLocation(Magistry.MOD_ID, "textures/models/crusher.png");
		setHarvestLevel("pickaxe", 1);
		setHardness(2);
	}
	
	@Override
	public ResourceLocation getMultiblockForPlacement(EntityPlayer player)
	{
		return new ResourceLocation(Magistry.MOD_ID, "crusher_" + player.getHorizontalFacing().name().toLowerCase());
	}
}