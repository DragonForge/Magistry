package com.dragonforge.magistry.blocks.machines.tiles;

import java.lang.ref.WeakReference;
import java.util.List;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.ce.CrystalStorage;
import com.dragonforge.magistry.blocks.tiles.TileGreaterCrystal;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockCore;
import com.dragonforge.magistry.blocks.tiles.TileNaturalCrystal;
import com.dragonforge.magistry.net.PacketDischargeCrystal;
import com.dragonforge.magistry.utils.ProcessDelayedTask;
import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.thunder.Thunder.Layer;
import com.zeitheron.hammercore.utils.AABBUtils;
import com.zeitheron.hammercore.utils.ListUtils;
import com.zeitheron.hammercore.utils.PositionedSearching;
import com.zeitheron.hammercore.utils.SoundUtil;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class TileGenerator extends TileMultiblockCore implements IEnergyStorage
{
	public final FEEnergyStorage feStorage = new FEEnergyStorage(10_000);
	public CrystalStorage cuStorage = new CrystalStorage(20, 0, 20);
	
	public int chargeTime;
	public int chargeTimeTotal = 200;
	
	public PositionedSearching<TileNaturalCrystal> naturalCrystals;
	public PositionedSearching<TileGreaterCrystal> greaterCrystals;
	
	public TileGenerator()
	{
		aabbMapper = this::$aabb;
	}
	
	public PositionedSearching<TileNaturalCrystal> getNaturalCrystals()
	{
		if(naturalCrystals == null)
		{
			naturalCrystals = new PositionedSearching<TileNaturalCrystal>(pos -> WorldUtil.cast(world.getTileEntity(pos), TileNaturalCrystal.class), t -> t != null && world.getTileEntity(t.getPos()) == t && t.charge > 0F, TileNaturalCrystal.class);
			naturalCrystals.setCenter(pos.up(3));
			naturalCrystals.setRadius(6, 4, 6);
		}
		return naturalCrystals;
	}
	
	public PositionedSearching<TileGreaterCrystal> getGreaterCrystals()
	{
		if(greaterCrystals == null)
		{
			greaterCrystals = new PositionedSearching<TileGreaterCrystal>(pos -> WorldUtil.cast(world.getTileEntity(pos), TileGreaterCrystal.class), t -> t == null || world.getTileEntity(t.getPos()) == t, TileGreaterCrystal.class);
			greaterCrystals.setCenter(pos.up(3));
			greaterCrystals.setRadius(6, 4, 6);
		}
		return greaterCrystals;
	}
	
	@Override
	public void tick()
	{
		if(world.isRemote)
		{
			
			return;
		}
		
		super.tick();
		
		getNaturalCrystals().update(10);
		List<TileNaturalCrystal> crystals = getNaturalCrystals().located;
		if(cuStorage.getCUCapacity() - cuStorage.getCUStored() > .1 && feStorage.getEnergyStored() >= 40)
		{
			if(!crystals.isEmpty())
			{
				feStorage.extractEnergy(40, false);
				++chargeTime;
				
				TileNaturalCrystal tile = ListUtils.random(crystals, getRNG());
				
				if(chargeTime >= chargeTimeTotal)
				{
					int rand = getRNG().nextInt(3) + 1;
					for(int i = 0; i < rand; ++i)
						HammerCore.particleProxy.spawnSimpleThunder(world, rngCoilPos(), new Vec3d(tile.getPos()).add(.5, .5, .5), getRNG().nextLong(), 20, 2F, new Layer(771, tile.getColor(), true), new Layer(1, tile.getColor(), true));
					
					final WeakReference<World> wref = new WeakReference<World>(world);
					final BlockPos pos = this.pos;
					
					new ProcessDelayedTask(12, () -> HCNet.INSTANCE.sendToAllAround(new PacketDischargeCrystal(pos, tile.getPos()), new WorldLocation(wref.get(), pos).getPointWithRad(100))).start();
					
					SoundUtil.playSoundEffect(world, "entity.firework.large_blast", pos, .5F, .7F, SoundCategory.BLOCKS);
					SoundUtil.playSoundEffect(world, "entity.firework.twinkle", pos, .5F, .7F, SoundCategory.BLOCKS);
					
					float sm = tile.saturation * 4F;
					float cr = 10F * MathHelper.clamp(1 - tile.saturation, .005F, 1);
					
					float maxAccept = Math.min(cuStorage.getCUCapacity() - cuStorage.getCUStored(), Math.min(cuStorage.getCUCapacity(), getRNG().nextFloat() * cr));
					float discharged = tile.discharge(maxAccept);
					cuStorage.setCU(cuStorage.getCUStored() + discharged * sm);
					chargeTime = 0;
				} else if(getRNG().nextInt(4) == 0)
				{
					HammerCore.particleProxy.spawnSimpleThunder(world, rngCoilPos(), rngCoilPos(), getRNG().nextLong(), 20, 8F, new Layer(771, tile.getColor(), true), new Layer(1, tile.getColor(), true));
					SoundUtil.playSoundEffect(world, "item.flintandsteel.use", pos, .5F, 1.6F + getRNG().nextFloat() * .4F, SoundCategory.BLOCKS);
				}
				if(chargeTime % 4 == 0)
					shockNearby(3F);
				chargeTime = Math.max(chargeTime, 0);
			} else if(chargeTime > 0 && getRNG().nextInt(4) == 0)
			{
				--chargeTime;
				shockNearby(3.5F);
				chargeTime = Math.max(chargeTime, 0);
			}
		} else if(!world.isRemote)
		{
			if(chargeTime > 0)
			{
				if(atTickRate(10))
					--chargeTime;
				if(getRNG().nextInt(4) == 0)
				{
					HammerCore.particleProxy.spawnSimpleThunder(world, rngCoilPos(), rngCoilPos(), getRNG().nextLong(), 20, 8F, new Layer(771, 0xFF2222, true), new Layer(1, 0xFF2222, true));
					SoundUtil.playSoundEffect(world, "item.flintandsteel.use", pos, .5F, 1.6F + getRNG().nextFloat() * .4F, SoundCategory.BLOCKS);
				}
			}
		}
	}
	
	public void shockNearby(float growth)
	{
		AxisAlignedBB aabb = new AxisAlignedBB(pos.getX() - 1, pos.getY(), pos.getZ() - 1, pos.getX() + 1, pos.getY() + 5, pos.getZ() + 1).grow(growth);
		List<EntityLivingBase> entitiesToMurder = world.getEntitiesWithinAABB(EntityLivingBase.class, aabb);
		if(!entitiesToMurder.isEmpty() && getRNG().nextBoolean())
		{
			EntityLivingBase murder = ListUtils.random(entitiesToMurder, getRNG());
			chargeTime -= Math.min(chargeTime, 4);
			murder.setFire(3);
			murder.attackEntityFrom(MagistryAPI.ELECTRICAL_DAMAGE, 10);
			HammerCore.particleProxy.spawnSimpleThunder(world, rngCoilPos(), AABBUtils.randomPosWithin(murder.getEntityBoundingBox(), getRNG()), getRNG().nextLong(), 5, 3F, new Layer(771, 0xFFAA00, true), new Layer(1, 0xFFAA00, true));
			SoundUtil.playSoundEffect(world, "item.flintandsteel.use", pos, .5F, 1.6F + getRNG().nextFloat() * .4F, SoundCategory.BLOCKS);
			SoundUtil.playSoundEffect(world, "entity.firework.large_blast", pos, .5F, .7F + getRNG().nextFloat() * .3F, SoundCategory.BLOCKS);
		}
	}
	
	public Vec3d rngCoilPos()
	{
		return new Vec3d(pos).add(.2 + getRNG().nextDouble() * .6, 3 + getRNG().nextDouble() * 1.7, .2 + getRNG().nextDouble() * .6);
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		super.writeNBT(nbt);
		feStorage.writeToNBT(nbt);
		cuStorage.writeToNBT(nbt);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		super.readNBT(nbt);
		feStorage.readFromNBT(nbt);
		cuStorage.readFromNBT(nbt);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY || super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return (T) this;
		return super.getCapability(capability, facing);
	}
	
	@Override
	public <T> T getSubCapability(BlockPos part, Capability<T> capability, EnumFacing facing)
	{
		if(part.getX() == 1 && part.getY() >= 3 && part.getZ() == 1 && capability == MagistryAPI.CAPABILITY_CRYSTAL_STORAGE)
			return (T) cuStorage;
		return null;
	}
	
	@Override
	public boolean hasSubCapability(BlockPos part, Capability<?> capability, EnumFacing facing)
	{
		if(part.getX() == 1 && part.getY() >= 3 && part.getZ() == 1 && capability == MagistryAPI.CAPABILITY_CRYSTAL_STORAGE)
			return true;
		return false;
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return feStorage.receiveEnergy(maxReceive, simulate);
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int getEnergyStored()
	{
		return feStorage.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return feStorage.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return false;
	}
	
	@Override
	public boolean canReceive()
	{
		return true;
	}
	
	private AxisAlignedBB $aabb(BlockPos rel)
	{
		int x = rel.getX();
		int y = rel.getY();
		int z = rel.getZ();
		
		if(y == 0)
			return new AxisAlignedBB(0, 0, 0, 1, 13 / 16D, 1);
		
		if(x == 1 && z == 1 && y == 2)
			return new AxisAlignedBB(0, 0, 0, 1, 14 / 16D, 1);
		
		if(x == 1 && z == 0 && y == 2)
			return new AxisAlignedBB(5 / 16D, 0, 10 / 16D, 11 / 16D, 9 / 16D, 1);
		
		if(x == 0 && z == 1 && y == 2)
			return new AxisAlignedBB(10 / 16D, 0, 5 / 16D, 1, 9 / 16D, 11 / 16D);
		
		if(x == 1 && z == 2 && y == 2)
			return new AxisAlignedBB(5 / 16D, 0, 0, 11 / 16D, 9 / 16D, 6 / 16D);
		
		if(x == 2 && z == 1 && y == 2)
			return new AxisAlignedBB(0, 0, 5 / 16D, 6 / 16D, 9 / 16D, 11 / 16D);
		
		if(x == 0 && z == 0)
			if(y == 1)
				return new AxisAlignedBB(1 / 16D, -3 / 16D, 1 / 16D, 1, 12 / 16D, 1);
			else if(y == 2)
				return new AxisAlignedBB(7.5 / 16D, -4 / 16D, 7.5 / 16D, 15.5 / 16, 1 / 16D, 15.5 / 16);
			
		if(x == 0 && z == 2)
			if(y == 1)
				return new AxisAlignedBB(1 / 16D, -3 / 16D, 0, 1, 12 / 16D, 15 / 16D);
			else if(y == 2)
				return new AxisAlignedBB(7.5 / 16D, -4 / 16D, 8.5 / 16D, 15.5 / 16, 1 / 16D, .5 / 16);
			
		if(x == 2 && z == 0)
			if(y == 1)
				return new AxisAlignedBB(0, -3 / 16D, 1 / 16D, 15 / 16D, 12 / 16D, 1);
			else if(y == 2)
				return new AxisAlignedBB(8.5 / 16D, -4 / 16D, 7.5 / 16D, .5 / 16, 1 / 16D, 15.5 / 16);
			
		if(x == 2 && z == 2)
			if(y == 1)
				return new AxisAlignedBB(0, -3 / 16D, 0, 15 / 16D, 12 / 16D, 15 / 16D);
			else if(y == 2)
				return new AxisAlignedBB(8.5 / 16D, -4 / 16D, 8.5 / 16D, .5 / 16, 1 / 16D, .5 / 16);
			
		if(y == 1)
			return new AxisAlignedBB(z == 1 ? 1 / 16D : -.5 / 16D, -3 / 16D, x == 1 ? 1 / 16D : -.5 / 16D, x == 2 ? 15 / 16D : 16.5 / 16, 17 / 16D, z == 2 ? 15 / 16D : 16.5 / 16);
		
		if(x == 1 && z == 1)
			if(y == 3 || y == 4)
				return new AxisAlignedBB(.1, y == 3 ? -2 / 16D : 0, .1, .9, y == 4 ? .8 : 1, .9);
			
		return Block.FULL_BLOCK_AABB;
	}
}