package com.dragonforge.magistry.blocks.machines;

import java.util.function.Supplier;

import com.dragonforge.magistry.api.multiblock.IMultiblockCore;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockCore;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockPart;
import com.dragonforge.magistry.client.fx.FXDigging;
import com.dragonforge.magistry.items.ItemMultiblockBuilder;
import com.zeitheron.hammercore.internal.blocks.IItemBlock;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class BlockMultiblockCore<T extends TileMultiblockCore> extends BlockDeviceHC<T> implements IMultiblockCore<T>, IItemBlock
{
	protected ResourceLocation fxSprites;
	
	public BlockMultiblockCore(Material materialIn, Class<T> tile, String name)
	{
		super(materialIn, tile, name);
	}
	
	public ResourceLocation getFxSprites()
	{
		return fxSprites;
	}
	
	@Override
	public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items)
	{
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileMultiblockCore part = WorldUtil.cast(worldIn.getTileEntity(pos), TileMultiblockCore.class);
		if(part != null && part.onSubBlockActivated(worldIn, new BlockPos(part.thisX, part.thisY, part.thisZ), state, playerIn, hand, facing, hitX, hitY, hitZ))
			return true;
		return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		TileMultiblockCore core = WorldUtil.cast(source.getTileEntity(pos), TileMultiblockCore.class);
		if(core != null)
			return core.getSubAABB(new BlockPos(core.thisX, core.thisY, core.thisZ));
		return super.getBoundingBox(state, source, pos);
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.INVISIBLE;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean addDestroyEffects(World world, BlockPos pos, ParticleManager manager)
	{
		int i = 2;
		for(int j = 0; j < i; ++j)
			for(int k = 0; k < i; ++k)
				for(int l = 0; l < i; ++l)
				{
					double d0 = (j + 0.5D) / i;
					double d1 = (k + 0.5D) / i;
					double d2 = (l + 0.5D) / i;
					manager.addEffect(new FXDigging(world, pos.getX() + d0, pos.getY() + d1, pos.getZ() + d2, d0 - 0.5D, d1 - 0.5D, d2 - 0.5D, fxSprites).setBlockPos(pos));
				}
		return true;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean addHitEffects(IBlockState state, World world, RayTraceResult target, ParticleManager manager)
	{
		BlockPos pos = target.getBlockPos();
		EnumFacing side = target.sideHit;
		int i = pos.getX();
		int j = pos.getY();
		int k = pos.getZ();
		AxisAlignedBB axisalignedbb = state.getBoundingBox(world, pos);
		double d0 = i + world.rand.nextDouble() * (axisalignedbb.maxX - axisalignedbb.minX - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minX;
		double d1 = j + world.rand.nextDouble() * (axisalignedbb.maxY - axisalignedbb.minY - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minY;
		double d2 = k + world.rand.nextDouble() * (axisalignedbb.maxZ - axisalignedbb.minZ - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minZ;
		if(side == EnumFacing.DOWN)
			d1 = (double) j + axisalignedbb.minY - 0.10000000149011612D;
		if(side == EnumFacing.UP)
			d1 = (double) j + axisalignedbb.maxY + 0.10000000149011612D;
		if(side == EnumFacing.NORTH)
			d2 = (double) k + axisalignedbb.minZ - 0.10000000149011612D;
		if(side == EnumFacing.SOUTH)
			d2 = (double) k + axisalignedbb.maxZ + 0.10000000149011612D;
		if(side == EnumFacing.WEST)
			d0 = (double) i + axisalignedbb.minX - 0.10000000149011612D;
		if(side == EnumFacing.EAST)
			d0 = (double) i + axisalignedbb.maxX + 0.10000000149011612D;
		manager.addEffect(new FXDigging(world, d0, d1, d2, 0.0D, 0.0D, 0.0D, fxSprites).setBlockPos(pos).multiplyVelocity(0.2F).multipleParticleScaleBy(0.6F));
		return true;
	}
	
	@Override
	public boolean addLandingEffects(IBlockState state, WorldServer worldObj, BlockPos blockPosition, IBlockState iblockstate, EntityLivingBase entity, int numberOfParticles)
	{
		return true;
	}
	
	@Override
	public boolean addRunningEffects(IBlockState state, World world, BlockPos pos, Entity entity)
	{
		return true;
	}
	
	private ItemMultiblockBuilder itemMultiblock;
	
	public abstract ResourceLocation getMultiblockForPlacement(EntityPlayer player);
	
	@Override
	public ItemBlock getItemBlock()
	{
		if(itemMultiblock == null)
			itemMultiblock = new ItemMultiblockBuilder(this, this::getMultiblockForPlacement);
		return itemMultiblock;
	}
}