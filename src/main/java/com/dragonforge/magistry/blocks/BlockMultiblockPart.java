package com.dragonforge.magistry.blocks;

import java.util.List;

import com.dragonforge.magistry.blocks.machines.BlockMultiblockCore;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockCore;
import com.dragonforge.magistry.blocks.tiles.TileMultiblockPart;
import com.dragonforge.magistry.client.fx.FXDigging;
import com.zeitheron.hammercore.api.INoItemBlock;
import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockMultiblockPart extends Block implements INoItemBlock, ITileBlock<TileMultiblockPart>, ITileEntityProvider
{
	public BlockMultiblockPart()
	{
		super(Material.ROCK);
		setTranslationKey("multiblock_part");
		setHardness(1.5F);
	}
	
	@Override
	public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
	{
		TileMultiblockPart part = WorldUtil.cast(world.getTileEntity(pos), TileMultiblockPart.class);
		if(part != null)
			return part.getItem(target, player);
		return super.getPickBlock(state, target, world, pos, player);
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		TileMultiblockPart part = WorldUtil.cast(source.getTileEntity(pos), TileMultiblockPart.class);
		if(part != null)
		{
			AxisAlignedBB a = part.getAABB();
			return a != null ? a : unselectableBB;
		}
		return super.getBoundingBox(state, source, pos);
	}
	
	@Override
	public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, Entity entityIn, boolean p_185477_7_)
	{
		AxisAlignedBB aabb = state.getCollisionBoundingBox(worldIn, pos);
		if(aabb != unselectableBB)
			addCollisionBoxToList(pos, entityBox, collidingBoxes, aabb);
	}
	
	AxisAlignedBB unselectableBB = new AxisAlignedBB(0, 0, 0, 0, 0, 0);
	
	@Override
	public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos)
	{
		TileMultiblockPart part = WorldUtil.cast(worldIn.getTileEntity(pos), TileMultiblockPart.class);
		if(part != null)
		{
			AxisAlignedBB aabb = part.getAABB();
			if(aabb != null)
				return aabb.offset(pos);
		}
		return unselectableBB;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.INVISIBLE;
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileMultiblockPart part = WorldUtil.cast(worldIn.getTileEntity(pos), TileMultiblockPart.class);
		if(part != null)
		{
			if(part.getCore().onSubBlockActivated(worldIn, part.getRelativePos(), state, playerIn, hand, facing, hitX, hitY, hitZ))
				return true;
			else
			{
				BlockPos op = part.core;
				IBlockState os = worldIn.getBlockState(op);
				if(os.getBlock().onBlockActivated(worldIn, op, os, playerIn, hand, facing, hitX, hitY, hitZ))
					return true;
			}
		}
		return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
	}
	
	@Override
	public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileMultiblockPart();
	}
	
	@Override
	public Class<TileMultiblockPart> getTileClass()
	{
		return TileMultiblockPart.class;
	}
	
	public ResourceLocation getFxSprites(World world, BlockPos pos)
	{
		TileMultiblockPart part = WorldUtil.cast(world.getTileEntity(pos), TileMultiblockPart.class);
		if(part != null)
		{
			TileMultiblockCore core = part.getCore();
			if(core != null)
			{
				BlockMultiblockCore bc = WorldUtil.cast(world.getBlockState(core.getPos()).getBlock(), BlockMultiblockCore.class);
				if(bc != null)
					return bc.getFxSprites();
			}
		}
		return null;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean addDestroyEffects(World world, BlockPos pos, ParticleManager manager)
	{
		ResourceLocation fxSprites = getFxSprites(world, pos);
		if(fxSprites == null)
			return true;
		
		int i = 2;
		for(int j = 0; j < i; ++j)
			for(int k = 0; k < i; ++k)
				for(int l = 0; l < i; ++l)
				{
					double d0 = (j + 0.5D) / i;
					double d1 = (k + 0.5D) / i;
					double d2 = (l + 0.5D) / i;
					manager.addEffect(new FXDigging(world, pos.getX() + d0, pos.getY() + d1, pos.getZ() + d2, d0 - 0.5D, d1 - 0.5D, d2 - 0.5D, fxSprites).setBlockPos(pos));
				}
		return true;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean addHitEffects(IBlockState state, World world, RayTraceResult target, ParticleManager manager)
	{
		BlockPos pos = target.getBlockPos();
		
		ResourceLocation fxSprites = getFxSprites(world, pos);
		if(fxSprites == null)
			return true;
		
		EnumFacing side = target.sideHit;
		int i = pos.getX();
		int j = pos.getY();
		int k = pos.getZ();
		AxisAlignedBB axisalignedbb = state.getBoundingBox(world, pos);
		double d0 = i + world.rand.nextDouble() * (axisalignedbb.maxX - axisalignedbb.minX - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minX;
		double d1 = j + world.rand.nextDouble() * (axisalignedbb.maxY - axisalignedbb.minY - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minY;
		double d2 = k + world.rand.nextDouble() * (axisalignedbb.maxZ - axisalignedbb.minZ - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minZ;
		if(side == EnumFacing.DOWN)
			d1 = (double) j + axisalignedbb.minY - 0.10000000149011612D;
		if(side == EnumFacing.UP)
			d1 = (double) j + axisalignedbb.maxY + 0.10000000149011612D;
		if(side == EnumFacing.NORTH)
			d2 = (double) k + axisalignedbb.minZ - 0.10000000149011612D;
		if(side == EnumFacing.SOUTH)
			d2 = (double) k + axisalignedbb.maxZ + 0.10000000149011612D;
		if(side == EnumFacing.WEST)
			d0 = (double) i + axisalignedbb.minX - 0.10000000149011612D;
		if(side == EnumFacing.EAST)
			d0 = (double) i + axisalignedbb.maxX + 0.10000000149011612D;
		manager.addEffect(new FXDigging(world, d0, d1, d2, 0.0D, 0.0D, 0.0D, fxSprites).setBlockPos(pos).multiplyVelocity(0.2F).multipleParticleScaleBy(0.6F));
		return true;
	}
	
	@Override
	public boolean addLandingEffects(IBlockState state, WorldServer worldObj, BlockPos blockPosition, IBlockState iblockstate, EntityLivingBase entity, int numberOfParticles)
	{
		return true;
	}
	
	@Override
	public boolean addRunningEffects(IBlockState state, World world, BlockPos pos, Entity entity)
	{
		return true;
	}
}