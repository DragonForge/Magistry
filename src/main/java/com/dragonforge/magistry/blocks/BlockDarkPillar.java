package com.dragonforge.magistry.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockDarkPillar extends Block
{
	public static final PropertyEnum<Axis> AXIS = PropertyEnum.create("axis", Axis.class);
	public AxisAlignedBB[] boundaries = new AxisAlignedBB[3];
	
	public BlockDarkPillar()
	{
		super(Material.ROCK);
		setTranslationKey("dark_pillar");
		updateAABB();
	}
	
	private void updateAABB()
	{
		boundaries[0] = new AxisAlignedBB(0, 3.5 / 16D, 3.5 / 16D, 1, 12.5 / 16D, 12.5 / 16D);
		boundaries[1] = new AxisAlignedBB(3.5 / 16D, 0, 3.5 / 16D, 12.5 / 16D, 1, 12.5 / 16D);
		boundaries[2] = new AxisAlignedBB(3.5 / 16D, 3.5 / 16D, 0, 12.5 / 16D, 12.5 / 16D, 1);
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		updateAABB();
		return boundaries[state.getValue(AXIS).ordinal()];
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(AXIS).ordinal();
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return getDefaultState().withProperty(AXIS, Axis.values()[meta]);
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, AXIS);
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer, EnumHand hand)
	{
		return getDefaultState().withProperty(AXIS, facing.getAxis());
	}
}