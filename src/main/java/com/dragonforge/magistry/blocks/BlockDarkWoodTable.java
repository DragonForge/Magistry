package com.dragonforge.magistry.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockDarkWoodTable extends Block
{
	public static final PropertyEnum<Axis> AXIS = PropertyEnum.create("rotate", Axis.class, Axis.X, Axis.Z);
	public static final AxisAlignedBB TABLE_AABB = new AxisAlignedBB(1 / 16D, 0, 1 / 16D, 15 / 16D, 12 / 16D, 15 / 16D);
	
	public BlockDarkWoodTable()
	{
		super(Material.WOOD);
		setTranslationKey("dark_wood_table");
		setSoundType(SoundType.WOOD);
		setHardness(2F);
		setResistance(5F);
		setHarvestLevel("axe", 0);
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, AXIS);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(AXIS) == Axis.X ? 0 : 1;
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return getDefaultState().withProperty(AXIS, meta == 0 ? Axis.X : Axis.Z);
	}
	
	@Override
	public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer, EnumHand hand)
	{
		return getDefaultState().withProperty(AXIS, placer.getHorizontalFacing().rotateY().getAxis());
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return FULL_BLOCK_AABB;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
}