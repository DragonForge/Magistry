package com.dragonforge.magistry.blocks;

import com.zeitheron.hammercore.utils.IRegisterListener;

import net.minecraft.block.BlockLog;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraftforge.oredict.OreDictionary;

public class BlockMLog extends BlockLog implements IRegisterListener
{
	public BlockMLog(String name)
	{
		setTranslationKey(name + "_log");
		setDefaultState(getDefaultState().withProperty(LOG_AXIS, EnumAxis.NONE));
	}
	
	@Override
	public void onRegistered()
	{
		OreDictionary.registerOre("logWood", this);
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, LOG_AXIS);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(LOG_AXIS).ordinal();
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return getDefaultState().withProperty(LOG_AXIS, EnumAxis.values()[meta % 4]);
	}
}