package com.dragonforge.magistry.blocks;

import java.util.Random;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.cfg.ConfigsM;
import com.dragonforge.magistry.init.ItemsM;
import com.zeitheron.hammercore.utils.IRegisterListener;
import com.zeitheron.hammercore.world.gen.IWorldGenFeature;
import com.zeitheron.hammercore.world.gen.WorldGenMinableHC;
import com.zeitheron.hammercore.world.gen.WorldRetroGen;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class BlockCrystalOre extends Block implements IRegisterListener
{
	public BlockCrystalOre()
	{
		super(Material.ROCK);
		setTranslationKey("crystal_ore");
		setHarvestLevel("pickaxe", 2);
		setHardness(2F);
	}
	
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return ItemsM.CRYSTAL_SHARD;
	}
	
	@Override
	public int quantityDroppedWithBonus(int fortune, Random random)
	{
		if(fortune > 0 && Item.getItemFromBlock(this) != this.getItemDropped((IBlockState) this.getBlockState().getValidStates().iterator().next(), random, fortune))
		{
			int i = random.nextInt(fortune + 2) - 1;
			if(i < 0)
				i = 0;
			return this.quantityDropped(random) * (i + 1);
		} else
			return this.quantityDropped(random);
	}
	
	@Override
	public int getExpDrop(IBlockState state, IBlockAccess world, BlockPos pos, int fortune)
	{
		Random rand = world instanceof World ? ((World) world).rand : new Random();
		if(getItemDropped(state, rand, fortune) != Item.getItemFromBlock(this))
			return MathHelper.getInt(rand, 2, 5);
		return 0;
	}
	
	@Override
	public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state)
	{
		return new ItemStack(this);
	}
	
	@Override
	public void onRegistered()
	{
		OreDictionary.registerOre("oreCrystalShard", this);
		WorldRetroGen.addWorldFeature(new WorldGen().setRegistryName(getRegistryName()));
	}
	
	private class WorldGen extends IForgeRegistryEntry.Impl<WorldGen> implements IWorldGenFeature<WorldGen>
	{
		@Override
		public int getMaxChances(World world, ChunkPos chunk, Random rand)
		{
			return 8;
		}
		
		@Override
		public int getMinY(World world, BlockPos pos, Random rand)
		{
			return 8;
		}
		
		@Override
		public int getMaxY(World world, BlockPos pos, Random rand)
		{
			return 24;
		}
		
		@Override
		public void generate(World world, BlockPos pos, Random rand)
		{
			if(ConfigsM.gen_crystalOre && rand.nextInt(4) == 0)
				new WorldGenMinableHC(getDefaultState(), 8).generate(world, rand, pos);
		}
	}
}