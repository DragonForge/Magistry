package com.dragonforge.magistry.blocks;

import static com.dragonforge.magistry.blocks.BlockNaturalCrystal.CRYSTAL_AABBs;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import com.dragonforge.magistry.blocks.tiles.TileOreCrystal;
import com.dragonforge.magistry.cfg.ConfigsM;
import com.dragonforge.magistry.utils.OreHelper;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockOrientable;
import com.zeitheron.hammercore.utils.IRegisterListener;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.world.gen.IWorldGenFeature;
import com.zeitheron.hammercore.world.gen.WorldRetroGen;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class BlockOreCrystal extends BlockDeviceHC<TileOreCrystal> implements IBlockOrientable, IRegisterListener
{
	public BlockOreCrystal()
	{
		super(Material.ROCK, TileOreCrystal.class, "ore_crystal");
		setSoundType(SoundType.STONE);
		setHarvestLevel("pickaxe", 1);
		setHardness(2F);
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return CRYSTAL_AABBs.get(state.getValue(IBlockOrientable.FACING).ordinal());
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean canPlaceBlockOnSide(World worldIn, BlockPos pos, EnumFacing side)
	{
		BlockPos tp = pos.offset(side.getOpposite());
		return worldIn.isSideSolid(tp, side);
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.ENTITYBLOCK_ANIMATED;
	}
	
	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos frompos)
	{
		EnumFacing face = state.getValue(IBlockOrientable.FACING);
		if(!canPlaceBlockOnSide(worldIn, pos, face.getOpposite()))
			worldIn.destroyBlock(pos, true);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, World player, List<String> tooltip, ITooltipFlag advanced)
	{
		NBTTagCompound tag = stack.getTagCompound();
		if(tag != null)
		{
			
		}
	}
	
	@Override
	public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		return getDefaultState().withProperty(IBlockOrientable.FACING, facing.getOpposite());
	}
	
	@Override
	public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
		TileOreCrystal tnc = WorldUtil.cast(world.getTileEntity(pos), TileOreCrystal.class);
		if(tnc != null)
			drops.add(tnc.generateStack());
	}
	
	@Override
	public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state)
	{
		TileOreCrystal crystal = WorldUtil.cast(worldIn.getTileEntity(pos), TileOreCrystal.class);
		if(crystal != null)
			return crystal.generateStack();
		return super.getItem(worldIn, pos, state);
	}
	
	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		TileOreCrystal crystal = WorldUtil.cast(worldIn.getTileEntity(pos), TileOreCrystal.class);
		if(crystal != null)
		{
			crystal.loadFromStack(stack);
			
			if(crystal.growing != null && crystal.growing.getBlock() != Blocks.AIR)
				return;
			
			IBlockState state2 = worldIn.getBlockState(pos.offset(state.getValue(IBlockOrientable.FACING)));
			if(OreHelper.forOre(state2) != null)
			{
				crystal.growing = state2;
				crystal.sendChangesToNearby();
			} else
				worldIn.destroyBlock(pos, false);
		}
	}
	
	@Override
	public void onRegistered()
	{
		WorldRetroGen.addWorldFeature(new WorldGen().setRegistryName(getRegistryName()));
	}
	
	private class WorldGen extends IForgeRegistryEntry.Impl<WorldGen> implements IWorldGenFeature<WorldGen>
	{
		@Override
		public int getMaxChances(World world, ChunkPos chunk, Random rand)
		{
			return 8192;
		}
		
		@Override
		public int getMinY(World world, BlockPos pos, Random rand)
		{
			return 0;
		}
		
		@Override
		public int getMaxY(World world, BlockPos pos, Random rand)
		{
			return 64;
		}
		
		@Override
		public void generate(World world, BlockPos pos, Random rand)
		{
			if(ConfigsM.gen_oreCrystals)
			{
				EnumFacing attach = getAttachSide(world, pos);
				if(attach != null)
				{
					world.setBlockState(pos, getDefaultState().withProperty(IBlockOrientable.FACING, attach.getOpposite()));
					TileOreCrystal toc = WorldUtil.cast(world.getTileEntity(pos), TileOreCrystal.class);
					if(toc != null)
						toc.growing = world.getBlockState(pos.offset(attach.getOpposite()));
				}
			}
		}
		
		@Nullable
		public EnumFacing getAttachSide(World world, BlockPos pos)
		{
			if(!world.isAirBlock(pos))
				return null;
			ChunkPos cp = new ChunkPos(pos);
			for(EnumFacing face : EnumFacing.VALUES)
				if(sameChunk(cp, pos.offset(face)) && world.isSideSolid(pos.offset(face), face.getOpposite()))
				{
					IBlockState state = world.getBlockState(pos.offset(face));
					if(OreHelper.forOre(state) != null)
						return face.getOpposite();
					
					Item asItem = state.getBlock().getItemDropped(state, RANDOM, 0);
				}
			return null;
		}
		
		private boolean sameChunk(ChunkPos cp, BlockPos pos)
		{
			return pos.getX() > cp.getXStart() && pos.getZ() > cp.getZStart() && pos.getX() < cp.getXEnd() && pos.getZ() < cp.getZEnd();
		}
	}
}