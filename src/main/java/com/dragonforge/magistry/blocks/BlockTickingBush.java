package com.dragonforge.magistry.blocks;

import java.util.Random;

import net.minecraft.block.BlockBush;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockTickingBush extends BlockBush
{
	{
		setSoundType(SoundType.PLANT);
	}
	
	@Override
	public int tickRate(World worldIn)
	{
		return 20;
	}
	
	@Override
	public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state)
	{
		if(!worldIn.isUpdateScheduled(pos, this))
			worldIn.scheduleUpdate(pos, this, tickRate(worldIn));
	}
	
	@Override
	public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
	{
		if(!worldIn.isUpdateScheduled(pos, this))
			worldIn.scheduleUpdate(pos, this, tickRate(worldIn));
		onTick(worldIn, pos, state, rand);
	}
	
	public void onTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
	{
		
	}
}