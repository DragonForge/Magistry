package com.dragonforge.magistry.client.model.item;

import com.dragonforge.magistry.Magistry;
import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class ModelCrystalStaff extends ModelSimple<ItemStack>
{
	public ModelRenderer shape1;
	public ModelRenderer shape2;
	public ModelRenderer shape3;
	public ModelRenderer shape3_1;
	public ModelRenderer shape3_2;
	public ModelRenderer shape10;
	public ModelRenderer shape10_1;
	public ModelRenderer shape10_2;
	public ModelRenderer shape6;
	public ModelRenderer shape6_1;
	public ModelRenderer shape6_2;
	public ModelRenderer shape6_3;
	
	public ModelCrystalStaff()
	{
		super(32, 32, new ResourceLocation(Magistry.MOD_ID, "textures/models/crystal_staff.png"));
		this.shape1 = new ModelRenderer(this, 0, 0);
		this.shape1.setRotationPoint(0.0F, 20.0F, 0.0F);
		this.shape1.addBox(-1.5F, 0.0F, -1.5F, 3, 3, 3, 0.0F);
		this.shape10_1 = new ModelRenderer(this, 27, 7);
		this.shape10_1.setRotationPoint(0.0F, -2.0F, 0.0F);
		this.shape10_1.addBox(-0.5F, 0.0F, -0.5F, 1, 5, 1, 0.0F);
		this.shape6 = new ModelRenderer(this, 25, 4);
		this.shape6.setRotationPoint(1.0F, 3.3F, -1.0F);
		this.shape6.addBox(-0.5F, 0.0F, -0.5F, 1, 1, 1, 0.0F);
		this.shape10 = new ModelRenderer(this, 0, 6);
		this.shape10.setRotationPoint(0.0F, -0.5F, 0.0F);
		this.shape10.addBox(-1.5F, 0.0F, -1.5F, 3, 2, 3, 0.0F);
		this.shape3_1 = new ModelRenderer(this, 22, 1);
		this.shape3_1.setRotationPoint(0.0F, 6.1F, 0.0F);
		this.shape3_1.addBox(-1.0F, 0.0F, -1.0F, 2, 1, 2, 0.0F);
		this.shape6_1 = new ModelRenderer(this, 22, 8);
		this.shape6_1.setRotationPoint(-1.0F, 3.3F, -1.0F);
		this.shape6_1.addBox(-0.5F, 0.0F, -0.5F, 1, 1, 1, 0.0F);
		this.shape3_2 = new ModelRenderer(this, 16, 4);
		this.shape3_2.setRotationPoint(0.0F, 4.8F, 0.0F);
		this.shape3_2.addBox(-1.5F, 0.0F, -1.5F, 3, 1, 3, 0.0F);
		this.shape6_3 = new ModelRenderer(this, 4, 11);
		this.shape6_3.setRotationPoint(1.0F, 3.3F, 1.0F);
		this.shape6_3.addBox(-0.5F, 0.0F, -0.5F, 1, 1, 1, 0.0F);
		this.shape2 = new ModelRenderer(this, 12, 0);
		this.shape2.setRotationPoint(0.0F, 5.0F, 0.0F);
		this.shape2.addBox(-0.5F, 0.0F, -0.5F, 1, 15, 1, 0.0F);
		this.shape3 = new ModelRenderer(this, 16, 0);
		this.shape3.setRotationPoint(0.0F, 18.0F, 0.0F);
		this.shape3.addBox(-1.0F, 0.0F, -1.0F, 2, 1, 2, 0.0F);
		this.shape10_2 = new ModelRenderer(this, 16, 8);
		this.shape10_2.setRotationPoint(0.0F, -1.5F, 0.0F);
		this.shape10_2.addBox(-1.0F, 0.0F, -1.0F, 2, 4, 2, 0.0F);
		this.shape6_2 = new ModelRenderer(this, 0, 11);
		this.shape6_2.setRotationPoint(-1.0F, 3.3F, 1.0F);
		this.shape6_2.addBox(-0.5F, 0.0F, -0.5F, 1, 1, 1, 0.0F);
	}
	
	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
	{
		this.shape1.render(scale);
		this.shape6.render(scale);
		this.shape3_1.render(scale);
		this.shape6_1.render(scale);
		this.shape3_2.render(scale);
		this.shape6_3.render(scale);
		this.shape2.render(scale);
		this.shape3.render(scale);
		this.shape6_2.render(scale);
		
//		this.shape10_1.render(scale);
//		this.shape10.render(scale);
//		this.shape10_2.render(scale);
	}
}