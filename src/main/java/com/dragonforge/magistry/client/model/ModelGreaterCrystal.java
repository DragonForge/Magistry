package com.dragonforge.magistry.client.model;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.blocks.tiles.TileGreaterCrystal;
import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelGreaterCrystal extends ModelSimple<TileGreaterCrystal>
{
	public ModelRenderer shape1;
	public ModelRenderer shape1_1;
	public ModelRenderer shape3;
	public ModelRenderer shape3_1;
	public ModelRenderer shape3_2;
	public ModelRenderer shape3_3;
	public ModelRenderer shape16;
	public ModelRenderer shape16_1;
	public ModelRenderer shape16_2;
	public ModelRenderer shape16_3;
	public ModelRenderer shape9;
	public ModelRenderer shape9_1;
	public ModelRenderer shape9_2;
	public ModelRenderer shape9_3;
	public ModelRenderer shape7;
	public ModelRenderer shape7_1;
	public ModelRenderer shape7_2;
	public ModelRenderer shape7_3;
	public ModelRenderer shape7_4;
	
	public ModelGreaterCrystal()
	{
		super(64, 64, new ResourceLocation(Magistry.MOD_ID, "textures/models/greater_crystal.png"));
		this.textureWidth = 64;
		this.textureHeight = 64;
		this.shape7 = new ModelRenderer(this, 8, 25);
		this.shape7.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.shape7.addBox(-1.0F, 0.0F, -1.0F, 2, 8, 2, 0.0F);
		this.shape9 = new ModelRenderer(this, 32, 21);
		this.shape9.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape9.addBox(-1.0F, -7.0F, -1.0F, 2, 7, 2, 0.0F);
		this.setRotateAngle(shape9, 0.5235987755982988F, 0.0F, 0.5235987755982988F);
		this.shape9_3 = new ModelRenderer(this, 0, 25);
		this.shape9_3.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape9_3.addBox(-1.0F, -7.5F, -1.0F, 2, 7, 2, 0.0F);
		this.setRotateAngle(shape9_3, 0.5235987755982988F, 0.0F, -0.5235987755982988F);
		this.shape1_1 = new ModelRenderer(this, 27, 2);
		this.shape1_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.shape1_1.addBox(-4.5F, 8.999999999999991F, -4.5F, 9, 2, 9, 0.0F);
		this.setRotateAngle(shape1_1, 0.0F, 0.7853981633974483F, 0.0F);
		this.shape7_2 = new ModelRenderer(this, 24, 28);
		this.shape7_2.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape7_2.addBox(-1.0F, -7.5F, -1.0F, 2, 8, 2, 0.0F);
		this.setRotateAngle(shape7_2, -0.3490658503988659F, 0.0F, 0.0F);
		this.shape3_1 = new ModelRenderer(this, 18, 13);
		this.shape3_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.shape3_1.addBox(-3.0F, 7.999999999999995F, -3.0F, 6, 1, 6, 0.0F);
		this.setRotateAngle(shape3_1, 0.0F, 0.7853981633974483F, 0.0F);
		this.shape3_2 = new ModelRenderer(this, 36, 14);
		this.shape3_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.shape3_2.addBox(-3.0F, 7.999999999999995F, -3.0F, 6, 1, 6, 0.0F);
		this.setRotateAngle(shape3_2, 0.0F, 0.39269908169872414F, 0.0F);
		this.shape16_3 = new ModelRenderer(this, 24, 20);
		this.shape16_3.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape16_3.addBox(-1.0F, -5.5F, -1.0F, 2, 6, 2, 0.0F);
		this.setRotateAngle(shape16_3, 0.0F, 0.0F, -1.0471975511965976F);
		this.shape7_3 = new ModelRenderer(this, 54, 28);
		this.shape7_3.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape7_3.addBox(-1.0F, -7.5F, -1.0F, 2, 8, 2, 0.0F);
		this.setRotateAngle(shape7_3, 0.0F, 0.0F, 0.3490658503988659F);
		this.shape1 = new ModelRenderer(this, 0, 0);
		this.shape1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.shape1.addBox(-4.5F, 8.999999999999991F, -4.5F, 9, 2, 9, 0.0F);
		this.shape16_1 = new ModelRenderer(this, 27, 0);
		this.shape16_1.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape16_1.addBox(-1.0F, -5.5F, -1.0F, 2, 6, 2, 0.0F);
		this.setRotateAngle(shape16_1, -1.0471975511965976F, 0.0F, 0.0F);
		this.shape3_3 = new ModelRenderer(this, 0, 18);
		this.shape3_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.shape3_3.addBox(-3.0F, 7.999999999999995F, -3.0F, 6, 1, 6, 0.0F);
		this.setRotateAngle(shape3_3, 0.0F, -0.39269908169872414F, 0.0F);
		this.shape9_2 = new ModelRenderer(this, 48, 21);
		this.shape9_2.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape9_2.addBox(-1.0F, -7.0F, -1.0F, 2, 7, 2, 0.0F);
		this.setRotateAngle(shape9_2, -0.5235987755982988F, 0.0F, 0.5235987755982988F);
		this.shape9_1 = new ModelRenderer(this, 40, 21);
		this.shape9_1.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape9_1.addBox(-1.0F, -7.0F, -1.0F, 2, 7, 2, 0.0F);
		this.setRotateAngle(shape9_1, -0.5235987755982988F, 0.0F, -0.5235987755982988F);
		this.shape7_1 = new ModelRenderer(this, 16, 25);
		this.shape7_1.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape7_1.addBox(-1.0F, -7.5F, -1.0F, 2, 8, 2, 0.0F);
		this.setRotateAngle(shape7_1, 0.3490658503988659F, 0.0F, 0.0F);
		this.shape16_2 = new ModelRenderer(this, 54, 0);
		this.shape16_2.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape16_2.addBox(-1.0F, -5.5F, -1.0F, 2, 6, 2, 0.0F);
		this.setRotateAngle(shape16_2, 0.0F, 0.0F, 1.0471975511965976F);
		this.shape3 = new ModelRenderer(this, 0, 11);
		this.shape3.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.shape3.addBox(-3.0F, 8.000000000000004F, -3.0F, 6, 1, 6, 0.0F);
		this.shape16 = new ModelRenderer(this, 0, 0);
		this.shape16.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape16.addBox(-1.0F, -5.5F, -1.0F, 2, 6, 2, 0.0F);
		this.setRotateAngle(shape16, 1.0471975511965976F, 0.0F, 0.0F);
		this.shape7_4 = new ModelRenderer(this, 32, 30);
		this.shape7_4.setRotationPoint(0.0F, 8.0F, 0.0F);
		this.shape7_4.addBox(-1.0F, -7.5F, -1.0F, 2, 8, 2, 0.0F);
		this.setRotateAngle(shape7_4, 0.0F, 0.0F, -0.3490658503988659F);
	}
	
	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
	{
		this.shape1.render(scale);
		GlStateManager.translate(0, .01 / 16, 0);
		this.shape1_1.render(scale);
		GlStateManager.translate(0, -.01 / 16, 0);
		
		this.shape3.render(scale);
		GlStateManager.translate(0, .01 / 16, 0);
		this.shape3_1.render(scale);
		GlStateManager.translate(0, .01 / 16, 0);
		this.shape3_2.render(scale);
		GlStateManager.translate(0, .01 / 16, 0);
		this.shape3_3.render(scale);
		GlStateManager.translate(0, -.03 / 16, 0);
		
		GlStateManager.enableBlend();
		GlStateManager.color(1, 1, 1, 1);
		GlStateManager.blendFunc(770, 774);
		this.shape7.render(scale);
		this.shape9.render(scale);
		this.shape9_3.render(scale);
		this.shape7_2.render(scale);
		this.shape16_3.render(scale);
		this.shape7_3.render(scale);
		this.shape16_1.render(scale);
		this.shape9_2.render(scale);
		this.shape9_1.render(scale);
		this.shape7_1.render(scale);
		this.shape16_2.render(scale);
		this.shape16.render(scale);
		this.shape7_4.render(scale);
		GlStateManager.blendFunc(770, 771);
	}
}
