package com.dragonforge.magistry.client.model;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.blocks.machines.tiles.TileGenerator;
import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelGenerator extends ModelSimple<TileGenerator>
{
	public ModelRenderer BottomPlate;
	public ModelRenderer SmallPlate;
	public ModelRenderer MainPart;
	public ModelRenderer AntennaBase;
	public ModelRenderer MainPart_1;
	public ModelRenderer SmallPlate_1;
	public ModelRenderer shell;
	public ModelRenderer shell_1;
	public ModelRenderer shell_2;
	public ModelRenderer shell_3;
	public ModelRenderer shell_4;
	public ModelRenderer shell_5;
	public ModelRenderer shell_6;
	public ModelRenderer Pillar;
	public ModelRenderer Support;
	public ModelRenderer Support_1;
	public ModelRenderer Support_2;
	public ModelRenderer Support_3;
	public ModelRenderer Bottom;
	
	public ModelGenerator()
	{
		super(512, 128, new ResourceLocation(Magistry.MOD_ID, "textures/models/generator.png"));
		this.Support = new ModelRenderer(this, 321, 0);
		this.Support.setRotationPoint(0.0F, -17.0F, -11.0F);
		this.Support.addBox(-3.0F, 0.0F, -3.0F, 6, 8, 6, 0.0F);
		this.SmallPlate = new ModelRenderer(this, 192, 0);
		this.SmallPlate.setRotationPoint(0.0F, 11.0F, 0.0F);
		this.SmallPlate.addBox(-21.5F, 0.0F, -21.5F, 43, 10, 43, 0.0F);
		this.Support_1 = new ModelRenderer(this, 339, 8);
		this.Support_1.setRotationPoint(0.0F, -17.0F, 11.0F);
		this.Support_1.addBox(-3.0F, 0.0F, -3.0F, 6, 8, 6, 0.0F);
		this.BottomPlate = new ModelRenderer(this, 0, 0);
		this.BottomPlate.setRotationPoint(0.0F, 21.0F, 0.0F);
		this.BottomPlate.addBox(-24.0F, 0.0F, -24.0F, 48, 3, 48, 0.0F);
		this.shell = new ModelRenderer(this, 144, 0);
		this.shell.setRotationPoint(0.0F, -25.00000000000007F, 0.0F);
		this.shell.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.shell_5 = new ModelRenderer(this, 183, 17);
		this.shell_5.setRotationPoint(0.0F, -45.00000000000007F, 0.0F);
		this.shell_5.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.SmallPlate_1 = new ModelRenderer(this, 270, 57);
		this.SmallPlate_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.SmallPlate_1.addBox(-19.5F, 0.0F, -19.5F, 39, 7, 39, 0.0F);
		this.Pillar = new ModelRenderer(this, 0, 0);
		this.Pillar.setRotationPoint(0.0F, -52.0F, 0.0F);
		this.Pillar.addBox(-3.5F, 0.0F, -3.5F, 7, 30, 7, 0.0F);
		this.Support_3 = new ModelRenderer(this, 339, 24);
		this.Support_3.setRotationPoint(11.0F, -17.0F, 0.0F);
		this.Support_3.addBox(-3.0F, 0.0F, -3.0F, 6, 8, 6, 0.0F);
		this.MainPart_1 = new ModelRenderer(this, 86, 53);
		this.MainPart_1.setRotationPoint(0.0F, 7.0F, 0.0F);
		this.MainPart_1.addBox(-23.0F, 0.0F, -23.0F, 46, 4, 46, 0.0F);
		this.shell_1 = new ModelRenderer(this, 456, 0);
		this.shell_1.setRotationPoint(0.0F, -29.00000000000007F, 0.0F);
		this.shell_1.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.Bottom = new ModelRenderer(this, 224, 53);
		this.Bottom.setRotationPoint(0.0F, -22.000000000000057F, 0.0F);
		this.Bottom.addBox(-8.0F, 0.0F, -8.0F, 16, 13, 16, 0.0F);
		this.MainPart = new ModelRenderer(this, 318, 7);
		this.MainPart.setRotationPoint(0.0F, -4.0F, 0.0F);
		this.MainPart.addBox(-23.0F, 0.0F, -23.0F, 46, 4, 46, 0.0F);
		this.AntennaBase = new ModelRenderer(this, 0, 51);
		this.AntennaBase.setRotationPoint(0.0F, -9.0F, 0.0F);
		this.AntennaBase.addBox(-16.5F, 0.0F, -16.5F, 33, 5, 33, 0.0F);
		this.shell_2 = new ModelRenderer(this, 183, 2);
		this.shell_2.setRotationPoint(0.0F, -33.00000000000007F, 0.0F);
		this.shell_2.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.shell_4 = new ModelRenderer(this, 456, 15);
		this.shell_4.setRotationPoint(0.0F, -41.00000000000007F, 0.0F);
		this.shell_4.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.shell_6 = new ModelRenderer(this, 456, 30);
		this.shell_6.setRotationPoint(0.0F, -49.00000000000007F, 0.0F);
		this.shell_6.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.shell_3 = new ModelRenderer(this, 144, 15);
		this.shell_3.setRotationPoint(0.0F, -37.00000000000007F, 0.0F);
		this.shell_3.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.Support_2 = new ModelRenderer(this, 321, 16);
		this.Support_2.setRotationPoint(-11.0F, -17.0F, 0.0F);
		this.Support_2.addBox(-3.0F, 0.0F, -3.0F, 6, 8, 6, 0.0F);
	}
	
	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
	{
		this.Support.render(scale);
		this.SmallPlate.render(scale);
		this.Support_1.render(scale);
		this.BottomPlate.render(scale);
		this.shell.render(scale);
		this.shell_5.render(scale);
		this.SmallPlate_1.render(scale);
		this.Pillar.render(scale);
		this.Support_3.render(scale);
		this.MainPart_1.render(scale);
		this.shell_1.render(scale);
		this.Bottom.render(scale);
		this.MainPart.render(scale);
		this.AntennaBase.render(scale);
		this.shell_2.render(scale);
		this.shell_4.render(scale);
		this.shell_6.render(scale);
		this.shell_3.render(scale);
		this.Support_2.render(scale);
	}
}