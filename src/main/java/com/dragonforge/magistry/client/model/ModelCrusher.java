package com.dragonforge.magistry.client.model;

import com.dragonforge.magistry.Magistry;
import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelCrusher extends ModelSimple<Object>
{
	public ModelRenderer shape32;
	public ModelRenderer shape17;
	public ModelRenderer shape17_1;
	public ModelRenderer shape17_2;
	public ModelRenderer shape15;
	public ModelRenderer shape15_1;
	public ModelRenderer shape15_2;
	public ModelRenderer shape15_3;
	public ModelRenderer shape11;
	public ModelRenderer shape12;
	public ModelRenderer shape13;
	public ModelRenderer shape13_1;
	public ModelRenderer shape22;
	public ModelRenderer shape27;
	public ModelRenderer shape28;
	public ModelRenderer shape20;
	public ModelRenderer shape20_1;
	public ModelRenderer shape23;
	public ModelRenderer shape23_1;
	public ModelRenderer shape25;
	public ModelRenderer shape26;
	public ModelRenderer shape29;
	public ModelRenderer shape29_1;
	public ModelRenderer shape29_2;
	public ModelRenderer shape5;
	public ModelRenderer shape6;
	public ModelRenderer shape6_1;
	public ModelRenderer shape6_2;
	public ModelRenderer shape6_3;
	public ModelRenderer shape5_1;
	public ModelRenderer shape1;
	public ModelRenderer shape1_1;
	public ModelRenderer shape1_2;
	
	public ModelCrusher()
	{
		super(256, 256, new ResourceLocation(Magistry.MOD_ID, "textures/models/crusher.png"));
		this.shape5 = new ModelRenderer(this, 171, 25);
		this.shape5.setRotationPoint(1.0F, -5.000000000000006F, 9.0F);
		this.shape5.addBox(0.0F, 0.0F, 0.0F, 14, 8, 14, 0.0F);
		this.shape1_1 = new ModelRenderer(this, 0, 99);
		this.shape1_1.setRotationPoint(0.0F, 14.999999999999993F, 0.0F);
		this.shape1_1.addBox(-14.0F, 0.0F, -22.0F, 28, 7, 44, 0.0F);
		this.shape11 = new ModelRenderer(this, 136, 0);
		this.shape11.setRotationPoint(-12.0F, -6.000000000000006F, -21.0F);
		this.shape11.addBox(0.0F, 0.0F, 0.0F, 24, 9, 9, 0.0F);
		this.shape13 = new ModelRenderer(this, 40, 11);
		this.shape13.setRotationPoint(-14.0F, -7.000000000000006F, -23.0F);
		this.shape13.addBox(0.0F, 0.0F, 0.0F, 28, 1, 13, 0.0F);
		this.shape17_2 = new ModelRenderer(this, 88, 0);
		this.shape17_2.setRotationPoint(-8.0F, -7.990000000000007F, 16.0F);
		this.shape17_2.addBox(-4.5F, 0.0F, -4.5F, 9, 2, 9, 0.0F);
		this.setRotateAngle(shape17_2, 0.0F, 0.7853981633974483F, 0.0F);
		this.shape23_1 = new ModelRenderer(this, 76, 25);
		this.shape23_1.setRotationPoint(-8.0F, -2.1999999999999993F, -1.5F);
		this.shape23_1.addBox(0.0F, 0.0F, 0.0F, 16, 2, 2, 0.0F);
		this.shape5_1 = new ModelRenderer(this, 82, 30);
		this.shape5_1.setRotationPoint(0.0F, -8.000000000000007F, 8.0F);
		this.shape5_1.addBox(0.0F, 0.0F, 0.0F, 16, 3, 16, 0.0F);
		this.shape20_1 = new ModelRenderer(this, 203, 8);
		this.shape20_1.setRotationPoint(-10.5F, -2.1999999999999993F, -2.0F);
		this.shape20_1.addBox(0.0F, 0.0F, 0.0F, 2, 2, 15, 0.0F);
		this.shape26 = new ModelRenderer(this, 236, 9);
		this.shape26.setRotationPoint(3.5F, -2.1999999999999993F, -11.0F);
		this.shape26.addBox(0.0F, 0.0F, 0.0F, 2, 2, 7, 0.0F);
		this.shape1 = new ModelRenderer(this, 0, 49);
		this.shape1.setRotationPoint(0.0F, 22.000000000000025F, 0.0F);
		this.shape1.addBox(-16.0F, 0.0F, -24.0F, 32, 2, 48, 0.0F);
		this.shape13_1 = new ModelRenderer(this, 111, 18);
		this.shape13_1.setRotationPoint(-13.0F, -8.000000000000007F, -22.0F);
		this.shape13_1.addBox(0.0F, 0.0F, 0.0F, 26, 1, 11, 0.0F);
		this.shape29_1 = new ModelRenderer(this, 222, 8);
		this.shape29_1.setRotationPoint(-8.8F, -0.19999999999999948F, 2.4F);
		this.shape29_1.addBox(0.0F, 0.0F, 0.0F, 4, 4, 1, 0.0F);
		this.shape1_2 = new ModelRenderer(this, 0, 150);
		this.shape1_2.setRotationPoint(0.0F, 3.0000000000000018F, 0.0F);
		this.shape1_2.addBox(-16.0F, 0.0F, -24.0F, 32, 12, 48, 0.0F);
		this.shape15_1 = new ModelRenderer(this, 6, 0);
		this.shape15_1.setRotationPoint(11.8F, -6.000000000000006F, -15.5F);
		this.shape15_1.addBox(0.2F, 0.0F, 0.0F, 1, 9, 2, 0.0F);
		this.shape23 = new ModelRenderer(this, 40, 25);
		this.shape23.setRotationPoint(-10.5F, -2.1999999999999993F, -4.0F);
		this.shape23.addBox(0.0F, 0.0F, 0.0F, 16, 2, 2, 0.0F);
		this.shape15 = new ModelRenderer(this, 0, 0);
		this.shape15.setRotationPoint(11.8F, -6.000000000000006F, -19.5F);
		this.shape15.addBox(0.2F, 0.0F, 0.0F, 1, 9, 2, 0.0F);
		this.shape6_3 = new ModelRenderer(this, 70, 29);
		this.shape6_3.setRotationPoint(1.5F, -4.000000000000006F, 8.5F);
		this.shape6_3.addBox(0.0F, 0.0F, 0.0F, 13, 6, 1, 0.0F);
		this.shape6 = new ModelRenderer(this, 227, 21);
		this.shape6.setRotationPoint(0.5F, -4.000000000000006F, 9.5F);
		this.shape6.addBox(0.0F, 0.0F, 0.0F, 1, 6, 13, 0.0F);
		this.shape17 = new ModelRenderer(this, 24, 0);
		this.shape17.setRotationPoint(-8.0F, -6.000000000000006F, 16.0F);
		this.shape17.addBox(-3.5F, 0.0F, -3.5F, 7, 9, 7, 0.0F);
		this.shape32 = new ModelRenderer(this, 0, 0);
		this.shape32.setRotationPoint(-17.0F, 16.0F, 0.0F);
		this.shape32.addBox(0.0F, -8.0F, -8.0F, 4, 16, 16, 0.0F);
		this.shape17_1 = new ModelRenderer(this, 52, 0);
		this.shape17_1.setRotationPoint(-8.0F, -8.000000000000007F, 16.0F);
		this.shape17_1.addBox(-4.5F, 0.0F, -4.5F, 9, 2, 9, 0.0F);
		this.shape15_3 = new ModelRenderer(this, 130, 0);
		this.shape15_3.setRotationPoint(-13.0F, -6.000000000000006F, -15.5F);
		this.shape15_3.addBox(0.0F, 0.0F, 0.0F, 1, 9, 2, 0.0F);
		this.shape6_1 = new ModelRenderer(this, 27, 29);
		this.shape6_1.setRotationPoint(14.5F, -4.000000000000006F, 9.5F);
		this.shape6_1.addBox(0.0F, 0.0F, 0.0F, 1, 6, 13, 0.0F);
		this.shape27 = new ModelRenderer(this, 45, 0);
		this.shape27.setRotationPoint(2.7F, -2.6000000000000005F, -12.0F);
		this.shape27.addBox(0.0F, 0.0F, 0.0F, 6, 3, 1, 0.0F);
		this.shape29_2 = new ModelRenderer(this, 0, 11);
		this.shape29_2.setRotationPoint(-0.8F, -0.19999999999999948F, 0.3F);
		this.shape29_2.addBox(0.0F, 0.0F, 0.0F, 4, 4, 1, 0.0F);
		this.setRotateAngle(shape29_2, 0.0F, 1.5707963267948966F, 0.0F);
		this.shape12 = new ModelRenderer(this, 193, 0);
		this.shape12.setRotationPoint(-10.0F, -5.000000000000006F, -21.5F);
		this.shape12.addBox(0.0F, 0.0F, 0.0F, 20, 7, 1, 0.0F);
		this.shape6_2 = new ModelRenderer(this, 42, 29);
		this.shape6_2.setRotationPoint(1.5F, -4.000000000000006F, 22.5F);
		this.shape6_2.addBox(0.0F, 0.0F, 0.0F, 13, 6, 1, 0.0F);
		this.shape20 = new ModelRenderer(this, 190, 8);
		this.shape20.setRotationPoint(-8.0F, -2.1999999999999993F, 0.5F);
		this.shape20.addBox(0.0F, 0.0F, 0.0F, 2, 2, 12, 0.0F);
		this.shape28 = new ModelRenderer(this, 231, 4);
		this.shape28.setRotationPoint(-8.0F, -4.000000000000006F, -12.0F);
		this.shape28.addBox(0.0F, 0.0F, 0.0F, 7, 1, 4, 0.0F);
		this.shape29 = new ModelRenderer(this, 206, 8);
		this.shape29.setRotationPoint(3.8F, -0.19999999999999948F, -6.4F);
		this.shape29.addBox(0.0F, 0.0F, 0.0F, 4, 4, 1, 0.0F);
		this.shape25 = new ModelRenderer(this, 222, 9);
		this.shape25.setRotationPoint(6.0F, -2.1999999999999993F, -11.5F);
		this.shape25.addBox(0.0F, 0.0F, 0.0F, 2, 2, 10, 0.0F);
		this.shape15_2 = new ModelRenderer(this, 124, 0);
		this.shape15_2.setRotationPoint(-13.0F, -6.000000000000006F, -19.5F);
		this.shape15_2.addBox(0.0F, 0.0F, 0.0F, 1, 9, 2, 0.0F);
		this.shape22 = new ModelRenderer(this, 79, 0);
		this.shape22.setRotationPoint(-7.0F, -3.0000000000000027F, -12.0F);
		this.shape22.addBox(0.0F, 0.0F, 0.0F, 5, 6, 3, 0.0F);
	}
	
	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
	{
		this.shape5.render(scale);
		this.shape1_1.render(scale);
		this.shape11.render(scale);
		this.shape13.render(scale);
		GlStateManager.pushMatrix();
		GlStateManager.translate(this.shape17_2.offsetX, this.shape17_2.offsetY, this.shape17_2.offsetZ);
		GlStateManager.translate(this.shape17_2.rotationPointX * scale, this.shape17_2.rotationPointY * scale, this.shape17_2.rotationPointZ * scale);
		GlStateManager.scale(1.0D, 0.98D, 1.0D);
		GlStateManager.translate(-this.shape17_2.offsetX, -this.shape17_2.offsetY, -this.shape17_2.offsetZ);
		GlStateManager.translate(-this.shape17_2.rotationPointX * scale, -this.shape17_2.rotationPointY * scale, -this.shape17_2.rotationPointZ * scale);
		this.shape17_2.render(scale);
		GlStateManager.popMatrix();
		this.shape23_1.render(scale);
		this.shape5_1.render(scale);
		this.shape20_1.render(scale);
		this.shape26.render(scale);
		this.shape1.render(scale);
		this.shape13_1.render(scale);
		this.shape29_1.render(scale);
		this.shape1_2.render(scale);
		this.shape15_1.render(scale);
		this.shape23.render(scale);
		this.shape15.render(scale);
		this.shape6_3.render(scale);
		this.shape6.render(scale);
		this.shape17.render(scale);
		GlStateManager.pushMatrix();
		GlStateManager.translate(this.shape32.offsetX, this.shape32.offsetY, this.shape32.offsetZ);
		GlStateManager.translate(this.shape32.rotationPointX * scale, this.shape32.rotationPointY * scale, this.shape32.rotationPointZ * scale);
		GlStateManager.scale(1.0D, 0.99D, 1.0D);
		GlStateManager.translate(-this.shape32.offsetX, -this.shape32.offsetY, -this.shape32.offsetZ);
		GlStateManager.translate(-this.shape32.rotationPointX * scale, -this.shape32.rotationPointY * scale, -this.shape32.rotationPointZ * scale);
		this.shape32.render(scale);
		GlStateManager.popMatrix();
		this.shape17_1.render(scale);
		this.shape15_3.render(scale);
		this.shape6_1.render(scale);
		this.shape27.render(scale);
		this.shape29_2.render(scale);
		this.shape12.render(scale);
		this.shape6_2.render(scale);
		this.shape20.render(scale);
		this.shape28.render(scale);
		this.shape29.render(scale);
		this.shape25.render(scale);
		this.shape15_2.render(scale);
		this.shape22.render(scale);
	}
}