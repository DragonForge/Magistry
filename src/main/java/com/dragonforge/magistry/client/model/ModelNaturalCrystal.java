package com.dragonforge.magistry.client.model;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.blocks.tiles.TileNaturalCrystal;
import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelNaturalCrystal extends ModelSimple<TileNaturalCrystal>
{
	// Base Parts
	public ModelRenderer shape1;
	public ModelRenderer shape2;
	
	// Crystal Parts
	public ModelRenderer shape3;
	public ModelRenderer shape4;
	public ModelRenderer shape5;
	public ModelRenderer shape6;
	public ModelRenderer shape7;
	
	public ModelNaturalCrystal()
	{
		super(64, 32, new ResourceLocation(Magistry.MOD_ID, "textures/models/natural_crystal.png"));
		this.shape1 = new ModelRenderer(this, 0, 0);
		this.shape1.setRotationPoint(0.0F, 23.0F, 0.0F);
		this.shape1.addBox(-4.0F, 0.0F, -4.0F, 8, 1, 8, 0.0F);
		this.shape5 = new ModelRenderer(this, 38, 7);
		this.shape5.setRotationPoint(0.0F, 23.0F, 0.0F);
		this.shape5.addBox(-2.4F, -8.0F, 0.0F, 2, 8, 2, 0.0F);
		this.setRotateAngle(shape5, -0.19058995431778078F, 0.0F, -0.16406094968746698F);
		this.shape2 = new ModelRenderer(this, 24, 0);
		this.shape2.setRotationPoint(0.0F, 22.0F, 0.0F);
		this.shape2.addBox(-3.0F, 0.0F, -3.0F, 6, 1, 6, 0.0F);
		this.shape7 = new ModelRenderer(this, 54, 7);
		this.shape7.setRotationPoint(0.0F, 23.0F, 0.0F);
		this.shape7.addBox(-0.8F, -12.0F, -0.9F, 2, 12, 2, 0.0F);
		this.setRotateAngle(shape7, 0.0F, 0.0F, -0.05235987755982988F);
		this.shape3 = new ModelRenderer(this, 48, 0);
		this.shape3.setRotationPoint(0.0F, 23.0F, 0.0F);
		this.shape3.addBox(-0.1F, -7.0F, 0.1F, 2, 7, 2, 0.0F);
		this.setRotateAngle(shape3, -0.17453292519943295F, 0.0F, 0.17453292519943295F);
		this.shape6 = new ModelRenderer(this, 0, 0);
		this.shape6.setRotationPoint(0.0F, 23.0F, 0.0F);
		this.shape6.addBox(-2.4F, -6.0F, -2.4F, 2, 6, 2, 0.0F);
		this.setRotateAngle(shape6, 0.18709929581379212F, -0.031415926535897934F, -0.17976891295541594F);
		this.shape4 = new ModelRenderer(this, 30, 7);
		this.shape4.setRotationPoint(0.0F, 23.0F, 0.0F);
		this.shape4.addBox(-0.5F, -9.0F, -2.0F, 2, 9, 2, 0.0F);
		this.setRotateAngle(shape4, 0.1884955592153876F, 0.0F, 0.19198621771937624F);
	}
	
	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
	{
		this.shape1.render(scale);
		this.shape2.render(scale);
		
		this.shape3.render(scale);
		this.shape4.render(scale);
		this.shape5.render(scale);
		this.shape6.render(scale);
		this.shape7.render(scale);
	}
}