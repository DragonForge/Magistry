package com.dragonforge.magistry.client.model;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.blocks.machines.tiles.TileEtherCollector;
import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelEtherCollector extends ModelSimple<TileEtherCollector>
{
	public ModelRenderer shape1;
	public ModelRenderer shape1_1;
	public ModelRenderer shape1_2;
	public ModelRenderer shape1_3;
	public ModelRenderer shape1_4;
	public ModelRenderer shape4;
	public ModelRenderer shape10;
	public ModelRenderer shape10_1;
	public ModelRenderer shape1_5;
	public ModelRenderer shape1_6;
	public ModelRenderer shape1_7;
	public ModelRenderer shape12;
	public ModelRenderer shape12_1;
	public ModelRenderer shape12_2;
	public ModelRenderer shape12_3;
	
	public ModelEtherCollector()
	{
		super(256, 128, new ResourceLocation(Magistry.MOD_ID, "textures/models/ether_collector.png"));
		this.textureWidth = 256;
		this.textureHeight = 128;
		this.shape1 = new ModelRenderer(this, 0, 0);
		this.shape1.setRotationPoint(0.0F, 18.0F, 0.0F);
		this.shape1.addBox(-16.0F, 0.0F, -16.0F, 32, 5, 32, 0.0F);
		this.shape4 = new ModelRenderer(this, 112, 27);
		this.shape4.setRotationPoint(0.0F, -17.0F, 0.0F);
		this.shape4.addBox(-8.0F, 0.0F, -8.0F, 16, 16, 16, 0.0F);
		this.shape12_3 = new ModelRenderer(this, 24, 65);
		this.shape12_3.setRotationPoint(-14.0F, -36.0F, 14.0F);
		this.shape12_3.addBox(-1.0F, 0.0F, -1.0F, 2, 54, 2, 0.0F);
		this.shape1_2 = new ModelRenderer(this, 192, 0);
		this.shape1_2.setRotationPoint(0.0F, -1.0F, 0.0F);
		this.shape1_2.addBox(-4.5F, 0.0F, -4.5F, 9, 16, 9, 0.0F);
		this.shape1_3 = new ModelRenderer(this, 219, 16);
		this.shape1_3.setRotationPoint(0.0F, -33.0F, 0.0F);
		this.shape1_3.addBox(-4.5F, 0.0F, -4.5F, 9, 16, 9, 0.0F);
		this.setRotateAngle(shape1_3, 0.0F, 0.7853981633974483F, 0.0F);
		this.shape1_5 = new ModelRenderer(this, 75, 37);
		this.shape1_5.setRotationPoint(0.0F, -33.0F, 0.0F);
		this.shape1_5.addBox(-4.5F, 0.0F, -4.5F, 9, 16, 9, 0.0F);
		this.shape1_6 = new ModelRenderer(this, 76, 62);
		this.shape1_6.setRotationPoint(0.0F, -36.0F, 0.0F);
		this.shape1_6.addBox(-12.0F, 0.0F, -12.0F, 24, 3, 24, 0.0F);
		this.shape12_2 = new ModelRenderer(this, 16, 65);
		this.shape12_2.setRotationPoint(14.0F, -36.0F, 14.0F);
		this.shape12_2.addBox(-1.0F, 0.0F, -1.0F, 2, 54, 2, 0.0F);
		this.shape12 = new ModelRenderer(this, 0, 65);
		this.shape12.setRotationPoint(-14.0F, -36.0F, -14.0F);
		this.shape12.addBox(-1.0F, 0.0F, -1.0F, 2, 54, 2, 0.0F);
		this.shape12_1 = new ModelRenderer(this, 8, 65);
		this.shape12_1.setRotationPoint(14.0F, -36.0F, -14.0F);
		this.shape12_1.addBox(-1.0F, 0.0F, -1.0F, 2, 54, 2, 0.0F);
		this.shape1_4 = new ModelRenderer(this, 183, 25);
		this.shape1_4.setRotationPoint(0.0F, -1.0F, 0.0F);
		this.shape1_4.addBox(-4.5F, 0.0F, -4.5F, 9, 16, 9, 0.0F);
		this.setRotateAngle(shape1_4, 0.0F, 0.7853981633974483F, 0.0F);
		this.shape10 = new ModelRenderer(this, 0, 37);
		this.shape10.setRotationPoint(0.0F, -6.0F, 0.0F);
		this.shape10.addBox(-12.5F, 0.0F, -12.5F, 25, 3, 25, 0.0F);
		this.setRotateAngle(shape10, 0.0F, 0.7853981633974483F, 0.0F);
		this.shape10_1 = new ModelRenderer(this, 151, 50);
		this.shape10_1.setRotationPoint(0.0F, -15.0F, 0.0F);
		this.shape10_1.addBox(-12.5F, 0.0F, -12.5F, 25, 3, 25, 0.0F);
		this.setRotateAngle(shape10_1, 0.0F, 0.7853981633974483F, 0.0F);
		this.shape1_1 = new ModelRenderer(this, 96, 0);
		this.shape1_1.setRotationPoint(0.0F, 15.0F, 0.0F);
		this.shape1_1.addBox(-12.0F, 0.0F, -12.0F, 24, 3, 24, 0.0F);
		this.shape1_7 = new ModelRenderer(this, 0, 89);
		this.shape1_7.setRotationPoint(0.0F, -41.0F, 0.0F);
		this.shape1_7.addBox(-16.0F, 0.0F, -16.0F, 32, 5, 32, 0.0F);
	}
	
	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
	{
		this.shape1.render(scale);
		this.shape4.render(scale);
		this.shape12_3.render(scale);
		this.shape1_2.render(scale);
		this.shape1_3.render(scale);
		this.shape1_5.render(scale);
		this.shape1_6.render(scale);
		this.shape12_2.render(scale);
		this.shape12.render(scale);
		this.shape12_1.render(scale);
		this.shape1_4.render(scale);
		this.shape1_1.render(scale);
		this.shape1_7.render(scale);
	}
}