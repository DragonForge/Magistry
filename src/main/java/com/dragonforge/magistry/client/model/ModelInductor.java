package com.dragonforge.magistry.client.model;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.blocks.machines.tiles.TileInductor;
import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelInductor extends ModelSimple<TileInductor>
{
	public ModelRenderer Bottomplate;
	public ModelRenderer Pillar;
	public ModelRenderer TopPlate;
	public ModelRenderer Itemplate;
	public ModelRenderer shell;
	public ModelRenderer shell_1;
	public ModelRenderer shell_2;
	public ModelRenderer shell_3;
	public ModelRenderer shell_4;
	
	public ModelInductor()
	{
		super(128, 64, new ResourceLocation(Magistry.MOD_ID, "textures/models/inductor.png"));
		this.TopPlate = new ModelRenderer(this, 0, 15);
		this.TopPlate.setRotationPoint(0.0F, -3.999999999999638F, 0.0F);
		this.TopPlate.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.shell_3 = new ModelRenderer(this, 0, 45);
		this.shell_3.setRotationPoint(0.0F, 5.000000000000168F, 0.0F);
		this.shell_3.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.shell_2 = new ModelRenderer(this, 39, 33);
		this.shell_2.setRotationPoint(0.0F, 9.0000000000001F, 0.0F);
		this.shell_2.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.Bottomplate = new ModelRenderer(this, 0, 0);
		this.Bottomplate.setRotationPoint(0.0F, 22.00000000000005F, 0.0F);
		this.Bottomplate.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.shell = new ModelRenderer(this, 67, 18);
		this.shell.setRotationPoint(0.0F, 16.99999999999997F, 0.0F);
		this.shell.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.shell_4 = new ModelRenderer(this, 39, 48);
		this.shell_4.setRotationPoint(0.0F, 1.0000000000002303F, 0.0F);
		this.shell_4.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.shell_1 = new ModelRenderer(this, 0, 30);
		this.shell_1.setRotationPoint(0.0F, 13.000000000000027F, 0.0F);
		this.shell_1.addBox(-6.5F, 0.0F, -6.5F, 13, 2, 13, 0.0F);
		this.Itemplate = new ModelRenderer(this, 80, 0);
		this.Itemplate.setRotationPoint(0.0F, -4.999999999999619F, 0.0F);
		this.Itemplate.addBox(-4.5F, 0.0F, -4.5F, 9, 1, 9, 0.0F);
		this.Pillar = new ModelRenderer(this, 52, 0);
		this.Pillar.setRotationPoint(0.0F, -1.999999999999662F, 0.0F);
		this.Pillar.addBox(-3.5F, 0.0F, -3.5F, 7, 24, 7, 0.0F);
	}
	
	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
	{
		this.TopPlate.render(scale);
		GlStateManager.pushMatrix();
		GlStateManager.translate(this.shell_3.offsetX, this.shell_3.offsetY, this.shell_3.offsetZ);
		GlStateManager.translate(this.shell_3.rotationPointX * scale, this.shell_3.rotationPointY * scale, this.shell_3.rotationPointZ * scale);
		GlStateManager.scale(0.81D, 1.0D, 0.81D);
		GlStateManager.translate(-this.shell_3.offsetX, -this.shell_3.offsetY, -this.shell_3.offsetZ);
		GlStateManager.translate(-this.shell_3.rotationPointX * scale, -this.shell_3.rotationPointY * scale, -this.shell_3.rotationPointZ * scale);
		this.shell_3.render(scale);
		GlStateManager.popMatrix();
		GlStateManager.pushMatrix();
		GlStateManager.translate(this.shell_2.offsetX, this.shell_2.offsetY, this.shell_2.offsetZ);
		GlStateManager.translate(this.shell_2.rotationPointX * scale, this.shell_2.rotationPointY * scale, this.shell_2.rotationPointZ * scale);
		GlStateManager.scale(0.81D, 1.0D, 0.81D);
		GlStateManager.translate(-this.shell_2.offsetX, -this.shell_2.offsetY, -this.shell_2.offsetZ);
		GlStateManager.translate(-this.shell_2.rotationPointX * scale, -this.shell_2.rotationPointY * scale, -this.shell_2.rotationPointZ * scale);
		this.shell_2.render(scale);
		GlStateManager.popMatrix();
		this.Bottomplate.render(scale);
		GlStateManager.pushMatrix();
		GlStateManager.translate(this.shell.offsetX, this.shell.offsetY, this.shell.offsetZ);
		GlStateManager.translate(this.shell.rotationPointX * scale, this.shell.rotationPointY * scale, this.shell.rotationPointZ * scale);
		GlStateManager.scale(0.81D, 1.0D, 0.81D);
		GlStateManager.translate(-this.shell.offsetX, -this.shell.offsetY, -this.shell.offsetZ);
		GlStateManager.translate(-this.shell.rotationPointX * scale, -this.shell.rotationPointY * scale, -this.shell.rotationPointZ * scale);
		this.shell.render(scale);
		GlStateManager.popMatrix();
		GlStateManager.pushMatrix();
		GlStateManager.translate(this.shell_4.offsetX, this.shell_4.offsetY, this.shell_4.offsetZ);
		GlStateManager.translate(this.shell_4.rotationPointX * scale, this.shell_4.rotationPointY * scale, this.shell_4.rotationPointZ * scale);
		GlStateManager.scale(0.81D, 1.0D, 0.81D);
		GlStateManager.translate(-this.shell_4.offsetX, -this.shell_4.offsetY, -this.shell_4.offsetZ);
		GlStateManager.translate(-this.shell_4.rotationPointX * scale, -this.shell_4.rotationPointY * scale, -this.shell_4.rotationPointZ * scale);
		this.shell_4.render(scale);
		GlStateManager.popMatrix();
		GlStateManager.pushMatrix();
		GlStateManager.translate(this.shell_1.offsetX, this.shell_1.offsetY, this.shell_1.offsetZ);
		GlStateManager.translate(this.shell_1.rotationPointX * scale, this.shell_1.rotationPointY * scale, this.shell_1.rotationPointZ * scale);
		GlStateManager.scale(0.81D, 1.0D, 0.81D);
		GlStateManager.translate(-this.shell_1.offsetX, -this.shell_1.offsetY, -this.shell_1.offsetZ);
		GlStateManager.translate(-this.shell_1.rotationPointX * scale, -this.shell_1.rotationPointY * scale, -this.shell_1.rotationPointZ * scale);
		this.shell_1.render(scale);
		GlStateManager.popMatrix();
		this.Itemplate.render(scale);
		this.Pillar.render(scale);
	}
}