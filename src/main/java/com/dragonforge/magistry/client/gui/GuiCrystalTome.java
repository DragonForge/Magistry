package com.dragonforge.magistry.client.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.gui.IGuiKnowledgeAcceptor;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.api.research.client.IAdditionalPage;
import com.dragonforge.magistry.api.research.client.IPreviewIcon;
import com.dragonforge.magistry.client.MagistryFonts;
import com.dragonforge.magistry.client.ttf2gl.ScaledTTFAtlas;
import com.dragonforge.magistry.init.KnowledgeM;
import com.dragonforge.magistry.init.SoundsM;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.intr.jei.IJeiHelper;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class GuiCrystalTome extends GuiScreen implements IGuiKnowledgeAcceptor
{
	static final int texRX = 57, texRY = 18, texRW = 155, texRH = 184;
	static final ResourceLocation texture = new ResourceLocation(Magistry.MOD_ID, "textures/gui/crystal_tome.png");
	static int page;
	static int researchPage;
	static ResearchBase research;
	
	final List<ResearchBase> researches = new ArrayList<>();
	final ScaledTTFAtlas font;
	List<String> tooltip = new ArrayList<>();
	String[] compiledDescPages;
	IPlayerKnowledge knowledge;
	int pageDelta = 0;
	boolean dirty;
	
	public GuiCrystalTome()
	{
		font = MagistryFonts.PARCHMENT.getScaledInstance(.92F);
		knowledge = MagistryAPI.getPlayerKnowledge(Minecraft.getMinecraft().player);
		if(knowledge != null)
			Minecraft.getMinecraft().displayGuiScreen(this);
		dirty = true;
		
		if(getCurrentPage() != null)
			getCurrentPage().onAppear();
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		setResearch(research);
	}
	
	@Override
	public void updateScreen()
	{
		if(dirty)
		{
			dirty = false;
			
			researches.clear();
			researches.addAll(knowledge.getListOfKnownResearchValues());
			
			if(!researches.isEmpty() && (research == null || !researches.contains(research)))
				setResearch(researches.get(0));
		}
	}
	
	public void complileDescription()
	{
		if(research == null)
		{
			compiledDescPages = new String[0];
			return;
		}
		List<String> pages = new ArrayList<>();
		String cp = "";
		for(String word : research.getDescription())
		{
			String np = cp + (!cp.isEmpty() ? " " : "") + word;
			if(font.getWordWrappedHeight(np, texRW) > texRH - (pages.isEmpty() ? 32 : 0) - 6)
			{
				pages.add(cp);
				cp = word;
			} else
				cp = np;
		}
		if(!cp.isEmpty())
			pages.add(cp);
		compiledDescPages = pages.toArray(new String[pages.size()]);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		GlStateManager.enableBlend();
		GlStateManager.color(1, 1, 1, 1);
		
		int dx, dy;
		
		UtilsFX.bindTexture(texture);
		GlStateManager.pushMatrix();
		GlStateManager.translate(dx = width / 2, dy = height / 2, 0);
		GlStateManager.scale(1.7, 1.7, 1.7);
		GlStateManager.translate(-128, -72, 0);
		RenderUtil.drawTexturedModalRect(0, 0, 0, 0, 256, 144);
		GlStateManager.popMatrix();
		
		dx -= 128 * 1.7;
		dy -= 72 * 1.7;
		
		int j = 0, x, y;
		ResearchBase rb;
		
		for(int i = researchPage * 42; i < researches.size(); ++i)
		{
			if(j >= 42)
				break;
			rb = researches.get(i);
			
			UtilsFX.bindTexture(Magistry.MOD_ID, "textures/gui/overlays.png");
			GlStateManager.pushMatrix();
			GlStateManager.translate(x = width / 2 + 8 + j % 6 * 26, y = dy + j / 6 * 26 + 22, 0);
			RenderUtil.drawTexturedModalRect(0, 0, 78, 0, 24, 24);
			if(research == rb)
				RenderUtil.drawTexturedModalRect(0, 0, 78, 0, 24, 24);
			GlStateManager.translate(4, 4, 4);
			RenderHelper.enableGUIStandardItemLighting();
			rb.getPreview().renderPreview();
			RenderHelper.disableStandardItemLighting();
			GlStateManager.popMatrix();
			
			if(mouseX >= x - 1 && mouseY >= y - 1 && mouseX <= x + 24 && mouseY <= y + 24)
				tooltip.add(I18n.format("research." + rb.getRegistryName() + ".name"));
			
			++j;
		}
		
		int dw = Mouse.getDWheel();
		float bob = MathHelper.sin((mc.player.ticksExisted / 3F)) * .2F + .1F;
		
		if(research != null)
		{
			if(page == 0)
			{
				IPreviewIcon prev = research.getPreview();
				
				GlStateManager.pushMatrix();
				GlStateManager.translate(dx + texRW / 2F * 1.7F, dy + texRY - 6, 0);
				GlStateManager.scale(2, 2, 2);
				String txt = I18n.format("research." + research.getRegistryName() + ".name");
				int w = MagistryFonts.PARCHMENT.getWidth(txt);
				if(w * 2 >= texRW)
				{
					float sr = texRW / (float) w / 2;
					GlStateManager.translate(-w / 2 * sr, 10 + 1F / sr, 0);
					GlStateManager.scale(sr, sr, sr);
				} else
					GlStateManager.translate(-w / 2, 10, 0);
				MagistryFonts.PARCHMENT.renderWithShadow(txt, 0, 0, 0xFFFFFFFF);
				GlStateManager.popMatrix();
				
				GlStateManager.pushMatrix();
				GlStateManager.translate(dx + texRX + texRW / 2F - 8, dy + texRY + 3, 0);
				RenderHelper.enableGUIStandardItemLighting();
				prev.renderPreview();
				GlStateManager.popMatrix();
			}
			
			if(dw < 0)
				nextPage();
			else if(dw > 0)
				prevPage();
			
			GlStateManager.color(1, 1, 1, 1);
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(dx + texRX, dy + texRY, 50);
			renderPage(partialTicks, mouseX - (dx + texRX), mouseY - (dy + texRY));
			GlStateManager.popMatrix();
			
			GlStateManager.enableTexture2D();
			GlStateManager.enableBlend();
			GlStateManager.color(1, 1, 1, 1);
			
			UtilsFX.bindTexture("textures/gui/gui_manual_page.png");
			
			RenderHelper.disableStandardItemLighting();
			GlStateManager.disableLighting();
			GlStateManager.color(.25F, .25F, .25F, 1);
			
			pageDelta = 0;
			
			if(page > 0)
			{
				drawTexturedModalRectScaled(dx + texRX + 6, dy + texRY + texRH - 2, 0, 184, 12, 8, bob);
				if(mouseX >= dx + texRX + 4 && mouseY >= dy + texRY + texRH - 4 && mouseX < dx + texRX + 20 && mouseY < dy + texRY + texRH + 10)
					pageDelta = -1;
			}
			
			if(page + 1 < compiledDescPages.length + research.getAdditionalPages().length)
			{
				drawTexturedModalRectScaled(dx + texRX + texRW - 20, dy + texRY + texRH - 2, 12, 184, 12, 8, bob);
				if(mouseX >= dx + texRX + texRW - 22 && mouseY >= dy + texRY + texRH - 4 && mouseX < dx + texRX + texRW - 22 + 14 && mouseY < dy + texRY + texRH + 10)
					pageDelta = 1;
			}
		}
		
		if(researchPage > 0)
		{
			drawTexturedModalRectScaled(width / 2 + 8, dy + texRY + texRH + 4, 0, 184, 12, 8, bob);
			if(mouseX >= width / 2 + 6 && mouseY >= dy + texRY + texRH + 2 && mouseX < width / 2 + 6 + 14 && mouseY < dy + texRY + texRH + 16 && Mouse.next() && Mouse.getEventButtonState() && Mouse.getEventButton() == 0)
			{
				mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
				--researchPage;
			}
		}
		
		if((researchPage + 1) * 42 < researches.size())
		{
			drawTexturedModalRectScaled(width / 2 + texRW - 6, dy + texRY + texRH + 4, 12, 184, 12, 8, bob);
			if(mouseX >= width / 2 + texRW - 8 && mouseY >= dy + texRY + texRH + 2 && mouseX < width / 2 + texRW - 8 + 14 && mouseY < dy + texRY + texRH + 16 && Mouse.next() && Mouse.getEventButtonState() && Mouse.getEventButton() == 0)
			{
				mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
				++researchPage;
			}
		}
		
		drawHoveringText(tooltip, mouseX, mouseY);
		tooltip.clear();
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		super.keyTyped(typedChar, keyCode);
		if(mc.currentScreen != this)
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
		
		KeyBinding kb = WorldUtil.cast(IJeiHelper.instance().getKeybind_showRecipes(), KeyBinding.class);
		if(kb != null && kb.isActiveAndMatches(keyCode) && !IAdditionalPage.mouseOverStack.get().isEmpty())
			IJeiHelper.instance().showRecipes(IAdditionalPage.mouseOverStack.get());
		kb = WorldUtil.cast(IJeiHelper.instance().getKeybind_showUses(), KeyBinding.class);
		if(kb != null && kb.isActiveAndMatches(keyCode) && !IAdditionalPage.mouseOverStack.get().isEmpty())
			IJeiHelper.instance().showUses(IAdditionalPage.mouseOverStack.get());
		
		if(keyCode == 203 && prevPage())
			;
		else if(keyCode == 205 && nextPage())
			;
		else if(getCurrentPage() != null)
			getCurrentPage().keyTyped(typedChar, keyCode);
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		
		int dx = width / 2, dy = height / 2;
		dx -= 128 * 1.7;
		dy -= 72 * 1.7;
		
		int j = 0, x, y;
		ResearchBase rb;
		
		for(int i = researchPage * 42; i < researches.size(); ++i)
		{
			if(j >= 42)
				break;
			rb = researches.get(i);
			x = width / 2 + 8 + j % 6 * 26;
			y = dy + j / 6 * 26 + 22;
			
			if(mouseX >= x - 1 && mouseY >= y - 1 && mouseX <= x + 24 && mouseY <= y + 24)
			{
				mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
				setResearch(rb);
				page = 0;
			}
			
			++j;
		}
		
		if(pageDelta > 0)
			nextPage();
		else if(pageDelta < 0)
			prevPage();
		
		IAdditionalPage p = getCurrentPage();
		if(p != null)
			p.mouseClicked(mouseX - (width / 2 - 128 + texRX), mouseY - (height / 2 - 128 + texRY), mouseButton, page - compiledDescPages.length);
	}
	
	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick)
	{
		IAdditionalPage p = getCurrentPage();
		if(p != null)
			p.mouseDragged(mouseX - (width / 2 - 128 + texRX), mouseY - (height / 2 - 128 + texRY), clickedMouseButton, page - compiledDescPages.length);
	}
	
	public void setResearch(ResearchBase research)
	{
		this.research = research;
		complileDescription();
	}
	
	public IAdditionalPage getCurrentPage()
	{
		if(research == null || compiledDescPages == null || compiledDescPages.length == 0)
			return null;
		if(page < compiledDescPages.length)
			return null;
		else
			return research.getAdditionalPages()[page - compiledDescPages.length];
	}
	
	public void renderPage(float pt, int rx, int ry)
	{
		IAdditionalPage.mouse.setLocation(rx, ry);
		IAdditionalPage.mouseOverStack.set(ItemStack.EMPTY);
		
		if(page < compiledDescPages.length)
		{
			int y = page == 0 ? 32 : 0;
			
			for(String ln : font.listFormattedStringToWidth(compiledDescPages[page], texRW))
			{
				font.renderWithShadow(ln, 0, y, 0xFF330055);
				y += font.getHeight();
			}
		} else
		{
			int p = page - compiledDescPages.length;
			IAdditionalPage ap = research.getAdditionalPages()[p];
			ap.renderPage(pt, rx, ry, texRW, texRH, p);
			ap.addInformation(tooltip, rx, ry, p);
		}
	}
	
	public boolean nextPage()
	{
		if(research == null || compiledDescPages == null || compiledDescPages.length == 0)
			return false;
		
		if(page + 1 < compiledDescPages.length + research.getAdditionalPages().length)
		{
			IAdditionalPage prev = getCurrentPage();
			++page;
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
			IAdditionalPage cur = getCurrentPage();
			
			if(prev != null)
				prev.onDisappear();
			if(cur != null)
				cur.onAppear();
			
			return true;
		}
		
		return false;
	}
	
	public boolean prevPage()
	{
		if(research == null || compiledDescPages == null || compiledDescPages.length == 0)
			return false;
		
		if(page > 0)
		{
			IAdditionalPage prev = getCurrentPage();
			--page;
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
			IAdditionalPage cur = getCurrentPage();
			
			if(prev != null)
				prev.onDisappear();
			if(cur != null)
				cur.onAppear();
			
			return true;
		}
		
		return false;
	}
	
	public void drawTexturedModalRectScaled(int par1, int par2, int par3, int par4, int par5, int par6, float scale)
	{
		GL11.glPushMatrix();
		float var7 = 0.00390625f;
		float var8 = 0.00390625f;
		Tessellator var9 = Tessellator.getInstance();
		GL11.glTranslatef(par1 + par5 / 2.0f, par2 + par6 / 2.0f, 0.0f);
		GL11.glScalef(1.0f + scale, 1.0f + scale, 1.0f);
		BufferBuilder b = var9.getBuffer();
		b.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		b.pos((-par5) / 2.0f, par6 / 2.0f, this.zLevel).tex((par3 + 0) * var7, (par4 + par6) * var8).endVertex();
		b.pos(par5 / 2.0f, par6 / 2.0f, this.zLevel).tex((par3 + par5) * var7, (par4 + par6) * var8).endVertex();
		b.pos(par5 / 2.0f, (-par6) / 2.0f, this.zLevel).tex((par3 + par5) * var7, (par4 + 0) * var8).endVertex();
		b.pos((-par5) / 2.0f, (-par6) / 2.0f, this.zLevel).tex((par3 + 0) * var7, (par4 + 0) * var8).endVertex();
		var9.draw();
		GL11.glPopMatrix();
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	public void accept(IPlayerKnowledge knowledge)
	{
		this.knowledge = knowledge;
		this.dirty = true;
	}
}