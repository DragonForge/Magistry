package com.dragonforge.magistry.client.gui;

import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.lwjgl.input.Mouse;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.gui.IGuiKnowledgeAcceptor;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.api.research.ResearchTag;
import com.dragonforge.magistry.blocks.machines.tiles.TileResearchTable;
import com.dragonforge.magistry.client.MagistryFonts;
import com.dragonforge.magistry.init.SoundsM;
import com.dragonforge.magistry.inventory.ContainerResearch;
import com.dragonforge.magistry.net.PacketSelectResearch;
import com.dragonforge.magistry.net.PacketSelectTag;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.client.util.ITooltipFlag.TooltipFlags;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;

public class GuiResearch extends GuiContainer implements IGuiKnowledgeAcceptor
{
	public final TileResearchTable tile;
	final List<ResearchTag> toggleTags = new ArrayList<>();
	final List<ResearchTag> allTags = new ArrayList<>();
	final Set<ResearchTag> enabledTagsLastTick = new HashSet<>();
	final List<ResearchBase> completableResearches = new ArrayList<>();
	static final Rectangle rect = new Rectangle();
	int catScroll;
	
	boolean scrollBarHooked;
	
	public GuiResearch(EntityPlayer player, TileResearchTable tile)
	{
		super(new ContainerResearch(player, tile));
		this.tile = tile;
		xSize = 176;
		ySize = 181;
	}
	
	Collection<? extends ResearchTag> tagsToAdd;
	
	@Override
	public void initGui()
	{
		super.initGui();
		tagsToAdd = getKnownTags(MagistryAPI.getPlayerKnowledge(mc.player));
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		GlStateManager.color(1, 1, 1, 1);
		super.drawScreen(mouseX, mouseY, partialTicks);
		renderHoveredToolTip(mouseX, mouseY);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		int xi = 0;
		int yi = 0;
		
		GlStateManager.color(1, 1, 1, 1);
		if(completableResearches.isEmpty())
			for(int i = catScroll * 5; i < allTags.size(); ++i)
			{
				ResearchTag tag = allTags.get(i);
				rect.setBounds(guiLeft + 30 + 18 * xi, guiTop + 12 + 18 * yi, 16, 16);
				if(rect.contains(mouseX, mouseY))
					drawHoveringText((enabledTagsLastTick.contains(tag) ? TextFormatting.GREEN : TextFormatting.RED) + I18n.format("restag." + tag + ".name"), mouseX - guiLeft, mouseY - guiTop);
				++xi;
				if(xi >= 5)
				{
					xi = 0;
					++yi;
				}
				if(yi >= 4)
					break;
			}
		else
			for(int i = catScroll; i < completableResearches.size(); ++i)
			{
				ResearchBase rb = completableResearches.get(i);
				
				String txt = I18n.format("research." + rb.getRegistryName() + ".name");
				rect.setBounds(guiLeft + 29, guiTop + 11 + 18 * yi, 90, 18);
				if(rect.contains(mouseX, mouseY))
				{
					GlStateManager.pushMatrix();
					GlStateManager.translate(0, 0, 200);
					GlStateManager.enableBlend();
					GlStateManager.disableTexture2D();
					RenderUtil.drawColoredModalRect(rect.x - guiLeft, rect.y - guiTop, rect.width, rect.height, 0x66FFFFFF);
					GlStateManager.enableTexture2D();
					
					drawHoveringText(txt, mouseX - guiLeft, mouseY - guiTop);
					GlStateManager.popMatrix();
				}
				++yi;
				if(yi >= 4)
					break;
			}
		
		Slot mouse = getSlotUnderMouse();
		
		if(mouse != null && !mouse.getHasStack() && mouse.inventory == tile.inventory)
		{
			ITooltipFlag flag = mc.gameSettings.advancedItemTooltips ? TooltipFlags.ADVANCED : TooltipFlags.NORMAL;
			if(mouse.getSlotIndex() == 0)
				drawHoveringText(Items.PAPER.getDefaultInstance().getTooltip(mc.player, flag), mouseX - guiLeft, mouseY - guiTop);
			if(tile.discovery != null && !tile.leftovers.isEmpty())
				for(int i = 0; i < Math.min(8, tile.leftovers.size()); ++i)
					if(mouse.getSlotIndex() == 2 + i)
					{
						ItemStack stack = tile.leftovers.get(i);
						if(!stack.isEmpty())
							drawHoveringText(stack.getTooltip(mc.player, flag), mouseX - guiLeft, mouseY - guiTop);
					}
		}
	}
	
	float scrollProgress, lastScrollProgress;
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GlStateManager.enableBlend();
		
		UtilsFX.bindTexture(Magistry.MOD_ID, "textures/gui/gui_research.png");
		
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		
		float prog = tile.discoveryProgress / 200F;
		RenderUtil.drawTexturedModalRect(guiLeft + 29, guiTop + 86, 0, 181, 93 * prog, 6);
		
		c: if(tile.discovery != null && tile.totalItemCount != 0 && !tile.leftovers.isEmpty())
		{
			int leftovers = 0;
			try
			{
				for(ItemStack is : tile.leftovers)
					leftovers += is.getCount();
			} catch(Throwable err)
			{
				break c;
			}
			prog = (tile.totalItemCount - leftovers) / (float) tile.totalItemCount;
			RenderUtil.drawTexturedModalRect(guiLeft + 135, guiTop + 84, 0, 187, 34 * prog, 6);
		}
		
		if(completableResearches.isEmpty())
		{
			float max = allTags.size() / 5F;
			scrollProgress = catScroll / (float) Math.ceil(max - 4);
			
			int dw = Mouse.getDWheel();
			if(dw != 0)
			{
				if(dw > 0 && catScroll > 0)
					catScroll = Math.max(0, catScroll - dw / 120);
				if(dw < 0 && catScroll < max - 4)
					catScroll = Math.min((int) Math.ceil(max - 4), catScroll - dw / 120);
			}
		} else
		{
			float max = completableResearches.size();
			scrollProgress = catScroll / (float) Math.ceil(max - 4);
			
			int dw = Mouse.getDWheel();
			if(dw != 0)
			{
				if(dw > 0 && catScroll > 0)
					catScroll = Math.max(0, catScroll - dw / 120);
				if(dw < 0 && catScroll < max - 4)
					catScroll = Math.min((int) Math.ceil(max - 4), catScroll - dw / 120);
			}
		}
		
		RenderUtil.drawTexturedModalRect(guiLeft + 119, guiTop + 10 + 56 * (lastScrollProgress + (scrollProgress - lastScrollProgress) * mc.getRenderPartialTicks()), 176, 18, 9, 18);
		
		if(completableResearches.isEmpty())
			drawTexturedModalRect(guiLeft + 29, guiTop + 11, 93, 181, 90, 72);
		
		rect.setBounds(guiLeft + 120, guiTop + 3, 8, 8);
		GlStateManager.pushMatrix();
		GlStateManager.translate(guiLeft + 120, guiTop + 3, 0);
		GlStateManager.scale(.5F, .5F, .5F);
		if(completableResearches.isEmpty())
		{
			ColorHelper.glColor1i(tile.matchingIncomplete.isEmpty() ? 0x662420 : rect.contains(mouseX, mouseY) ? 0x2652B0 : 0x26196E);
			drawTexturedModalRect(0, 0, !tile.matchingIncomplete.isEmpty() ? 177 : 194, 1, 16, 16);
		}
		GlStateManager.popMatrix();
		
		int xi = 0;
		int yi = 0;
		
		GlStateManager.color(1, 1, 1, 1);
		if(completableResearches.isEmpty())
			for(int i = catScroll * 5; i < allTags.size(); ++i)
			{
				ResearchTag tag = allTags.get(i);
				UtilsFX.bindTexture(tag.getTexture());
				boolean cs = tile.isTagEnabled(tag);
				boolean ps = enabledTagsLastTick.contains(tag);
				float v = cs != ps ? .5F + (cs ? mc.getRenderPartialTicks() : 1 - mc.getRenderPartialTicks()) * .5F : !cs ? .5F : 1F;
				GlStateManager.color(v, v, v, 1F);
				RenderUtil.drawFullTexturedModalRect(guiLeft + 30 + 18 * xi, guiTop + 12 + 18 * yi, 16, 16);
				GlStateManager.color(1, 1, 1, 1);
				
				++xi;
				if(xi >= 5)
				{
					xi = 0;
					++yi;
				}
				if(yi >= 4)
					break;
			}
		else
			for(int i = catScroll; i < completableResearches.size(); ++i)
			{
				ResearchBase rb = completableResearches.get(i);
				
				GlStateManager.pushMatrix();
				GlStateManager.translate(guiLeft + 30, guiTop + 12 + yi * 18, 0);
				RenderHelper.enableGUIStandardItemLighting();
				rb.getPreview().renderPreview();
				RenderHelper.disableStandardItemLighting();
				
				String txt = I18n.format("research." + rb.getRegistryName() + ".name");
				
				int cuts = 0;
				while(MagistryFonts.PARCHMENT.getWidth(txt) >= 72)
				{
					txt = txt.substring(0, txt.length() - 3);
					++cuts;
				}
				
				if(cuts > 0)
					txt += "...";
				
				MagistryFonts.PARCHMENT.render(txt, 16, 2, 0xFFAAAAAA);
				
				GlStateManager.popMatrix();
				
				++yi;
				if(yi >= 4)
					break;
			}
		
		RenderHelper.enableGUIStandardItemLighting();
		GlStateManager.blendFunc(771, 772);
		if(tile.discovery != null && !tile.leftovers.isEmpty())
			for(int i = 0; i < Math.min(8, tile.leftovers.size()); ++i)
				if(tile.inventory.getStackInSlot(2 + i).isEmpty())
				{
					int x = guiLeft + 135 + 18 * (i % 2), y = guiTop + 11 + 18 * (i / 2);
					ItemStack stack = tile.leftovers.get(i);
					
					RenderItem ri = mc.getRenderItem();
					{
						IBakedModel bakedmodel = ri.getItemModelWithOverrides(stack, null, null);
						GlStateManager.pushMatrix();
						ri.textureManager.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
						ri.textureManager.getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).setBlurMipmap(false, false);
						GlStateManager.enableRescaleNormal();
						GlStateManager.enableAlpha();
						GlStateManager.alphaFunc(516, 0.1F);
						GlStateManager.enableBlend();
						GlStateManager.blendFunc(770, 772);
						GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
						ri.setupGuiTransform(x, y, bakedmodel.isGui3d());
						bakedmodel = net.minecraftforge.client.ForgeHooksClient.handleCameraTransforms(bakedmodel, ItemCameraTransforms.TransformType.GUI, false);
						ri.renderItem(stack, bakedmodel);
						GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
						GlStateManager.disableAlpha();
						GlStateManager.disableRescaleNormal();
						GlStateManager.disableLighting();
						GlStateManager.popMatrix();
						ri.textureManager.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
						ri.textureManager.getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).restoreLastBlurMipmap();
					}
					
					GlStateManager.pushMatrix();
					GlStateManager.translate(x, y + 14, 200);
					GlStateManager.scale(.75F, .75F, .75F);
					MagistryFonts.PARCHMENT.render(stack.getCount() + "", .5F, -MagistryFonts.PARCHMENT.getHeight() / 2 + .5F, 0xFF000000);
					MagistryFonts.PARCHMENT.render(stack.getCount() + "", -.5F, -MagistryFonts.PARCHMENT.getHeight() / 2 - .5F, 0xFF000000);
					MagistryFonts.PARCHMENT.render(stack.getCount() + "", .5F, -MagistryFonts.PARCHMENT.getHeight() / 2 - .5F, 0xFF000000);
					MagistryFonts.PARCHMENT.render(stack.getCount() + "", -.5F, -MagistryFonts.PARCHMENT.getHeight() / 2 + .5F, 0xFF000000);
					MagistryFonts.PARCHMENT.render(stack.getCount() + "", 0, -MagistryFonts.PARCHMENT.getHeight() / 2, 0xFFFFFFFF);
					GlStateManager.popMatrix();
				}
		GlStateManager.blendFunc(771, 771);
		RenderHelper.disableStandardItemLighting();
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		int xi = 0;
		int yi = 0;
		
		if(completableResearches.isEmpty())
			for(int i = catScroll * 5; i < allTags.size(); ++i)
			{
				ResearchTag tag = allTags.get(i);
				rect.setBounds(guiLeft + 30 + 18 * xi, guiTop + 12 + 18 * yi, 16, 16);
				if(rect.contains(mouseX, mouseY))
				{
					toggleTags.add(tag);
					mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
					return;
				}
				++xi;
				if(xi >= 5)
				{
					xi = 0;
					++yi;
				}
				if(yi >= 4)
					break;
			}
		else
			for(int i = catScroll; i < completableResearches.size(); ++i)
			{
				ResearchBase rb = completableResearches.get(i);
				
				rect.setBounds(guiLeft + 29, guiTop + 11 + 18 * yi, 90, 18);
				if(rect.contains(mouseX, mouseY))
				{
					mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
					HCNet.INSTANCE.sendToServer(new PacketSelectResearch(rb));
				}
				++yi;
				if(yi >= 4)
					break;
			}
		
		rect.setBounds(guiLeft + 120, guiTop + 3, 8, 8);
		if(completableResearches.isEmpty() && rect.contains(mouseX, mouseY))
		{
			completableResearches.addAll(tile.matchingIncomplete);
			completableResearches.sort((a, b) -> a.getRegistryName().compareTo(b.getRegistryName()));
			catScroll = 0;
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
			return;
		}
		
		if(new Rectangle(guiLeft + 119, guiTop + 10, 9, 74).contains(mouseX, mouseY))
			scrollBarHooked = true;
		
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick)
	{
		if(scrollBarHooked)
		{
			float y = Math.max(0, Math.min(mouseY - guiTop - 11, 72)) / 72F;
			
			if(completableResearches.isEmpty())
			{
				int max = (int) Math.ceil((allTags.size() - 1) / 5D);
				int tempt = Math.round(y * (max + 3));
				catScroll = Math.max(0, Math.min(max - 3, tempt));
			} else
			{
				int max = completableResearches.size();
				int tempt = Math.round(y * (max - 4));
				catScroll = Math.max(0, Math.min(max - 4, tempt));
			}
		} else
			super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
	}
	
	@Override
	protected void mouseReleased(int mouseX, int mouseY, int state)
	{
		scrollBarHooked = false;
		super.mouseReleased(mouseX, mouseY, state);
	}
	
	@Override
	public void updateScreen()
	{
		if(tagsToAdd != null)
		{
			allTags.clear();
			allTags.addAll(tagsToAdd);
			allTags.sort((a, b) -> a.compareTo(b));
			tagsToAdd = null;
		}
		
		lastScrollProgress = scrollProgress;
		enabledTagsLastTick.clear();
		enabledTagsLastTick.addAll(tile.enabledTags);
		for(int i = 0; i < toggleTags.size(); ++i)
			HCNet.INSTANCE.sendToServer(new PacketSelectTag(toggleTags.get(i)));
		toggleTags.clear();
	}
	
	@Override
	public void accept(IPlayerKnowledge knowledge)
	{
		tagsToAdd = getKnownTags(knowledge);
	}
	
	public static Collection<? extends ResearchTag> getKnownTags(IPlayerKnowledge know)
	{
		return ResearchTag.TAGS.values().stream().filter(t -> t.isSelectable(know)).collect(Collectors.toList());
	}
}