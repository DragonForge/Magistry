package com.dragonforge.magistry.client.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.api.research.client.IAdditionalPage;
import com.dragonforge.magistry.api.research.client.IPreviewIcon;
import com.dragonforge.magistry.client.MagistryFonts;
import com.dragonforge.magistry.init.SoundsM;
import com.zeitheron.hammercore.client.gui.GuiCentered;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.intr.jei.IJeiHelper;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class GuiLostNotes extends GuiCentered
{
	static final int texRX = 50, texRY = 17, texRW = 155, texRH = 228;
	static final ResourceLocation texture = new ResourceLocation(Magistry.MOD_ID, "textures/gui/lost_notes.png");
	
	int page;
	final ResearchBase research;
	String[] compiledDescPages;
	
	public GuiLostNotes(ResearchBase res)
	{
		this.research = res;
		Minecraft.getMinecraft().displayGuiScreen(this);
	}
	
	@Override
	public void initGui()
	{
		xSize = texRW;
		ySize = texRH;
		super.initGui();
		complileDescription();
	}
	
	public void complileDescription()
	{
		List<String> pages = new ArrayList<>();
		String cp = "";
		for(String word : research.getDescription())
		{
			String np = cp + (!cp.isEmpty() ? " " : "") + word;
			if(MagistryFonts.PARCHMENT.getWordWrappedHeight(np, texRW) > texRH - (pages.isEmpty() ? 32 : 0) - 6)
			{
				pages.add(cp);
				cp = word;
			} else
				cp = np;
		}
		if(!cp.isEmpty())
			pages.add(cp);
		compiledDescPages = pages.toArray(new String[pages.size()]);
	}
	
	List<String> tooltip = new ArrayList<>();
	
	int pageDelta = 0;
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		drawDefaultBackground();
		GlStateManager.enableBlend();
		GlStateManager.color(1, 1, 1, 1);
		
		int dx, dy;
		
		UtilsFX.bindTexture(texture);
		RenderUtil.drawTexturedModalRect(dx = width / 2 - 128, dy = height / 2 - 128, 0, 0, 256, 256);
		
		if(page == 0)
		{
			IPreviewIcon prev = research.getPreview();
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(width / 2, height / 2 - 128 + texRY - 6, 0);
			GlStateManager.scale(2, 2, 2);
			String txt = I18n.format("research." + research.getRegistryName() + ".name");
			int w = MagistryFonts.PARCHMENT.getWidth(txt);
			if(w * 2 >= texRW)
			{
				float sr = texRW / (float) w / 2;
				GlStateManager.translate(-w / 2 * sr, 10 + 1F / sr, 0);
				GlStateManager.scale(sr, sr, sr);
			} else
				GlStateManager.translate(-w / 2, 10, 0);
			MagistryFonts.PARCHMENT.renderWithShadow(txt, 0, 0, 0xFFFFFFFF);
			GlStateManager.popMatrix();
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(width / 2, height / 2 - 128 + texRY - 6, 0);
			GlStateManager.scale(1.5, 1.5, 1.5);
			GlStateManager.translate(-8, 0, 0);
			RenderHelper.enableGUIStandardItemLighting();
			prev.renderPreview();
			GlStateManager.popMatrix();
		}
		
		int dw = Mouse.getDWheel();
		
		if(dw < 0)
			nextPage();
		else if(dw > 0)
			prevPage();
		
		GlStateManager.color(1, 1, 1, 1);
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(width / 2 - 128 + texRX, height / 2 - 128 + texRY, 50);
		renderPage(partialTicks, mouseX - (width / 2 - 128 + texRX), mouseY - (height / 2 - 128 + texRY));
		GlStateManager.popMatrix();
		
		GlStateManager.enableTexture2D();
		GlStateManager.enableBlend();
		GlStateManager.color(1, 1, 1, 1);
		
		UtilsFX.bindTexture("textures/gui/gui_manual_page.png");
		
		float bob = MathHelper.sin((mc.player.ticksExisted / 3F)) * .2F + .1F;
		
		RenderHelper.disableStandardItemLighting();
		GlStateManager.disableLighting();
		GlStateManager.color(.25F, .25F, .25F, 1);
		
		pageDelta = 0;
		
		if(page > 0)
		{
			drawTexturedModalRectScaled(dx + texRX + 6, dy + texRY + texRH - 2, 0, 184, 12, 8, bob);
			if(mouseX >= dx + texRX + 4 && mouseY >= dy + texRY + texRH - 4 && mouseX < dx + texRX + 20 && mouseY < dy + texRY + texRH + 10)
				pageDelta = -1;
		}
		
		if(page + 1 < compiledDescPages.length + research.getAdditionalPages().length)
		{
			drawTexturedModalRectScaled(dx + texRX + texRW - 20, dy + texRY + texRH - 2, 12, 184, 12, 8, bob);
			if(mouseX >= dx + texRX + texRW - 22 && mouseY >= dy + texRY + texRH - 4 && mouseX < dx + texRX + texRW - 22 + 14 && mouseY < dy + texRY + texRH + 10)
				pageDelta = 1;
		}
		
		drawHoveringText(tooltip, mouseX, mouseY);
	}
	
	public void drawTexturedModalRectScaled(int par1, int par2, int par3, int par4, int par5, int par6, float scale)
	{
		GL11.glPushMatrix();
		float var7 = 0.00390625f;
		float var8 = 0.00390625f;
		Tessellator var9 = Tessellator.getInstance();
		GL11.glTranslatef(par1 + par5 / 2.0f, par2 + par6 / 2.0f, 0.0f);
		GL11.glScalef(1.0f + scale, 1.0f + scale, 1.0f);
		BufferBuilder b = var9.getBuffer();
		b.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		b.pos((-par5) / 2.0f, par6 / 2.0f, this.zLevel).tex((par3 + 0) * var7, (par4 + par6) * var8).endVertex();
		b.pos(par5 / 2.0f, par6 / 2.0f, this.zLevel).tex((par3 + par5) * var7, (par4 + par6) * var8).endVertex();
		b.pos(par5 / 2.0f, (-par6) / 2.0f, this.zLevel).tex((par3 + par5) * var7, (par4 + 0) * var8).endVertex();
		b.pos((-par5) / 2.0f, (-par6) / 2.0f, this.zLevel).tex((par3 + 0) * var7, (par4 + 0) * var8).endVertex();
		var9.draw();
		GL11.glPopMatrix();
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		super.keyTyped(typedChar, keyCode);
		if(mc.currentScreen != this)
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
		
		KeyBinding kb = WorldUtil.cast(IJeiHelper.instance().getKeybind_showRecipes(), KeyBinding.class);
		if(kb != null && kb.isActiveAndMatches(keyCode) && !IAdditionalPage.mouseOverStack.get().isEmpty())
			IJeiHelper.instance().showRecipes(IAdditionalPage.mouseOverStack.get());
		kb = WorldUtil.cast(IJeiHelper.instance().getKeybind_showUses(), KeyBinding.class);
		if(kb != null && kb.isActiveAndMatches(keyCode) && !IAdditionalPage.mouseOverStack.get().isEmpty())
			IJeiHelper.instance().showUses(IAdditionalPage.mouseOverStack.get());
		
		if(keyCode == 203 && prevPage())
			;
		else if(keyCode == 205 && nextPage())
			;
		else if(getCurrentPage() != null)
			getCurrentPage().keyTyped(typedChar, keyCode);
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		
		if(pageDelta > 0)
			nextPage();
		else if(pageDelta < 0)
			prevPage();
		
		IAdditionalPage p = getCurrentPage();
		if(p != null)
			p.mouseClicked(mouseX - (width / 2 - 128 + texRX), mouseY - (height / 2 - 128 + texRY), mouseButton, page - compiledDescPages.length);
	}
	
	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick)
	{
		IAdditionalPage p = getCurrentPage();
		if(p != null)
			p.mouseDragged(mouseX - (width / 2 - 128 + texRX), mouseY - (height / 2 - 128 + texRY), clickedMouseButton, page - compiledDescPages.length);
	}
	
	public IAdditionalPage getCurrentPage()
	{
		if(page < compiledDescPages.length)
			return null;
		else
			return research.getAdditionalPages()[page - compiledDescPages.length];
	}
	
	public void renderPage(float pt, int rx, int ry)
	{
		tooltip.clear();
		IAdditionalPage.mouse.setLocation(rx, ry);
		IAdditionalPage.mouseOverStack.set(ItemStack.EMPTY);
		
		if(page < compiledDescPages.length)
		{
			int y = page == 0 ? 32 : 0;
			
			for(String ln : MagistryFonts.PARCHMENT.listFormattedStringToWidth(compiledDescPages[page], texRW))
			{
				MagistryFonts.PARCHMENT.renderWithShadow(ln, 0, y, 0xFF330055);
				y += MagistryFonts.PARCHMENT.getHeight();
			}
		} else
		{
			int p = page - compiledDescPages.length;
			IAdditionalPage ap = research.getAdditionalPages()[p];
			ap.renderPage(pt, rx, ry, texRW, texRH, p);
			ap.addInformation(tooltip, rx, ry, p);
		}
	}
	
	public boolean nextPage()
	{
		if(page + 1 < compiledDescPages.length + research.getAdditionalPages().length)
		{
			IAdditionalPage prev = getCurrentPage();
			++page;
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
			IAdditionalPage cur = getCurrentPage();
			
			if(prev != null)
				prev.onDisappear();
			if(cur != null)
				cur.onAppear();
			
			return true;
		}
		
		return false;
	}
	
	public boolean prevPage()
	{
		if(page > 0)
		{
			IAdditionalPage prev = getCurrentPage();
			--page;
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsM.PAGE_TURNS, 1F));
			IAdditionalPage cur = getCurrentPage();
			
			if(prev != null)
				prev.onDisappear();
			if(cur != null)
				cur.onAppear();
			
			return true;
		}
		
		return false;
	}
}