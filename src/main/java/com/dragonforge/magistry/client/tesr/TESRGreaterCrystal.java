package com.dragonforge.magistry.client.tesr;

import com.dragonforge.magistry.blocks.tiles.TileGreaterCrystal;
import com.dragonforge.magistry.client.model.ModelGreaterCrystal;
import com.dragonforge.magistry.init.BlocksM;
import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.internal.blocks.base.IBlockOrientable;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.GlStateManager.DestFactor;
import net.minecraft.client.renderer.GlStateManager.SourceFactor;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

@AtTESR(TileGreaterCrystal.class)
public class TESRGreaterCrystal extends TESR<TileGreaterCrystal>
{
	public static TESRGreaterCrystal INSTANCE;
	public final ModelGreaterCrystal model = new ModelGreaterCrystal();
	{
		INSTANCE = this;
	}
	
	@Override
	public void renderTileEntityAt(TileGreaterCrystal te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		GlStateManager.pushMatrix();
		IBlockState s = te == null ? Blocks.AIR.getDefaultState() : te.getWorld().getBlockState(te.getPos());
		if(s.getBlock() != BlocksM.GREATER_CRYSTAL)
			return;
		translateFromOrientation(x, y, z, s.getValue(IBlockOrientable.FACING));
		
		model.bindTexture(te);
		for(int i = 0; i < (destroyStage != null ? 2 : 1); ++i)
		{
			if(i == 1)
			{
				GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.DST_COLOR);
				GlStateManager.enableBlend();
				GlStateManager.color(1, 1, 1, .3F);
				UtilsFX.bindTexture(destroyStage);
			}
			GlStateManager.translate(0, 5 / 16F, 0);
			model.render(null, partialTicks, 0, 0, 0, 0, 1 / 16F);
			if(i == 1)
			{
				GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.SRC_ALPHA);
				GlStateManager.disableBlend();
			}
		}
		
		GlStateManager.popMatrix();
	}
	
	@Override
	public void renderItem(ItemStack item)
	{
		GlStateManager.pushMatrix();
		translateFromOrientation(0, 0, 0, EnumFacing.DOWN);
		
		model.bindTexture(null);
		GlStateManager.translate(0, 7 / 16F, 0);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		
		GlStateManager.popMatrix();
	}
}