package com.dragonforge.magistry.client.tesr;

import java.util.Random;

import com.dragonforge.magistry.blocks.machines.tiles.TileInductor;
import com.dragonforge.magistry.client.model.ModelInductor;
import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

@AtTESR(TileInductor.class)
public class TESRInductor extends TESR<TileInductor>
{
	public static TESRInductor INSTANCE;
	{
		INSTANCE = this;
	}
	
	public final ModelInductor model = new ModelInductor();
	
	@Override
	public void renderTileEntityAt(TileInductor te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		Tessellator t = Tessellator.getInstance();
		BufferBuilder bb = t.getBuffer();
		BlockRendererDispatcher brd = mc.getBlockRendererDispatcher();
		
		if(!te.output.getStackInSlot(0).isEmpty())
		{
			Random r = te.getRNG();
			r.setSeed((te.getPos().toString()).hashCode());
			float tick = (((r.nextLong() + te.output.getStackInSlot(0).hashCode() + te.getWorld().getTotalWorldTime()) % 360L) + partialTicks) % 360L;
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(x + .5, y + 15 / 16D + (Math.sin(Math.toRadians(tick * 4))) / 24D, z + .5);
			GlStateManager.rotate(tick, 0, 1, 0);
			Minecraft.getMinecraft().getRenderItem().renderItem(te.output.getStackInSlot(0), TransformType.GROUND);
			GlStateManager.popMatrix();
		}
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(x + .5, y + .5, z + .5);
		GlStateManager.rotate(180, 1, 0, 0);
		model.bindTexture(te);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		GlStateManager.popMatrix();
	}
	
	@Override
	public void renderItem(ItemStack item)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(.5, .95, .5);
		GlStateManager.rotate(180, 1, 0, 0);
		GlStateManager.scale(.75F, .75F, .75F);
		model.bindTexture(null);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		GlStateManager.popMatrix();
	}
}