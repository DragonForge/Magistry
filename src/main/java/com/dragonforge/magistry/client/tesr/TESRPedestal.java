package com.dragonforge.magistry.client.tesr;

import java.util.Random;

import com.dragonforge.magistry.blocks.tiles.TilePedestal;
import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.util.ResourceLocation;

@AtTESR(TilePedestal.class)
public class TESRPedestal extends TESR<TilePedestal>
{
	@Override
	public void renderTileEntityAt(TilePedestal te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		if(te.getItem().isEmpty())
			return;
		
		Random r = te.getRNG();
		r.setSeed((te.getPos().toString()).hashCode());
		float tick = (((r.nextLong() + te.getItem().hashCode() + te.getWorld().getTotalWorldTime()) % 360L) + partialTicks) % 360L;
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(x + .5, y + 14 / 16D + (Math.sin(Math.toRadians(tick * 4))) / 24D, z + .5);
		GlStateManager.rotate(tick, 0, 1, 0);
		
		Minecraft.getMinecraft().getRenderItem().renderItem(te.getItem(), TransformType.GROUND);
		GlStateManager.popMatrix();
	}
}