package com.dragonforge.magistry.client.tesr;

import com.dragonforge.magistry.blocks.machines.tiles.TileEtherCollector;
import com.dragonforge.magistry.client.model.ModelCrusher;
import com.dragonforge.magistry.client.model.ModelEtherCollector;
import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

@AtTESR(TileEtherCollector.class)
public class TESREtherCollector extends TESR<TileEtherCollector>
{
	public static TESREtherCollector INSTANCE;
	{
		INSTANCE = this;
	}
	
	public final ModelEtherCollector model = new ModelEtherCollector();
	
	@Override
	public void renderTileEntityAt(TileEtherCollector te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(x + .5, y + 1.5 - 1 / 16D, z + .5);
		GlStateManager.rotate(180, 1, 0, 0);
		
		model.bindTexture(te);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		
		GlStateManager.pushMatrix();
		GlStateManager.rotate(te.rotator.getActualRotation(partialTicks), 0, 1, 0);
		model.shape10.render(1 / 16F);
		GlStateManager.popMatrix();

		GlStateManager.pushMatrix();
		GlStateManager.rotate(-te.rotator.getActualRotation(partialTicks), 0, 1, 0);
		model.shape10_1.render(1 / 16F);
		GlStateManager.popMatrix();
		
		GlStateManager.popMatrix();
	}
	
	@Override
	public void renderItem(ItemStack item)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(.5, .35, .5);
		GlStateManager.rotate(90, 0, 1, 0);
		GlStateManager.rotate(180, 1, 0, 0);
		GlStateManager.scale(.35F, .35F, .35F);
		model.bindTexture(null);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		
		float rot = System.currentTimeMillis() % 3600L / 10F;
		
		GlStateManager.pushMatrix();
		GlStateManager.rotate(rot, 0, 1, 0);
		model.shape10.render(1 / 16F);
		GlStateManager.popMatrix();

		GlStateManager.pushMatrix();
		GlStateManager.rotate(-rot, 0, 1, 0);
		model.shape10_1.render(1 / 16F);
		GlStateManager.popMatrix();
		
		GlStateManager.popMatrix();
	}
}