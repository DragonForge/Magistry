package com.dragonforge.magistry.client.tesr;

import com.dragonforge.magistry.blocks.tiles.TileOreCrystal;
import com.dragonforge.magistry.client.model.ModelNaturalCrystal;
import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.internal.blocks.base.IBlockOrientable;
import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.GlStateManager.DestFactor;
import net.minecraft.client.renderer.GlStateManager.SourceFactor;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.entity.minecart.MinecartCollisionEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

@AtTESR(TileOreCrystal.class)
public class TESROreCrystal extends TESR<TileOreCrystal>
{
	public static TESROreCrystal INSTANCE;
	public final ModelNaturalCrystal model = new ModelNaturalCrystal();
	
	{
		INSTANCE = this;
	}
	
	@Override
	public void renderTileEntityAt(TileOreCrystal te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		EnumFacing face = te.getWorld().getBlockState(te.getPos()).getValue(IBlockOrientable.FACING);
		
		GlStateManager.pushMatrix();
		translateFromOrientation(x, y, z, face);
		
		GlStateManager.translate(0, -.5, 0);
		{
			IBakedModel ibm = Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes().getModelForState(te.growing);
			ResourceLocation loc = new ResourceLocation(ibm.getParticleTexture().getIconName());
			
			UtilsFX.bindTexture(loc.getNamespace(), "textures/" + loc.getPath() + ".png");
		}
		
		for(int i = 0; i < (destroyStage != null ? 2 : 1); ++i)
		{
			GlStateManager.color(1, 1, 1, 1F);
			
			if(i == 1)
			{
				GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.DST_COLOR);
				GlStateManager.enableBlend();
				GlStateManager.color(1, 1, 1, .3F);
				UtilsFX.bindTexture(destroyStage);
			}
			
			float s = 1 / 16F;
			model.shape1.render(s);
			model.shape2.render(s);
			
			GlStateManager.pushMatrix();
			GlStateManager.rotate((te.getPos().toString().hashCode() % 360 + Block.getStateId(te.growing) % 360 + face.ordinal() * 45) % 360, 0, 1, 0);
			if(i == 0)
			{
				GlStateManager.enableBlend();
				GlStateManager.blendFunc(770, 774);
			}
			model.shape3.render(s);
			model.shape4.render(s);
			model.shape5.render(s);
			model.shape6.render(s);
			model.shape7.render(s);
			if(i == 0)
				GlStateManager.blendFunc(770, 771);
			GlStateManager.popMatrix();
			
			if(i == 1)
			{
				GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.SRC_ALPHA);
				GlStateManager.disableBlend();
			}
		}
		
		GlStateManager.disableBlend();
		GlStateManager.color(1, 1, 1);
		
		GlStateManager.popMatrix();
	}
	
	@Override
	public void renderItem(ItemStack item)
	{
		NBTTagCompound tag = item.getTagCompound();
		
		GlStateManager.pushMatrix();
		translateFromOrientation(0, 0, 0, EnumFacing.DOWN);
		
		GlStateManager.translate(0, -.35, 0);
		
		IBlockState growing = Blocks.COBBLESTONE.getDefaultState();
		
		if(tag != null && tag.hasKey("Block"))
		{
			Block b = ForgeRegistries.BLOCKS.getValue(new ResourceLocation(tag.getString("Block")));
			if(b != null)
				growing = b.getStateFromMeta(tag.getInteger("Meta"));
		}
		
		IBakedModel ibm = Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes().getModelForState(growing);
		ResourceLocation loc = new ResourceLocation(ibm.getParticleTexture().getIconName());
		UtilsFX.bindTexture(loc.getNamespace(), "textures/" + loc.getPath() + ".png");
		
		float s = 1 / 16F;
		model.shape1.render(s);
		model.shape2.render(s);
		
		GlStateManager.color(1, 1, 1);
		
		GlStateManager.blendFunc(770, 774);
		model.shape3.render(s);
		model.shape4.render(s);
		model.shape5.render(s);
		model.shape6.render(s);
		model.shape7.render(s);
		GlStateManager.blendFunc(770, 771);
		
		GlStateManager.popMatrix();
	}
}