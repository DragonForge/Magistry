package com.dragonforge.magistry.client.tesr;

import com.dragonforge.magistry.blocks.machines.tiles.TileResearchTable;
import com.dragonforge.magistry.client.model.ModelResearchTable;
import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

@AtTESR(TileResearchTable.class)
public class TESRResearchTable extends TESR<TileResearchTable>
{
	public static TESRResearchTable INSTANCE;
	{
		INSTANCE = this;
	}
	
	public final ModelResearchTable model = new ModelResearchTable();
	
	@Override
	public void renderTileEntityAt(TileResearchTable te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(x + 1, y + 1.5F, z + 1);
		GlStateManager.rotate(90, 0, 1, 0);
		GlStateManager.rotate(180, 1, 0, 0);
		model.bindTexture(te);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		GlStateManager.popMatrix();
	}
	
	@Override
	public void renderItem(ItemStack item)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(.5, .8, .5);
		GlStateManager.rotate(90, 0, 1, 0);
		GlStateManager.rotate(180, 1, 0, 0);
		GlStateManager.scale(.5F, .5F, .5F);
		model.bindTexture(null);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		GlStateManager.popMatrix();
	}
}
