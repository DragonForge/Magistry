package com.dragonforge.magistry.client.tesr;

import com.dragonforge.magistry.blocks.machines.tiles.TileCrusher;
import com.dragonforge.magistry.client.model.ModelCrusher;
import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;

import net.minecraft.client.model.ModelRabbit;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

@AtTESR(TileCrusher.class)
public class TESRCrusher extends TESR<TileCrusher>
{
	public static TESRCrusher INSTANCE;
	{
		INSTANCE = this;
	}
	
	public final ModelCrusher model = new ModelCrusher();
	
	@Override
	public void renderTileEntityAt(TileCrusher te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(x + 0, y + .5 - 1 / 16D, z + .5);
		
		if(te.getFacing() == EnumFacing.SOUTH)
			GlStateManager.translate(1, 0, 1);
		if(te.getFacing() == EnumFacing.NORTH)
		{
			GlStateManager.rotate(180, 0, 1, 0);
			GlStateManager.translate(0, 0, 1);
		}
		if(te.getFacing() == EnumFacing.EAST)
		{
			GlStateManager.rotate(90, 0, 1, 0);
			GlStateManager.translate(.5, 0, 1.5);
		}
		if(te.getFacing() == EnumFacing.WEST)
		{
			GlStateManager.rotate(-90, 0, 1, 0);
			GlStateManager.translate(.5, 0, .5);
		}
		
		GlStateManager.rotate(180, 1, 0, 0);
		
		model.bindTexture(te);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		GlStateManager.translate(0, -5 / 16F, 0);
		GlStateManager.scale(1, .5, 1);
		model.shape17_1.render(1 / 16F);
		model.shape17_2.render(1 / 16F);
		
		GlStateManager.popMatrix();
	}
	
	@Override
	public void renderItem(ItemStack item)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(.5, .8, .5);
		GlStateManager.rotate(90, 0, 1, 0);
		GlStateManager.rotate(180, 1, 0, 0);
		GlStateManager.scale(.5F, .5F, .5F);
		model.bindTexture(null);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		GlStateManager.popMatrix();
	}
}