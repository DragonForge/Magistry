package com.dragonforge.magistry.client.tesr;

import com.dragonforge.magistry.blocks.machines.tiles.TileGenerator;
import com.dragonforge.magistry.client.model.ModelGenerator;
import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

@AtTESR(TileGenerator.class)
public class TESRGenerator extends TESR<TileGenerator>
{
	public static TESRGenerator INSTANCE;
	{
		INSTANCE = this;
	}
	
	public final ModelGenerator model = new ModelGenerator();
	
	@Override
	public void renderTileEntityAt(TileGenerator te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(x + .5, y + 1.5, z + .5);
		GlStateManager.rotate(180, 1, 0, 0);
		model.bindTexture(null);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		GlStateManager.popMatrix();
	}
	
	@Override
	public void renderItem(ItemStack item)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(.5, .4, .5);
		GlStateManager.scale(1 / 3.3F, 1 / 3.3F, 1 / 3.3F);
		GlStateManager.rotate(180, 1, 0, 0);
		model.bindTexture(null);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		GlStateManager.popMatrix();
	}
}