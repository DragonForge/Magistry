package com.dragonforge.magistry.client.tesr;

import com.dragonforge.magistry.blocks.tiles.TileNaturalCrystal;
import com.dragonforge.magistry.client.model.ModelNaturalCrystal;
import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.internal.blocks.base.IBlockOrientable;
import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.GlStateManager.DestFactor;
import net.minecraft.client.renderer.GlStateManager.SourceFactor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

@AtTESR(TileNaturalCrystal.class)
public class TESRNaturalCrystal extends TESR<TileNaturalCrystal>
{
	public static TESRNaturalCrystal INSTANCE;
	public final ModelNaturalCrystal model = new ModelNaturalCrystal();
	
	{
		INSTANCE = this;
	}
	
	@Override
	public void renderTileEntityAt(TileNaturalCrystal te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		EnumFacing face = te == null ? EnumFacing.DOWN : te.getWorld().getBlockState(te.getPos()).getValue(IBlockOrientable.FACING);
		
		GlStateManager.pushMatrix();
		translateFromOrientation(x, y, z, face);
		
		GlStateManager.translate(0, -.5, 0);
		model.bindTexture(te);
		
		for(int i = 0; i < (destroyStage != null ? 2 : 1); ++i)
		{
			if(i == 1)
			{
				GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.DST_COLOR);
				GlStateManager.enableBlend();
				GlStateManager.color(1, 1, 1, .3F);
				UtilsFX.bindTexture(destroyStage);
			}
			
			float s = 1 / 16F;
			model.shape1.render(s);
			model.shape2.render(s);
			
			if(i == 0)
				ColorHelper.glColor1ia(255 << 24 | te.getRenderColor());
			
			GlStateManager.pushMatrix();
			GlStateManager.rotate((te.getPos().toString().hashCode() % 360 + te.getColor() % 360 + te.saturation * 360F + face.ordinal() * (te.saturation * 180)) % 360, 0, 1, 0);
			if(i == 0)
			{
				GlStateManager.enableBlend();
				GlStateManager.blendFunc(770, 774);
			}
			model.shape3.render(s);
			model.shape4.render(s);
			model.shape5.render(s);
			model.shape6.render(s);
			model.shape7.render(s);
			if(i == 0)
				GlStateManager.blendFunc(770, 771);
			GlStateManager.popMatrix();
			
			if(i == 1)
			{
				GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.SRC_ALPHA);
				GlStateManager.disableBlend();
			}
		}
		
		GlStateManager.disableBlend();
		GlStateManager.color(1, 1, 1);
		
		GlStateManager.popMatrix();
	}
	
	@Override
	public void renderItem(ItemStack item)
	{
		NBTTagCompound tag = item.getTagCompound();
		
		GlStateManager.pushMatrix();
		translateFromOrientation(0, 0, 0, EnumFacing.DOWN);
		
		int rgb = item.hashCode();
		
		if(tag != null)
		{
			float charge = tag.getFloat("Charge");
			float maxCharge = tag.getFloat("MaxCharge");
			int color = tag.getInteger("Color");
			
			rgb = ColorHelper.interpolate(ColorHelper.getBrightnessRGB(color), color, charge / maxCharge);
		}
		
		GlStateManager.translate(0, -.35, 0);
		model.bindTexture(null);
		
		float s = 1 / 16F;
		model.shape1.render(s);
		model.shape2.render(s);
		
		ColorHelper.glColor1ia(255 << 24 | rgb);
		
		model.shape3.render(s);
		model.shape4.render(s);
		model.shape5.render(s);
		model.shape6.render(s);
		model.shape7.render(s);
		
		GlStateManager.color(1, 1, 1);
		GlStateManager.popMatrix();
	}
}