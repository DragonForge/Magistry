package com.dragonforge.magistry.client.ttf2gl;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class TTFAtlas
{
	public final Font font;
	public final Map<Character, Rectangle> uvMap = new HashMap<>();
	public BufferedImage atlas;
	
	public boolean dirty = false;
	
	public Consumer<BufferedImage> textureMapUploader = i ->
	{
	};
	
	public Runnable bindTexture = () ->
	{
	};
	
	public TTFAtlas(Font font)
	{
		this.font = font;
	}
	
	public void render(String str, float x, float y, int rgba)
	{
		render(str, x, y, ColorHelper.getRed(rgba), ColorHelper.getGreen(rgba), ColorHelper.getBlue(rgba), ColorHelper.getAlpha(rgba));
	}
	
	public void renderWithShadow(String str, float x, float y, int rgba)
	{
		renderWithShadow(str, x, y, ColorHelper.getRed(rgba), ColorHelper.getGreen(rgba), ColorHelper.getBlue(rgba), ColorHelper.getAlpha(rgba));
	}
	
	public void renderWithShadow(String str, float x, float y, float red, float green, float blue, float alpha)
	{
		render(str, x + .5F, y + .5F, red / 3, green / 3, blue / 3, alpha);
		render(str, x, y, red, green, blue, alpha);
	}
	
	public void render(String str, float x, float y, float red, float green, float blue, float alpha)
	{
		Tessellator tess = Tessellator.getInstance();
		GlStateManager.color(red, green, blue, alpha);
		tess.getBuffer().begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		for(char c : str.toCharArray())
			x += render(x, y, c, tess.getBuffer(), red, green, blue, alpha);
		if(dirty)
			uploadChanges();
		tess.draw();
	}
	
	public int render(float x, float y, char c, BufferBuilder bb, float r, float g, float b, float a)
	{
		bindTexture.run();
		Rectangle rect = getUV(c);
		
		float u1 = (float) (rect.x) / atlas.getWidth();
		float v1 = (float) (rect.y) / atlas.getHeight();
		float u2 = (float) (rect.x + rect.width) / atlas.getWidth();
		float v2 = (float) (rect.y + rect.height) / atlas.getHeight();

		bb.pos(x, y + rect.height, 0).tex(u1, v2).endVertex();
		bb.pos(x + rect.width, y + rect.height, 0).tex(u2, v2).endVertex();
		bb.pos(x + rect.width, y, 0).tex(u2, v1).endVertex();
		bb.pos(x, y, 0).tex(u1, v1).endVertex();
		
		return rect.width;
	}
	
	public void init(String cs)
	{
		init(cs.toCharArray());
	}
	
	public void init(char... cs)
	{
		for(char c : cs)
			if(!uvMap.containsKey(c))
				uvMap.put(c, append(TTFLib.create(font, c)));
		if(dirty)
			uploadChanges();
	}
	
	public int getWidth(String line)
	{
		int len = 0;
		for(char c : line.toCharArray())
			len += getWidth(c);
		if(dirty)
			uploadChanges();
		return len;
	}
	
	public int getWidth(char c)
	{
		return getUV(c).width;
	}
	
	public int getHeight()
	{
		return atlas == null ? 0 : atlas.getHeight();
	}
	
	public int getWordWrappedHeight(String str, int maxLength)
	{
		return getHeight() * this.listFormattedStringToWidth(str, maxLength).size();
	}
	
	public List<String> listFormattedStringToWidth(String str, int wrapWidth)
	{
		return Arrays.<String> asList(this.wrapFormattedStringToWidth(str, wrapWidth).split("\n"));
	}
	
	String wrapFormattedStringToWidth(String str, int wrapWidth)
	{
		int i = this.sizeStringToWidth(str, wrapWidth);
		
		if(str.length() <= i)
		{
			return str;
		} else
		{
			String s = str.substring(0, i);
			char c0 = str.charAt(i);
			boolean flag = c0 == ' ' || c0 == '\n';
			String s1 = getFormatFromString(s) + str.substring(i + (flag ? 1 : 0));
			return s + "\n" + this.wrapFormattedStringToWidth(s1, wrapWidth);
		}
	}
	
	private int sizeStringToWidth(String str, int wrapWidth)
	{
		int i = str.length();
		int j = 0;
		int k = 0;
		int l = -1;
		
		for(boolean flag = false; k < i; ++k)
		{
			char c0 = str.charAt(k);
			
			switch(c0)
			{
			case '\n':
				--k;
			break;
			case ' ':
				l = k;
			default:
				j += this.getWidth(c0);
				
				if(flag)
				{
					++j;
				}
			break;
			}
			
			if(c0 == '\n')
			{
				++k;
				l = k;
				break;
			}
			
			if(j > wrapWidth)
			{
				break;
			}
		}
		
		return k != i && l != -1 && l < k ? l : k;
	}
	
	public Rectangle getUV(char c)
	{
		if(uvMap.containsKey(c))
			return uvMap.get(c);
		else
		{
			Rectangle r;
			uvMap.put(c, r = append(TTFLib.create(font, c)));
			return r;
		}
	}
	
	private Rectangle append(BufferedImage sub)
	{
		dirty = true;
		
		if(atlas == null)
		{
			atlas = new BufferedImage(sub.getWidth(), sub.getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = atlas.createGraphics();
			g.drawImage(sub, 0, 0, null);
			g.dispose();
			return new Rectangle(0, 0, sub.getWidth(), sub.getHeight());
		} else
		{
			BufferedImage newAtlas = new BufferedImage(atlas.getWidth() + sub.getWidth(), Math.max(sub.getHeight(), atlas.getHeight()), BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = newAtlas.createGraphics();
			int x;
			g.drawImage(atlas, 0, 0, null);
			g.drawImage(sub, x = atlas.getWidth(), 0, null);
			g.dispose();
			atlas = newAtlas;
			return new Rectangle(x, 0, sub.getWidth(), sub.getHeight());
		}
	}
	
	public void uploadChanges()
	{
		if(dirty && textureMapUploader != null)
			textureMapUploader.accept(atlas);
		dirty = false;
	}
	
	/**
	 * Checks if the char code is a hexadecimal character, used to set colour.
	 */
	private static boolean isFormatColor(char colorChar)
	{
		return colorChar >= '0' && colorChar <= '9' || colorChar >= 'a' && colorChar <= 'f' || colorChar >= 'A' && colorChar <= 'F';
	}
	
	/**
	 * Checks if the char code is O-K...lLrRk-o... used to set special
	 * formatting.
	 */
	private static boolean isFormatSpecial(char formatChar)
	{
		return formatChar >= 'k' && formatChar <= 'o' || formatChar >= 'K' && formatChar <= 'O' || formatChar == 'r' || formatChar == 'R';
	}
	
	/**
	 * Digests a string for nonprinting formatting characters then returns a
	 * string containing only that formatting.
	 */
	public static String getFormatFromString(String text)
	{
		String s = "";
		int i = -1;
		int j = text.length();
		
		while((i = text.indexOf(167, i + 1)) != -1)
		{
			if(i < j - 1)
			{
				char c0 = text.charAt(i + 1);
				
				if(isFormatColor(c0))
				{
					s = "\u00a7" + c0;
				} else if(isFormatSpecial(c0))
				{
					s = s + "\u00a7" + c0;
				}
			}
		}
		
		return s;
	}
}