package com.dragonforge.magistry.client.ttf2gl;

import java.awt.Rectangle;
import java.util.Arrays;
import java.util.List;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class ScaledTTFAtlas
{
	public final TTFAtlas origin;
	public final float scale;
	
	public ScaledTTFAtlas(TTFAtlas atlas, float scale)
	{
		this.origin = atlas;
		this.scale = scale;
	}
	
	public void render(String str, float x, float y, int rgba)
	{
		render(str, x, y, ColorHelper.getRed(rgba), ColorHelper.getGreen(rgba), ColorHelper.getBlue(rgba), ColorHelper.getAlpha(rgba));
	}
	
	public void renderWithShadow(String str, float x, float y, int rgba)
	{
		renderWithShadow(str, x, y, ColorHelper.getRed(rgba), ColorHelper.getGreen(rgba), ColorHelper.getBlue(rgba), ColorHelper.getAlpha(rgba));
	}
	
	public void renderWithShadow(String str, float x, float y, float red, float green, float blue, float alpha)
	{
		render(str, x + .5F, y + .5F, red / 3, green / 3, blue / 3, alpha);
		render(str, x, y, red, green, blue, alpha);
	}
	
	public void render(String str, float x, float y, float red, float green, float blue, float alpha)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, 0);
		GlStateManager.scale(scale, scale, scale);
		origin.render(str, 0, 0, red, green, blue, alpha);
		GlStateManager.popMatrix();
	}
	
	public int getWidth(String line)
	{
		return Math.round(origin.getWidth(line) * scale);
	}
	
	public int getWidth(char c)
	{
		return Math.round(origin.getWidth(c) * scale);
	}
	
	public int getHeight()
	{
		return Math.round(origin.getHeight() * scale);
	}
	
	public int getWordWrappedHeight(String str, int maxLength)
	{
		return getHeight() * this.listFormattedStringToWidth(str, maxLength).size();
	}
	
	public List<String> listFormattedStringToWidth(String str, int wrapWidth)
	{
		return origin.listFormattedStringToWidth(str, Math.round(wrapWidth / scale));
	}
	
	public ScaledTTFAtlas getScaledInstance(float scaleMult)
	{
		return new ScaledTTFAtlas(origin, scale * scaleMult);
	}
}