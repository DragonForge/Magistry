package com.dragonforge.magistry.client.teisr;

import com.zeitheron.hammercore.client.render.item.IItemRender;

import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.entity.RenderEntityItem;
import net.minecraft.item.ItemStack;

public abstract class TEISR implements IItemRender
{
	protected abstract void applyTransforms(TransformType type, boolean builtin);
	
	protected abstract void doRender(ItemStack stack);
	
	@Override
	public void renderItem(ItemStack stack)
	{
		StackTraceElement[] trace = Thread.currentThread().getStackTrace();
		
		if(RenderEntityItem.class.getName().equals(trace[4].getClassName()))
		{
			GlStateManager.pushMatrix();
			applyTransforms(TransformType.GROUND, true);
			doRender(stack);
			GlStateManager.popMatrix();
		}
		
		String t6 = trace[6].getClassName();
		if(t6.equals(RenderItem.class.getName()) || GuiContainer.class.getName().equals(t6) || GuiIngame.class.getName().equals(t6) || t6.equals("mezz.jei.plugins.vanilla.ingredients.ItemStackRenderer"))
		{
			GlStateManager.pushMatrix();
			applyTransforms(TransformType.GUI, true);
			doRender(stack);
			GlStateManager.popMatrix();
		}
	}
	
	@Override
	public void renderItem(ItemStack stack, IBakedModel bakedmodel, TransformType transform)
	{
		GlStateManager.pushMatrix();
		applyTransforms(transform, false);
		doRender(stack);
		GlStateManager.popMatrix();
	}
}