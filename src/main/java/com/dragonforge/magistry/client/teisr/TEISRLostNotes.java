package com.dragonforge.magistry.client.teisr;

import com.dragonforge.magistry.api.MagistryAPI;
import com.dragonforge.magistry.api.research.IPlayerKnowledge;
import com.dragonforge.magistry.api.research.ResearchBase;
import com.dragonforge.magistry.api.research.client.IPreviewIcon;
import com.dragonforge.magistry.api.research.client.ItemPreview;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class TEISRLostNotes extends TEISR
{
	boolean render;
	int recursion;
	
	@Override
	protected void applyTransforms(TransformType type, boolean builtin)
	{
		render = type == TransformType.GUI;
	}
	
	@Override
	protected void doRender(ItemStack stack)
	{
		if(!render)
		{
			render = true;
			return;
		}
		
		NBTTagCompound tag = stack.getTagCompound();
		if(tag != null && tag.hasKey("Research") && GuiScreen.isShiftKeyDown() && recursion <= 1)
		{
			ResearchBase res = MagistryAPI.getResearch(tag.getString("Research"));
			IPreviewIcon prev = ResearchBase.noPreview;
			if(res != null)
				prev = res.getPreview();
			
			GlStateManager.translate(8, 8, 0);
			GlStateManager.scale(.5, .5, .5);
			
			++recursion;
			prev.renderPreview();
			recursion = 0;
			
			IPlayerKnowledge ipk = MagistryAPI.getPlayerKnowledge(Minecraft.getMinecraft().player);
			if(ipk != null && !ipk.hasResearch(tag.getString("Research")))
				Minecraft.getMinecraft().getRenderItem().renderItemIntoGUI(Item.getItemFromBlock(Blocks.BARRIER).getDefaultInstance(), -16, 0);
		}
	}
}