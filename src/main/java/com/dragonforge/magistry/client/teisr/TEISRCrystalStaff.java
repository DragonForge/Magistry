package com.dragonforge.magistry.client.teisr;

import com.dragonforge.magistry.client.model.item.ModelCrystalStaff;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.item.ItemStack;

public class TEISRCrystalStaff extends TEISR
{
	public final ModelCrystalStaff model = new ModelCrystalStaff();
	
	@Override
	protected void applyTransforms(TransformType type, boolean builtin)
	{
		if(builtin)
		{
			if(type == TransformType.GUI)
			{
				GlStateManager.translate(.5, 1.18, .5);
				GlStateManager.rotate(180, 1, 0, 0);
			} else if(type == TransformType.GROUND)
			{
				GlStateManager.translate(.5, 1.18, .5);
				GlStateManager.rotate(180, 1, 0, 0);
			}
			return;
		}
		
		if(type == TransformType.FIRST_PERSON_LEFT_HAND)
		{
			GlStateManager.translate(0, 1.3F, 0);
			GlStateManager.rotate(180, 1, 0, 0);
		} else if(type == TransformType.FIRST_PERSON_RIGHT_HAND)
		{
			GlStateManager.translate(1, 1.3F, 0);
			GlStateManager.rotate(180, 1, 0, 0);
		} else if(type == TransformType.THIRD_PERSON_LEFT_HAND || type == TransformType.THIRD_PERSON_RIGHT_HAND)
		{
			GlStateManager.translate(.5, 1.3F, .5);
			GlStateManager.rotate(180, 1, 0, 0);
		} else if(type == TransformType.GUI)
		{
			GlStateManager.translate(0, 0, 100);
		} else if(type == TransformType.GROUND)
		{
			GlStateManager.translate(.5, .8, .5);
			GlStateManager.rotate(180, 1, 0, 0);
			GlStateManager.scale(.25, .25, .25);
		} else if(type == TransformType.FIXED)
		{
			GlStateManager.translate(.5, 1, .5);
			GlStateManager.rotate(180, 1, 0, 0);
			GlStateManager.scale(.75, .75, .75);
		} else if(type == TransformType.HEAD)
		{
			GlStateManager.translate(.5, 1, -.75);
			GlStateManager.rotate(90, 1, 0, 0);
		}
	}
	
	@Override
	protected void doRender(ItemStack stack)
	{
		long cycle = 3000;
		
		model.bindTexture(stack);
		model.render(null, 0, 0, 0, 0, 0, 1 / 16F);
		GlStateManager.pushMatrix();
		GlStateManager.translate(0, .02 * Math.sin(Math.toRadians((Minecraft.getSystemTime() % cycle) / (double) cycle * 360)), 0);
		model.shape10.render(1 / 16F);
		model.shape10_1.render(1 / 16F);
		model.shape10_2.render(1 / 16F);
		GlStateManager.popMatrix();
	}
}