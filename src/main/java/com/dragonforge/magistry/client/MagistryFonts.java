package com.dragonforge.magistry.client;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;

import com.dragonforge.magistry.Magistry;
import com.dragonforge.magistry.client.ttf2gl.ScaledTTFAtlas;
import com.dragonforge.magistry.client.ttf2gl.TTFAtlas;
import com.dragonforge.magistry.client.ttf2gl.TTFLib;
import com.zeitheron.hammercore.utils.FinalFieldHelper;

import net.minecraftforge.fml.common.ProgressManager;
import net.minecraftforge.fml.common.ProgressManager.ProgressBar;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class MagistryFonts
{
	public static final ScaledTTFAtlas PARCHMENT = null;
	
	public static void init()
	{
		ProgressBar bar = ProgressManager.push("Loading fonts...", 1);
		
		bar.step("Loading parchment.ttf");
		try(InputStream in = MagistryFonts.class.getResourceAsStream("/assets/magistry/fonts/parchment.ttf"))
		{
			Font amatic = Font.createFont(Font.TRUETYPE_FONT, in).deriveFont(48F);
			TTFAtlas atl = TTFLib.createOpenGLAtlas(amatic);
			String alphabet = "";
			for(char c = 'A'; c < 'Z'; ++c)
				alphabet += c;
			for(char c = 'a'; c < 'z'; ++c)
				alphabet += c;
			for(char c = '0'; c < '9'; ++c)
				alphabet += c;
			atl.init(alphabet);
			if(FinalFieldHelper.setStaticFinalField(MagistryFonts.class, "PARCHMENT", new ScaledTTFAtlas(atl, 0.15307693F)))
				Magistry.LOG.info("Created parchment OpenGL Font.");
		} catch(IOException | FontFormatException ioe)
		{
			ioe.printStackTrace();
		}
		
		ProgressManager.pop(bar);
	}
}