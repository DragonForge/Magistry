package com.dragonforge.magistry.client.tex;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import com.dragonforge.magistry.Magistry;
import com.zeitheron.hammercore.client.utils.texture.TextureSpriteFX;
import com.zeitheron.hammercore.lib.zlib.tuple.ThreeTuple;
import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class TextureRandomSparkling extends TextureSpriteFX
{
	public BufferedImage original;
	public BufferedImage sparklie;
	
	public final String name;
	public final ResourceLocation loadTex;
	public int colorAux = 0x88_FFFFFF, colorMain = 0xFF_FFFFFF;
	
	public TextureRandomSparkling(String name, ResourceLocation texture)
	{
		super(32, Magistry.MOD_ID + ":sparking_" + name);
		this.name = name;
		this.loadTex = texture;
	}
	
	public TextureRandomSparkling withColor(int red, int green, int blue)
	{
		this.colorAux = (0x88 << 24) | (red << 16) | (green << 8) | blue;
		this.colorMain = (0xFF << 24) | (red << 16) | (green << 8) | blue;
		return this;
	}
	
	@Override
	public void reload(int pass)
	{
		BufferedImage bi = null;
		try(InputStream is = Minecraft.getMinecraft().getResourceManager().getResource(loadTex).getInputStream())
		{
			bi = ImageIO.read(is);
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
		if(bi == null)
			return;
		original = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		sparklie = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = original.createGraphics();
		g.drawImage(bi, 0, 0, 32, 32, null);
		g.dispose();
		onTextureDimensionsUpdate(32, 32);
	}
	
	@Override
	public void setup()
	{
		imageData = ((DataBufferInt) sparklie.getRaster().getDataBuffer()).getData();
	}
	
	public List<ThreeTuple.Atomic<Integer, Integer, Integer>> sparklies = new ArrayList<>();
	
	public void addSparkle(int x, int y)
	{
		sparklies.add(new ThreeTuple.Atomic<>(0, x, y));
	}
	
	final Random rng = new Random();
	
	private void renderSparkle(ThreeTuple<Integer, Integer, Integer> sp)
	{
		float progress = sp.get1().floatValue() / 20;
		
		BufferedImage sparklie = new BufferedImage(3, 3, BufferedImage.TYPE_INT_ARGB);
		
		sparklie.setRGB(1, 0, colorAux);
		sparklie.setRGB(0, 1, colorAux);
		sparklie.setRGB(1, 1, colorMain);
		sparklie.setRGB(2, 1, colorAux);
		sparklie.setRGB(1, 2, colorAux);
		
		double sc = Math.sin(Math.toRadians(progress * 90));
		Graphics2D g = this.sparklie.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g.translate(sp.get2() + 1.5, sp.get3() + 1.5);
		g.rotate(Math.toRadians(progress * 180));
		g.scale(sc, sc);
		g.translate(-1.5, -1.5);
		g.drawImage(sparklie, 0, 0, null);
		g.dispose();
	}
	
	boolean update = false;
	
	@Override
	public void onTick()
	{
		int os = sparklies.size();
		update = false;
		Arrays.fill(imageData, 0x00000000);
		
		for(int i = 0; i < sparklies.size(); ++i)
		{
			ThreeTuple.Atomic<Integer, Integer, Integer> sparkle = sparklies.get(i);
			sparkle.set1(sparkle.get1() + 1);
			if(sparkle.get1() >= 20)
			{
				sparklies.remove(i);
				--i;
			} else
				renderSparkle(sparkle);
		}
		
		int rx = rng.nextInt(32);
		int ry = rng.nextInt(32);
		
		boolean spawn = true;
		for(int x = -2; x <= 2; ++x)
			for(int z = -2; z <= 2; ++z)
				if(rx + x > 0 && rx + x < 32 && ry + z > 0 && ry + z < 32 && ((original.getRGB(rx + x, ry + z) >> 24) & 0xFF) <= 0)
				{
					spawn = false;
					break;
				}
			
		if((original == null || spawn) && sparklies.size() < 10)
			addSparkle(rx, ry);
		
		if(os != sparklies.size() || !sparklies.isEmpty())
			update = true;
	}
	
	@Override
	public boolean changed()
	{
		return update;
	}
}