#Project info
author=Zeitheron
start_git_commit=8a7722b8a17f606e3506ebd1fc2a1b79d29306bd
git_commit=https://gitlab.com/DragonForge/Magistry/commit/{{hash}}

#Runtime info
forge_version=14.23.5.2768
minecraft_version=1.12.2
mcp=stable_39
hc_version=2.0.4.7
ml_version=10a
hm_version=15r
jei_version=4.11.0.206

#Mod info
trusted_build=true
mod_id=magistry
mod_id_fancy=Magistry
mod_name=Magistry
mod_version=1b-bc190327
cf_project=296160
release_type=beta